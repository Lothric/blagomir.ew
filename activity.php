<?php
    require_once('blocks/ewinit.php');
    $count = getSQLdata(COUNT_NEWS, $p['id'])[0]['total'];

    $pagination = 6;
    $pages_amount = ($count / $pagination) - (int)($count / $pagination) > 0 ? (int)($count / $pagination) + 1 : (int)($count / $pagination);

    $page_number = $_GET['page'] ?: 1;

    $add = 'LIMIT ' . strval($pagination) . ' OFFSET ' . strval($pagination * ($page_number - 1));

    $news = getSQLdata (NEWS_QUERY, ID_ALLBLOCKS, $p['id'], $add);
?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <?php
        require_once('blocks/ewhead.php');
        require_once('blocks/jslibs.php');
    ?>
</head>

<body>
    <div class="page-preloader">
        <div class="preloader"></div>
    </div>
    <div class="root">

        <?php
            require_once('blocks/menu.php');
        ?>

        <main>

            <?php
                require_once('blocks/breadcrumbs.php');
            ?>

            <section class="section classic-text _fon">
                <div class="container">
                    <a class="go-back" href="<?= $parentPages[count($parentPages) - 1]['link'] ?>">
                        <div class="button-prev i-arrow _sm"></div><?= $parentPages[count($parentPages) - 1]['title'] ?>
                    </a>
                    <h1><?= $p['title'] ?></h1>
                    <?php 
                        if ($p['info']) {
                            echo $p['info'];
                            echo '<br><br>';
                        } 
                    ?>

                    <div class="merops">

                        <?php
                            foreach ($news as $n) {
                                $n['data'] = datetotext($n['data'], '<span>#d#</span> #m#', $all['months']);
                                $n['logo'] = $p['dopinfo'];

                                echo writeTemplate($n, '
                                    <a class="merop" href="#link#">
                                        <div class="merop__date">#logo##data#</div>
                                        <div class="merop__desc">
                                            <div class="merop__title">#title#</div>
                                            <div class="merop__location">#preinfo#</div>
                                        </div>
                                    </a>
                                ');
                            }
                        ?>
                        
                    </div>

                    <?php
                        if ($pages_amount > 1) {
                            echo '<div class="pagination">';

                            for ($i = 1; $i <= $pages_amount; $i++) {
                                $class = $i == $page_number ? ' _active' : '';
                                echo '<a class="pagination__item' . $class . '" href="?page=' . $i . '">' . $i . '</a>';
                            }

                            echo '</div>';
                        }
                    ?>
                    
                </div>
            </section>
        </main>

        <?php
            require_once('blocks/ewfooter.php');
        ?>
        
    </div>

    <?php
        require_once('blocks/unterblock.php');
    ?>

</body>

</html>