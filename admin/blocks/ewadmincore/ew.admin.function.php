<?php
//БИБЛИОТЕКА ФУНКЦИЙ ПАНЕЛИ АДМИНИСТРАТОРА

/*детерминированная функция возвращающая результат проверки условия для двух массивов от анонимной функции
array @p - массив данных одной строки из БД
array @settings - условия для формирования проверки:
	string @param - перечень ключей из массива @p задействованных в ананонимной функции,
	@func - анонимная функция принимающая в качестве аргументов переменные с именами этих ключей @param и проводящая проверку с ними, возвращающая результат
	int @notfunc - универсальное условие, не требующее вызова функции и проверки, можно сразу задать = 1 и условие будет исполняться всегда
*/
function ewUsl ($p=[], $s=[])
{
	$res=false;

	if (count($p))
	{
			$keys = []; $newparam=[];
			if ($s['param'])//разбираем параметры
			{
				$keys = explode(',', str_replace(" ","",$s['param']));//получаем массив ключей параметров

				foreach ($keys as $k)
					$newparam[]=$p[$k];//формируем  ужатый массив с атрибутами только задействованными при сравнении
				//phpalert($s=>func($newparam));
			}

			if (((!empty($s["func"]) && $s["func"](...$newparam)) || $s["notfunc"]))//проверяем отсеялись ли параметры и передаем их анониманой функции
			$res=true;
	}

	return $res;
}

/*функция выводящая кнопки управления для верхнего меню
array @p - тиражируемый запрос к БД
array @settings - условия выделения блока или же кнопки управления (см. ewUsl().s)
	string @settings=>item=>@button - если в результате необходимо добавить кнопку-управление
	[
		param=>'candel=1',
		func=>function ($arg..){},
		newbutton=>'<a href="red_pages.php?id=%s" title="перейти к редактированию"><img src="picture/ref.gif" width="20" height="20"/></a>'
	]
*/
function pageslistnav ($p=[], $settings=[])
{
	$res='';

		foreach ($settings as $s)
		{
			if (ewUsl($p, $s) && $s['newbutton'])//проверяем отсеялись ли параметры и передаем их анониманой функции
				//если условие срабатывает и задается кнопка
				$res.=writeTemplate ($p, $s['newbutton']);//шаблонизируем кнопку

		}

	return $res;
}


/*функция выводящая список страниц для админки
int @kat - идентификатор
string @template_query - тиражируемый запрос к БД
array @settings - условия выделения блока или же кнопки управления (см. ewUsl() аттрибут s ):
	string @param - перечень ключей з массива полученного из бд по запросу template_query,
	@func - анонимная функция принимающая в качестве аргументов массив этих ключей-значений и проводящая проверку с ними, возвращающая этот результат. Он используется как условие
	int @notfunc - универсальное условие, не требующее вызова функции и проверки, можно сразу задать = 1 и условие будет исполняться всегда
	string @button - если в результате необходимо добавить кнопку-управление
	string @class - если в результате необходимо поставить/отметить класс
	[
		param=>'candel=1',
		func=>function ($arg..){},
		newbutton=>'<a href="red_pages.php?id=%s" title="перейти к редактированию"><img src="picture/ref.gif" width="20" height="20"/></a>'
	]
	или
	[
		param=>'p.public>1',
		func=>function ($arg..){}
		newclass=>'yellow'
	]
	или
	[
		notfunc=>1 //универсальное условие, не требующее вызова функции и проверки
		newclass=>'yellow'
	]
string @orderby - определение правил сортировки
string @template - шаблон самой строчки вывода
function afterQuery - анониманая функция вида которая выполниться для каждой строки $p и дополнит его содержимое элементом
	@int ud обозначающим уровень доступа текущего пользователя к этой записи
		function (array $p) {
		return int ud //дложна всегда возвращать число вида ud
	}
*/
function ew_pagesList ($kat=0, $template_query='', $orderby=' order by id desc ', $settings=[], $template='', $ud_callback=null)
{
	$result = '';
	if (!isset($template) || $template=='') $template = '<li class="ui-state-default #newclass#" id="#id#" title="#bg_title#">
<span class="opisanie"><a href="#pages_link#" target="_blank
">#id# - #title#</a></span>#newbutton#</li>';

	$inputdata = [
		'kat'=>$kat,
		'orderby'=>$orderby
	];//массив для использования с шаблонизатором для query
	//echo writeTemplate($inputdata, $template_query);
	$query = ew_mysqli_query(writeTemplate($inputdata, $template_query));
	while ($p = mysqli_fetch_assoc($query))
	{
		$p['pages_link']=UrlManager::createUrl($p['id']);
		$p['newbutton']='';
		$p['newclass']='';
		$p['bg_title']='';

		if (isset($ud_callback))//есть callback функция для УД
		{
			$p['ud'] = $ud_callback($p);
		}

		$p['datatime']=datatimeforhuman($p['data']);
		$p['humandata']=dataforhuman($p['data']);

		$p['foto']=nearest($p['picname2'], $p['picname'], NOFILE);

		/*проверка дополнительных условий*/

		foreach ($settings as $s)
		{
			if (ewUsl($p, $s))//если срабатывает условие
			{
				if ($s['bg_title'])//если задан bg_title
				$p['bg_title'] = $s['bg_title'];

				if ($s['newbutton'])//если задается кнопка
					$p['newbutton'].=writeTemplate ($p, $s['newbutton']);//шаблонизируем кнопку

				if ($s['newclass'])
					$p['newclass']=' '.$s['newclass'];
			}
		}
		//-------------------------------//

		$result .= writeTemplate($p, $template);
	}

	return $result;
}

/*функция формируюбщая html разметку для word редактора
$title - отображаемый заголовок поля
$pid - значение идентификатора
$stupid - название поля идентификатора (например id или id_pages)
$pol - название редактируемого поля
$info - новое значение для редактируемого поля
$tab - название таблицы
$template - шаблон для формируемой разметки html
*/
function ew_wordRedaktor ($title='', $pid=0, $info='', $pol='info', $tab='pages', $stupid='id', $template='<form class="wrd"  method="post" action="#">
           <label><i>%s</i><img src="picture/loader.gif" width="20" height="20" style="display:none;" class="wrd_loader">
		   <input name="pid" type="hidden" value="%d">
		   <input name="stupid" type="hidden" value="%s">
		   <input name="table" type="hidden" value="%s">
		   <input name="pol" type="hidden" value="%s">
           <textarea name="info" id="mywrd" cols="40" rows="5">%s</textarea></label></form>')
{
	if ($pid && $pol)//только если есть данные
		echo writeTemplate([$title, $pid, $stupid, $tab, $pol, $info], $template, 1);

}

/*функция вывода html селектор для фильтрации
string title - название селекта в label
array data - массив со значениями, которые будем перебирать для вывода
string pole - поле из этого массива по которому будем сверять, что option должен быть уже выбран и  отмечен как selected
int tek - текущее значение для проверки с data.pole
array def - первое значение по умолчанию:
	string title - названия начального поля по-умолчанию = -
	int id - значение начального поля по умолчанию = NULL
string select_template - шаблон оформления селекта
string option_template - шаблон оформления option
*/
function ew_filtr ($title, $data=[], $tek=0, $url_param_name="kat", $pole='id', $def=[], $select_template='<label>#title#</label> <select class="filtr" pole="#pole#" name="#url_param_name#">#result#</select>', $option_template='<option pid="#id#" #isselected#>#title#</option>')
{
	$res = '';

	if ($def)//если есть значение по-умолчанию то добваляется оно
			array_unshift ($data, $def);

	if ($data)
	{
		foreach ($data as &$t)
		{
			if ($t[$pole]==$tek)
				$t['isselected']='selected';

			$res .= writeTemplate($t, $option_template);
		}

		$input = [
			'title'=>$title,
			'pole'=>$pole,
			'url_param_name'=>$url_param_name,
			'tek'=>$tek,
			'result'=>$res
		];

		$res = writeTemplate($input, $select_template);
	}

	return $res;
}


/*функция формируюбщая html селектора
array/string $input - либо массив с элементами options либо строка для sql запроса
$tek_val - текущее значение страницы (например id_city=11)
int $pid - значение уникального ключа страницы
string $input_pole='id' - название поля уникального ключа
//получается тек. значение $tek_val сравнивается со значением ключа $input_pole для кадого элемента массива $input. При совпадении получается что данный элемент уже выбран.

string $title - отображаемый заголовок селектора

далее данные для записи в БД
string $stupid - название ключа идентификатора (например id или id_pages) по которому будем записывать, т.е. условие WHERE $stupid = $pid
string $pol - название редактируемого поля (например id_city)
$info - новое значение для редактируемого поля
string $table - название таблицы
string $template - шаблон для формируемой разметки html
*/
function ew_select ($input=0, $tek_val=0, $pid, $title="Прикреплено к разделу", $input_pole='id', $table="pages", $pol="id_pages",$stupid="id", $template='<label>%s </label><br><select name="arhi" class="myselect" pid="%d" table="%s" pol="%s" stupid="%s" >%s</select><img src="picture/loader.gif" width="20" height="20" style="display:none;"><br><br>', $itemsTemplate = '<option info="#id#" #isselected#>#id# - #title#</option>')
{
	$res='';


	if ($input)
	{
		$options = [['id'=>'', 'title'=>'']];//массив options сразу с первым элементом по-умолчанию
		if (is_string ($input))//если передана строка производим выборку из бд, считаем что была передана query строка
		{
			$result = ew_mysqli_query($input);
			while ($arr = mysqli_fetch_assoc($result))
			{
				$arr['title'] = badnews ($arr['title'], 50);
				$options[]=$arr;
			}

		}
		if (is_array ($input))//значит передан готовы массив
			$options = $input;

		foreach ($options as $t)
		{
			if ($t[$input_pole]==$tek_val)//отмечаем если выбрано
				$t['isselected']='selected';

			$res.=writeTemplate($t, $itemsTemplate);
		}
	}

	printf ($template, $title, $pid, $table, $pol, $stupid, $res);
}

/*функция обработки чекбоксов*/
function changeFlag($title, $tek, $table, $pol, $pid, $yestext=NULL, $notext=NULL)//функция смены флага
{
	if ($tek)//если отмечено
	{
		$txt = $notext;
		$txt2 = $yestext;
		$ty='checked';
	}
	else
	{
		$txt = $yestext;
		$txt2 = $notext;
		$ty='';
	}

		printf ('<div><input name="chek" class="changeFlag" type="checkbox"  table="%s" pol="%s" pid="%s" value="%s" %s> <label>%s</label> <img src="picture/loader.gif" width="20" height="20" style="display:none;" class="changeFlagLoader"><br>
<span class="changeFlagNexttext">
%s</span></div><br>', $table, $pol, $pid, $txt, $ty, $title, $txt2);

}


/*функция формируюбщая html разметку текстового редактора
$title - отображаемый заголовок поля
$pid - значение идентификатора
$stupid - название поля идентификатора (например id или id_pages)
$pol - название редактируемого поля
$info - новое значение для редактируемого поля
$tab - название таблицы
$template - шаблон для формируемой разметки html
*/
function ew_textRedaktor ($title='', $pid=0, $pol='', $info='', $tab='pages', $stupid='id', $template='<i>%s</i> <img src="picture/loader.gif" width="20" height="20" style="display:none;"> <li class="editable" pid="%d" stupid="%s" tab="%s" pol="%s">%s</li><br>')
{
	if ($pid && $pol)//только если есть данные
		echo writeTemplate([$title, $pid, $stupid, $tab, $pol, htmlspecialchars($info)], $template, 1);

}


/*функция получения данных для редактирования, предназначена для того, чтобы при декларативном задании исходной таблицы и ее расширяющих подмножеств получить массив со всеми данными
int $value - значение ключа или query строка запроса
char $key - ключ для идентификации
char $table - название таблицы для вывода
array $dopinfo - ассоциативный массив, каждый кортеж котрого должен содержать параметры для вызова аналогичной функции getRedInfo для подсоединения данных расшрияющего подмножества. При этом эти элементы будут подсоеденены как вложенный массив в элемент с именем ключа параметров

пример запроса
$m3=getRedInfo($id, 'id', 'pages', '*', [
		'podpages'=>['#id_pages#']//исползуем шаблонизатор чтобы вытащить значение по ключу, получаемому в результате начального запроса
	]);
//таким образом в $m3 помимо '*' элементов таблицы pages будут подсоеденены в атрибут podpages значения доп строки отобранной по ключу #id_pages#

*/
function getRedInfo ($value, $table='pages', $key='id', $rows='*', $dopinfo=[])
{
	$res=[];

	$query = '';

	if (is_numeric ($value))//если число
		$query = "SELECT ".$rows." FROM ".$table." WHERE ".$key."='".$value."' LIMIT 1";
	elseif (is_string ($value)) //tcikb cnhjrf
		$query = $value;

		//echo $query;

	if ($value)
	{
		$result = ew_mysqli_query($query);
		$m3 = mysqli_fetch_assoc($result);

		if ($m3['id_pages'])
			$m3['id_hparent'] = ($m3['id_pages']>0 && $m3['id_pages']!=$m3['id']) ? getpraded ($m3['id_pages'], 'pages'): 0 ;//страница не самого верхнего уровня, можно поискать подстраницы

		$res=$m3;

		foreach ($dopinfo as $i => $t)
		{
			$t[0]=writeTemplate($res, $t[0]);
			$res[$i]=getRedInfo(...$t);//получаем расширяющие подмножетсва, если они есть
		}
	}

	return $res;
}

/*функция добавления новой строки в таблицу БД с возможностью добавления расширяющих подмножеств
char $table - название таблицы
array $rows - ассоциативный массив с перечнем ключ-значение атрибутов, где ключ=заголвоку столбца, а значение - вносимому значению
array $dopinfo - ассоциативный массив с перечнем параметров для добавления новой строки в расширяющее подмножество
*/
function insertRedInfo ($rows=[], $table='pages', $dopinfo=[])
{
	$res_id=0; $query='';

	if($rows && ($query=createSetQuery($rows)))
	{
		try{
			beginTransaction();//работаем в транзакции
			global $db;
			if (!ew_mysqli_query ("INSERT INTO ".$table." SET ".$query) || ew_mysqli_error())
				throw new Exception ("Ошибка первичного добавления: ".ew_mysqli_error());

			$res_id=ew_mysqli_insert_id();
			$rows['id']=$res_id;

			//проверяем на расширяющие подмножества
			foreach ($dopinfo as $t)
			{
				foreach ($t as $j => $tt)//для параметра $rows каждого расширяющего подмножества шаблонизируем значения
					$t[$i]=writeTemplate($rows, $tt);

				insertRedInfo(...$t);//получаем расширяющие подмножетсва, если они есть
			}
			//

			commitTransaction();
		}
		catch(Exception $ex){
			rollbackTransaction();
			echo $ex->getMessage();
			return 0;
		}
		UrlManager::rewriteCache(); //обновляем кэш

	}

	return $res_id;
}

/*функция удаления страницы по ее pages.id
подчищает прикрепленные файлы к этой странице
*/
function delpagesContent ($kodi)
 {
	//очистка документов и фоток
	$resultd = ew_mysqli_query("SELECT * FROM files WHERE id_pages='$kodi'");
	while ($dres = mysqli_fetch_assoc($resultd))
	{
	  $mydir = ($dres['docsorfoto'])? FOTO_DIR : DOCS_DIR;

	  if ($dres['picname'])
		  @unlink ('..'.$mydir.$dres['picname']);

	  if ($dres['picname2'])
		  @unlink ('..'.$mydir.$dres['picname2']);
	}

	//удаление записи
	$aresult = ew_mysqli_query ("DELETE FROM pages WHERE id='$kodi'");//версии контента при этом удаляться автоматически

	return $aresult;
 }

/*Удаление всей ветки страницы и ее подстраниц по id самой верхней страницы (начальной). */
function delpages($id)
{
	$id_parent=$id;//определение верхнего уровня узла

	$do=array();

	$do[$id_parent]=$id_parent;

	$i=0;

	do
	{
		if ($i==0)
		$id=$do[$id];
		else $i=0;

	 	$resultpul = ew_mysqli_query("SELECT id, id as rid, id_pages, (select count(*) FROM pages where pages.id_pages=rid) as kind FROM pages WHERE id_pages='$id'");
		while ($pul = mysqli_fetch_assoc($resultpul))
		{
			if ($pul['kind']==0)
			{
				delpagesContent($pul['id']);
			}
			else
			{
				$id=$pul['id'];
				$do[$id]=$pul['id_pages'];
				$i++;
			}
		}

	}while ($id!=$id_parent);

	if($id==$id_parent)//удаление заглавного
		if (!delpagesContent($id))
		{
			alertAndRedirect('Ошибка удаления!');
		}

	//дополнительные условия
	/* if ($kat==4)//если удаляем участника то декрмент счетчика
			$resultpul2 = ew_mysqli_query("update allblocks SET kol=kol-1 WHERE id=1 limit 1");	 */
}

/*функция для удаления контента версии страницы
должна возвращать массив, где 0 элемент это флаг true/false результтата выполнения операции, а 
элемент 1 - сообщение

*/
function del_pcontent($id, $id_allblocks, $USER)
{
	$log = [];
	if(isset($USER) && $USER->access_action_itemslist('pages', $id, $id_allblocks)!=WRITE_ACCESS)//если есть сведения о пользователе, надо проверить уровень доступа
		return [0, $id.' - '.EWAdminUser::DELETE_ACCESS_ERROR];
	
	//получаем информацию о текущей странице
	$result = ew_mysqli_query("SELECT p.id, pc.id_allblocks, pc.id as pc_id, (SELECT count(*) FROM pcontent WHERE pcontent.id_pages=p.id) as kol_pc FROM pages p INNER JOIN pcontent pc ON p.id=pc.id_pages WHERE pc.id_allblocks='".$id_allblocks."' AND p.id='".$id."' limit 1");
	if ($m = mysqli_fetch_assoc($result))
	{
		if ($m['kol_pc']>1)//если есть параллельный контент страницы в других версиях
		{
			$resultfc = ew_mysqli_query("SELECT f.*, fc.id as fc_id, (SELECT count(*) FROM fcontent WHERE fcontent.id_files=f.id) as ff_kol FROM files f INNER JOIN fcontent fc ON fc.id_files=f.id WHERE f.id_pages='".$m['id']."' and fc.id_allblocks='".$id_allblocks."'"); //перебор файлов
			while ($ff = mysqli_fetch_assoc($resultfc))
			{
				if ($ff['ff_kol']>1)//если несколько версий контента у файла
				{
					ew_mysqli_query("delete FROM fcontent WHERE id = '".$ff['fc_id']."'"); //удаляем версию
					$log[] = 'Удаление версии контента файла fcontent'.$ff['fc_id'];
				}
				else //иначе нужно удалить вместе с файлами
				{
					  $mydir = ($ff['docsorfoto']) ? FOTO_DIR : DOCS_DIR;

					  if ($ff['picname'])
						  @unlink ('..'.$mydir.$ff['picname']);

					  if ($ff['picname2'])
						  @unlink ('..'.$mydir.$ff['picname2']);
					  
					  //удаление записи
					  ew_mysqli_query ("DELETE FROM files WHERE id='".$ff['id']."'");//версии контента при этом удаляться автоматически
					  $log[] = 'Удаление файла files'.$ff['id'];
				}
				
				if (ew_mysqli_error())
						return [0, ew_mysqli_error().'#1'];
			}
			ew_mysqli_query("delete FROM pcontent WHERE id='".$m['pc_id']."'");//удаляем контент страницы
			if (ew_mysqli_error())
					return [0, ew_mysqli_error().'#2'];
			$log[] = 'Удаление контента страницы pcontent'.$m['pc_id'];
		}
		else //если нет контента в других версиях - удалим из pages и files
		{
			delpagesContent ($id);
			$log[] = 'Полное удаление страницы pages'.$id;
		}
			
		
	}
	else return [0, $id.' - Нет информации о странице'];
	
	//получаем перечень подстраниц
	$result = ew_mysqli_query("SELECT p.id, pc.id_allblocks FROM pages p INNER JOIN pcontent pc ON p.id=pc.id_pages WHERE pc.id_allblocks='".$id_allblocks."' AND p.id_pages='".$id."'");
	while ($m3 = mysqli_fetch_assoc($result))
	{
		$t = del_pcontent ($m3['id'], $id_allblocks, $USER);
		if (!$t[0])//если ошибка
			return $t;
	}
	
	return [1, 'Контент удален', implode("; ", $log)];
}

/*функция удаления контента для файлов*/
function del_fcontent ($content_delete=0, $id=0, $query_select="SELECT count(*) as kol FROM fcontent WHERE id_files='%d'", $query_delete="delete FROM fcontent WHERE id_files='%d' AND id_allblocks = '%d'")
{
	$resultprov = ew_mysqli_query(sprintf($query_select, $id));
	$prov = mysqli_fetch_assoc($resultprov);
	if ($prov['kol']<=1)
	{
		phpalert('Файл имеет один единственный контент. Он не может быть удален. Создайте дополнительный контент и повторите попытку!');
		return $content_delete;
	}

	if (!ew_mysqli_query(sprintf($query_delete, $id, $content_delete)) || ew_mysqli_error())
	phpalert("Ошибка удаления версии ".ew_mysqli_error());

	return $content_delete;
}

/*функция удаления документа*/
function delfiles ($id, $s_update=true)
{
	$res = false;

	$result = ew_mysqli_query("SELECT * FROM files WHERE id='$id' limit 1");
	if ($m3 = mysqli_fetch_assoc($result))
	{
		$mydir = ($m3['docsorfoto'])? FOTO_DIR : DOCS_DIR;

		if ($m3['picname'])
			@unlink ('..'.$mydir.$m3['picname']);
		if ($m3['picname2'])
			@unlink ('..'.$mydir.$m3['picname2']);

		//
		$myquery = ($s_update) ? "UPDATE files SET picname='', picname2='' WHERE id='$id' limit 1" : "DELETE FROM files WHERE id='$id'";//определяем что делать дальше

		if (!($df = ew_mysqli_query ($myquery)) || ew_mysqli_error ())
		{
			phpalert("Ошибка удаления файла! ".ew_mysqli_error ());
		}
		else $res = true;
	}


	return $res;
}

/* Функция формирования названия файла путем формирования транскрипции из сущ. названия файла с проверкой на уникальность */
function getFileName ($filename)
{
	$translit = array(

            'а' => 'a',   'б' => 'b',   'в' => 'v',

            'г' => 'g',   'д' => 'd',   'е' => 'e',

            'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',

            'и' => 'i',   'й' => 'j',   'к' => 'k',

            'л' => 'l',   'м' => 'm',   'н' => 'n',

            'о' => 'o',   'п' => 'p',   'р' => 'r',

            'с' => 's',   'т' => 't',   'у' => 'u',

            'ф' => 'f',   'х' => 'kh',   'ц' => 'c',

            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sh',

            'ь' => '',  'ы' => 'y',   'ъ' => '',

            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',

            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',

            'Ё' => 'YO',   'Ж' => 'Zh',  'З' => 'Z',

            'И' => 'I',   'Й' => 'J',   'К' => 'K',

            'Л' => 'L',   'М' => 'M',   'Н' => 'N',

            'О' => 'O',   'П' => 'P',   'Р' => 'R',

            'С' => 'S',   'Т' => 'T',   'У' => 'U',

            'Ф' => 'F',   'Х' => 'Kh',   'Ц' => 'C',

            'Ч' => 'CH',  'Ш' => 'SH',  'Щ' => 'SHH',

            'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',

            'Э' => 'E',   'Ю' => 'YU',  'Я' => 'YA',

			' ' => '_'

        );

		$word = strtr(substr($filename, 0, 89), $translit); // транслитерация. Переменная $word получит значение 'prochee'

		$result = ew_mysqli_query("SELECT docs.id FROM docs WHERE docs.picname LIKE '".$word."%' limit 1");
		if ($pages = mysqli_fetch_assoc($result))//если есть
			$word=substr($word, 0, 80).date ("His");

		return $word;

}

/*функция изменения размера картинки*/
function imageresize($dest,$src , &$width, &$height, $quality=80) {
  if(!file_exists($src)) return 1; // исходный файля не найден
  $size=getimagesize($src);
  if($size===false) return 2; // не удалось получить параметры файла

  // Определяем исходный формат по MIME-информации и выбираем соответствующую imagecreatefrom-функцию.
  $format=strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
  $icfunc="imagecreatefrom".$format;
  if(!function_exists($icfunc)) return 3; // не существует подходящей функции преобразования

  // Определяем необходимость преобразования размера
  if( $width<$size[0] || $height<$size[1] )
    $ratio = min($width/$size[0],$height/$size[1]);
  else
    $ratio=1;

  $width=floor($size[0]*$ratio);
  $height=floor($size[1]*$ratio);
  $isrc=$icfunc($src);
  $idest=imagecreatetruecolor($width,$height);

  imagecopyresampled($idest,$isrc,0,0,0,0,$width,$height,$size[0],$size[1]);
  imagejpeg($idest,$dest,$quality);
  chmod($dest,0666);
  imagedestroy($isrc);
  imagedestroy($idest);
  return 0; // успешно
}

/*Удаление глоссария по ключу*/
function delglossary ($kodi, $table='glossary')
{
	$aresult = ew_mysqli_query ("DELETE FROM ".$table." WHERE id='$kodi'");
	if ($aresult) return true;
	else return false;
}

/*функция выборки из sql в массив
$query - sql строка запроса
$islight - флаг, что если только одна строка получена, то делаем ее одномерным массивом
*/
function getSQLArr ($query='', $islight=false)
{
	$t = [];

	$resultfl = ew_mysqli_query($query);
	while ($fl = mysqli_fetch_assoc($resultfl))
		$t[]=$fl;

	return ($islight) ? getLightArray($t) : $t;

}

/*функция добавления записи о новом файле*/
function createFile ($id_pages=0, $docs_or_foto=0, $id_allblocks=1)
{
	$id=0;

	ew_mysqli_query ("INSERT INTO files (id_pages, docsorfoto) VALUES ('".$id_pages."', '".$docs_or_foto."')");//вносим запись
	$id=ew_mysqli_insert_id();
	if (ew_mysqli_error() || !$id)
		exit('Ошибка добавления #1 '.ew_mysqli_error());

	ew_mysqli_query ("INSERT INTO fcontent (id_files, id_allblocks) VALUES ('".$id."', '".$id_allblocks."')");//вносим запись

	if (ew_mysqli_error())
		exit('Ошибка добавления #2 '.ew_mysqli_error());

	return $id;
}

/*функция создания нового контента для файлов*/
function createContent ($content_create=0, $id=0, $query_select="SELECT * FROM fcontent WHERE id_files='%d' AND id_allblocks = '%d' limit 1", $query_insert="INSERT INTO fcontent (id_files, id_allblocks) values ('%d', '%d')", $doselect=true)
{
	if($query_insert)
	{
		if($query_select && $doselect)
		{
			$resultprov = ew_mysqli_query(sprintf($query_select, $id, $content_create));
			if($prov = mysqli_fetch_assoc($resultprov))
			{
				phpalert('Контент уже был создан ранее!');
				return $content_create;
			}
		}

		if (!ew_mysqli_query(sprintf($query_insert, $id, $content_create)) || ew_mysqli_error())//вносим новый контент
			phpalert("Ошибка добавления новой версии контента  ".ew_mysqli_error());
	}

	return $content_create;
}

/*функция удаления контента для файлов*/
function deleteContent ($content_delete=0, $id=0, $query_select="SELECT count(*) as kol FROM fcontent WHERE id_files='%d'", $query_delete="delete FROM fcontent WHERE id_files='%d' AND id_allblocks = '%d'")
{
	$resultprov = ew_mysqli_query(sprintf($query_select, $id));
	$prov = mysqli_fetch_assoc($resultprov);
	
	if (!ew_mysqli_query(sprintf($query_delete, $id, $content_delete)) || ew_mysqli_error())
		phpalert("Ошибка удаления контента файла ".ew_mysqli_error());
	
	if ($prov['kol']==1)//Файл имеет один единственный контент
		delfiles ($id, false);//полное удаление

	return $content_delete;
}

/*функция получения подстраниц из массива, где все страницы в одном ряду*/
function getPodPages ($arr=[], $id_pages='', $ud_template, $podpages_toggle_template)
{
	$res = '';
	foreach ($arr as $t)
	{
		if ($t['id_pages']==$id_pages)
		{
			$dopres = '';
			$res .= ' <li type="pages" pid="'.$t['id'].'" id_pages='.$t['id_pages'].'>'.$t['title'].$ud_template;

			$dopres .= getPodPages($arr, $t['id'], $ud_template, $podpages_toggle_template);
			if ($dopres) $res .= $podpages_toggle_template.$dopres; //если есть подстраницы

			$res .= '</li>';
		}

	}

	return ($res) ? ' <ul>'.$res.'</ul>' : '';
}


/*функция добавления контента для страницы в для определенной версии
@int $id_pages - id страницы
@int $new_id_allblocks - id версии контента куда копируем
@int $old_id_allblocks - id версии контента куда копируем
@bool flag_addDopContent - флаг копировать только структуру (false) или текстовки тоже (true)
*/
function insNewPageConent ($id_pages=0, $new_id_allblocks=0, $old_id_allblocks=1, $flag_addDopContent=false, $USER)
{	
	$count=["pages"=>0, "df"=>0];
	$opisanie = '';
	try {
		beginTransaction();

		//копирование контента страниц
		if($flag_addDopContent)//в зависимости от этого флага копировать только структуру или еще и данные?
		{//структура + данные
			$query1 = "INSERT INTO pcontent (title, banner, direct_title, keywords, description, info, dopinfo, adr, x, y, id_pages, id_allblocks) 
							SELECT pc.title, pc.banner, pc.direct_title, pc.keywords, pc.description, pc.info, pc.dopinfo, pc.adr, pc.x, pc.y, pc.id_pages, '".$new_id_allblocks."' 
							FROM pcontent pc 
							INNER JOIN pages p ON pc.id_pages=p.id
							WHERE p.id = '$id_pages' AND pc.id_allblocks='$old_id_allblocks'";
							
			$query2 = "INSERT INTO fcontent (title, info, href, id_files, id_allblocks) 
			SELECT fc.title, fc.info, fc.href, fc.id_files, '".$new_id_allblocks."' 
			FROM fcontent fc
			INNER JOIN files f ON fc.id_files=f.id
			WHERE f.id_pages='$id_pages' AND fc.id_allblocks = '$old_id_allblocks'";
			
			$opisanie = 'структура + данные';
		}
		else //копироваться будет только структура
		{
			$query1 = "INSERT INTO pcontent (title, id_pages, id_allblocks, inherit) 
							SELECT pc.title, pc.id_pages, '".$new_id_allblocks."', '".$old_id_allblocks."'   
							FROM pcontent pc 
							INNER JOIN pages p ON pc.id_pages=p.id
							WHERE p.id = '$id_pages' AND pc.id_allblocks='$old_id_allblocks'";
							
			$query2 = "INSERT INTO fcontent (title, info, href, id_files, id_allblocks) 
			SELECT fc.title, fc.info, fc.href, fc.id_files, '".$new_id_allblocks."' 
			FROM fcontent fc
			INNER JOIN files f ON fc.id_files=f.id
			WHERE f.id_pages='$id_pages' AND fc.id_allblocks = '$old_id_allblocks'";
			
			$opisanie = 'только структура';
		}

		
		ew_mysqli_query($query1);

		if (ew_mysqli_error())
			throw new Exception ("№1 " . ew_mysqli_error());

		$count["pages"]+=ew_mysqli_affected_rows();

		//копирование контента документов и фоток
		if(isset($query2))
		{
			ew_mysqli_query($query2);
			if (ew_mysqli_error())
				throw new Exception ("№2 " . ew_mysqli_error());
			$count["df"] += ew_mysqli_affected_rows();
		}
		
		commitTransaction();

		$USER->save_log("Копирован контент со страницы #$id_pages в версию #$new_id_allblocks и ".$count["df"]." документов и фотографий" . ($opisanie ));
		
		return $count["pages"]; 
	}
	catch (Exception $ex){
		rollbackTransaction ();
		phpalert("Ошибка транзакции " .$ex->getMessage());
		$USER->save_log("Ошибка транзацкии при попытке копирования контента версии контент со страницы #$id_pages в версию #$new_id_allblocks ". $ex->getMessage(), $opisanie);
		return 0;
	}
}

/*функция возвращает массив/ветку подстраниц страницы
$id_pages - id начальной страницы
$keyName - название внешнего ключа у подстраниц 
 */
function getPodstr ($id_pages)
{
	$arr = [];
	$query = ew_mysqli_query("SELECT * FROM pages WHERE id_pages='$id_pages'");
	while ($pul = mysqli_fetch_assoc($query))
	{
		$arr[$pul['id']] = $pul;
		$query2 = ew_mysqli_query("SELECT * FROM pages WHERE id_pages='".$pul['id']."'");
		while ($pul2 = mysqli_fetch_assoc($query2))
			$arr = getPodstr($pul2['id']) + $arr;
	}
	
	$query3 = ew_mysqli_query("SELECT * FROM pages WHERE id='$id_pages' limit 1");
	while ($pul3 = mysqli_fetch_assoc($query3))
		$arr[$pul3['id']] = $pul3;
	
	return $arr;
}

/*функция сохранения картинки из ресурса в файл с определнием принадлежности по расширению
string $path_name - путь и название картинки, например /img/first.jpg
resource $new_img - ресурс для изменения картинки
return bool - возвращает результат создания картинки true или false
*/
function save_img ($new_img, $path_name)
{
	if (isset($path_name))
	$type = explode ('.', $path_name);
	$type = strtolower($type[count($type)-1]);//последний элемент должен хранить расщирение
	switch ($type)
	{
		case "jpg":
		case "jpeg":
			return imagejpeg ($new_img, $path_name);
		break;
		
		case "png":
			return imagepng ($new_img, $path_name, 0);
		break;
		
		case "bmp":
			return imagebmp ($new_img, $path_name);
		break;
		
		case "gif":
			return imagegif ($new_img, $path_name);
		break;
		
		default:
			return false;
		break;
	}
}

/*функция для создания новой страницы на основе существующей с копированием всей информации
$idParentPages - id страницы с которой надо сделать копию
$id_pages -  что указывать в качестве id_pages для созданной копии. Если =0, то тоже самое что у родительской страницы
*/
function copy_pages ($idParentPages=0, $id_allblocks=1, $id_pages=0, $callback=[])
{
	$idNewPages = 0;
	$insertPagesKeys = [];
	
	if(!($callback instanceof Closure))
		$callback = function ($idNewPages, $idParentPages, $id_allblocks){};
		
	if($idParentPages)
	{
		//#1 добавляем в pages
		$query1 = ew_mysqli_query("SELECT * FROM pages WHERE id='$idParentPages' limit 1");
		if (!$parentPages = mysqli_fetch_assoc($query1))
			throw new Exception ($idNewPages . '#1 Ошибка при выборке страницы '.ew_mysqli_error());
		
		if(!$id_pages)
			$id_pages = $parentPages['id_pages'];
		
		$insertPagesKeys = get_keystring_by_assocarray($parentPages, ['id', 'id_pages', 'url'], ', '); //формируем перечень ключей для запроса insert
		
		ew_mysqli_query("INSERT INTO pages(".$insertPagesKeys.", id_pages) SELECT ".$insertPagesKeys.", '".$id_pages."' FROM pages WHERE id='".$idParentPages."'");
		if (ew_mysqli_error() || !($idNewPages = ew_mysqli_insert_id ()))
			throw new Exception ($idNewPages . '#2 Ошибка при копировании страницы '.ew_mysqli_error());
		//------------------------//
		
		//#2 добавляем в pcontent
		$query2 = ew_mysqli_query("SELECT * FROM pcontent WHERE id_pages='$idParentPages' AND id_allblocks='$id_allblocks' limit 1");
		if (!$parentContentPages = mysqli_fetch_assoc($query2))
			throw new Exception ($idNewPages . '#3 Ошибка при выборке контента страницы '.ew_mysqli_error());
		
		$insertKeys = get_keystring_by_assocarray($parentContentPages, ['id', 'id_pages', 'url'], ', '); //формируем перечень ключей для запроса insert
		
		ew_mysqli_query("INSERT INTO pcontent(".$insertKeys.", id_pages) SELECT ".$insertKeys.", '$idNewPages' FROM pcontent WHERE id='".$parentContentPages['id']."'");
		if (ew_mysqli_error())
			throw new Exception ($idNewPages . '#4 Ошибка при копировании контента страницы '.ew_mysqli_error());
		//----------------------//
		
		//#3 копируем файлы и картинки
		$query3 = ew_mysqli_query("SELECT * FROM files WHERE id_pages='$idParentPages'");
		while ($tekFiles = mysqli_fetch_assoc($query3))
			copy_foto_or_documents ($tekFiles, $idNewPages, $id_allblocks);
		//-------------------//
		
		//#4 добавляем в подстраницы
		$query4 = ew_mysqli_query("SELECT id FROM pages WHERE id_pages='$idParentPages'");
		while ($childPages = mysqli_fetch_assoc($query4))
			copy_pages ($childPages['id'], $id_allblocks, $idNewPages);
		//-------------------//
		
		//callback функция и обработка расширяющих подможеств
		$callback ($idNewPages, $idParentPages, $id_allblocks);
		
		UrlManager::rewriteCache();		
	}

	return $idNewPages;
}

//функция для создания копии фото или документа с привязкой к определенной странице
function copy_foto_or_documents ($idFile=0, $idPages=0, $id_allblocks=0)
{
	$idNewFile = 0;
	$insertKeys = [];
	$pathPrefix = '..';
	$parentFile = [];
	
	if(is_array ($idFile))//если передан только id файла 
	{
		$parentFile = $idFile;
		$idFile = $idFile['id'];
	}
	else 
	{
		$query1 = ew_mysqli_query("SELECT * FROM files WHERE id='$idFile' limit 1");
		if(!$parentFile = mysqli_fetch_assoc($query1))
			throw new Exception ($idNewPages . '###6 Ошибка при запросе данных файла '.$idFile.'-'.ew_mysqli_error());
	}
	
	if (count($parentFile))
	{
		$insertKeys = get_keystring_by_assocarray($parentFile, ['id', 'id_pages', 'picname', 'picname2'], ', ');  //формируем перечень ключей для запроса insert
		
		$newPicname = ($parentFile['picname']) ? rand(0,99).'_'.$idPages.'_'.$parentFile['picname'] : '';
		$newPicname2 = ($parentFile['picname2']) ? rand(0,99).'_'.$idPages.'_'.$parentFile['picname2'] : '';
		$pathToFile = ($parentFile['docsorfoto']) ? FOTO_DIR : DOCS_DIR;
		$pathToFile = $pathPrefix.$pathToFile;
		
		if($parentFile['picname'] && !copy($pathToFile.$parentFile['picname'], $pathToFile.$newPicname))
			throw new Exception ($idPages . '###1 Ошибка при копировании файла '.$pathToFile.$parentFile['picname']);
		
		if($parentFile['picname2'] && !copy($pathToFile.$parentFile['picname2'], $pathToFile.$newPicname2))
			throw new Exception ($idPages . '###2 Ошибка при копировании файла '.$pathToFile.$parentFile['picname2']);
		
		ew_mysqli_query("INSERT INTO files(".$insertKeys.", id_pages, picname, picname2) SELECT ".$insertKeys.", '$idPages', '$newPicname', '$newPicname2' FROM files WHERE id='".$idFile."'");
		if (ew_mysqli_error() || !($idNewFile = ew_mysqli_insert_id ()))
		{
			@unlink ($pathToFile.$newPicname, $pathToFile.$newPicname2);
			throw new Exception ($idNewFile . '###3 Ошибка при копировании данных файла '.ew_mysqli_error());
		}	
		
		//#4 добавляем в fcontent
		$query2 = ew_mysqli_query("SELECT * FROM fcontent WHERE id_files='$idFile' AND id_allblocks='$id_allblocks' limit 1");
		if (!$parentContentFiles = mysqli_fetch_assoc($query2))
			throw new Exception ($idNewFile . '###4 Ошибка при выборке контента файла '.$idFile.'-'.ew_mysqli_error());
		
		$insertKeys = get_keystring_by_assocarray($parentContentFiles, ['id', 'id_files'], ', '); //формируем перечень ключей для запроса insert
		
		ew_mysqli_query("INSERT INTO fcontent(".$insertKeys.", id_files) SELECT ".$insertKeys.", '$idNewFile' FROM fcontent WHERE id='".$parentContentFiles['id']."' limit 1");
		if (ew_mysqli_error())
			throw new Exception ($idNewPages . '###5 Ошибка при копировании контента файла '.$idFile.'-'.ew_mysqli_error());
		//-------------------//
	}
	//-------------------//	
	
	return $idNewFile;
}

/*
функция принимает ассоциативный массив и возвращает массив ключей этого массива, 
за исключением указанных в массиве 
*/
function get_keystring_by_assocarray ($arr=[], $stopKey=[], $toStringSeparator=false)
{
	$newArr = [];
	
	if(count($arr))
	{
		foreach ($arr as $key => $param)
			if(array_search ($key, $stopKey)===false)//если данный ключ можно сохранять
				$newArr[] = $key;
	}
	else $newArr = $arr;
	
	return ($toStringSeparator) ? implode($toStringSeparator, $newArr) : $newArr;
}
?>
