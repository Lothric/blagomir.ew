<?php
/* Класс для ведения авторизации  */
class EWAdminUser
{
	public $id, $id_usertype, $usertype, $email, $username,
					$glb_access=1; //чтобы хранить информацию о доступе к заглавному разделу
	private $authStatus=false, $user_access=[], $type_access=[], $isbanned=true;

	const NO_ACCESS_ERROR = "<p>Вы не имеете прав доступа к разделу/странице</p><p><a href='index.php'>Вернуться на главную страницу админки</a></p>";
	const NO_PAGES_ACCESS_ERROR = "<p>Вы не имеете прав доступа к разделу/странице в данной версии контента</p><p><a href='index.php'>Вернуться на главную страницу админки</a></p>";
	const AJAX_NO_ACCESS_ERROR = "Вы не имеете прав доступа к редактированию информации на этой странице";
	const DELETE_ACCESS_ERROR = "Вы не имеете прав доступа на удаление данной информации!";
	const CREATE_ACCESS_ERROR = "Вы не имеете прав доступа на добавление данной информации!";
	/*
	function  __construct () //конструктор
	{
		$HOST_NAME = HOST_NAME;//глобальное имя домена сайта

		$this->myhost = $HOST_NAME;
		$this->sessionName = $sname;
		session_set_cookie_params(7200, "/", $this->myhost, false, false);
		session_name($this->sessionName);
		session_start();

		//$this->err = getglossary (self::ERR_CODE, 'id_glossary', 0, 0 , 'npp'); //сбор кодов ошибо

		$this->ID=0;//по-умолчанию
	}
	*/

	/*методо проведения авторизации и получения данных из БД о пользователе*/
	public function auth($name, $pass)
	{
		$name = addslashes($name);
		$pass = addslashes($pass);

        $lst = ew_mysqli_query("SELECT u.id as uid, u.id_usertype, u.user, u.email, u.isbanned, u.firstpass, u.myaccess FROM userlist as u WHERE u.user='".$name."' AND pass='".sha1($pass)."' limit 1") or die (ew_mysqli_error());
		if ($people =  @mysqli_fetch_assoc($lst))
		{
			//операции по преоброазованию данных
			$this->id = $people['uid'];
			$this->id_usertype = $people['id_usertype'];
			$this->username = $people['user'];
			$this->email = $people['email'];
			$this->isbanned = $people['isbanned'];
			$this->firstpass = $people['firstpass'];
			$this->user_access = (isJSON($people['myaccess'])) ? json_decode($people['myaccess'], true) : [];

			$lst2 = ew_mysqli_query("SELECT * FROM usertype where id = '".$this->id_usertype."' limit 1") or die (ew_mysqli_error()); //подгружаем информацю о УД по типу пользователя
			if ($dop =  @mysqli_fetch_assoc($lst2))
			{
				$this->usertype = $dop['title'];
				$this->type_access = $dop;
				unset($this->type_access->title, $this->type_access->id);//убираем лишние
			}

			$this->authStatus = true;
		}
		return $this->authStatus;
	}

	//метод дает ответ, забанен или нет
	public function isbanned (){
		return $this->isbanned;
	}

	/*метод для проверки уровня доступа к детерминированным страницам админки (userlist, glossary и т.п.)*/
	public function dtrm_check_pages ($key=0)
	{
		return (isset ($this->type_access[$key])) ? $this->type_access[$key] : false;
	}

	//метод получает результат выполнение check_dtrm_pages и производит действие закрытия доступа к детерминированным страницам
	public function dtrm_access_action ($flag=false)
	{
		if (is_string ($flag))//если вместо флага передан ключ
			$flag = $this->dtrm_check_pages($flag);

		if (!$flag)
			exit ($this::NO_ACCESS_ERROR);
		
		return $flag;
	}

	//метод говорит, админ это или нет
	public function isAdmin ()
	{
		if ($this->id_usertype==1)//если действует админ проверки не нужны
		{
			$this->glb_access = WRITE_ACCESS;
			return true;
		}
		else return false;
	}


	/*метод для проверки доступности раздела/страницы.
	$key - ключ, м.б. allblocks или pages
	$id - идетификатор страницы, если есть
	$id_allblocks - идентификатор версии контента, если есть
	$flag_action - флаг необходимости предпринимать действия
	*/
	public function access_action ($key, $id=0, $id_allblocks=0, $flag_action=false)
	{
		if ($this->isAdmin ()) return; //если действует админ проверки не нужны

		switch ($key) {
			case 'allblocks':
				$this->glb_access = READ_ACCESS;
			break;

			case 'red_allblocks':
				$this->glb_access = nearestInt($this->user_access[$id]['ud'], NO_ACCESS);
			break;
			
			case 'pages':
			case 'red_pages':
				if (!isset($this->user_access[$id_allblocks]['ud']) || $this->user_access[$id_allblocks]['ud']==NO_ACCESS)//если нет доступа по id_allblocks
				{
					$best_all_id = $this->best_allblocks_access(); //попробуем версию контента с лучшим уд для данного пользователя
					if (!$best_all_id || $this->user_access[$best_all_id]['ud'] < READ_ACCESS) //если лучше не нашли
						exit ($this::NO_PAGES_ACCESS_ERROR); 
					else header("Refresh:0; url=?id_allblocks=".$best_all_id); //иначе переадресуем
				}
				
				$this->glb_access = $this->get_pages_access ($id, $id_allblocks);
			break;
			
			case 'files':
			case 'fcontent':
			case 'backchat':
				$this->glb_access = $this->get_follow_access ($id, $key, $id_allblocks);
			break;

			default:
				// code...
			break;
		}
		
		if ($flag_action)
		{
			if ($this->glb_access < READ_ACCESS) 
				exit ($this::NO_ACCESS_ERROR);
		}
			
	}

	/*метод для проверки доступности подстраницы раздела.
	$key - ключ, м.б. allblocks или pages
	$id - идетификатор страницы, если есть
	$id_allblocks - идентификатор версии контента, если есть
	*/
	public function access_action_itemslist ($key, $id=0, $id_allblocks=0)
	{
		if ($this->isAdmin ()) return WRITE_ACCESS; //если действует админ проверки не нужны

		switch ($key) {
			
			case 'allblocks':
			//case 'red_allblocks':
				 $res = $this->user_access[$id]['ud'];
			break;
			
			case 'pages':
				 $res = $this->get_pages_access($id, $id_allblocks);
			break;
			
			case 'backchat':
			case 'files':
			case 'pcontent':
			case 'fcontent':
				 $res = $this->get_follow_access($id, $key, $id_allblocks);
			break;

			default:
				// code...
			break;
		}
		return ($res)?: NO_ACCESS;
	}

	//метод для тестирования выводит содержимое $user_access
	public function test_user_access ()
	{
		print_r ($this->user_access);
	}

	//метод логинрования действий пользователя
	public function save_log ($title="Действие", $info='')
	{
		$templ = "\r\n --> #datatime#; UID #id#; #title#; UDATA #udata#; #info#; #ip#";
		$filename = $_SERVER['DOCUMENT_ROOT'].'/admin/user_log.txt';

		$udata = [
		"id_usertype" => $this->id_usertype,
		"usertype" => $this->usertype,
		"email" => $this->email,
		"username" => $this->username,
		"glb_access" => $this->glb_access, //чтобы хранить информацию о доступе к заглавному разделу
		"authStatus" => $this->authStatus,
		"user_access" => $this->user_access,
		"type_access" => $this->type_access,
		"isbanned" => $this->isbanned
		];

		$t = [
			"title" => $title,
			"info" => $info,
			"datatime" => date('d.m.Y, H:i:s'),
			"id" => $this->id,
			"udata" => php2js($udata),
			"ip" => $_SERVER['REMOTE_ADDR']
		];

		@file_put_contents($filename, writeTemplate($t, $templ), FILE_APPEND);
	}

	//метод возвращает id версии контента к которой наивысший УД у пользователя
	private function best_allblocks_access ()
	{
		$max_id = 0;
		foreach ($this->user_access as $key => $t)
		{
			if($t['ud'] && ($t['ud']>$this->user_access[$max_id]['ud']))
				$max_id = $key;
		}
		return $max_id;
	}
	
	//метод возвращает уд к объекту (файлу, контенту, сообщению)
	private function get_follow_access ($id=0, $key, $id_allblocks=0)
	{
		$query = '';
		$res = NO_ACCESS;
		
		switch ($key)
		{
			case 'pcontent': 
				$query = "SELECT id_pages as id, id_allblocks FROM pcontent WHERE id='".$id."' limit 1"; 
			break;
			
			case 'files': 
				$query = "SELECT f.id_pages as id, fc.id_allblocks FROM files f INNER JOIN fcontent fc ON f.id=fc.id_files WHERE f.id='".$id."' limit 1"; 
			break;
			
			case 'fcontent': 
				$query = "SELECT files.id_pages as id, fcontent.id_allblocks FROM fcontent INNER JOIN files ON files.id=fcontent.id_files WHERE fcontent.id='".$id."' limit 1"; 
			break;
			
			case 'backchat': 
				$query = "SELECT id_pages as id, id_allblocks FROM backchat WHERE id='".$id."' limit 1"; 
			break;
			
			default: return NO_ACCESS; break;
		}
		
		if($query)
		{
			$lst = ew_mysqli_query($query) or die (ew_mysqli_error());
			if ($pc =  @mysqli_fetch_assoc($lst))//если есть запись о контенте
			{
				$id_allblocks = ($id_allblocks) ? $id_allblocks : $pc['id_allblocks'];
				$res = $this->get_pages_access ($pc['id'], $id_allblocks);
			}
				
		}
		
		return $res;
	}
	
	//метод возвращает УД к странице с учетом наследования
	private function get_pages_access ($id, $id_allblocks=0)
	{
		$all = $this->user_access[$id_allblocks]['pages'];
		$tek_ud = NO_ACCESS;
		
		if (!isset($all[$id]))//такой записи нет в json УД
		{
			$tree = array_reverse (getPraInfo ("SELECT p.id, p.id_pages FROM pages p INNER JOIN pcontent pc ON pc.id_pages=p.id WHERE p.id=%d AND pc.id_allblocks=%d LIMIT 1", $id, $id_allblocks));//получаем массив деревьев
			$flag_nashli = false; 
	
			if(count($tree))
			{
				foreach ($tree as $t)
					if (isset($all[$t['id']]))//если нашли совпадение с json ud, т.е. есть за что зацепиться
					{
						$id = $t['id'];
						$flag_nashli = true;
						break;
					}
			}
			
			if (!$flag_nashli)//если и в БД ничего не нащли
				return $this->user_access[$id_allblocks]['ud'];//возвращаем уд по версии контента
		}
		
		if($all[$id]['ud']) //если уд назначен самой странице
			$tek_ud = $all[$id]['ud'];
		else //если уд странице напрямую не был назначен, надо смотреть предков
			$tek_ud = (isset($all[$id]['id_pages'])) ? $this->get_pages_access($all[$id]['id_pages'], $id_allblocks) : nearestInt($this->user_access[$id_allblocks]['ud'], NO_ACCESS);//если нет предков берем УД от версии контента

		return ($tek_ud=="" || $tek_ud==0) ?  NO_ACCESS : $tek_ud;
	}
	
	/*метод соотносит массивы $arr и $barr. И возвращает $arr наполненный только такими элементами, которые встречаются и в $barr по параметру id. При этом в массивах ключи элементов являются их id-шниками
	@int $id_allblocks - содержит id версии контента. Если передан, тогда в barr будет сожержимое pages  версии контента по id_allblocks. Иначе - будет сравнение по allblocks"
	*/
	public function arr_filtr($arr=[], $id_allblocks=0)
	{
		$res = [];
		$barr = ($id_allblocks) ? $this->user_access[$id_allblocks]['pages'] : $this->user_access;
		
		foreach ($arr as $a)
		{
			if (isset ($barr[$a['id']]) && $barr[$a['id']]['ud']>=READ_ACCESS)//если такой элемент есть в массиве и его УД не меньше чтения
				$res[] = $a;
		}
		return $res; 
	}
}
?>
