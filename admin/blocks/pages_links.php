<?php
//вывод меню
echo pageslistnav($m3,[
	[//добавить подстраницу в раздел
		'param'=>'cansplit, maxnode, teknode, canadd',
		'func'=>function ($cansplit, $maxnode, $teknode, $canadd) use ($USER) {return ($USER->glb_access==WRITE_ACCESS && $cansplit==1 && ($maxnode>$teknode || $maxnode==0) && $canadd==1)?: 0; }, // проверка функцией
		'newbutton'=>' <a href="red_pages.php?kat=#id#&id_allblocks='.$id_allblocks.'" title="добавить подстраницу в #title#"><img src="picture/add.png" width="50" height="50"></a> '
	],
	[//кнопка редактировать
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="red_pages.php?id=#id#&id_allblocks='.$id_allblocks.'" title="перейти к редактированию страницы #title#"><img src="picture/ref.gif" width="50" height="50"/></a>'
	],
	[
		'param'=>'canfoto',
		'func'=>function ($canfoto) {return $canfoto;}, // проверка функцией
		'newbutton'=>'  <a href="foto.php?id_pages=#id#&id_allblocks='.$id_allblocks.'" title="Смотреть прикрепленные фото к странице #title#"><img src="picture/photo.png" width="50" height="50"></a> '
	],
	[
		'param'=>'candocs',
		'func'=>function ($candocs) {return $candocs;}, // проверка функцией
		'newbutton'=>'  <a href="documents.php?id_pages=#id#&id_allblocks='.$id_allblocks.'" title="Смотреть прикрепленные документы к странице #title#"><img src="picture/word.png" width="50" height="50"></a>  '
	],
	[
		'param'=>'cansplit, maxnode, teknode, canadd',
		'func'=>function ($cansplit, $maxnode, $teknode, $canadd) {return ($cansplit==1 && ($maxnode>$teknode || $maxnode==0)); }, // проверка функцией
		'newbutton'=>'  <a href="pages.php?kat=#id#&id_allblocks='.$id_allblocks.'" title="Смотреть  прикрепленные страницы"><img src="picture/limit.png" width="50" height="50"></a>  '
	],
	[
		'param'=>'canguestbook',
		'func'=>function ($canguestbook) use ($USER) {
				return ($USER->dtrm_check_pages('message')) ? $canguestbook : 0;
			}, // проверка функцией
		'newbutton'=>'
<a href="backchat.php?kat=#id#" title="Смотреть сообщения страницы #title#"><img src="picture/gbook.png" width="50" height="50"></a>  '
	],
	/*
	[
		'param'=>'isgoods',
		'func'=>function ($isgoods) use ($USER) {
				return ($USER->dtrm_check_pages('filter') && $isgoods);
			}, // проверка функцией
		'newbutton'=>' <a href="filter.php?id_pages=#id#&id_allblocks='.$id_allblocks.'" title="Управлять фильтрами #title#"><img src="picture/filter_list.png" width="50" height="50"></a> <a href="property.php?id_pages=#id#&id_allblocks='.$id_allblocks.'" title="Характеристики товара #title#"><img src="picture/property.png" width="50" height="50"></a>'
	],
	
	[
		'param'=>'iscategory',
		'func'=>function ($iscategory) use ($USER) {
				return ($USER->dtrm_check_pages('filter') && $iscategory);
			}, // проверка функцией
		'newbutton'=>' <a href="filter.php?id_pages=#id#&id_allblocks='.$id_allblocks.'" title="Управлять фильтрами #title#"><img src="picture/filter_list.png" width="50" height="50"></a> '
	],
	*/
	[//кнопка вверх по списку
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="pages.php?kat=#id_pages#&id_allblocks='.$id_allblocks.'" title="Вернуться к верхнему списку подстраниц"><img src="picture/list.png" width="50" height="50"></a> '
	]
]);

?>
