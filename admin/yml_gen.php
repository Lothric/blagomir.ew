<?php require_once('../blocks/ewcore/bd.php');
 require_once('blocks/ewadmincore/ew.admin.init.php');
 $USER->dtrm_access_action('yml');//проверка доступа к странице
?>
<html>
<?php require_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php require_once('blocks/menu.php'); ?>
</td>

<td align="center" valign="middle">
<div align="center"><h2>Генерация yml-файла</h2></div><br>
<br> 
<p><a href="/yml.xml" target="_blank">Смотреть файл yml.xml</a></p>
<div class="block">
<?php
const FILENAME = 'yml.xml';
const COMPANY = 'ООО «Аквалайф»';
$all = getLightArray (getSQLdata (ALLBLOCK_QUERY));
define('ID_ALLBLOCKS', $all['id']);
//данные о товарах
//$goods = arrPersonality(getSQLdata (GOODS_MENU_QUERY, PRODUCT_PAGES, ID_ALLBLOCKS),'id');
$goods = getGoodsData('aggs=0&size=10000')['hits'];

$content = '<?xml version="1.0" encoding="UTF-8"?>
<yml_catalog date="'.date('Y-m-d H:i').'">
    <shop>
		<name>'.$all['title'].'</name>
		<company>'.COMPANY.'</company>
		<url>'.HOST_PROTOCOL.HOST_NAME.'</url>
		<currencies>
		  <currency id="RUR" rate="1"/>
		</currencies>
		<categories>
		  <category id="1">Функциональное питание</category>
		</categories>
		<delivery-options>
		   <option cost="350" days="2-5" order-before="18"/>
		</delivery-options>
		<offers>';
		
foreach ($goods as $t)
{
	$t['first_foto'] = (($t['foto'][GOODS_FOTO_TYPE])) ? $t['foto'][GOODS_FOTO_TYPE][0]['pic'] : FOTO_DIR.NOFOTO;
	
	$content .= writeTemplate ($t, '<offer id="#id#" type="vendor.model" available="true">
	<url>'.HOST_PROTOCOL.HOST_NAME.'#link#</url>
	<price>#price#</price>
	<currencyId>RUR</currencyId>
	<categoryId>1</categoryId>
	<model>#title#</model>
	<picture>'.HOST_PROTOCOL.HOST_NAME.'#first_foto#</picture>
	<delivery>true</delivery>
	<delivery-options>
	<option cost="350" days="2-5" order-before="18"/>
	</delivery-options>
	<pickup>true</pickup>
	<country_of_origin>Россия</country_of_origin>
	<vendor>'.COMPANY.'</vendor>
	<name>#title#</name>
	<description>
	<![CDATA[
	#info# #dopinfo# #banner#
	]]>
	</description>
	<sales_notes>Необходима предоплата.</sales_notes>
	<manufacturer_warranty>true</manufacturer_warranty>
	<cpa>1</cpa>
	</offer>');
}
		
$content .= '
		</offers>
	</shop>
</yml_catalog>';

if(file_put_contents('../'.FILENAME, $content))
	echo '<p>YML.xml успешно сгенерирован!</p>';
else
	echo '<p>Ошибка в генерации :( Попробуйте ещё раз!</p>';

?>
</div>


</td>
</table>

<!-- BODY END HERE -->

<?php require_once('blocks/unterblock.php'); ?>

</body>
</html>