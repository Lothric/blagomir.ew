<?php require_once('../blocks/ewcore/bd.php'); 
require_once('blocks/ewadmincore/ew.admin.init.php'); 
$dtrm_access = $USER->dtrm_access_action('message');//проверка доступа к странице
$id_allblocks=safeGetNumParam($_GET, 'id_allblocks', 1);
$ud_pages=[];

if ($kat=safeGetNumParam($_GET, 'kat'))
{
	$resultm = ew_mysqli_query("SELECT pages.*, pc.title, pc.id as pc_id FROM pages left JOIN pcontent as pc ON pc.id_pages=pages.id AND pc.id_allblocks='$id_allblocks' WHERE pages.id = '$kat' LIMIT 1");
	$m3 = mysqli_fetch_assoc($resultm);
	$USER->access_action('pages', $kat, $id_allblocks, true);//определяем глобальный УД, т.е. УД к самой странице
	$ud_pages[$kat]=$USER->glb_access;
	echo $USER->glb_access;
}


if ($id=safeGetNumParam($_GET, 'id', 0))//удаление 
{
	if(!$kat)//был передан id страницы, значит УД к ней не был определен
		$USER->access_action('backchat', $id, $id_allblocks, true);//определяем глобальный УД, т.е. УД к самой странице
	
	if ($USER->glb_access==WRITE_ACCESS)
	{
		$resultd = ew_mysqli_query("SELECT * FROM backchat WHERE id='$id' limit 1");
		if($dres = mysqli_fetch_assoc($resultd))
		{
			$dres['json_info'] = html_entity_decode($dres['json_info']);
			if($dres['json_info'] and ($json_info=safeGetParam($dres, 'json_info', 'JSON')) AND $json_info['file'])
				@unlink (PEOPLE_DIR.$json_info['file']);
		}
		ew_mysqli_query("DELETE FROM backchat WHERE id='$id'");
		$USER->save_log("Удаление сообщения ".$id);
	}
	else 
	{
		phpalert(EWAdminUser::DELETE_ACCESS_ERROR);
		$USER->save_log("Ошибка удаления сообщения: ".$id, EWAdminUser::DELETE_ACCESS_ERROR);
	}
}
	

$dopquery=array();

if ($kat) $dopquery[0]="backchat.id_pages=".$kat;

$dopquery[]='id_allblocks='.$id_allblocks;

if($id_type=safeGetNumParam($_GET, 'id_type', false))
	$dopquery[]='id_type='.$id_type;


if (count ($dopquery))
$dopquery=" WHERE ".implode(' AND ', $dopquery);
else $dopquery='';

$id_orderby=safeGetNumParam($_GET, 'id_orderby', 0);
switch($id_orderby)//определяем сортировку
{
	case 2:  $orderby='id asc';break;	
	default: $orderby='id desc'; break;
}

if ($uid=safeGetNumParam($_GET, 'public', 0))//публикуем
{
	if(!$kat && !$id)//не был передан id страницы, значит УД к ней не был определен
		$USER->access_action('backchat', $uid, $id_allblocks, true);//определяем глобальный УД, т.е. УД к самой странице
	
	if ($USER->glb_access==WRITE_ACCESS)
	{
		ew_mysqli_query("update backchat SET public=1 WHERE id='$uid' limit 1");
		$USER->save_log("Публикация сообщения ".$id);
	}
	else 
	{
		phpalert(EWAdminUser::AJAX_NO_ACCESS_ERROR);
		$USER->save_log("Ошибка публикации сообщения: ".$id, EWAdminUser::AJAX_NO_ACCESS_ERROR);
	}
}
	
if ($uid=safeGetNumParam($_GET, 'unpublic', 0))//публикуем
{
	if(!$kat && !$id)//не был передан id страницы, значит УД к ней не был определен
		$USER->access_action('backchat', $uid, $id_allblocks, true);//определяем глобальный УД, т.е. УД к самой странице
		
	if ($USER->glb_access==WRITE_ACCESS)
	{
		ew_mysqli_query("update backchat SET public=0 WHERE id='$uid' limit 1");
		$USER->save_log("Снятие с публикации сообщения ".$id);
	}	
	else 
	{
		phpalert(EWAdminUser::AJAX_NO_ACCESS_ERROR);
		$USER->save_log("Ошибка снятия с публикации сообщения: ".$id, EWAdminUser::AJAX_NO_ACCESS_ERROR);
	}
}
	
$verse = getSQLArr("SELECT allblocks.name as title, allblocks.id FROM allblocks order by npp asc"); //массив с типами версий контентов allblocks
$verse = ($USER->isAdmin()) ? $verse : $USER->arr_filtr ($verse);//если мы админ, то не надо фильтровать перечень доступных версий контента

?>
<html>
<head>
<?php include_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>

<td align="center" valign="middle">
<div align="center"><h2>Сообщения <?php echo $m3["title"]; ?></h2>
<?php include_once('blocks/pages_links.php'); ?>
<br>
<br>

<?
echo ew_filtr ('Сортировка: ', [
['id'=>1, 'title'=>'сначала новые'],
['id'=>2, 'title'=>'сначала старые']
], $id_orderby, 'id_orderby'), '<br><br>';

$mytype = getglossary(CALLBACK_TYPE_GLOSSARY, 'id_glossary', 0, 0, 'npp');//словарь для главной страницы
echo ew_filtr ('Фильтр по теме: ', $mytype, $id_type, 'id_type', 'npp', ['id'=>0, 'title'=>'Нет'], '<label>#title#</label> <select class="filtr" pole="#pole#" name="#url_param_name#">#result#</select>', '<option pid="#npp#" #isselected#>#title#</option>'), '<br><br>';

echo ew_filtr ('Фильтр по версии контента: ', $verse, $id_allblocks, 'id_allblocks', 'id'), '<br><br>';

?>

</div>
<div  style="max-width:400px; text-align:left;">
<?php
echo ew_pagesList ($dopquery, "SELECT backchat.*, (SELECT title FROM glossary WHERE backchat.id_type=glossary.npp AND glossary.id_glossary='".CALLBACK_TYPE_GLOSSARY."' limit 1) as type FROM backchat  
#kat# ORDER BY #orderby#", $orderby, [
	[//кнопка редактировать
		'param' => 'public',
		'func'=> function ($public){return (!$public);},//анонимная функция проверяющая условия для отображения кнопки
		'newbutton'=>'  <a href="?public=#id#&kat=#id_pages#" title="Публиковать? Сейчас не опубликовано"><img src="picture/no.png" width="20" height="20"/></a>'
	],
	[//кнопка редактировать
		'param' => 'public',
		'func'=> function ($public){return ($public);},//анонимная функция проверяющая условия для отображения кнопки
		'newbutton'=>'  <a href="?unpublic=#id#&kat=#id_pages#" title="Убрать с публикации? Сейчас опубликовано"><img src="picture/yes.png" width="20" height="20"/></a>'
	],
	[//кнопка редактировать
		'param' => 'ud',
		'func'=> function ($ud){return ($ud==WRITE_ACCESS);},//анонимная функция проверяющая условия для отображения кнопки
		'newbutton'=>'  <a href="red_backchat.php?id=#id#" title="перейти к редактированию"><img src="picture/ref.gif" width="20" height="20"/></a>
		<a href="?id=#id#&kat=#id_pages#" class="delete" title="Удалить"><img src="picture/del.gif" width="20" height="20" /></a>'
	],
	[//вообще отображение записи
		'param'=>'ud',
		'func'=> function ($ud) use ($USER) {
			return !($ud>=READ_ACCESS); //если УД на чтение или пользователь является админом
		},//анонимная функция проверяющая условия для отображения кнопки
		'newclass'=>'access_close'
	]
], '<div class="#newclass#"><p>#newbutton#<br>
Дата: #datatime#<br><b><a href="?kat=#id_pages#" title="фильтровать по связанной странице">#title#</a></b> <br>Тема: 	 <a href="?id_type=#id_type#" title="фильтровать по теме">#otd_type#</a><br>Телефон: #tel#<br>Email: <a href="mailto:#email#" target="_blank">#email#</a><br>IP-адрес: #ip#<br>Сообщение: <i>#info#</i><br>Ответ: <i>#otvet#</i></p></div>', function ($p) use ($USER, $id_allblocks, $ud_pages){
	if(!isset($ud_pages[$p['id_pages']]))
		$ud_pages[$p['id_pages']]=$USER->access_action_itemslist('pages', $p['id_pages'], $id_allblocks);
	//echo $ud_pages[$p['id_pages']];
	return $ud_pages[$p['id_pages']];
});
	
?>
</div>
</td>
</table>

<!-- BODY END HERE -->
<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript">
$(document).ready(function() {	
	<? if ($USER->glb_access==WRITE_ACCESS || $dtrm_access): ?>
		delete_init ();	
	<? endif ?>

	js_filtr ({
		ind: '.filtr'
	}); 
});
</script>
</body>
</html>