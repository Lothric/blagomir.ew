<?php require_once('../blocks/ewcore/bd.php'); 
require_once('blocks/ewadmincore/ew.admin.init.php');

const DOCS_OR_FOTO=1;// интерфейс для работы с фото

$id_allblocks=safeGetNumParam ($_GET,'id_allblocks',1);//фильтрация по типу контента
$verse = getSQLArr("SELECT allblocks.name as title, allblocks.id FROM allblocks order by npp asc"); //массив с типами версий контентов allblocks
$verse = ($USER->isAdmin()) ? $verse : $USER->arr_filtr ($verse);//если мы админ, то не надо фильтровать перечень доступных версий контента


if ($id=safeGetNumParam ($_GET,'id_pages'))
{
	$resultm = ew_mysqli_query("SELECT pages.*, pc.title, pc.id as pc_id FROM pages left JOIN pcontent as pc ON pc.id_pages=pages.id AND pc.id_allblocks='$id_allblocks' WHERE pages.id = '$id' LIMIT 1");
	$m3 = mysqli_fetch_assoc($resultm);
} 
else exit ("Ошибка! Нет данных о заглавной странице!");

$USER->access_action('pages', $id, $id_allblocks, true);//определяем глобальный УД, т.е. УД к самой странице


if ($id_del_file=safeGetNumParam($_GET, 'id_del_file', false))//запрос на удаление
{
	if ($USER->isAdmin())
	{
		delfiles ($id_del_file, false); //удаление файла вместе с записью
		$USER->save_log("Полное удаление файла ".$id_del);
	}
	else phpalert(EWAdminUser::DELETE_ACCESS_ERROR);
}
//удаление контента файла
if ($id_del_fc=safeGetNumParam ($_GET, 'id_del_fc'))
{
	if ($USER->glb_access==WRITE_ACCESS)
	{
		deleteContent ($id_allblocks, $id_del_fc);
		$USER->save_log("Удаление контента  ".$id_allblocks." файла ".$id_del_fc);
	}
	else phpalert(EWAdminUser::DELETE_ACCESS_ERROR);
}
	

if($type=safeGetNumParam($_GET, 'type',0))
	$dopquery=' AND type='.$type;

echo $USER->glb_access;

$countContentVerse = count($verse);
?>
<html>
<head>
<?php include_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>
<div align="center"><h2 align="center">Прикрепленные фото страницы <?= $m3['title']; ?></h2>

<?php 
$gid=$m3['id'];
include_once('blocks/pages_links.php'); ?>
<br>

<p>
<? if ($USER->glb_access==WRITE_ACCESS): ?>
	<a href="red_foto.php?id_pages=<?= $m3['id']; ?>&id_allblocks=<?= $id_allblocks; ?>"><img src="picture/add.png"></a> 
<? endif ?>

<span id="sort_button"></span>
</p>
<br>
<br>
	<?= ew_filtr ('Версия контента: ', $verse, $id_allblocks, 'id_allblocks') ?>
<br>
<br>
<?= ew_filtr ('Фильтр по типу: ', getSQLArr("SELECT title, npp as id FROM glossary WHERE id_glossary='".SLIDER_GLOSSARY."' order by npp asc"), $type, 'type', 'id', [
	'title'=>'Показывать все типы',
	'id'=>0
]) ?>
<br>
<br><br>
<br>
</div>

<td align="center" valign="middle"  >
<ul id="sortable" class="myfoto">

<?= ew_pagesList ($id,"SELECT files.*, fc.title, (select count(*) FROM fcontent WHERE fcontent.id_files=files.id) as fc_count FROM files INNER JOIN fcontent fc ON fc.id_files=files.id WHERE files.id_pages='#kat#' AND fc.id_allblocks='".$id_allblocks."' AND files.docsorfoto='".DOCS_OR_FOTO."' ".$dopquery." order by #orderby#", 'npp asc, id desc', [
	[//кнопки
		//'func'=> function (){return 1;},//анонимная функция проверяющая условия для отображения кнопки
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="red_foto.php?id=#id#&id_allblocks='.$id_allblocks.'"><img src="picture/ref.gif" width="20" height="20"/></a>'
	],
	[//удалить контент картинки 
		'param'=>'fc_count',
		'func'=> function ($fc_count) use ($USER)//если не все версии контента были созданы для страницы подсвечиваем
			{
				return ($USER->glb_access==WRITE_ACCESS);
			},
		'newbutton'=>'<a href="foto.php?id_del_fc=#id#&id_pages=#id_pages#&id_allblocks='.$id_allblocks.'"  class="fc_delete"  title="Удалить контент файла"><img src="picture/del.gif" width="20" height="20"/></a>'
	],
	[//удалить полностью картинку 
		'param'=>'fc_count',
		'func'=> function ($fc_count) use ($USER)//если не все версии контента были созданы для страницы подсвечиваем
			{
				return ($USER->isAdmin());
			},
		'newbutton'=>'<a href="foto.php?id_del_file=#id#&id_pages=#id_pages#&id_allblocks='.$id_allblocks.'"  class="delete" title="Удалить полностью файл со всеми контентами"><img src="picture/del2.png" width="20" height="20"/></a>'
	],
	[//кподсветка 
		'param'=>'fc_count',
		'func'=> function ($fc_count) use ($countContentVerse)//если не все версии контента были созданы для страницы подсвечиваем
			{
				return ($fc_count==1 && $countContentVerse>1);
			},
		'newclass'=>'yellowbg',
		'bg_title'=>'Для данной картинки создана только одна версия контента. Значит в каких то версиях она не будет отображаться'
	]
],
'<li class="ui-state-default #newclass#" id="#id#" title="#bg_title#"><a href="'.FOTO_DIR.'#picname#" class="gallery"><img src="'.FOTO_DIR.'#foto#"></a><br>#newbutton#</li>');
?>
      </ul>   
</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript" src="js/ew.plugin.sort.js"></script>
<script type="text/javascript">
$(document).ready(function() {		
	delete_init ('Полностью удалить файл (включая все версии контента)?');
	
	delete_init ('Удалить контент файла (сам файл не будет удален с сервера, если для него создан хотя бы один контент)?', '.fc_delete');
	
	<? if ($USER->glb_access==WRITE_ACCESS): ?>
	$('#sortable').ewSorter({
	  table: 'files',//таблица бд
	  button_block: $("#sort_button"),
	  button_block_template: '#button_template# #loader_template#'
	});
	<? endif ?>
	
	//работа фильтров
	js_filtr ({
		ind: '.filtr',
		clear_val: 'id_del_fc, id_del_file'
	}); 
});
</script>
</body>
</html>