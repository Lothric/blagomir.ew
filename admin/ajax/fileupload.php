<?php
if(isset($_POST))
{
	require_once('../../blocks/ewcore/bd.php');
	require_once('../blocks/ewadmincore/ew.admin.init.php');
	$filename=$_POST['filename'];
	$fileext=$_POST['fileext'];
	$folder="../../".$_POST['folder'];
	$table=$_POST['table'];
	$stupid=$_POST['stupid'];
	$colname=$_POST['colname'];
	$colext=$_POST['colext'];
	$imgres=json_decode('[' . $_POST['imgres'] . ']', true);
	$id=safeGetNumParam($_POST,'id', safeGetNumParam($_GET,'id'));
	
	if ($id)//конкретная запись выбирается
	{
		$result = ew_mysqli_query("SELECT  $table.*, pages.id_pages, pages.id AS pid, pages.*, $table.$stupid as did FROM $table INNER JOIN pages ON $table.id_pages=pages.id WHERE $table.$stupid='$id'  LIMIT 1");
	}
	else //айди не получен
	{
		echo "Ошибка! id документа не получен!";
		exit();
	}

	if ($m3 = mysqli_fetch_assoc($result)) //выборка получилась
			$id = $m3['did'];
	else
	{
		echo "Ошибка выборки добавления!";
		exit();
	}
	//прием файла
	if (file_exists($folder.$filename) && ($m3['picname'] != $filename))
	{
		$path_parts = pathinfo($filename);
		if (is_null($path_parts['extension']))
			$filename = $path_parts['filename'].date("HisdmY");
		elseif ($path_parts['extension'] == '')
			$filename = $path_parts['filename'].date("HisdmY").".";
		else
			$filename = $path_parts['filename'].date("HisdmY").".".$path_parts['extension'];
	}
		
	// Каталог, в который мы будем принимать файл:
	$uploaddir = $folder;
	
	$d = $filename;
	
	$path_parts = pathinfo($filename);
	$pic=array($path_parts['filename'],$path_parts['extension']);
	$filesize = filesize($_FILES['uploadfile']['tmp_name']);
	$uploadfile = $uploaddir.basename($filename);
	
	// Копируем файл из каталога для временного хранения файлов:
	if (copy($_FILES['uploadfile']['tmp_name'], $uploaddir.$d))
	{
		$t=$d;
		if (($m3['picname'] != '') && ($m3['picname'] != $filename))
		{
			@unlink ($uploaddir.$m3['picname']);
			//переименоавние новой картнки в старую, что бы не менять код
			$path_parts_old = pathinfo($m3['picname']);
			$picold=array($path_parts_old['filename'],$path_parts_old['extension']);

			if ((is_null($picold[1]) && is_null($pic[1])) || $picold[1]==$pic[1])//расширение не поменялось, возможно просто обновление файла
			{
				$pic[0]=$picold[0];
				$d=$m3['picname'];
				//переименование картинки, для исключения совпадений
				rename ($uploaddir.$t, $uploaddir.$m3['picname']);
				$t=$m3['picname'];
			}
		}
		
		//	 если загружааетеся картинка, а не файл документа и т.п. - сжатие картинки
		if (in_array($pic[1], array("jpg","jpeg", 'JPG')))
		{
			//----- определение ориентаци картинки
			$picture=getimagesize ($uploaddir.$t);
			if ($picture[0]>$picture[1])//горизонтальная ориентация
			{
				$w=$imgres[0]; $h=$imgres[1];	$gorver=0;
			}
			else //вертикальная ориентация
			{
				$w=$imgres[1]; $h=$imgres[0];	$gorver=1;	
			}
			//-----
					
			
					
			if ($picture[0]>$w || $picture[1]>$h)
			{
				if ($gorver==0)
				{
					$w0=$w;
					$h0=$w*$picture[1]/$picture[0];
				}
				else 
				{
					$h0=$h;
					$w0=$h*$picture[1]/$picture[0];
				}
			imageresize($uploaddir.$t, $uploaddir.$t, $w0,$h0, $imgres[2]);
			
			}
		}
				  
	
		//-----------------------
		$wresult = ew_mysqli_query ("UPDATE $table SET $colname='$t', $colext='".$pic[1]."', filesize='$filesize' WHERE $stupid='$id'");
		$m3['picname']=$t;
		$USER->save_log("AJAX/FILEUPLOAD: Загрузка файла ".$m3['picname'], "UPDATE $table SET $colname='$t', $colext='".$pic[1]."', filesize='$filesize' WHERE $stupid='$id'");
		echo $t;
	}
	else
	{
		$USER->save_log("AJAX/FILEUPLOAD: Ошибка загрузки файла ", json_encode($_POST));
		echo "Ошибка! Не удалось загрузить файл на сервер!";
	}
		
}
?> 