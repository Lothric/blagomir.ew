<?php
if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
{
	require_once('../../blocks/ewcore/bd.php');
	require_once('../blocks/ewadmincore/ew.admin.init.php');

	$pid=safeGetParam($_POST, 'pid', 'INT');
	$fid=safeGetParam($_POST, 'fid', 'INT');
	$res=safeGetParam($_POST, 'res', 'INT');

	$ud = $USER->access_action_itemslist("filter", $pid);
	if ($ud != WRITE_ACCESS) 
	{
		$USER->save_log("AJAX/ADDON: Запрет действия", json_encode($_POST).' - '.$ud);
		exit ($USER::AJAX_NO_ACCESS_ERROR);
	}
		

	if ($pid && $fid)
	{
		$query = '';
		if($res)//если записываем галочку
		{
			$dopquery1 = ew_mysqli_query("SELECT id FROM goods_filter WHERE id_goods = '$pid' AND id_filter='$fid' limit 1");
			$filter_isset = mysqli_fetch_assoc($dopquery1)['id'];
			if(!$filter_isset)//если такой записи еще нет
				$query = "INSERT INTO goods_filter SET id_goods = '$pid', id_filter='$fid'";
			else exit ("1"); //если есть - то ничего не делаем
		}
		else
		{
			$query = "delete FROM goods_filter WHERE id_goods = '$pid' and id_filter='$fid'";
		}
		
		$result = ew_mysqli_query ($query);
		if ($result == TRUE)
		{
			$USER->save_log("AJAX/ADD_FILTER: ($res) Добавление или удаление фильтра $fid для товара $pid", $query);
			exit ("1");
		}
		else
		{
			$USER->save_log("AJAX/ADD_FILTER: Ошибка сохранения фильтра $fid для товара $pid".ew_mysqli_error(), $query);
			exit ("Ошибка сохранения фильтра");
		}		
	} 
	echo "Ошибка сохранения!";
}
?>