<?php
if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
{
	require_once('../../blocks/ewcore/bd.php');
	require_once('../blocks/ewadmincore/ew.admin.init.php');

	$pr_id=safeGetParam($_POST, 'pr_id', 'INT');
	$pid=safeGetParam($_POST, 'pid', 'INT');
	$type=safeGetParam($_POST, 'type', 'INT');
	$val=safeGetParam($_POST, 'val', 'ALL');

	$ud = $USER->access_action_itemslist("property", $pid);
	if ($ud != WRITE_ACCESS) 
	{
		$USER->save_log("AJAX/ADDON: Запрет действия", json_encode($_POST).' - '.$ud);
		exit ($USER::AJAX_NO_ACCESS_ERROR);
	}
		

	if ($pid && $pr_id && $type)
	{
		$query = '';
		$log_title = '';
		switch ($type)
		{
			case 1: //сохранение 
				$dopquery1 = ew_mysqli_query("SELECT id FROM goods_property WHERE id_property='$pr_id' AND id_goods='$pid' limit 1");
				if($t = mysqli_fetch_assoc($dopquery1)['id'])//если запись уже есть
					$query = "UPDATE goods_property SET value='$val' WHERE id='$t' limit 1";
				else 
					$query = "INSERT INTO goods_property SET id_property='$pr_id' , id_goods='$pid', value='$val'";

				$log_title = "Добавление характеристики $pr_id для товара $pid со значением $val";
			break;
			
			case 2: //удаление
				$query = "delete FROM goods_property WHERE id_property='$pr_id' AND id_goods='$pid'";
				$log_title = "Удаление характеристики $pr_id для товара $pid";
			break;
		}
		
		$result = ew_mysqli_query ($query) OR die ($query.ew_mysqli_error());
		if ($result == TRUE)
		{
			$USER->save_log('AJAX/PROPERTY: '.$log_title, $query);
			exit ("1");
		}
		else
		{
			$USER->save_log("AJAX/PROPERTY: Ошибка! ".$log_title.ew_mysqli_error(), $query);
			exit ("Ошибка обновления данных!");
		}		
	} 
	echo "Ошибка! Переданы неверные данные!";
}
?>