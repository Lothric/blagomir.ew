<?php
if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && isset($_POST))
{
	//создание урезанной фотографии + миниатюры
	include_once('../../blocks/ewcore/bd.php'); 
	include_once('../blocks/ewadmincore/ew.admin.init.php');
	$source = $_POST['i'];
	$outWidth = $_POST['w'];
	$outHeight = $_POST['h'];
	$outX = $_POST['x'];
	$outY = $_POST['y'];
	$quality = 97;
	$gorW = $_POST['gorw'];
	$gorH = $_POST['gorh'];
	$verW = $_POST['verw'];
	$verH = $_POST['verh'];

	$table=safeGetParam($_POST, 'table', 'CHAR', 'files');
	//авторизация фото 
	$uploaddir='../../foto/';
	$id=$_POST['id'];

	$result = ew_mysqli_query("SELECT picname2 FROM $table WHERE id='$id' LIMIT 1");
	if($p = mysqli_fetch_assoc($result))
		@unlink ($uploaddir.$p['picname2']);//удаление старой
	//--------------------

	$size = getimagesize($source);
	$gorver = ($size[0]>=$size[1]) ? 0 : 1;//горизонтальная ориентация или вертикальная ориентация

	if ($size === false) return(0);
	$format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
	$icfunc = 'imagecreatefrom' . $format;
	if (!function_exists($icfunc)) return(false);
	$isource = $icfunc($source);
	$image = imagecreatetruecolor($outWidth, $outHeight);
	imagecopyresampled($image, $isource, 0, 0, $outX, $outY, $outWidth, $outHeight, $outWidth, $outHeight);
	imagedestroy($isource);
	$filename = 'tmp'.date ("HisdmY").'.'.$format;
	imagejpeg($image, $uploaddir.$filename, $quality);
	imagedestroy($image);
	//миниатюрка
	$picture=$size;

	//дополнительное уменьшение картинки если больше стандарта
	if ($gorver==0)
	{
		$w=$gorW; $h=$gorH;
	}
	else 
	{
		$w=$verW; $h=$verH;
	}
		
	if ($picture[0]>$w || $picture[1]>$h)
	{
		if ($gorver==0)
		{
			$w0=$w;
			$h0=$h;
		}
		else 
		{
			$h0=$h;
			$w0=$w;
		}
	imageresize($uploaddir.$filename, $uploaddir.$filename, $w0,$h0, 100);
	}
	//---------------------------

	ew_mysqli_query ("UPDATE $table SET picname2='$filename', gorver=$gorver WHERE id='$id'");	

	$res=[($filename) ? '1' : '0', $filename];   

	echo json_encode($res);
}
?>