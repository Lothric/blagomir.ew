<?php require_once('../blocks/ewcore/bd.php'); 
require_once('blocks/ewadmincore/ew.admin.init.php'); 

const DOCS_OR_FOTO=0;// интерфейс для работы с документами

$id_allblocks=safeGetNumParam ($_GET,'id_allblocks',1);//фильтрация по типу контента
$table = 'files';
//прием id
$id=safeGetNumParam($_POST,'id', safeGetNumParam($_GET,'id'));

if ($id==0)//добавление нового
{
	if ($kat=safeGetNumParam($_GET, 'kat', 1))
	{
		$USER->access_action('pages', $kat, $id_allblocks, true);
		if ($USER->glb_access!=WRITE_ACCESS)
			exit ($USER::NO_ACCESS_ERROR);
		
		$id = createFile($kat, DOCS_OR_FOTO, $id_allblocks);
		$USER->save_log("Создание записи о новом файле ".$id);
	}
	else exit ('Ошибка! Нет данных о заглавной странице!');
}
else $USER->access_action($table, $id, $id_allblocks, true);//если нет id_pages надо его дозапросить и определить УД

$verse = getSQLArr("SELECT allblocks.name as title, allblocks.id FROM allblocks order by npp asc"); //массив с типами версий контентов allblocks
$verse = ($USER->isAdmin()) ? $verse : $USER->arr_filtr ($verse);//если мы админ, то не надо фильтровать перечень доступных версий контента

//удаление файла
if ($id_foto=safeGetNumParam($_GET, 'id_foto'))
{
	if ($USER->glb_access==WRITE_ACCESS)
	{
		delfiles ($id_foto);//удаление только файла
		$USER->save_log("Удаление файла  ".$id_foto);
	}
	else phpalert(EWAdminUser::DELETE_ACCESS_ERROR);
}
//---------------

//создание нового контента
if ($content_create=safeGetNumParam ($_GET,'content_create'))
{
	if ($USER->glb_access==WRITE_ACCESS)
	{
		$id_allblocks = createContent($content_create, $id);
		$USER->save_log("Добавление контента  ".$id_allblocks." файла ".$id);
	}
	else phpalert(EWAdminUser::DELETE_ACCESS_ERROR);
}

//удаление контента
if ($content_delete=safeGetNumParam ($_GET,'content_delete'))
{
	if ($USER->glb_access==WRITE_ACCESS)
	{
		$id_allblocks = deleteContent ($content_delete, $id);
		$USER->save_log("Удаление контента  ".$id_allblocks." файла ".$id);
	}
	else phpalert(EWAdminUser::DELETE_ACCESS_ERROR);
}

$result = ew_mysqli_query("SELECT  docs.*, pages.id_pages, pages.id AS pid, pages.*, docs.id as did, fc.title as name, fc.info, fc.id as fc_id FROM files as docs 
	INNER JOIN pages ON docs.id_pages=pages.id 
	left JOIN fcontent fc ON fc.id_files=docs.id  AND fc.id_allblocks='".$id_allblocks."' 
	WHERE docs.id='$id'  LIMIT 1");

if ($m3 = mysqli_fetch_assoc($result)) //выборка получилась
		$id = $m3['did'];
else alertAndRedirect ("Ошибка выборки добавления!");

$ded = getpraded ($m3['id_pages'], 'pages');

echo $USER->glb_access; 
?>
<html>
<head>
<?php include_once('blocks/upblock.php'); ?>

</head>
<body pid="<?= $m3['did']; ?>">
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>
<div align="center"><h2><?= nearest ($m3['name'], "!Документ без имени!"); ?></h2>
<?php 
if ($m3['pid']!=1)//если общие документы, то скрываем меню
	include_once('blocks/pages_links.php'); 
else echo ' <a href="documents.php"><img src="picture/list.png" title="к документам"></a> ';
 ?>
 <br>
<br>
<? if ($USER->glb_access==WRITE_ACCESS): ?>
<a href="?kat=<?= $m3['pid'] ?>&id_allblocks=<?= $id_allblocks; ?>" title="Добавить еще один документ"><img src="picture/add.png"></a> 
<? endif ?>

<div class="block">
<ul>
<?= ew_filtr ('Версия контента: ', $verse, $id_allblocks, 'id_allblocks') ?>

<?php 
if ($USER->glb_access==WRITE_ACCESS)//только для УД=запись
{
	echo ($m3['fc_id']) 
		? 
		'<p><a href="?id_allblocks='.$id_allblocks.'&content_delete='.$id_allblocks.'&id='.$id.'" class="content_delete">Удалить контент для этой версии?</a></p>' 
		: 
		'<p><strong>Контент не создан!</strong> <br><a href="?id_allblocks='.$id_allblocks.'&content_create='.$id_allblocks.'&id='.$id.'" class="content_create">Создать контент для этой версии?</a></p>';  //есть контент или нет?
}
?>

 <br>
<br>
<?php 
 if ($m3['pid']!=1)//если прикрепление к странице, а не вообще
	//показываем типы приклепления
	ew_select (getglossary(DOCS_TYPE_GLOSSARY, 'id_glossary', 0, 0, 'npp'), $m3['type'], $m3['did'], "Тип размещения:", 'npp', $table, 'type', "id", '<label>%s </label><br><select name="arhi" class="js_select" pid="%d" table="%s" pol="%s" stupid="%s" >%s</select><img src="picture/loader.gif" width="20" height="20" style="display:none;"><br><br>', '<option info="#npp#" res="#info#" #isselected#>#npp# - #title#</option>'); 

ew_textRedaktor ('Название:', $m3['fc_id'], 'title', $m3['name'], 'fcontent');

ew_textRedaktor ('Тип файла:', $id, 'filetype', $m3['filetype'], $table);
?>
<p><b>Ссылка:</b> ..<span id="vyvod"><?= DOCS_DIR.$m3['picname']; ?></span></p></ul>
</div></div>
<br>
<td align="center" valign="middle">

<?php
	
if ($USER->glb_access==WRITE_ACCESS && $m3['fc_id'])
{
	if ($fl==0)
	printf ('<span> Загрузить файл!</span><br />
<div class="ser">Файл должен называться латинскими символами и весить не более 10 Мб</div><br>
<div class="dndloader" pid="'.$id.'"></div>
<br>', $id);


	if ($m3['picname'])//если есть файл
	{
		//надо сделать чтобы при загрузке не каритнок отображалась иконка по-умолчанию
		if ($m3['filetype']=='jpg' || $m3['filetype']=='png' || $m3['filetype']=='gif' || $m3['filetype']=='jpeg')
			$m3['picname'] = NOFILE;
		
		echo '<a id="previewfile" title="Удалить!"  href ="?id='.$id.'&id_foto='.$id.'&id_allblocks='.$id_allblocks.'"  ><img src="'.DOCS_DIR.$m3['picname'].'" class="preview"  /></a>';
	}
}

	
				?>
</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?> 
<script type="text/javascript" src="js/ew.plugin.dndfile.js"></script>
<script type="text/javascript">
$(document).ready(function() {	
	
<? if ($USER->glb_access==WRITE_ACCESS): ?>
	js_textRedaktor ('.editable');
	selectChange ('.js_select');
	
	$(".dndloader").ewDnDFile({ changeext:$('.editable[pol=filetype]'),
	folder:'docs/',
	callback: function(dom, file, s){
		$('#vyvod').text('/'+s.folder+file);
	}					   
	});
<? endif ?>
	
	//работа фильтров
	js_filtr ({
		ind: '.filtr',
		clear_val: 'content_create, content_delete'//исключать переменные из урл, если они там попадаются при работе фильтра
		}); 
	
	$('.content_create').click(function (e){
		if (!confirm("Создать контент для данной версии? Если Вы создадите контент, но не заполните его, то пользователи будут видеть на его месте тексты из версии по-умолчанию"))
			e.preventDefault();
		else history.pushState(null, null, '');
	});
	
	$('.content_delete').click(function (e){
		if (!confirm("Удалить контент для данной версии? Если контент будет удален, то в этой версии контента пользователь не увидет данный файл. Его там не будет!"))
			e.preventDefault();
		else history.pushState(null, null, '');
	});
});

</script>
</body>
</html>