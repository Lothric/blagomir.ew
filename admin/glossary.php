<?php include_once('../blocks/ewcore/bd.php');
require_once('blocks/ewadmincore/ew.admin.init.php');

$USER->dtrm_access_action('glossary');//проверка доступа к странице

if($id=safeGetNumParam($_GET, 'id', false))
{
	if(!delglossary($id))
		phpalert( "Ошибка удаления!"); 
	
}
if ($kat=safeGetNumParam($_GET, 'kat', false))
{
	$kat=$_GET['kat'];
	$resultkat = ew_mysqli_query("SELECT * FROM glossary WHERE id='$kat' LIMIT 1");
	$pkat = mysqli_fetch_assoc($resultkat);
	$nkat=$pkat['title'];
}
else 
{
$nkat='Словари';
$kat='NULL';
}

$dopquery=array(
0=>"glossary.id_glossary=".$kat.""
);

if ($kat!=0 && $kat!='NULL') $selquery = 'where '.$dopquery[0];
else $dopquery[0]='glossary.id_glossary is null';

if($id_pages=safeGetNumParam($_GET, 'id_pages', false))
{
	if ($kat==0)
		unset($dopquery[0]);
	$dopquery[]='id_pages='.$id_pages;
}
if($id_allblocks=safeGetNumParam($_GET, 'id_allblocks'))
{
	$dopquery[]='id_allblocks='.$id_allblocks;
}
if($id_usl=safeGetNumParam($_GET, 'id_usl', false))
{
	if ($kat==0)
		unset($dopquery[0]);
	$dopquery[]='id_usl='.$id_usl;
}

$dopquery=implode(' AND ', $dopquery);

$id_orderby=safeGetNumParam($_GET, 'id_orderby', 0);
switch($id_orderby)//определяем сортировку
{
	case 2:  $orderby='glossary.id';break;
	case 3:  $orderby='glossary.title';break;
	
	default: $orderby='glossary.npp'; break;
}
?>
<html>
<head>
<?php include_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>
<div align="center"><h2 align="center"><?php echo $nkat; ?></h2>
<?php if ($kat!=0):?>

<a href="red_glossary.php?kat=<?php echo $kat; ?>"><img src="picture/add.png" width="32" height="32"></a> 

<a href="red_glossary.php?id=<?php echo $kat; ?>"><img src="picture/ref.gif" width="32" height="32"></a> 

<a href="glossary.php?kat=<?php echo $pkat['id_glossary']; ?>"><img src="picture/list.png" width="32" height="32"></a>
<?php else : ?>
 
<a href="red_glossary.php?kat=<?php echo $kat; ?>"><img src="picture/add.png" width="32" height="32"></a> 
<?php endif;?>

</div><br>
<td align="center" valign="middle"  >
<?= ew_filtr ('Сортировка: ', [
['id'=>1, 'title'=>'по сортируемому порядку'],
['id'=>2, 'title'=>'по id'],
['id'=>3, 'title'=>'по названию'],
], $id_orderby, 'id_orderby')?>

<br>
<br>
<?= ew_filtr ('Фильтр по странице: ', getSQLArr("SELECT pages.title, pages.id FROM glossary inner join pages on glossary.id_pages=pages.id ".$selquery." group by pages.id order by pages.id asc"), $id_pages, 'id_pages', 'id', ['id'=>0, 'title'=>'Без фильтра']);
?>

<br><br>

<?= ew_filtr ('Фильтр по версии контента: ', getSQLArr("SELECT id, name as title FROM allblocks order by id asc"), $id_allblocks, 'id_allblocks', 'id', ['id'=>0, 'title'=>'Без фильтра']);
?>

<br><br>

<?= ew_filtr ('Фильтр по условию: ', getSQLArr("SELECT usl.title, usl.id FROM glossary inner join glossary as usl on glossary.id_usl=usl.id ".$selquery." group by usl.id order by usl.id asc"), $id_usl, 'id_usl', 'id', ['id'=>0, 'title'=>'Без фильтра']);
?>


<br>
<br>
<ul id="sortable" mode="glossary">
<?= ew_pagesList ($kat, "SELECT * FROM glossary WHERE ".$dopquery." ORDER BY #orderby# ASC", $orderby, [
	[//кнопки
		//'func'=> function (){return 1;},//анонимная функция проверяющая условия для отображения кнопки
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="red_glossary.php?id=#id#"><img src="picture/ref.gif" width="20" height="20"/></a>
		
		<a href="glossary.php?kat=#id#"><img src="picture/limit.png" width="20" height="20"/></a>
		
		<a href="glossary.php?id=#id#&&kat=#id_glossary#"  class="delete"><img src="picture/del.gif" width="20" height="20"/></a>
		'
	]
],
'<li class="ui-state-default #newclass#" id="#id#">
<span class="opisanie">#npp#. #title#</span>#newbutton#</li>'
);
?>
</ul>   
</td>
</table>
<!-- BODY END HERE -->
<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript" src="js/ew.plugin.sort.js"></script>
<script type="text/javascript">
$(document).ready(function() {		
	delete_init ();
	$('#sortable').ewSorter({
	  table: 'glossary'//таблица бд
	});
	
	//работа фильтров
	js_filtr ({
		ind: '.filtr'
		}); 
});
</script>
</body>
</html>