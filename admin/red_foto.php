<?php require_once('../blocks/ewcore/bd.php'); 
require_once('blocks/ewadmincore/ew.admin.init.php'); 

const DOCS_OR_FOTO=1;// интерфейс для работы с фото

$id_allblocks=safeGetNumParam ($_GET,'id_allblocks',1);//фильтрация по типу контента
$table = 'files';
$id=safeGetNumParam($_POST, 'id', safeGetNumParam($_GET, 'id'));
$settings = getglossary(SLIDER_GLOSSARY, 'id_glossary', 0, 0, 'npp');

if ($id==0)//добавление нового
{
	if ($id_pages=safeGetNumParam($_GET, 'id_pages'))
	{
		$USER->access_action('pages', $id_pages, $id_allblocks, true);
		if ($USER->glb_access!=WRITE_ACCESS)
			exit ($USER::NO_ACCESS_ERROR);
		
		$id = createFile($id_pages, DOCS_OR_FOTO, $id_allblocks);
		$USER->save_log("Создание записи о новом файле ".$id);
	}
	else exit ('Ошибка! Нет данных о заглавной странице!');
}
else $USER->access_action($table, $id, $id_allblocks, true);//если нет id_pages надо его дозапросить и определить УД

$verse = getSQLArr("SELECT allblocks.name as title, allblocks.id FROM allblocks order by npp asc"); //массив с типами версий контентов allblocks
$verse = ($USER->isAdmin()) ? $verse : $USER->arr_filtr ($verse);//если мы админ, то не надо фильтровать перечень доступных версий контента

//создание нового контента
if ($content_create=safeGetNumParam ($_GET,'content_create'))
{
	if ($USER->glb_access==WRITE_ACCESS)
	{
		$id_allblocks = createContent($content_create, $id);
		$USER->save_log("Добавление контента  ".$id_allblocks." файла ".$id);
	}
	else phpalert(EWAdminUser::DELETE_ACCESS_ERROR);
}
	

//удаление контента
if ($content_delete=safeGetNumParam ($_GET,'content_delete'))
{
	if ($USER->glb_access==WRITE_ACCESS)
	{
		$id_allblocks = deleteContent ($content_delete, $id);
		$USER->save_log("Удаление контента  ".$id_allblocks." файла ".$id);
	}
	else phpalert(EWAdminUser::DELETE_ACCESS_ERROR);
}
	

//редактирование
$result = ew_mysqli_query("SELECT  foto.*, pages.id_pages, pages.id AS id, pages.id as pid, pages.*, foto.id as did, fc.title as name, fc.info, fc.href, fc.id as fc_id, fc.dopinfo 
FROM files as foto 
	INNER JOIN pages ON foto.id_pages=pages.id 
	left JOIN fcontent fc ON fc.id_files=foto.id  AND fc.id_allblocks='".$id_allblocks."' 
	WHERE foto.id='$id'  LIMIT 1");
if ($m3 = mysqli_fetch_assoc($result)) //выборка получилась
		$id = $m3['did'];
else alertAndRedirect ("Ошибка выборки добавления!");

$id_pages = $m3['id'];
$ded = getpraded ($m3['id_pages'], 'pages');

echo $USER->glb_access; 

if(safeGetNumParam($_GET, 'rotate') && $USER->glb_access==WRITE_ACCESS && $m3['picname']!='')
{
	//переворот картинки
	// Файл и угол поворота
	$filename = '..'.FOTO_DIR.$m3['picname'];
	$tekfile = file_get_contents($filename);
	if(!$tekfile)
	{
		phpalert ("Ошибка чтения файла картинки!");
		return;
	}
	
	$degrees = 90;
	
	// Загрузка изображения
	$source = imagecreatefromstring ($tekfile);

	// Поворот
	$rotate = imagerotate($source, $degrees, 0);
	
	//file_put_contents ($filename, $rotate);//перезаписываем файл
	save_img ($rotate, $filename);

	// Высвобождение памяти
	imagedestroy($rotate);
	imagedestroy($source);
}
?>
<html>
<head>
<?php include_once('blocks/upblock.php'); ?>
<link href="css/imgareaselect-animated.css" rel="stylesheet" type="text/css" />
</head>
<body pid="<?= $m3['did']; ?>">
<table align="center" id="tab">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>
<td align="center" valign="middle">
<div align="center"><h2><?= nearest ($m3['name'], "!Фото без названия!"); ?></h2>
<?php 
include_once('blocks/pages_links.php'); ?><br>
<br>
<? if ($USER->glb_access==WRITE_ACCESS): ?>
	<a href="?id_pages=<?= $id_pages; ?>&id_allblocks=<?= $id_allblocks; ?>" title="Добавить еще одно фото"><img src="picture/add.png"></a> 
<? endif ?>
</div><br>
<?= ew_filtr ('Версия контента: ', $verse, $id_allblocks, 'id_allblocks') ?>
<br>
<br>
<?php
if ($USER->glb_access==WRITE_ACCESS)//только для УД=запись
{
	echo ($m3['fc_id']) ? 
		'<p><a href="?id_allblocks='.$id_allblocks.'&content_delete='.$id_allblocks.'&id='.$id.'" class="content_delete">Удалить контент для этой версии?</a></p>' 
		: 
		'<p><strong>Контент не создан!</strong> <br><a href="?id_allblocks='.$id_allblocks.'&content_create='.$id_allblocks.'&id='.$id.'" class="content_create">Создать контент для этой версии?</a></p>';  //есть контент или нет?
}

	ew_select ($settings, $m3['type'], $m3['did'], "Тип размещения:", 'npp', $table, 'type', "id", '<label>%s </label><br><select name="arhi" id="js_select" pid="%d" table="%s" pol="%s" stupid="%s" upldir="'.FOTO_DIR.'" >%s</select><img src="picture/loader.gif" width="20" height="20" style="display:none;"><br><br>', '<option info="#npp#" res="#info#" #isselected#>#npp# - #title#</option>');


$resultdoc = ew_mysqli_query("SELECT  foto.npp, fc.title, fc.info, foto.id 
FROM files as foto 
	left JOIN fcontent fc ON fc.id_files=foto.id  AND fc.id_allblocks='".$id_allblocks."' 
	WHERE foto.type=". BOTTOM_DOCSTYPE ." AND docsorfoto=0 AND foto.id_pages=". $id_pages);
$docs = [['id'=>'', 'title'=>'']];
while($doc = mysqli_fetch_assoc($resultdoc)) //выборка получилась
	$docs[] = $doc;
	
if(isset($docs[1]))
	ew_select ($docs, $m3['id_sovmdocs'], $m3['did'], "Ассоциировать с документом:", 'id', $table, 'id_sovmdocs'); 

?>

<? if ($USER->glb_access==WRITE_ACCESS && $m3['fc_id']): ?>
 <div style="width:400px;height: 200px;">
    <span> Загрузить картинку (не более 10 мб):</span><br /><br>
		<div id="dndloader" pid="<?= $id ?>"></div>
	
 </div>
<? endif ?>
<div id="createImgSelection" picname="<?=$m3['picname'] ?>" picname2="<?=$m3['picname2'] ?>"></div>

</div>
</td>
</table>
<table align="center">
<td align="center" valign="middle">
<div class="block">
<ul>
<?php 
ew_textRedaktor ('Название', $m3['fc_id'], 'title', $m3['name'], 'fcontent');

ew_textRedaktor ('Описание', $m3['fc_id'], 'info', $m3['info'], 'fcontent');

ew_textRedaktor ('Дополнительное описание (при необходимости)', $m3['fc_id'], 'dopinfo', $m3['dopinfo'], 'fcontent');

ew_textRedaktor ('Информация', $m3['fc_id'], 'preinfo', $m3['preinfo'], 'fcontent');

ew_textRedaktor ('Ссылка (URL)', $m3['fc_id'], 'href', $m3['href'], 'fcontent');
?>

</ul>
</div>
</td>
</table>
<!-- BODY END HERE -->
<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript" src="js/jquery.imgareaselect.min.js"></script>
<script type="text/javascript" src="js/ew.plugin.dndfile.js"></script>

<script type="text/javascript">
$(function () 
{
	var deffoto = 'closebox.png';
	var id_select = '#js_select';
	var upldir = $(id_select).attr('upldir');
	areaSelect = false;
	
	ramki = $(id_select).find(':selected').attr('res').split(',');//начальные рамки
	pid = $(id_select).attr('pid');
	
	selectChange ('.myselect');
	
	var massiv = new Array ();
	
		//работа фильтров
		js_filtr ({
			ind: '.filtr',
			clear_val: 'content_create, content_delete'//исключать переменные из урл, если они там попадаются при работе фильтра
			});

		//alert($('#createImgSelection').attr('picname'));
		if($('#createImgSelection').attr('picname')!='')
			createImgSelection ($('#createImgSelection'));//вызываем формирование блока урезания картинки
	
	<? if ($USER->glb_access==WRITE_ACCESS): ?>
	js_textRedaktor ('.editable');
		
	//работа выпадающего селекта с типами картинки
	selectChange (id_select, function (result){
			//здесь надо вычленить значения для рамки миниатюрки ивнедрить ее в код обрезания
			ramki = $(id_select).find(':selected').attr('res').split(',');
			if($('#big:hidden').html()!=null)//блок скрыт
				$('#tab').find('#skrylnik').click();
			
			if (areaSelect!=undefined && areaSelect)//если каратинка уже была и есть область вырезания
				{
					areaSelect.setOptions({ 
						aspectRatio: ramki[0]+':'+ramki[1],
						x1: 0, 
						y1: 0,
						x2: ramki[0],
						y2: ramki[1],
						minHeight: ramki[1],
						minWidth: ramki[0] 
					});
					areaSelect.update();
				}
			
			//location.reload();
			/*перезагружаем страницу для ввода в действие новых рамок, хотя тут может лучше без перезагрузки что-то с кодом намутить..?*/
		});
	
	$("#dndloader").ewDnDFile({
		table:'files',	
		folder: 'foto/',
		autoview:false,
		callback: function ($dom, filename) 
		{
			if($('#createImgSelection').attr('picname')=='')//если изначально картинки не было
				createImgSelection ($('#createImgSelection'));
				//alert(filename);
			$('#duck').attr('src', upldir+filename+ "?" + Math.random());//заменяем картинку
			if($('#big:hidden').html()!=null)//если блок исходной картинки скрыт
				$('#tab').find('#skrylnik').click();//открываем его
		}
	});
	
	//скрыть вскрыть
	$('#skrylnik').live('click', function () 
	{
		
		 if($(this).attr('src')=='picture/down.png')
		 {
			 $(this).attr('src', 'picture/up.png');
			 $('#big').slideUp('slow'); 
			 areaSelect.cancelSelection();
			 $('#rotate').fadeOut();
		 }
		 else if ($(this).attr('src')=='picture/up.png')
		 {
			 $(this).attr('src', 'picture/down.png');
			 $('#big').slideDown('slow');
			// areaSelect.setSelection();
			  areaSelect.update();
			   $('#rotate').fadeIn();
		 }
	});

	//обработка миниатюрки
	$('#savemini').live('click', function (e){
		e.preventDefault ();
		$(this).hide ();
		$('.imgareaselect-border1 , .imgareaselect-border2 , .imgareaselect-border3 , .imgareaselect-border4').hide ();
		$('#loader').fadeIn ();
		$.post('ajax/portfolio.php',
		{
			w: massiv.w,
			h: massiv.h,
			x: massiv.x,
			y: massiv.y,
			i: massiv.i, 
			id: pid,
			gorw: ramki[0], 
			gorh:ramki[1], 
			verw:ramki[0], 
			verh:ramki[1]
		},
		function(res){
			res = JSON.parse(res);
			var result = res[0];
			if (result==1)
			{
				//alert ('Информация обновлена!');
				$('#tab').find('#skrylnik').click();
				$('#tmp img').attr('src', upldir+res[1]);//меняем миниатюрку
				//location.replace ('');
			}
			else alert ('Ошибка!');
			$('#loader').fadeOut ();
		});				 
	}); 
	
	<? endif ?>
	
	/*функция для создания html по обрезанию фотки*/
	function createImgSelection ($t)
	{
		if($t.html()=='')
			$t.html('<img src="picture/down.png" style="cursor:pointer;" width="48" height="48" id="skrylnik" title="Скрыть/показать основное фото" alt="Скрыть/показать основное фото"> <a href="?id=<?=$id?>&id_allblocks=<?=$id_allblocks?>&rotate=1" title="Повернуть основное изображение" id="rotate" ><img src="picture/rotate.png" alt="Повернуть основное изображение"></a> <a href="#" id="savemini" style="display:none;"><img src="picture/save.png" width="30" height="30" alt="Сохранить изменения"></a><img src="picture/loader.gif" width="30" height="30" alt="Сохранение изменений" id="loader" style="display:none;"><br><br><div id="big"><img id="duck" src="/foto/'+$t.attr('picname')+'" /><br><br></div><div id="tmp"><p>Миниатюра</p><img src="../foto/'+nearest($t.attr('picname2'), deffoto)+'" >');
		
		areaSelect = $('#duck').imgAreaSelect({
			aspectRatio: ramki[0]+':'+ramki[1],
			handles: 'true',	
			instance: true,
			show: true,
			x1: 0, 
			y1: 0,
			x2: ramki[0],
			y2: ramki[1],
			minHeight: ramki[1],
			minWidth: ramki[0],
			onSelectEnd: function (img, selection) {
				massiv = {
				w: selection.width,
				h: selection.height,
				x: selection.x1,
				y: selection.y1,
				i: img.src,
			};
				<? if ($USER->glb_access==WRITE_ACCESS): ?>
					$('#savemini').fadeIn ();
				<? endif ?>
				}
		});
	}
	
	
});
</script>
</body>
</html>