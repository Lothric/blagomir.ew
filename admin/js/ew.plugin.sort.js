// JavaScript Document
(function( $ ) {
jQuery.fn.ewSorter = function(options) {

	 $dom = this;
	 var parent = $dom.parent();
	 var def = '';
	
	//настройки по-умолчанию
      var settings = $.extend( {
      table: 'pages',//таблица бд
	  pole: 'id',//по какому полю берем id
	  item_selector: 'li', //идентификатор для элемента, который будет подвержен drag'n'drop
	  ajax: 'ajax/resortcategory.php',
	  button_template: '<a href="#" id="savesort" style="display:none;" title="сохранить сортировку"><img src="picture/save.png" width="30" height="30" alt="Сохранить изменения"></a>',
	  button_block: $dom,
	  loader_template: '<img src="picture/loader.gif" width="30" height="30" alt="Сохранение изменений" id="loader" style="display:none;">',//блок loader
	  button_block_template:'<div style="padding-bottom:15px;">#button_template# #loader_template#</div>',
	  save_sort_selector: '#savesort',
	  loader_selector: '#loader',
	  callback: function ($t){}
    }, options);
	//----------------------

	//внедрение первичного html
	settings.button_block.before (writeTemplate(settings, settings.button_block_template));
	
	
	$dom.sortable({
			placeholder: "ui-state-highlight",
			change: function(event, ui) { $(settings.save_sort_selector).fadeIn ();  }
		});
	$dom.disableSelection();
	
	//alert(settings.save_sort_selector);
	// сохранение результатов сортировки
	$('div '+settings.save_sort_selector).live('click', function (e){
		e.preventDefault();
			$(this).hide ();
			
			$(settings.loader_selector).fadeIn ();
			
			var massiv= new Object(); var i=0;
			$( settings.item_selector ).each (function (){
				massiv[i]={
					id : $(this).attr(settings.pole)
						};
				i++;
				});
				
				//подключаем AJAX и передаем в него массив
				var msg=massiv;
				 $.post (settings.ajax, {data : msg, table : settings.table}, function (otvet) {
					if (otvet==0 || otvet==null)
						alert ('Ошибка!');
					else 
					{
						if (otvet!=1)
							alert ("Ошибка! Информация НЕ обновлена!");
					}
						$(settings.loader_selector).fadeOut ();
						settings.callback($dom);
					}); 	
	});
	
	return $dom;
		
};
})(jQuery);