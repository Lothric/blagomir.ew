// JavaScript Document
(function( $ ) {
jQuery.fn.ewDnDFile = function(options) { //плагин drag'n'drop загрузки файлов
	$dom = this;
	//настройки по-умолчанию
	var settings = $.extend( {
		folder: 'docs/', //папка загрузки относительно корня веб-сайта
		table: 'files',//таблица бд
		colname: 'picname', //столбец с именем файла
		colext: 'filetype', //столбец с расширением файла
		stupid: 'id',//по какому полю берем id
		id: $(this).attr('pid'), // id файла
		imgres: [1200, 1200, 80], //как жать jpg
		maxsize: 57428800, //макс размер файла 50мб
		changename: true, //можно ли менять имя файла
		ajaxquery: 'ajax/fileupload.php', //куда слать ajax
		changeext: '', //меняем значение расширения
		autoview: true, //если true то картинка автоматически выведется под блоком при закгрузке, иначе все отдаем на откуп callback
		callback: function (dom, file, s){} //что делать после
	}, options);
	//----------------------
	
	$dom.after('<form id="dndform" method="post" enctype="multipart/form-data"><form>');
	$dom.appendTo($('#dndform'));
	
	var basechangename = settings.changename, //сохраняем значение требуется ли менять имя файла
		basecaption = "Перетащите сюда файл для загрузки или<br />нажмите для выбора файла"; // начальный текст
	//внедрение первичного html и стиля
	$dom.css({
		"color": "#999",
		"font-size": "18px",
		"text-align": "center",
		"padding": "50px 0",
		"margin-bottom": "20px",
		"background": "#f5f5f5",
		"border": "1px dashed #999",
		"width": "100%",
		"min-height": "30px"
	});
	$dom2 = $dom.clone().insertAfter($dom); // клонируем основной блок, чтобы в него выводить информацию
	$dom2.hide();
	$dom.html(basecaption); // вставляем начальный текст
	$dom.parent().append('<input type="file" name="uploadfile" style="display: none;">')
	var filechoosersel = $("input[name=uploadfile]"),
		filechooser = filechoosersel[0],
		filename;

		  
	function uploadFile(file) { // функция загрузки файла
		if (file.size > settings.maxsize) {
			$dom.html("Ошибка! Максимальный размер файла " + settings.maxsize/1048576 + " МБ");
			return false;
		}
		$dom.hide(); //скрываем блок, чтобы не работала повторная загрузка
		$dom2.show(); //сюда будем выводить инфу
		if(!settings.changename) //если нельзя менять имя файла или уже сменили
		{
			settings.changename = basechangename;
			$('#previewfile').hide();
			$dom2.html("Загрузка файла \""+ filename +"\"...");
			var fileext = filename.substr((filename.lastIndexOf('.')+1));
			fd = new FormData(($dom.parent())[0]); //собираем данные формы для отправки
			fd.append("filename", filename); //имя файла
			fd.append("folder", settings.folder); //папка
			fd.append("id", settings.id); //id файла
			fd.append("stupid", settings.stupid); //имя столбца с id
			fd.append("table", settings.table); //таблица бд
			fd.append("colname", settings.colname);
			fd.append("colext", settings.colext);
			fd.append("imgres", settings.imgres);
			
			var xhr = new XMLHttpRequest(); // создаем новый запрос
			xhr.open('POST', settings.ajaxquery, true);
			xhr.upload.onprogress = function(e) { // прогресс загрузки
				if (e.lengthComputable) {
					var percentComplete = parseInt((e.loaded / e.total) * 100);
					$dom2.html("Загрузка файла \""+ filename +"\" (" + percentComplete + "%)...");
				}
			};
			xhr.onload = function() { // по завершению загрузки
				if (this.status == 200) {
					var resp = this.response;
					resp = resp.trim(); //режем пробел в конце строки
					if((resp.substr((resp.lastIndexOf('.')+1)) == fileext) || (fileext == filename)) //если получили имя файла
					{
						$('#previewfile').remove();
						$dom2.hide();
						$dom.html("Файл \""+ resp +"\" успешно загружен!");
						$dom.show();
						if(settings.changeext != '')
							if(fileext == filename)
								settings.changeext.html('');
							else
								settings.changeext.html(fileext);
						
						if (settings.autoview)
						{
							if($.inArray(fileext,["gif","jpg","jpeg","bmp","png"]) != -1) // если картинка, то выводим её как кнопку удаления
								$dom2.after('<a id="previewfile" title="Удалить!"  href ="red_documents.php?id=' + settings.id + '&id_foto=' + settings.id + '"  ><img src="/'+ settings.folder + resp + '" class="preview"  /></a>');
							else // иначе просто выводим иконку удаления
								$dom2.after('<a id="previewfile" title="Удалить!"  href ="red_documents.php?id=' + settings.id + '&id_foto=' + settings.id + '"  ><img src="./images/closebox.png" class="preview"  /></a>');
						}
						
							settings.callback($dom, resp, settings);
					}
					else //иначе выводим полученное сообщение
					{
						$('#previewfile').show();
						$dom2.hide();
						$dom.html(resp);
						$dom.show();
					}
				}
				else {
					$('#previewfile').show();
					$dom2.hide();
					$dom.html("Ошибка! Файл не загружен. Проверьте ваше интернет-соединение.");
					$dom.show();
				}
			};
			xhr.send(fd); // отправляем запрос с данными формы
			/* $.ajax({
				url: settings.ajaxquery, 
				type: 'POST',
				data: fd, // The form with the file inputs.
				processData: false  // Using FormData, no need to process data.
			}).done(function(result){
				if(result == 1)
					$dom2.html("Файл \""+ filename +"\" успешно загружен!");
				else
					$dom2.html(result);
			}).fail(function(){
				$dom2.html("Ошибка! Файл не загружен. Проверьте ваше интернет-соединение.");
			}); */
		}
		else // если можно сменить имя файла
		{
			renameFile(file);
		}
	}

	function renameFile(file) { //предлагаем переименовать файл
		$dom2.html('Введите имя: <input type="text" name="filename" value="'+ filename +'" /> <button>OK</button>');
		$dom2.find("button").click(function() {
			filename = $dom2.find("input[name=filename]").val();
			if(filename != "")
			{
				settings.changename = false;
				filename = translit(filename); // транслитеруем имя файла
				uploadFile(file);
			}
		});
	}

	$dom[0].ondragover = function() {
		$dom.css({
			"border": "1px solid #999"
		});
		$dom.html("Отпустите файл для начала загрузки");
		return false;
	};

	$dom[0].ondragleave = function() {
		$dom.css({
			"border": "1px dashed #999"
		});
		$dom.html(basecaption);
		return false;
	};

	$dom[0].ondrop = function(e) { // если закинули файл через dnd
		e.preventDefault();
		filechoosersel.val('');
		filechooser.files = e.dataTransfer.files;
		var file = filechooser.files[0];
		filename = translit(file.name); // транслитеруем имя файла
		uploadFile(file);
	};

	$dom.click(function(e) { // выбираем файл вручную
		e.preventDefault();
		filechoosersel.val('');
		filechooser.click();
	});

	$(filechooser).change(function() { // если выбрали файл вручную
		var file = filechooser.files[0];
		filename = translit(file.name); // транслитеруем имя файла
		uploadFile(file);
	});
};
})(jQuery);


// Функция транслитерации
function translit(text) {
	// Символ, на который будут заменяться все спецсимволы
	var space = '-';
	// Берем значение из нужного поля и переводим в нижний регистр
	text = text.toLowerCase();
	// Массив для транслитерации
	var transl = {
	'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh',
	'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
	'о': 'o', 'п': 'p', 'р': 'r','с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
	'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh','ъ': space, 'ы': 'y', 'ь': space, 'э': 'e', 'ю': 'yu', 'я': 'ya',
	' ': space, '`': space, '~': space, '!': space, '@': space,
	'#': space, '$': space, '%': space, '^': space, '&': space, '*': space,
	'\=': space, '+': space, '[': space,
	']': space, '\\': space, '|': space, '/': space, ',': space,
	'{': space, '}': space, '\'': space, '"': space, ';': space, ':': space,
	'?': space, '<': space, '>': space, '№':space
	}
	var result = '';
	var curent_sim = '';
	for(i=0; i < text.length; i++) {
		// Если символ найден в массиве то меняем его
		if(transl[text[i]] != undefined) {
			if(curent_sim != transl[text[i]] || curent_sim != space){
				result += transl[text[i]];
				curent_sim = transl[text[i]];
			}
		}
		// Если нет, то оставляем так как есть
		else {
			result += text[i];
			curent_sim = text[i];
		}
	}
	result = result.trim();
	// Выводим результат
	return result;
}
