/*функция инициализирует обработчик события использования чекбокса*/
function js_changeFlag()
{
		//отправка данных
		$('.changeFlag').change (function ()
		{
			var newval = 0;
			if ($(this).attr('checked')!=null)
			newval = 1;

			var loader = $(this).parent().find('.changeFlagLoader');
			loader.fadeIn();
			var newtext = $(this).val();
			var nexttext = loader.parent().find('.changeFlagNexttext');

			$(this).val(nexttext.html());

			ew_addon ({
				pid : $(this).attr('pid'),
				table : $(this).attr('table'),
				stupid : "id",
				pol : $(this).attr('pol'),
				info: newval
			}, function (result){
											if (result!=1)
											alert("Ошибка");
											loader.fadeOut();
											nexttext.html(newtext);
											} );



		});
		//---------------
}

/*функция срабатывания всплывающего меню разметки длоя простого блока редактирования*/
function admin (id)
{
	var mouseX = 0;
	var mouseY = 0;
	var flag=false;
	$(id).mousemove(function(e) {
	// получаем координаты курсора мыши
	mouseX = e.pageX;
	mouseY = e.pageY;
	});
	$(id).mousedown(function() {
	$("#menu").fadeOut("1000");
	});
	$(id).keypress(function() {
	$("#menu").fadeOut("1000");
	});
	$(id).select(function() {
	// получаем координаты мыши в показанном меню
	$("#menu").css("top", mouseY+50).css("left", mouseX + 100).fadeIn("1000");
	flag=true;
	});
	$("#bold").click(function(e) {
		e.preventDefault ();
		if (flag)
		wrapText("<b>", "</b>", id);
		$("#menu").fadeOut("1000");flag=false;
	});
	$("#br").click(function(e) {
		e.preventDefault ();
		if (flag)
		wrapText("<br>", "", id);
		$("#menu").fadeOut("1000");flag=false;
	});
	$("#italic").click(function(e) {
		e.preventDefault ();
		if (flag)
		wrapText("<i>", "</i>", id);
		$("#menu").fadeOut("1000");flag=false;
	});
	$("#underline").click(function(e) {
		e.preventDefault ();
		if (flag)
		wrapText("<u>", "</u>", id);
		$("#menu").fadeOut("1000");flag=false;
	});
	$("#link").click(function(e) {
		e.preventDefault ();
		if (flag){
		var url = prompt("Введите URL", "http://");
		if (url != null)
		wrapText("<a href='" + url + "'>", "</a>", id);}
		$("#menu").fadeOut("1000");flag=false;
	});
}

function wrapText(startText, endText, id)
{
	// Получаем текст перед выделением
	var before = $(id).val().substring(0, $(id).caret().start);
	// Получаем текст после выделения
	var after = $(id).val().substring($(id).caret().end, $(id).val().length);
	// Объединяем текст перед выделением, измененное выделение и текст после выделения
	$(id).val(before + startText + $(id).caret().text + endText + after);
}

/*функция обработки событий с простым блоком редактирования (ПБР)*/
function js_textRedaktor (id)
{
	var oldText, newText;

	$(id).hover(
		function()
		{
			$(this).addClass("editHover");
		},
		function()
		{
			$(this).removeClass("editHover");
		}
	);

	$(id).bind("click", replaceHTML);
	$(".btnSave").live("click",
		function(e)
		{
			e.preventDefault();
			var mess=$(this).siblings("form").children(".editBox");
			newText = mess.val();
			var loader = mess.parent().parent().prev('img');
			loader.fadeIn();
			//подключаем ajax
			ew_addon (
				{
				table : mess.attr('tab'),
				pid : mess.attr('pid'),
				pol : mess.attr ('pol'),
				stupid : mess.attr ('stupid'),
				info : newText
				},
				function (result){
					if (result!=1)
						alert (result);
					loader.hide();
				}
			);

			//------------------------------------
			$(this).parent()
			.html(newText)
			.removeClass("noPad")
			.bind("click", replaceHTML);
		}
	);
	$(".btnDiscard").live("click",
		function(e)
		{
			e.preventDefault();
			$(this).parent()
			.html(oldText)
			.removeClass("noPad")
			.bind("click", replaceHTML);
		}
	);
	function replaceHTML()
	{
		var pid=$(this).attr ('pid');
		var tab=$(this).attr ('tab');
		var pol=$(this).attr ('pol');
		var stupid=$(this).attr ('stupid');
		oldText = $(this).html();
		$(this).addClass("noPad")
		.html("")
		.html('<form><textarea class="editBox" stupid="'+stupid+'" pid="'+pid+'" tab="'+tab+'" pol="'+pol+'" cols="40" rows="6" id="info">' + oldText + "</textarea></form><a href=\"#\" class=\"btnSave\">Сохранить</a> <a href=\"#\" class=\"btnDiscard\">Отмена</a>")
		.unbind('click', replaceHTML);admin ('#info');
	}
}

/*функция обработки селекта и переноса им данных
id - css идентификатор селекта или группы селектов
*/
function selectChange(id, after, ajaxFunction, confirmBefore)
{
	if (after==null || after==undefined)
		after=function (result, id, t){};
	
	if (ajaxFunction==null || ajaxFunction==undefined)//функция обработки ajax по умолчанию
		ajaxFunction=function (p, id, t, loader, after){
			ew_addon (
						{
							pid : p.attr('pid'),
							table : p.attr('table'),
							stupid :p.attr('stupid'),
							pol: p.attr('pol'),
							info : t.attr('info')
						},
						function (result)
						{
							if (result!=1)
								alert ('Ошибка!');

							after(result, id, t);
								//document.location.href=after;

							loader.fadeOut(500);
						}
					);
		};

	$(id).change (function (){
			var loader = $(this).next('img');
			var t = $(this).find(':selected');
			var p = t.parent();
					loader.fadeIn(500);
					
					if (confirmBefore==null || confirmBefore==undefined || confirmBefore=='' || confirm(confirmBefore))
						ajaxFunction(p, id, t, loader, after);
					else {
						loader.hide();
						alert("Информация не сохранена");
					}

			});
}
//----------------------------------------------



/*функция для работы с сортировкой*/
function sorter_init ()
{
	$( "#sortable" ).sortable({
			placeholder: "ui-state-highlight",
			change: function(event, ui) { $('#savesort').fadeIn (); }
		});
	$( "#sortable" ).disableSelection();

	// сохранение результатов сортировки
	$('#savesort').click (function (e)
	{
			e.preventDefault ();
			$(this).hide ();$('#loader').fadeIn ();
		var massiv= new Object(); var i=0;
			$( "li" ).each (function (){
				massiv[i]={
					id : $(this).attr('id')
						};
				i++;
				});

				//подключаем AJAX и передаем в него массив
				var msg=massiv;
				 $.post ("ajax/resortcategory.php", {data : msg, table : $('#sortable').attr("mode")}, function (otvet) {
					if (otvet==0 || otvet==null)
						alert ('Ошибка!');
					else
					{
						if (otvet!=1)
							alert ("Ошибка! Информация НЕ обновлена!");
					}
						$('#loader').fadeOut ();
					});
			   //------------------------------------------
		});
}

/*функция инициализации контроля удаления*/
function delete_init (text, cl)
{
	if (cl==null || cl==undefined) 
		cl = '.delete'; 
	
	if (text==null || text==undefined)
		text = "Удалить?";

	$(cl).click (function (e){
			if (!confirm(text))
				e.preventDefault ();
	});
}

/*функция отправки данных на запись через ajax/addon.php*/
function ew_addon (options, callback)
{
	//настройки по-умолчанию
   var settings = $.extend( {
	  pid : 0,
	  table : 'pages',
	  stupid : "id",
	  pol : '',
	  info: 0,
	  url: 'ajax/addon.php'
    }, options);
	//----------------------

	if (settings.pid==0 || settings.pol=='')
		return callback(null);

	$.post ('ajax/addon.php',
		 {
			 pid: settings.pid,
			 table: settings.table,
			 stupid: settings.stupid,
			 pol: settings.pol,
			 info: settings.info
		 },
		 function (result)
		 {
			 callback(result);
		 }
    ).fail(function() { alert("Ошибка сохранения!"); });
	/*.done(function() { alert("second success"); })

	.always(function() { alert("finished"); })*/


}

/*полезная функция вывода всех элементов объекта*/
function allProperties(obj, objName){
    var result = "";
    for (var i in obj) //на каждом шаге обращаемся к свойствам объекта по индексу
        result += objName + "." + i + " = " + obj[i] + "<br />\n";
    return result;
}

/*функция заполнения шаблона данными ассоциативного массива
	 text* template - html-код шаблона со вставками вида #ТЭГ#
	 array* massiv - кортеж с атрибутами вида ТЭГ, которые и будут искаться в шаблоне
	 return вовзращает готовый html-код для вставки
	*/
function writeTemplate (massiv, template)
{
	for (var key in massiv) {
   		 var val = massiv [key];
    	template=template.replace(new RegExp('#'+key+'#','g'),val);
	}
	return template;
}

/*функция обработчик работы фильтров для списков
string ind - строка индентификатор для навешивания события
function callback - функция о том, что делать потом
string pole - название поля на которое влияет фильтр, по-сути это значение переменной для url
int tek - новое значение этого поля,
string clear_val - строка содержащая названия переменных, которые надо вычистить из url. например "id_pages, all_in" и т.п.
*/
function js_filtr (options)
{
	//настройки по-умолчанию
   var settings = $.extend( {
	  ind : '.filtr',
	  pole_name : 'name',
	  tek_name : "pid",
	  clear_val: '',
	  callback: function (new_url){history.pushState(null, null, new_url); location.reload();}
    }, options);
	//----------------------

	$(settings.ind).change(function () {
		//получаем данные
		var pole = $(this).attr(settings.pole_name),
			tek = $(this).find(':selected').attr(settings.tek_name),
			url_data = getUrlVar (),//получаем данные о текущих параметрах в url
			new_url=[];

		if (settings.clear_val!=null)//есть исключения
		url_data[pole]=tek; // переназначаем

		//формируем url
		for (var key in url_data)
		{
   		 	var val = url_data [key];

			if (settings.clear_val.indexOf(key)==-1)//если нет свопадений с исключениями
    			new_url.push(key+'='+val);
		}
		
		if(url_data['id']==undefined && $('body').attr('pid')!=undefined)
			new_url.push('id='+$('body').attr('pid'));
		
		new_url = '?'+new_url.join('&');

		settings.callback (new_url);

		return $(this);
	});
}

/*функция формирующая из текущего url массив с GET данными*/
function getUrlVar(){
    var urlVar = window.location.search; // получаем параметры из урла
    var arrayVar = {}; // массив для хранения переменных
    var valueAndKey = {}; // массив для временного хранения значения и имени переменной
    var resultArray = {}; // массив для хранения переменных
    arrayVar = (urlVar.substr(1)).split('&'); // разбираем урл на параметры

    for (i = 0; i < arrayVar.length; i ++) { // перебираем все переменные из урла
        valueAndKey = arrayVar[i].split('='); // пишем в массив имя переменной и ее значение
        resultArray[valueAndKey[0]] = valueAndKey[1]; // пишем в итоговый массив имя переменной и ее значение
		;
    }
    return resultArray; // возвращаем результат
}

/**
 * Вернет первую непустую строку из переданных аргументов, если все пустые, вернет пустую строку ''
 * @return string
 */
function nearest() {

  for (var arg in arguments)
  {
	var val = arguments[arg];
    if (val && val!='' && val!=null)
	return val;
  }
  return null;
}
