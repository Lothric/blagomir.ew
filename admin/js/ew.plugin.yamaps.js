// JavaScript Document
(function( $ ) {
jQuery.fn.ewYandMaps = function(options) {
	 $dom = this;
	 var def = '';
	//настройки по-умолчанию
     var settings = $.extend( { 
	  title: "Укажите местоположение объекта на карте:<br>", //заголовок
	  id: $dom.attr('pid'), // id страницы,
      table: 'pages',//таблица бд
	  stupid: 'id',//по какому полю сравниваем id
	  xpole: 'x',//в какое поле вносим новое значение
	  ypole: 'y',//в какое поле вносим новое значение
	  
	  xcoord: $('li[pol="x"]'),//ссылка на объект
	  ycoord: $('li[pol="y"]'),//ссылка на объект
	  city: '',//само значение
	  adr: $('li[pol="adr"]'),//ссылка на объект
	  xdef: 54.738139,
	  ydef: 55.978526,
	  
	  ajax: 'ajax/getkoord.php',
	  abutton: '#abutton',//название идентификатора
	  abutton_template: 'Или используйте <a href="#" id="abutton">автопоиск</a>: <img src="picture/loader.gif" width="20" height="20" alt="Сохранение изменений" style="display:none;"><br><br>',
	  
	  callback: function ($t){}
	  
    }, options);
	//----------------------
	
	if (settings.id=='undefined')
		return;
	
	//внедрение первичного html
	$dom.html ('<div id="ewmap" style="width: 100%; height: 400px; text-align:center; padding-bottom: 30px;"></div>');
	$dom.prepend(settings.title+settings.abutton_template);
	
	$loader = $dom.find('img');
	
	var x=settings.xcoord.text(),
		y = settings.ycoord.text();
	
	 ymaps.ready(init);
    var myMap;

    function init(){  
	
		if (x==0 || y==0)
			 myMap = new ymaps.Map ("ewmap", {
				center: [settings.xdef, settings.ydef], 
				zoom: 6});
		else
		  myMap = new ymaps.Map ("ewmap", {
            center: [x, y], 
            zoom: 15});
		
		myMap.controls.add('mapTools');
		myMap.controls.add('typeSelector');
		myMap.controls.add('zoomControl');
		myMap.controls.add('searchControl');
		
		if (x==0 || y==0)
			{
				x=ymaps.geolocation.latitude;
				y=ymaps.geolocation.longitude;
				//settings.title = ymaps.geolocation.city;
			}
		
		metka =  new ymaps.Placemark(
        [x, y],
        {
            balloonContentHeader: settings.title,
            balloonContentFooter: 'Местоположение объекта'
        }
    );
		
		myMap.geoObjects.add(metka);

 // Обработка события, возникающего при щелчке
            // левой кнопкой мыши в любой точке карты.
            // При возникновении такого события откроем балун.
            myMap.events.add('click', function (e) {
                if (!myMap.balloon.isOpen()) {
                    var coords = e.get('coordPosition');
                    myMap.balloon.open(coords, {
                        contentHeader: 'Объект находится здесь?',
                        contentBody: '<p>Координаты: ' + [
                                coords[0].toPrecision(6),
                                coords[1].toPrecision(6)
                            ].join(', ') + '</p>',
                        contentFooter: '<sup><a href="#" id="getkoord">Сохранить координаты</a></sup>'
                    });
					
					$('#getkoord').click(function (e) {
							e.preventDefault();
							//подключаем ajax
				
						savecoord ({pid : settings.id,
											x : coords[0].toPrecision(6),
											y : coords[1].toPrecision(6),
											stupid: settings.stupid,
											table: settings.table,
											ajax: settings.ajax,
											callback: function($d, result)
											{
												if (result!=1)
													alert ('Ошибка сохранения!');
												
												myMap.balloon.close(); //закрываем окошко
												metka.geometry.setCoordinates(coords); //переносим метку
												settings.xcoord.text(coords[0].toPrecision(6));
												settings.ycoord.text(coords[1].toPrecision(6));
											}
										});
						//------------------------------------
						
						
						});
					
					
                } else {
                    myMap.balloon.close();
                }
            });	
    }
	
	
	$(settings.abutton).live('click', function (e) {
		e.preventDefault (); 
		var adr;
		
		if (settings.city!=null)//то подразумевается, что город просто в адресе сразу храниться 
		adr += settings.city+', ';
		
		adr+=settings.adr.text();
		
		var myGeocoder = ymaps.geocode(adr);
myGeocoder.then(
    function (res) {
		if (res.geoObjects.get(0)!=null)
		{
			var koord=String (res.geoObjects.get(0).geometry.getCoordinates());
			koord = koord.split (',');
			savecoord ({
				pid : settings.id,
				x : koord[0],
				y : koord[1],
				table: settings.table,
				stupid: settings.stupid,
				ajax: settings.ajax,
				callback: function($d, result)
					{
						if (result!=1)
							alert ('Ошибка сохранения!');
							
						
						metka.geometry.setCoordinates(koord); //переносим метку
						settings.xcoord.text(koord[0]);
						settings.ycoord.text(koord[1]);
						
						myMap.setCenter([koord[0],koord[1]], 15);
					}
			});
		
		}
		else {
        alert('Не заполнен город и адрес объекта!');
		$loader.fadeOut();
   			 }
       
    },
    function (err) {
        alert('Не удалось найти координаты объекта!');
		$loader.fadeOut();
    }
);
		});

	/*функция сохранения координат в бд*/
	function savecoord (opt)
	{
		var ss = $.extend( { 
		  pid: 0,
		  x: 0,
		  y: 0,
		  ajax: 'ajax/getkoord.php',
		  table: 'pages',
		  stupid: 'id',
		  callback: function ($d, result){}
		}, opt);
		
		if (ss.x && ss.y)
		{
			$loader.fadeIn();
			//подключаем ajax
			$.post (ss.ajax, {
					pid :ss.pid,
					x : ss.x,
					y : ss.y,
				    stupid : ss.stupid,
					table: ss.table
					}, function (result){
						ss.callback($dom, result);
						$loader.fadeOut();
					});
			//-----------------
		}
	}	

};
})(jQuery);