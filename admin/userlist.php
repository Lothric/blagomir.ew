<?php require_once('../blocks/ewcore/bd.php'); 

require_once('blocks/ewadmincore/ew.admin.init.php');
$USER->dtrm_access_action('userlist');//проверка доступа к странице
if ($did=safeGetNumParam ($_GET,'did'))//удаление версии контента 
	ew_mysqli_query("delete FROM userlist WHERE id='".$did."'");
 ?>
<html>
<head>
<?php 
include_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>
<div align="center">
<h2 align="center">Пользователи панели управления сайтом</h2>

<a href="red_userlist.php" title="добавить нового пользователя"><img src="picture/add.png" width="40" height="40"></a>

</div>
<br>
<td align="center" valign="middle">
<ul id="sortable">
<?php 

$myquery = "SELECT id, user, isbanned, id_usertype FROM userlist WHERE id!=1 ORDER BY id asc";//

echo ew_pagesList ($kat, $myquery, NULL, [
	[//кнопка редактировать
		//'func'=> function (){return 1;},//анонимная функция проверяющая условия для отображения кнопки
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="red_userlist.php?id=#id#" title="перейти к редактированию"><img src="picture/ref.gif" width="20" height="20"/></a> '
	],
	
	[//подцвевтка по типу пользователя
		'param'=>'id_usertype',
		'func'=> function ($id_usertype){return ($id_usertype==1);}, //забаненный пользователь?
		'newclass'=>'browntext',
		'bg_title'=>'Администратор'
	],
	
	[//забаненый?
		//'func'=> function (){return 1;},//анонимная функция проверяющая условия для отображения кнопки
		'param'=>'isbanned',
		'func'=> function ($isbanned){return $isbanned;}, //забаненный пользователь?
		'newclass'=>'orangebg',
		'bg_title'=>'Пользователь заблокирован'
	],
	
	[//кнопка удаления
		'param'=>'id',
		'func'=> function ($id){return $id!=1;},//анонимная функция проверяющая условия для отображения кнопки
		'newbutton'=>' <a href="?did=#id#" class="delete" title="удалить"><img src="picture/del.gif" width="20" height="20"/></a> '
	]
], '<li class="ui-state-default #newclass#" id="#id#"  title="#bg_title#">
<span class="opisanie">#user#</span>#newbutton#</li>');
?>
      </ul>   
</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript" src="js/ew.plugin.sort.js"></script>
<script type="text/javascript">
$(document).ready(function() {		
	delete_init ('Удалить тип контента? При это удаляться все версии контента для страниц по данному типу');
	
	/*$('#sortable').ewSorter(
		{
			table: 'allblocks'//таблица бд
		}
	);*/
	
	//подтверждение на создание копий
	$('.addcontent').click (function (e){
			if (!confirm("После этого действия для всех страниц сайта будут созданы версии контента с этим типом. Продолжить?"))
				e.preventDefault ();
	});	
});
</script>
</body>
</html>