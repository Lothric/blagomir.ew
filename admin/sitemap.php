<?php require_once('../blocks/ewcore/bd.php'); 
require_once('blocks/ewadmincore/ew.admin.init.php');
$USER->dtrm_access_action('sitemap');//проверка доступа к странице
?>
<html>
<?php include_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>

<td align="center" valign="middle">
<div align="center"><h2>Генерация sitemap.xml</h2></div><br>
<br> 
<div class="block">
<p><a href="/sitemap.xml" target="_blank">Скачать файл</a></p>

<form action="" method="post"> 
	<input type="hidden" value="1" name="new">
	<button type="submit">Сгенерировать sitemap</button>
</form>
<?php 
if ($_POST['new']==1)
{
	$file_name = 'sitemap.xml';
	$template = '<url>
		<loc>%s</loc>
		<lastmod>%s+01:00</lastmod>
		<priority>%s</priority>
	</url>
	';
	$content = '';
	$dop_content = '';
	$kol_pages = 0;
	$kol_hotels = 0;
	$kol_tours = 0;
	/*
	$result = ew_mysqli_query("SELECT * FROM allblocks WHERE id=1 LIMIT 1");
	$all = mysqli_fetch_assoc($result);
	*/

	//обработка страниц
	$resultp = ew_mysqli_query("SELECT p.id, pc.last_mod, p.link FROM pages p INNER JOIN pcontent pc ON pc.id_pages=p.id WHERE p.visible = 1 AND p.hiddencontent = 0 AND p.cansearch=1 order by id asc");
	while($p = mysqli_fetch_assoc($resultp))
	{
		$kt = UrlManager::createUrl($p['id']);
		$p['link'] = (substr_count ($kt, HOST_PROTOCOL)) ? $kt : HOST_PROTOCOL.HOST_NAME.$kt;
		$p['stamp'] = date('Y-m-d', strtotime ($p['last_mod'])).'T'.date('H:i:s', strtotime ($p['last_mod']));
		$p['prior'] = 1.0 - 0.2*(substr_count($p['link'], '/')-3);
		if ($p['prior']<0.2) $p['prior'] = 0.2;
		if ($p['prior']>1) $p['prior'] = 1.0;
		$dop_content .= sprintf ($template, $p['link'], $p['stamp'], $p['prior']);
		$kol_pages++;
	}

	$content = '<?xml version="1.0" encoding="UTF-8"?>
			<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
			'.$dop_content.'
			</urlset>';

	if(file_put_contents('../'.$file_name, $content))
		echo '<p>Файл успешно сгенерирован!</p>', '<p>Зафиксировано страниц: '.$kol_pages.'</p>', '<p>Зафиксировано туров: '.$kol_tours.'</p>', '<p>Зафиксировано отелей: '.$kol_hotels.'</p>';
	else echo '<p>Ошибка в генерации :( Попробуйте ещё раз!</p>';

}

?>


</div>


</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>

<script type="text/javascript">
$(document).ready(function() {		

});
</script>
</body>
</html>