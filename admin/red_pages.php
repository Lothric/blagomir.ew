<?php require_once('../blocks/ewcore/bd.php'); 
require_once('blocks/ewadmincore/ew.admin.init.php');
$doptable = 'pcontent';

$id_allblocks=safeGetNumParam ($_GET,'id_allblocks',1);

if ($id_copypages=safeGetNumParam($_GET, 'id_copypages', 0))//копирование товара
{
	$copy_goods=getRedInfo($id_copypages);//получаем информацию о копируемом товаре
	try {	
		beginTransaction();
		$id = copy_pages ($id_copypages, $id_allblocks);
		
		$USER->save_log("Копирован контент со страницы #$id_pages в страницу #$id");
		
		//throw new Exception ($id);
		
		commitTransaction();
	}catch(Exception $ex){
		rollbackTransaction ();
		exit ($ex->getMessage());
	}	
}

//создаем новую страницу как подстраницу $kat
if ($kat=safeGetNumParam($_GET, 'kat', 0))
{
	if($USER->access_action_itemslist('pages', $kat, $id_allblocks)!=WRITE_ACCESS)//проверяем УД для создаваемой страницы 
		exit ($USER::NO_ACCESS_ERROR);
	
	 //добавление новой страницы
	 	$newdata = [];//массив с новыми данными
		$parents = getPraInfo ('SELECT p.*, pc.banner FROM pages p INNER JOIN pcontent pc ON pc.id_pages=p.id WHERE p.id=%d AND pc.id_allblocks=%d LIMIT 1', $kat, $id_allblocks);//получаем иерархию предков страницы	
		
		$m=$parents[count($parents)-1];//непосредственный родитель страницы
		
		if ($m['cansplit']==1 && ($m['maxnode']>$m['teknode'] || $m['maxnode']==0))//разрешающие условия
		{
			$newdata['teknode']=$m['teknode']+1;
			$newdata['maxnode']=$m['maxnode'];
			
			
			//условия назначения атрибутов по родителю
			//??? в идеале бы это специфицировать на уровне бд
			switch ($kat)
			{	
				case ADDRESSES_PAGES:
					$newdata['canfoto'] = 1;
					$newdata['ishtmlinfo'] = 1;
					$newdata['hiddencontent'] = 1;
					$newdata['canmaps'] = 1;
				break;

				case FUND_ACTIVITY_PAGES:
				case OUR_TARGETS_PAGES:
				case SOCIAL_HELP_PAGES:
				case TEXTSLIDER_HISTORY_PAGES:
				case UNITS_PAGES:
				case CLUB_TARGETS_PAGES:
					$newdata['ishtmlinfo'] = 1;
					$newdata['hiddencontent'] = 1;
				break;

				case CLUB_PAGES:
				case FAQ_PAGES:
				case OUR_ACTIVITIES_PAGES:
				case MODAL_PAGES:
					$newdata['simplycontent'] = 1;
					$newdata['hiddencontent'] = 1;
				break;

				case HISTORY_POINTS_PAGES:
				case CALENDAR_PAGES:
					$newdata['hiddencontent'] = 1;
				break;

				case CLUB_PARTNERS_PAGES:
					$newdata['canfoto'] = 1;
				break;

				case REGIONAL_PAGES:
					$newdata['canfoto'] = 1;
					$newdata['hiddencontent'] = 1;
				break;

				case MAINSLIDER_PAGES:
					$newdata['ishtmlinfo'] = 1;
					$newdata['hiddencontent'] = 1;
					$newdata['canfoto'] = 1;
				break;

				case DOCS_PAGES:
					$newdata['ishtmlinfo'] = 1;
					$newdata['hiddencontent'] = 1;
					$newdata['canfoto'] = 1;
					$newdata['candocs'] = 1;
				break;

				case PEOPLE_PAGES:
					$newdata['ishashtags'] = 1;
					$newdata['canfoto'] = 1;
					$newdata['hiddencontent'] = 1;
				break;

				case STRUCTURE_FACES_PAGES:
					$newdata['hiddencontent'] = 1;
					$newdata['ishtmlinfo'] = 1;
					$newdata['canhashtags'] = 1;
				break;

				default: 
					$newdata['canfoto'] = 1;
					$newdata['candocs'] = 1;
				break;
			}
			
			if(count($parents)>1)//если уровень иерархии больше 1, т.е. есть дедушки и т.п.
			{
				// if ($parents[0]['id'] == PRODUCTS_PAGES) {
				// 	$newdata['canfoto'] = 0;
				// 	$newdata['candocs'] = 0;
				// 	$newdata['simplycontent'] = 1;
				// 	$newdata['hiddencontent'] = 1;
				// }
			}
			
			$newdata['id_pages']=$kat;
			$newdata['data']=date ("Y-m-d");
			//??? тут бы  еще npp инкрементировать
			
			if (!$id = insertRedInfo($newdata))
				exit ("Ошибка добавления новой страницы!");
			
			//добавление контента для страницы
			ew_mysqli_query("INSERT INTO pcontent (id_pages, id_allblocks) values ('".$id."', '".$id_allblocks."')");
			
			//??? и добавление расширяющих подмножеств
			if(isset ($m) and $m['id_pages']==CATALOG_PAGES)//для товаров
				ew_mysqli_query("insert into goods (id) value (".$id.')') or die (ew_mysqli_error());
			
			$USER->save_log("Добавление новой страницы ".$id." и контента для версии ".$id_allblocks);
		}
		else exit ("Ошибка! Превышен уровень иерархии!");
		
}

if (empty($id) && !($id=safeGetNumParam($_GET, 'id', 0)))//если есть id страницы
	exit ("<p>Ошибка! Нет информации о странице!</p><p><a href='index.php'>Вернуться на главную страницу админки</a></p>");//стоп при не обнаружении дданных

$USER->access_action('red_pages', $id, $id_allblocks, true);//определяем глобальный УД, т.е. УД к самой странице

//создание нового контента
if ($content_create=safeGetNumParam ($_GET,'content_create'))
{
	if ($USER->glb_access==WRITE_ACCESS)
	{
		$id_allblocks = createContent($content_create, $id, "SELECT * FROM pcontent WHERE id_pages='%d' AND id_allblocks = '%d' limit 1", "INSERT INTO pcontent (id_pages, id_allblocks) values ('%d', '%d')");
		$USER->save_log("Добавление нового контента страницы ".$id.' - '.$id_allblocks);
	}
	else 
	{
		phpalert ($USER::CREATE_ACCESS_ERROR);
		$USER->save_log("Ошибка добавления нового контента страницы ".$id.' - '.$content_create);
	}
}

//удаление контента
if ($content_delete=safeGetNumParam ($_GET,'content_delete'))
{
	try{
		beginTransaction();
		$t = del_pcontent ($id, $content_delete, $USER);
		if(!$t[0]) throw new Exception($t[1]);
		$USER->save_log("Удаление контента страниц: ".$t[2]);
		commitTransaction();
	}
	catch (Exception $ex)  //d случае ошибки
	{
		rollbackTransaction();
		$USER->save_log("Ошибка удаления контента страниц: ".$ex->getMessage());
		phpalert ($ex->getMessage());
	}
}

$m3=getRedInfo($id);//получаем информацию о странице

if(!count ($m3))//если ничего нет, наверное все удалили
	exit ("<p>Страница была полностью удалена!</p><p><a href='index.php'>Вернуться на главную страницу админки</a></p>");

if(isset($m3['id_pages']))
{
	$parents = getPraInfo ('', $m3['id_pages'], $id_allblocks);//получаем иерархию предков страницы	
	$m=$parents[count($parents)-1];//непосредственный родитель страницы
}
//	$m=getRedInfo($m3['id_pages']);//родитель страницы;


if (!empty($id_allblocks))//догружаем контент
{
	$dopm=getRedInfo('SELECT pc.*, pc.id as pc_id FROM 
pcontent as pc WHERE pc.id_pages = "'.$id.'" AND pc.id_allblocks="'.$id_allblocks.'" limit 1');//получаем информацию о странице
	
	if ($dopm)
	$m3=array_merge($dopm, $m3 );
} 

echo $USER->glb_access;

if($isgoods = $m3['isgoods'])
{
	$goods=getRedInfo('SELECT * FROM goods WHERE goods.id = "'.$id.'" limit 1');
	$m3['goods'] = $goods;
}
	
?>
<html>
<head>
<?php include_once('blocks/upblock.php'); ?>
</head>
<body pid="<?= $m3['id']; ?>">
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>

<td align="center" valign="middle">
<div align="center"><h2><?= nearest ($m3['title'], "!Контент не создан!"); ?></h2></div>
<?php include_once('blocks/pages_links.php'); ?>
<br>
<br> 
<?php
if ($USER->glb_access==WRITE_ACCESS)
{
	if ($m3['candrop']==1)//если можно перемещать
	ew_select ("SELECT pc.title, pages.id FROM pages 
	INNER JOIN pcontent as pc ON pc.id_pages=pages.id
	where cansplit=1 AND (teknode<maxnode or maxnode=0) AND pc.id_allblocks='".$id_allblocks."' AND pages.id!='".$m3['id']."' order by pages.id, pc.title asc", $m3['id_pages'], $m3['id'], "Прикреплено к разделу", 'id', "pages", "id_pages", "id",  '<label>%s </label><br><select name="arhi" class="myselect2" pid="%d" table="%s" pol="%s" stupid="%s" >%s</select><img src="picture/loader.gif" width="20" height="20" style="display:none;"><br><br>');

	changeFlag('Страница включена?', $m3['visible'], 'pages', 'visible', $m3['id'], 'Выводится на сайте', 'Не отображается на сайте');
	
	changeFlag('Отображать в поиске?', $m3['cansearch'], 'pages', 'cansearch', $m3['id'], 'Отображается в результатах поиска', 'Не отображается в результатах поиска');
	 
	changeFlag('Скрыть контент самой страницы?',$m3['hiddencontent'], 'pages', 'hiddencontent', $m3['id'], 'При переходе на нее будет открываться первая подстраница или родительская страница');
	
	changeFlag('Простая страница?', $m3['simplycontent'], 'pages', 'simplycontent', $m3['id'], '', 'Будет отображаться только один ворд редактор');
	 
	if (!$m3['id_pages'])//для страниц верхнего уровня
	{
		changeFlag('Отображать в верхнем меню?',$m3['inupmenu'], 'pages', 'inupmenu', $m3['id']);
		changeFlag('Отображать в нижнем меню?',$m3['indownmenu'], 'pages', 'indownmenu', $m3['id']);
	}
}

if($USER->dtrm_check_pages('other_verse'))//проверка доступа к управлению версиями контента
{
	$verse = getSQLArr("SELECT allblocks.name as title, allblocks.id FROM allblocks order by npp asc"); //массив с типами версий контентов allblocks
	$verse = ($USER->isAdmin()) ? $verse : $USER->arr_filtr ($verse);//если мы админ, то не надо фильтровать перечень доступных версий контента
	
	if(count($verse)>1)
	{
		echo ew_filtr ('Версия контента: ', $verse, $id_allblocks, 'id_allblocks'); 

		if ($USER->glb_access==WRITE_ACCESS)
		{
			if(count($dopm))
			{
				echo '<p><a href="?content_delete='.$id_allblocks.'&&id='.$id.'&&id_allblocks='.$id_allblocks.'" class="content_delete">Удалить контент?</a></p>';
				
				ew_select($verse, $m3['id_pcinherit'], $m3['pc_id'], "Наследовать контент (если текущий не заполнен):", 'id', $doptable, 'id_pcinherit', 'id', '<label>%s </label><br><select name="arhi" class="myselect" pid="%d" table="%s" pol="%s" stupid="%s" ><option info="">Без наследования</option>%s</select><img src="picture/loader.gif" width="20" height="20" style="display:none;"><br><br>');
			}
			else 
			echo '<p><a href="?content_create='.$id_allblocks.'&&id='.$id.'&&id_allblocks='.$id_allblocks.'" class="content_create">Создать контент для этой версии?</a></p>';  //есть контент или нет
		}
	}
}

if($USER->dtrm_check_pages('inherit_pages') && $m3['caninherit'])//проверка доступа к управлению наследованием страницы
{
	ew_select("SELECT pc.title, pages.id FROM pages 
	INNER JOIN pcontent as pc ON pc.id_pages=pages.id
	where pages.visible=1 AND pc.id_allblocks='".$id_allblocks."' AND pages.id!='".$m3['id']."' order by pages.id, pc.title asc", $m3['id_inherit'], $m3['id'], "Использовать содержимое со страницы:", 'id', 'pages', 'id_inherit', 'id');
}


if($USER->glb_access==WRITE_ACCESS && $m3['cancopy'])//копирование страниц
echo '<a href="red_pages.php?id_copypages='.$id.'&id_allblocks='.$id_allblocks.'" title="создать копию страницы" target="_blank"><img src="picture/copy.png" width="20" height="20"/> Создать копию страницы</a>';

?>


<div class="block">
<ul>
<?php 

ew_textRedaktor ('Название:', $m3['pc_id'], 'title', $m3['title'], $doptable);

ew_textRedaktor ('Отдельный заголовок вкладки браузера (если нужен для продвижения):', $m3['pc_id'], 'direct_title', $m3['direct_title'], $doptable);

ew_textRedaktor ('Ключевые слова страницы (для поисковиков):', $m3['pc_id'], 'keywords', $m3['keywords'], $doptable);

ew_textRedaktor ('Краткое описание страницы (для поисковиков):', $m3['pc_id'], 'description', $m3['description'], $doptable);

ew_textRedaktor ('Дополнительная информация:', $m3['pc_id'], 'preinfo', $m3['preinfo'], $doptable);

if(isset ($parents) && $parents[count($parents) - 1]['id']==PEOPLE_PAGES) {
	ew_textRedaktor ('Должность:', $m3['pc_id'], 'position', $m3['position'], $doptable);

	ew_textRedaktor ('Звание:', $m3['pc_id'], 'rank', $m3['rank'], $doptable);
}

ew_textRedaktor ('Дата создания (формат ДД.ММ.ГГГГ, например '.date('d.m.Y').'):', $m3['id'], 'data', dataforhuman($m3['data']));

ew_textRedaktor ('URL (красивый путь к странице вида '.$_SERVER['HTTP_HOST'].'/<b>about</b>) или абсолютная ссылка (вида http://'.$_SERVER['HTTP_HOST'].') для пересылки:', $m3['id'], 'url', $m3['url']);

ew_textRedaktor ('Исполняемый шаблон (<a href="glossary.php?kat='.PHP_TEMPLATE_GLOSSARY .'" target="_blank">см. примеры</a>). Если пусто, то используется шаблон текстовой страницы по-умолчанию (pages.php):', $m3['id'], 'link', $m3['link']);

if ($m3['id'] == CLUB_TARGETS_PAGES) {
	ew_textRedaktor ('CSS класс для фона:', $m3['id'], 'css_icon', $m3['css_icon']);
}

if ($m3['id'] == ABOUT_LINKS_PAGES) {
	ew_textRedaktor ('CSS класс:', $m3['id'], 'css_icon', $m3['css_icon']);
}

if(isset ($parents) && $parents[count($parents) - 1]['id']==ADDRESSES_PAGES) {
	ew_textRedaktor ('CSS позиционирование логотипа представительства:', $m3['id'], 'css_icon', $m3['css_icon']);
}

if(isset ($parents) && $parents[count($parents) - 1]['id']==REGIONAL_PAGES) {
	ew_textRedaktor ('Заголовок:', $m3['pc_id'], 'adr', $m3['adr'], $doptable);
	ew_textRedaktor ('Телефон:', $m3['pc_id'], 'position', $m3['position'], $doptable);
	ew_textRedaktor ('Email:', $m3['pc_id'], 'rank', $m3['rank'], $doptable);
	ew_textRedaktor ('CSS-позиционирование метки на карте:', $m3['id'], 'css_icon', $m3['css_icon']);
	ew_textRedaktor ('CSS-позиционирование логотипа представительства:', $m3['id'], 'csspoint', $m3['csspoint']);
}

if(isset ($parents) && $parents[count($parents) - 1]['id']==HISTORY_POINTS_PAGES) {
	ew_textRedaktor ('CSS-позиционирование метки на графике:', $m3['id'], 'css_icon', $m3['css_icon']);
	ew_textRedaktor ('CSS-позиционирование всплывающей карточки:', $m3['id'], 'csspoint', $m3['csspoint']);
}

if(isset ($parents) && $parents[count($parents) - 1]['id']==UNITS_PAGES) {
	ew_textRedaktor ('CSS класс для звёздочки (_star):', $m3['id'], 'css_icon', $m3['css_icon']);
}

if(!$m3['simplycontent'])//если не включен режим простого контента
{
	ew_textRedaktor ('Максимальный уровень иерархии (Текущий: <b>'.$m3['teknode'].'</b>). Указывайте в соответствии с щаблонами страниц (<a href="glossary.php?kat='.PHP_TEMPLATE_GLOSSARY .'" target="_blank">см. примеры</a>)', $m3['id'], 'maxnode', $m3['maxnode']);

	if ($m3['cansplit']==1 && $m3['maxnode']>$m3['teknode'])
		ew_textRedaktor ('Исполняемый шаблон (<a href="glossary.php?kat='.PHP_TEMPLATE_GLOSSARY .'" target="_blank">см. примеры</a>) <strong>для подстраниц</strong>:', $m3['id'], 'childlink', $m3['childlink']);
}	
if ($m3['ishashtags'])
	ew_textRedaktor ('Собственный хэштег:', $m3['id'], 'name_hashtags', $m3['name_hashtags']);
	
if ($m3['canhashtags'])
	ew_textRedaktor ('Связи с другими хэштегами (перечень хэш-тегов других страниц):', $m3['id'], 'hashtags', $m3['hashtags']);

if($m3['simplycontent'])//если simplecontent
{
	ew_textRedaktor ('<strong>Дополнительный текст</strong>:', $m3['pc_id'], 'dopinfo', $m3['dopinfo'], $doptable);
	
	ew_textRedaktor ('<strong>Дополнительный текст 2</strong>:', $m3['pc_id'], 'banner', $m3['banner'], $doptable);

}
	
if ($m3['canmaps'])
{
	ew_textRedaktor ('Адрес:', $m3['pc_id'], 'adr', $m3['adr'], $doptable);

	if ($parents[0]['id'] != PROJECTS_PAGES) {
		ew_textRedaktor ('Х:', $m3['pc_id'], 'x', $m3['x'], $doptable);
		ew_textRedaktor ('Y:', $m3['pc_id'], 'y', $m3['y'], $doptable); 
	}
}
	
if ($m3['ishtmlinfo'] && !$m3['simplycontent'])//если надо вместо ворд редактора использовать упрощенный
{
	ew_textRedaktor ('<strong>Основной текст</strong>:', $m3['pc_id'], 'info', $m3['info'], $doptable);
	
	ew_textRedaktor ('<strong>Дополнительный текст</strong>:', $m3['pc_id'], 'dopinfo', $m3['dopinfo'], $doptable);
	
	ew_textRedaktor ('<strong>Дополнительный текст 2</strong>:', $m3['pc_id'], 'banner', $m3['banner'], $doptable);
}

?>
</ul>


</div>
<div>

	<?php if ($m3['canmaps'] && $parents[0]['id'] != PROJECTS_PAGES):?>
		<div id="maps" pid="<?= $m3['pc_id'] ?>"></div>  
	<?php endif; ?> 
  
 <?php 
//--------------------

if (!$m3['ishtmlinfo']  && !$m3['simplycontent'])//иначе используем word редакторы
{
	ew_wordRedaktor ('Основной текст', $m3['pc_id'], $m3['info'], 'info', $doptable);
 
 	ew_wordRedaktor ('Дополнительный текст (после серединного фото-слайдера)', $m3['pc_id'], $m3['dopinfo'], "dopinfo", $doptable);
	
	ew_wordRedaktor ('Баннер страницы в правой колонке', $m3['pc_id'], $m3['banner'], "banner", $doptable);
}
 
if($m3['simplycontent']) 
	ew_wordRedaktor ('Основной текст', $m3['pc_id'], $m3['info'], 'info', $doptable);
 ?> 

      </div>

</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>

<? if ($m3['canmaps']): ?>	
	<script src="https://api-maps.yandex.ru/2.0-stable/?apikey=<?= YANDEX_API_KEY ?>&load=package.standard&lang=ru-RU" type="text/javascript"></script>
	<script src="js/ew.plugin.yamaps.js" type="text/javascript"></script>
<? endif ?>	

<script type="text/javascript">
$(document).ready(function() {	
	<? if ($USER->glb_access==WRITE_ACCESS): ?>	
		js_textRedaktor ('.editable');
	<? endif ?>	
		
	selectChange ('.myselect');
	selectChange ('.myselect2', null, null, "Перенести страницу в другой раздел?");
	
	js_changeFlag();
	tinymce.init(universal_settings);
	
	if ($('#maps').attr('pid')!=undefined)
			$('#maps').ewYandMaps({
				table:'pcontent'
			}); 
	
	//работа фильтров
	js_filtr ({
		ind: '.filtr',
		clear_val: 'content_create, content_delete, kat',//исключать переменные из урл, если они там попадаются при работе фильтра
		}); 
	
	$('.content_create').click(function (e){
		if (!confirm("Создать контент для данной версии? Если Вы создадите контент, но не заполните его, то пользователи будут видеть на его месте тексты из версии по-умолчанию"))
			e.preventDefault();
		else history.pushState(null, null, '');
	});
	
	$('.content_delete').click(function (e){
		if (!confirm("Удалить контент для данной версии? Если контент будет удален, то в этой версии контента пользователь не увидет данной страницы. Ее там не будет!"))
			e.preventDefault();
		else history.pushState(null, null, '');
	});
});
</script>
</body>
</html>