<?php require_once('../blocks/ewcore/bd.php'); 
require_once('blocks/ewadmincore/ew.admin.init.php');

$USER->dtrm_access_action('glossary');//проверка доступа к странице

if (isset ($_GET['id']) && $_GET['id']!='')
{
	$id=$_GET['id']; 
	$result = ew_mysqli_query("SELECT * FROM glossary WHERE id='$id' LIMIT 1");
	$m3 = mysqli_fetch_assoc($result);
} 
else 
{
	if (isset ($_GET['kat'])&& $_GET['kat']!='')
	{
		$kat=$_GET['kat'];
		
		if ($kat==0)
		$kat="null";
		
		$resultcount = ew_mysqli_query("SELECT count(*) as kol FROM glossary WHERE id_glossary = $kat");
		$count = mysqli_fetch_assoc($resultcount);
		$count=safeGetParam($count, 'kol', 'INT',0);

		$aresult = ew_mysqli_query ("INSERT INTO glossary SET title='New', id_glossary = $kat, npp = ".($count+1));
		
		if (!$aresult) alertAndRedirect ("Ошибка добавления");
		else
		{
			$result = ew_mysqli_query("SELECT * FROM glossary WHERE title like 'New' ORDER BY id DESC LIMIT 1");
			$m3 = mysqli_fetch_assoc($result);
			
			$id=$m3['id']; 
		}
	}

else alertAndRedirect ("Ошибка");

}

$table= 'glossary';
?>

<html><head>
<?php include_once('blocks/upblock.php'); ?>
</head>

<body>

<table align="center">

<td>

<?php include_once('blocks/menu.php'); ?>

</td>

<td align="center" valign="middle">

<div align="center"><h2><?php echo $m3['title']; ?></h2></div>

<a href="red_glossary.php?id=<?php echo $m3['id']; ?>"  title="Редактор текстов"><img src="picture/ref.gif" width="64" height="64"></a>  


<a href="glossary.php?kat=<?php echo $m3['id']; ?>" title="К подкатегориям"><img src="picture/limit.png" width="64" height="64"></a> 

<a href="glossary.php?kat=<?php echo $m3['id_glossary']; ?>"  title="К уровню выше"><img src="picture/list.png" width="64" height="64"></a> 


<br>

<br> 

<?php
ew_select ("SELECT title, id FROM glossary where  id!='$id' order by id asc", $m3['id_glossary'], $m3['id'], "Прикреплено к разделу:", 'id', $table, 'id_glossary');//Прикреплено к разделу

ew_select ("SELECT pc.title, pages.id FROM pages INNER JOIN pcontent as pc ON pages.id=pc.id_pages order by pages.id, pc.title asc", $m3['id_pages'], $m3['id'], "Страница:", 'id', $table, 'id_pages');//принадлежность странице

ew_select ("SELECT name as title, id FROM allblocks order by id asc", $m3['id_allblocks'], $m3['id'], "Версия контента: ", 'id', $table, 'id_allblocks');////принадлежность версии контента

ew_select ("SELECT title, id FROM glossary where id!='$id' order by id asc", $m3['id_usl'], $m3['id'], "Условие: ", 'id', $table, 'id_usl');////условие
?>

<div class="block">
<ul>
<!-- <i>ID js-объекта: <strong>#event<?= $m3['id']; ?></strong></i><br> -->
<i>Вызов виджета:  <strong>{{wdgt<?= $m3['id']; ?>}}</strong></i><br>
<br>
<?php 
ew_textRedaktor ('Название:', $m3['id'], 'title', $m3['title'], $table);

ew_textRedaktor ('Числовое значение:', $m3['id'], 'val', $m3['val'], $table);

ew_textRedaktor ('Текст (без HTML):', $m3['id'], 'info', $m3['info'], $table);
?>
</ul>

</div>
<?php ew_wordRedaktor ('Основной текст', $m3['id'], $m3['info'], 'info', $table);?>

</td>

</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {		
	js_textRedaktor ('.editable');
	selectChange ('.myselect');
	tinymce.init(universal_settings);
});
</script>

</body>

</html>