<?php require_once('../blocks/ewcore/bd.php'); 
require_once('blocks/ewadmincore/ew.admin.init.php'); 

const DOCS_OR_FOTO=0;// интерфейс для работы с документами

$id_allblocks=safeGetNumParam ($_GET,'id_allblocks',1);//фильтрация по типу контента
$verse = getSQLArr("SELECT allblocks.name as title, allblocks.id FROM allblocks order by npp asc"); //массив с типами версий контентов allblocks
$verse = ($USER->isAdmin()) ? $verse : $USER->arr_filtr ($verse);//если мы админ, то не надо фильтровать перечень доступных версий контента

$id_pages=safeGetNumParam($_GET, 'id_pages', 1);

if ($id_pages!=1)
{
	$resultm = ew_mysqli_query("SELECT pages.*, pc.title, pc.id as pc_id FROM pages left JOIN pcontent as pc ON pc.id_pages=pages.id AND pc.id_allblocks='$id_allblocks' WHERE pages.id = '$id_pages' LIMIT 1");
	$m3 = mysqli_fetch_assoc($resultm);
}
else $m3['id']=1;//общий случай

$USER->access_action('pages', $m3['id'], $id_allblocks, true);//определяем глобальный УД, т.е. УД к самой странице

if ($id_del_file=safeGetNumParam($_GET, 'id_del_file', false))//запрос на удаление
{
	if ($USER->isAdmin())
	{
		delfiles ($id_del_file, false); //удаление файла вместе с записью
		$USER->save_log("Полное удаление файла ".$id_del);
	}
	else phpalert(EWAdminUser::DELETE_ACCESS_ERROR);
}
//удаление контента файла
if ($id_del_fc=safeGetNumParam ($_GET, 'id_del_fc'))
{
	if ($USER->glb_access==WRITE_ACCESS)
	{
		deleteContent ($id_allblocks, $id_del_fc);
		$USER->save_log("Удаление контента  ".$id_allblocks." файла ".$id_del_fc);
	}
	else phpalert(EWAdminUser::DELETE_ACCESS_ERROR);
}

if($type=safeGetNumParam($_GET, 'type'))
	$dopquery=' AND files.type='.$type;

echo $USER->glb_access;

$countContentVerse = count($verse);
?>
<html>
<head>
<title>Панель администратора</title>
<?php include_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>

<td align="center" valign="middle" width="400" >
<div align="center" style="width:500"><h2 align="center">Файлы <?php echo $m3['title']; ?></h2>

<?php 
if ($m3['id']!=1)
{
	include_once('blocks/pages_links.php'); 
	echo '<p>Здесь Вы можете загрузить документ. Ссылка на него автоматически отразится на странице!</p>';
}
?>

<p>
<? if ($USER->glb_access==WRITE_ACCESS): ?>
	<a href="red_documents.php?kat=<?= $m3['id']; ?>&id_allblocks=<?= $id_allblocks; ?>" title="добавить"><img src="picture/add.png" width="32" height="32"></a>
<? endif ?>
<span id="sort_button"></span>
</p>
<?= ew_filtr ('Версия контента: ', $verse, $id_allblocks, 'id_allblocks') ?>
<br>
<br>
<?= ew_filtr ('Фильтр по типу: ', getSQLArr("SELECT title, npp as id FROM glossary WHERE id_glossary='".DOCS_TYPE_GLOSSARY."'  order by npp asc"), $type, 'type', 'id', [
	'title'=>'Показывать все типы',
	'id'=>0
]) ?>
</div><br>
<br>
<ul id="sortable">
<?= ew_pagesList ($id_pages, "SELECT files.*, fc.title, (select count(*) FROM fcontent WHERE fcontent.id_files=files.id) as fc_count FROM files INNER JOIN fcontent fc ON fc.id_files=files.id WHERE files.id_pages='#kat#' AND fc.id_allblocks='".$id_allblocks."' AND files.docsorfoto='".DOCS_OR_FOTO."' ".$dopquery." order by #orderby#", 'npp asc, id desc', [
	[//кнопки
		//'func'=> function (){return 1;},//анонимная функция проверяющая условия для отображения кнопки
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="red_documents.php?id=#id#&id_allblocks='.$id_allblocks.'"><img src="picture/ref.gif" width="20" height="20"/></a>'
	],
	[//удалить контент картинки 
		'param'=>'fc_count',
		'func'=> function ($fc_count) use ($USER)//если не все версии контента были созданы для страницы подсвечиваем
			{
				return ($USER->glb_access==WRITE_ACCESS);
			},
		'newbutton'=>'<a href="documents.php?id_del_fc=#id#&id_pages=#id_pages#&id_allblocks='.$id_allblocks.'"  class="fc_delete"  title="Удалить контент файла"><img src="picture/del.gif" width="20" height="20"/></a>'
	],
	[//удалить полностью картинку 
		'param'=>'fc_count',
		'func'=> function ($fc_count) use ($USER)//если не все версии контента были созданы для страницы подсвечиваем
			{
				return ($USER->isAdmin());
			},
		'newbutton'=>'<a href="documents.php?id_del_file=#id#&id_pages=#id_pages#&id_allblocks='.$id_allblocks.'"  class="delete" title="Удалить полностью файл со всеми контентами"><img src="picture/del2.png" width="20" height="20"/></a>'
	],
	[//кподсветка 
		'param'=>'fc_count',
		'func'=> function ($fc_count) use ($countContentVerse)//если не все версии контента были созданы для страницы подсвечиваем
			{
				return ($fc_count==1 && $countContentVerse>1);
			},
		'newclass'=>'yellowbg',
		'bg_title'=>'Для данного файла создана только одна версия контента. Значит в каких то версиях он не будет отображаться'
	]
	
],
'<li class="ui-state-default #newclass#" id="#id#" title="#bg_title#">
<b>#title#</b>
#newbutton#
 <br><a href="../docs/#picname#" title="Скачать/просмотреть файл" target="_blank">../docs/#picname#</a>
 
 <a href="view_path.php?path=../docs/#picname#" target="_blank" title="Скопировать ссылку"><img src="picture/link.png" width="25" height="25" class="icons" ></a>
</li> '
);
?>
      </ul>   
</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript" src="js/ew.plugin.sort.js"></script>
<script type="text/javascript">
$(document).ready(function() {		
	delete_init ('Полностью удалить файл (включая все версии контента)?');
	
	delete_init ('Удалить контент файла (сам файл не будет удален с сервера, если для него создан хотя бы один контент)?', '.fc_delete');
	
	<? if ($USER->glb_access==WRITE_ACCESS): ?>
	$('#sortable').ewSorter({
      table: 'files',//таблица бд
	  button_block: $("#sort_button"),
	  button_block_template: '#button_template# #loader_template#'
    });
	<? endif ?>
	
	//работа фильтров
	js_filtr ({
		ind: '.filtr',
		clear_val: 'id_del_fc, id_del_file'
	}); 
});
</script>
</body>
</html>