<?php require_once('../blocks/ewcore/bd.php');
require_once('blocks/ewadmincore/ew.admin.init.php');

$table = 'allblocks';
if (!($id=safeGetNumParam($_GET, 'id', 0)))
{
	if (!$USER->isAdmin())//если это не админ
		exit ("Ошибка добавления новой версии контента! Нет прав доступа!");
	
	$startAll = getAdminEmail (" *, (SELECT count(*) FROM allblocks) as kol ");
	$startAll['id']+=$startAll['kol'];
	$startAll['name']="Новая версия";
	$startAll['domen']="#";
	unset ($startAll['kol']);
	$new_query = 'INSERT INTO allblocks SET '.createSetQuery($startAll);
	if (!ew_mysqli_query ($new_query) || ew_mysqli_error())
		exit("Ошибка первичного добавления: ".ew_mysqli_error());
	
	 //добавление нового типа контента
		if (!($id = ew_mysqli_insert_id()))
			exit ("Ошибка добавления новой версии контента!");
		else $USER->save_log("Добавление новой версии контента", $id);
}

$m3=getRedInfo($id, $table);

if (!count ($m3))
	exit ("Ошибка загрузки информации!");
/* if (!$m3=getRedInfo($id, 'allblocks'));//получаем информацию о странице
	 */
$USER->access_action('red_allblocks', $m3['id'], 0, true);
?>
<html>
<head>
<?php include_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>

<td align="center" valign="middle">
<div align="center"><h2><?= $m3['name']; ?></h2></div>
<?= pageslistnav($m3,[
	[//добавить подстраницу в раздел
		'param'=>'id',
		'func' => function ($id) use ($USER) {
			return ($USER->glb_access == WRITE_ACCESS);
		},
		'newbutton'=>' <a href="?" title="добавить новую версию контента"><img src="picture/add.png" width="50" height="50"></a> '
	],
	[//кнопка редактировать
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="?id=#id#" title="перейти к редактированию страницы #title#"><img src="picture/ref.gif" width="50" height="50"/></a>'
	],
	[
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="pages.php?allblocks=#id#" title="Смотреть страницы версии"><img src="picture/limit.png" width="50" height="50"></a>  '
	],
	[//кнопка вверх по списку
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="allblocks.php" title="Вернуться к списку версий контента"><img src="picture/list.png" width="50" height="50"></a> '
	]
]);

?>
<br>
<br>

<div class="block">
<ul>
<?php
ew_textRedaktor ('Название версии контента:', $m3['id'], 'name', $m3['name'], $table);

ew_textRedaktor ('Текст перед названием:', $m3['id'], 'pretitle', $m3['pretitle'], $table);

ew_textRedaktor ('Заголовок (во вкладке браузера):', $m3['id'], 'title', $m3['title'], $table);

ew_textRedaktor ('Ключевые слова (для всего сайта):', $m3['id'], 'keywords', $m3['keywords'], $table);

ew_textRedaktor ('Краткое описание (для всего сайта):', $m3['id'], 'description', $m3['description'], $table);

ew_textRedaktor ('Поддомен для версии контента:', $m3['id'], 'domen', $m3['domen'], $table);

ew_textRedaktor ('Названия месяцев:', $m3['id'], 'months', $m3['months'], $table);

//ew_textRedaktor ('Названия дней:', $m3['id'], 'day_name', $m3['day_name'], $table);

ew_textRedaktor ('Название типов хранения данных:', $m3['id'], 'filesizes', $m3['filesizes'], $table);

ew_textRedaktor ('Телефон:', $m3['id'], 'tel', $m3['tel'], $table);

ew_textRedaktor ('Адрес:', $m3['id'], 'adr', $m3['adr'], $table);

ew_textRedaktor ('Email:', $m3['id'], 'email', $m3['email'], $table);

ew_textRedaktor ('Текст в поле поиска:', $m3['id'], 'search', $m3['search'], $table);

ew_textRedaktor ('Памятная дата:', $m3['id'], 'remdate', $m3['remdate'], $table);

ew_textRedaktor ('Биография:', $m3['id'], 'biography', $m3['biography'], $table);

// ew_textRedaktor ('Email:', $m3['id'], 'info', $m3['info'], $table);

ew_textRedaktor ('Текст в футере (внизу слева):', $m3['id'], 'untertextleft', $m3['untertextleft'], $table);

// ew_textRedaktor ('Ссылка в футере (справа):', $m3['id'], 'ewlabel', $m3['ewlabel'], $table);

ew_textRedaktor ('Кнопка "Подробнее":', $m3['id'], 'more', $m3['more'], $table);

ew_textRedaktor ('Кнопка "Узнать больше":', $m3['id'], 'learnmore', $m3['learnmore'], $table);

ew_textRedaktor ('Кнопка "Показать на карте":', $m3['id'], 'showmap', $m3['showmap'], $table);

ew_textRedaktor ('Заголовок футера:', $m3['id'], 'footertitle', $m3['footertitle'], $table);

ew_textRedaktor ('Ссылка на YouTube:', $m3['id'], 'youtube', $m3['youtube'], $table);

ew_textRedaktor ('Ссылка на Facebook:', $m3['id'], 'facebook', $m3['facebook'], $table);

ew_textRedaktor ('Ссылка на VK:', $m3['id'], 'vk', $m3['vk'], $table);

ew_textRedaktor ('Текст соглашения в форме:', $m3['id'], 'agreement', $m3['agreement'], $table);

?>

</ul>
</div>
	<!-- <div>
		 <?php
         ew_wordRedaktor ('Форма', $m3['id'], $m3['form'], "form", $table);
         ?>
 	</div> -->
</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	<? if ($USER->glb_access==WRITE_ACCESS): ?>
	js_textRedaktor ('.editable');
	<? endif ?>
	
	tinymce.init(universal_settings);
});
</script>
</body>
</html>
