<?php require_once('../blocks/ewcore/bd.php'); 

require_once('blocks/ewadmincore/ew.admin.init.php');
$USER->dtrm_access_action('stat');//проверка доступа к странице
?><html>
<head>
<?php include_once('blocks/upblock.php'); 
  ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?></td>

<td align="center" valign="middle">
<h1>Статистика</h1><br>

<div class="block">
    <p>
    <strong>Общая информация</strong><br><br>
        <?php 
        $stat1 = ew_mysqli_query("SELECT obj.title, (select count(*) FROM obj as kind WHERE kind.id_obj=obj.id) as kol FROM obj WHERE id_obj is null");
        while ($s1 = mysqli_fetch_assoc($stat1))
        printf ("<em>%s</em>: %s<br>", $s1['title'], number_format(  $s1['kol'] ,  0  , '.'  ,  ' ' ));
        ?>
    </p>
    
    <p>
    <strong>Платежная информация</strong><br><br>
    	<?php 
		$mass2=array();
        $stat2 = ew_mysqli_query("SELECT count(*) as kol, sum(gold) as gold FROM payhistory
		UNION SELECT count(*) as kol, sum(gold) as gold FROM payhistory  WHERE data > LAST_DAY(CURDATE()) + INTERVAL 1 DAY - INTERVAL 1 MONTH
  AND data < DATE_ADD(LAST_DAY(CURDATE()), INTERVAL 1 DAY)

		");
       while( $s2 = mysqli_fetch_assoc($stat2))
	   $mass2[]=$s2;
       
	   printf ("<em>Количество транзакций (всего)</em>: %s<br>
		<em>Оборот средств (всего)</em>: %s<br>", number_format(  $mass2[0]['kol'] ,  0  , '.'  ,  ' ' ), number_format(  $mass2[0]['gold'] ,  2  , '.'  ,  ' ' ));
		
		printf ("<br><em>Количество транзакций (прошлый месяц)</em>: %s<br>
		<em>Оборот средств (прошлый месяц)</em>: %s<br>", number_format(  $mass2[1]['kol'] ,  0  , '.'  ,  ' ' ), number_format(  $mass2[1]['gold'] ,  2  , '.'  ,  ' ' ));
        ?>
    
    </p>
    
    <p>
    <strong>Персонализированная информация</strong><br><br>
    	<?php 
		$mass=array();
        $stat3 = ew_mysqli_query("
		SELECT count(*) as kol, sum(gold) as gold FROM people WHERE gold<0
		UNION SELECT count(*) as kol, sum(gold) as gold FROM people WHERE gold>0
		UNION SELECT min(gold) as kol, max(gold) as gold FROM people
		");
        while($s3 = mysqli_fetch_assoc($stat3))
		$mass[]=$s3;
		
        printf ("<em>Количество должников</em>: %s<br>
		<em>Общая сумма задолженности</em>: %s<br>
		", number_format(  $mass[0]['kol'] ,  0  , '.'  ,  ' ' ), number_format(  $mass[0]['gold'] ,  2  , '.'  ,  ' ' ));
		
		printf ("<br><em>Количество авансирующих клиентов</em>: %s<br>
		<em>Общая сумма авансов</em>: %s<br>
		", number_format(  $mass[1]['kol'] ,  0  , '.'  ,  ' ' ), number_format(  $mass[1]['gold'] ,  2  , '.'  ,  ' ' ));
		
		printf ("<br><em>Max задолженность</em>: %s<br>
		<em>Max аванс</em>: %s<br>
		", number_format(  $mass[2]['kol'] ,  2  , '.'  ,  ' ' ), number_format(  $mass[2]['gold'] ,  2  , '.'  ,  ' ' ));
        ?>
    
    </p>
    
</div>
</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>
</body>
</html>