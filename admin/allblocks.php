<?php require_once('../blocks/ewcore/bd.php');
require_once('blocks/ewadmincore/ew.admin.init.php');

$USER->access_action('allblocks');//определяем глобальный УД, т.е. УД к самой странице

if (($did=safeGetNumParam ($_GET,'did')) && $USER->isAdmin())//удаление версии контента
{
	ew_mysqli_multi_query("delete FROM allblocks WHERE id='".$did."'");
	$USER->save_log("Удаление версии контента ".$did);// логирование действия пользователя
}

//$USER->test_user_access();

if (($addcontent=safeGetNumParam ($_GET, 'addcontent')) && $USER->isAdmin())//добавление версий контента с соответствующим типом для всех страниц
{
	$count=["pages"=>0, "df"=>0];
	$opisanie = '';
	try {
		beginTransaction();

		//копирование контента страниц
		if(safeGetNumParam ($_GET, 'addfirstcontent'))//делаем контент из pretitle
		{
			$query1 = "INSERT INTO pcontent (title, id_pages, id_allblocks) SELECT pretitle, id, '".$addcontent."' FROM pages";
		}
		elseif(safeGetNumParam ($_GET, 'addcontent_dop'))//в зависимости от этого флага копировать только структуру или еще и данные?
		{//структура + данные
			$query1 = "INSERT INTO pcontent (title, banner, direct_title, data, keywords, description, info, dopinfo, adr, x, y, id_pages, id_allblocks) SELECT title, banner, direct_title, data, keywords, description, info, dopinfo, adr, x, y, id_pages, '".$addcontent."' FROM pcontent WHERE id_allblocks = 1";
			$query2 = "INSERT INTO fcontent (title, info, href, id_files, id_allblocks) SELECT title, info, href, id_files, '".$addcontent."' FROM fcontent WHERE id_allblocks = 1";
			$opisanie = 'структура + данные';
		}
		else //копироваться будет только структура
		{
			$query1 = "INSERT INTO pcontent (title, id_pages, id_allblocks) SELECT title, id_pages, '".$addcontent."' FROM pcontent WHERE id_allblocks = 1";
			$query2 = "INSERT INTO fcontent (title, info, href, id_files, id_allblocks) SELECT title, info, href, id_files, '".$addcontent."' FROM fcontent WHERE id_allblocks = 1";
			$opisanie = 'только структура';
		}


		ew_mysqli_query($query1);

		if (ew_mysqli_error())
			throw new Exception ("№1 " . ew_mysqli_error());

		$count["pages"]+=ew_mysqli_affected_rows();

		//копирование контента документов и фоток
		if(isset($query2))
			ew_mysqli_query($query2);

		if (ew_mysqli_error())
			throw new Exception ("№2 " . ew_mysqli_error());

		if(isset($query2))
			$count["df"] += ew_mysqli_affected_rows();
		else
			$count["df"] = 0;

		commitTransaction();

		$USER->save_log("Копирован контент на ".$count["pages"]." страниц(ы) и ".$count["df"]." документов и фотографий для версии контента ".$addcontent, $opisanie);

		phpalert("Копирован контент на ".$count["pages"]." страниц(ы) и ".$count["df"]." документов и фотографий");
	}
	catch (Exception $ex){
		rollbackTransaction ();
		phpalert("Ошибка транзакции" .$ex->getMessage());
		$USER->save_log("Ошибка транзацкии при попытке копирования контента версии из ".$addcontent.". ". $ex->getMessage(), $opisanie);
	}

}

 ?>
<html>
<head>
<?php
include_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>
<div align="center">
<h2 align="center">Версии контента страниц</h2>

	<? if ($USER->glb_access==WRITE_ACCESS): ?>
		<a href="red_allblocks.php" title="добавить тип контента"><img src="picture/add.png" width="40" height="40"></a>
	<? endif ?>

</div>
<br>
<td align="center" valign="middle"  >
<ul id="sortable">
<?php

$myquery = "SELECT allblocks.*, (SELECT count(*) FROM pcontent WHERE id_allblocks=allblocks.id) as pkol FROM allblocks ORDER BY id asc";//

echo ew_pagesList ($kat, $myquery, NULL, [
	[//кнопка редактировать
		//'func'=> function (){return 1;},//анонимная функция проверяющая условия для отображения кнопки
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="red_allblocks.php?id=#id#" title="перейти к редактированию"><img src="picture/ref.gif" width="20" height="20"/></a> '
	],

	[//кнопка иерархии
		'notfunc'=>true,//анонимная функция проверяющая условия для отображения кнопки
		'newbutton'=>' <a href="pages.php?id_allblocks=#id#" title="смотреть страницы для этого типа контента"><img src="picture/limit.png" width="20" height="20"/></a> '
	],

	[//если нет ни одной созданной страницы для данной версии контента
		'param'=>'pkol, ud',
		'func'=> function ($pkol, $ud) use ($USER) {
			 if (($ud==WRITE_ACCESS || $USER->isAdmin())) return !$pkol; //если УД на чтение или пользователь является админом
			 else return 0; //иначе убираем кнопку
		 },//анонимная функция проверяющая условия для отображения кнопки
		'newclass'=>'yellowbg',
		'bg_title'=>'Нет ни одной созданной страницы для данной версии контента',
		'newbutton'=>' <a href="?addcontent=#id#" class="addcontent" title="копировать структура из главной версии"><img src="picture/add_vers.png" width="20" height="20"/></a>
		<a href="?addcontent=#id#&addcontent_dop=1" class="addcontent_dop" title="копировать структуру и данные из главной версии"><img src="picture/copy.png" width="20" height="20"/></a>
		<a href="?addcontent=#id#&addfirstcontent=1" class="addcontent_dop" title="создать структуру и данные из pretitle"><img src="picture/copy.png" width="20" height="20"/></a>'
	],
	
	[//кнопка удаления
		'param'=>'id, ud',
		'func'=> function ($id, $ud) use ($USER) {
			if ($USER->isAdmin()) return !($id==1); //если удалить версию контента может только админ, ито исключая главную
 		  else return 0; //иначе убираем кнопку
		},//анонимная функция проверяющая условия для отображения кнопки
		'newbutton'=>' <a href="?did=#id#" class="delete" title="удалить"><img src="picture/del.gif" width="20" height="20"/></a> '
	],
	[//вообще отображение записи
		'param'=>'ud',
		'func'=> function ($ud) use ($USER) {
			return !($ud>=READ_ACCESS || $USER->isAdmin()); //если УД на чтение или пользователь является админом
		},//анонимная функция проверяющая условия для отображения кнопки
		'newclass'=>'access_close'
	]
], '<li class="ui-state-default #newclass#" id="#id#"  title="#bg_title#">
<span class="opisanie">#name#</span>#newbutton#</li>',
function ($p) use ($USER){//анонимная функция для создания в массиве информации о каждой конкретной страницы информации о УД к ней текущего пользователя. Этот параметр $p['ud'] можно использовать в фильтрах выше
	return $USER->access_action_itemslist('allblocks', $p['id']);
});
?>
      </ul>
</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript" src="js/ew.plugin.sort.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	delete_init ('Удалить тип контента? При это удаляться все версии контента для страниц по данному типу');

	/*$('#sortable').ewSorter(
		{
			table: 'allblocks'//таблица бд
		}
	);*/

	//подтверждение на создание копий
	$('.addcontent').click (function (e){
			if (!confirm("Копировать структуру из главной версии в данную версию контента?"))
				e.preventDefault ();
	});

	$('.addcontent_dop').click (function (e){
			if (!confirm("Копировать структуру и данные из главной версии в данную версию контента?"))
				e.preventDefault ();
	});
});
</script>
</body>
</html>
