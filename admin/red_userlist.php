<?php require_once('../blocks/ewcore/bd.php');
require_once('blocks/ewadmincore/ew.admin.init.php');
$USER->dtrm_access_action('userlist');//проверка доступа к странице
$table = 'userlist';
if (!$id=safeGetNumParam($_GET, 'id', 0))
{
	$firstpass = getpass();
	 //добавление нового типа контента
		if (!$id = insertRedInfo(['user'=>"New", 'firstpass'=> $firstpass, 'pass'=> sha1($firstpass)],$table))
			exit ("Ошибка добавления нового пользователя!");
}
$m3=getRedInfo($id, $table);

if (!count ($m3))
	exit ("Ошибка загрузки информации!");


$usertype = arrPersonality (getSQLArr ('SELECT * FROM usertype order by id asc'));


?>
<html>
<head>
<?php include_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>

<td align="center" valign="middle">
<div align="center"><h2><?= $m3['user']; ?></h2></div>
<?= pageslistnav($m3,[
	[//добавить подстраницу в раздел
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>' <a href="" title="добавить нового пользователя"><img src="picture/add.png" width="50" height="50"></a> '
	],
	[//кнопка редактировать
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="?id=#id#" title="перейти к редактированию #user#"><img src="picture/ref.gif" width="50" height="50"/></a>'
	],

	[//кнопка вверх по списку
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="userlist.php" title="Вернуться к списку пользователей"><img src="picture/list.png" width="50" height="50"></a> '
	]
]);

?>
<br>
<br>

<div class="block">
<?

	ew_select ($usertype, $m3['id_usertype'], $m3['id'], "Тип пользователя", 'id', 'userlist', 'id_usertype');


	//changeFlag('Сделать доступным как окно? <em><br>(в окне не будт отражаться фотослайдеры и привязанные документы, только текст)</em>',$m3['iswindows'], 'pages', 'iswindows', $m3['id'], 'Ссылка: <i>#wind'.$id.'</i> для класса <i>getwindow</i>');
	changeFlag('Заблокировать пользователя?',$m3['isbanned'], 'userlist', 'isbanned', $m3['id'], 'Пользователь забаннен');

?>
<ul>
<?php
ew_textRedaktor ('Логин:', $m3['id'], 'user', $m3['user'], $table);
?>
<p>Первичный сгенерированный пароль: <b><?= $m3['firstpass'] ?></b> </p>
<?
ew_textRedaktor ('Текущий пароль:', $m3['id'], 'pass', '', $table);

ew_textRedaktor ('Email:', $m3['id'], 'email', $m3['email'], $table);

?>
</ul>
<p><strong>Права доступа в соответствии с типом пользователя <?= $usertype[$m3['id_usertype']]['title'] ?>:</strong></p>
<?
//вывод прав на ключевые страницы унаследованные от типа пользователя
foreach ($usertype[$m3['id_usertype']] as $key => $t)
{
	if($key!='title' && $key!='id')
	{
		$ty = ($t) ? 'checked' : '';
		echo '<input name="chek" type="checkbox" disabled '.$ty.'><label>'.$key.'</label> ';
	}

}
if($m3['id_usertype']>1):
?>
<p>&nbsp;</p>
<p><strong>Назначить права доступа к версиям контента и страницам:</strong></p>
<p>Начните выбор с доступных версий контента</p>
<form name="accessCreate">
<ul class="accesstype">
	<?
		$all = getSQLArr ('SELECT id, name FROM allblocks order by id asc');
		$accesstype = getglossary(USERTYPE_GLOSSARY);//типы уровней доступа
		$ud_pages = ' <select><option value="0"></option>'.writeItemsTemplate($accesstype, '<option value="#npp#">#title#</option>').'</select>'; //шаблон селектов для страниц
		$ud_allblocks = ' <select>'.writeItemsTemplate($accesstype, '<option value="#npp#">#title#</option>').'</select>'; //шаблон селектов для версий контента
		$podpages_toggle = ' <a href="#" class="to_show_icon" title="Сернуть/развернуть подстраницы"></a> ';

		foreach ($all as $t)
		{
			$pages = getSQLArr ('SELECT p.id, pc.title, p.id_pages FROM pages p INNER JOIN pcontent pc ON pc.id_pages=p.id WHERE pc.id_allblocks="'.$t['id'].'"'); //получаем перечень всех страниц версии контента

			echo '<li type="allblocks" pid="'.$t['id'].'">'.$t['name'].$ud_allblocks.$podpages_toggle.getPodPages($pages, '', $ud_pages, $podpages_toggle).'</li>';
		}


	?>

</ul>
<button type="button">Сохранить права доступа</button>
<img src="picture/loader.gif" width="20" height="20" style="display:none;">
</form>
<? endif ?>
</div>

</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>

<script type="text/javascript">
$(document).ready(function() {
	js_textRedaktor ('.editable');
	selectChange ('.myselect', function (result, id, t){
					if(result==1) location.href='red_userlist.php?id='+$(id).attr('pid'); //обновление страницы из-за смены типа пользователя
				});
	js_changeFlag();

	//обработчик события клика по значку раскрытия/скрытия подстраниц
	$('ul.accesstype li a.to_show_icon').live('click', function (e){
		e.preventDefault();
		$(this).parent().toggleClass('active');
	});

	//изменения select
	$('ul.accesstype li select').change(function(){
		var $t = $(this),
			$parent = $t.parent().parent().siblings('select'),
			link = $t.next('a');

		switch (parseInt($t.val()))
		{
			case 0:
			case 1:
			//устанавливаем у всех детей УД = 0
				childrenRecomplete($t);

				if ($t.parent().hasClass('active'))//скрываем список подстраниц
					$t.next('a').click();
			break;

			case 2:
			case 3:
				parentRecomplete ($parent);

				if (!$t.parent().hasClass('active'))//открываем список подстраниц
					$t.next('a').click();
			break;
		}


	});

	$('form[name="accessCreate"] > button').click(function (){
		var str = {},
			$btn = $(this),
			$loader = $btn.next('img');
		$btn.hide(); $loader.show();
		$(this).parent().find('ul.accesstype > li').each(function (){//начинаем перебор версий контента
			var pg = {}; //заготовка для перечня страниц

			$(this).find('ul li').each (function (){//перебор информации о УД страниц
				pg[$(this).attr('pid')] = {
					id: $(this).attr('pid'),
					type: $(this).attr('type'),
					id_pages: $(this).attr('id_pages'),
					ud: $(this).children('select').val()
				};
			});

			str[$(this).attr('pid')] = {
				id: $(this).attr('pid'),
				type: $(this).attr('type'),
				ud: $(this).children('select').val(),
				pages: pg
			};
		});

		ew_addon ({
		  pid : '<?= $m3['id'] ?>',
		  table : 'userlist',
		  pol : 'myaccess',
		  info: JSON.stringify(str),
		}, function (){
			$loader.hide(); $btn.fadeIn();
		});

		//console.log(str);
	});

	tekUDView ();

	/*функция изменения селектов детей  при погружении*/
	function childrenRecomplete ($children)
	{
		$children.siblings('ul').find('option:selected').removeAttr('selected');
		$children.siblings('ul').find('option[value="0"]').attr('selected', 'selected');
	}

	/*функция изменения селектов родителей - всплытие*/
	function parentRecomplete ($parent)
	{
		if($parent!=undefined && $parent.val()<2)//есть родитель
		{
			$parent.find('option:selected').removeAttr('selected');
			$parent.find('option[value="2"]').attr('selected', 'selected');
			var $prev = $parent.parent().parent().siblings('select');
			if($prev!=undefined)//есть еще подстраницы
				parentRecomplete($prev);
		}
	}

	//функция для отображения текущего состояния дерева УД
	function tekUDView ()
	{
		var myaccess = '<?= $m3['myaccess'] ?>';
		if (myaccess!=='')
		{
			myaccess = JSON.parse (myaccess); //декодируем json
			console.log(myaccess);
			$('ul.accesstype > li').each(function (){//начинаем перебор версий контента
				var tek_alb = $(this).attr('pid');
				if (myaccess[tek_alb]!=undefined) //есть такая версия
				{
					$(this).find(' > select option:selected').removeAttr('selected');
					$(this).find(' > select option[value="'+myaccess[tek_alb].ud+'"]').attr('selected', 'selected');
				}
				
				$(this).find('ul li').each (function (){//перебор информации о УД страниц
					if (myaccess[tek_alb].pages[$(this).attr('pid')]!=undefined)
					{
						$(this).find(' > select option:selected').removeAttr('selected');
					
						$(this).find(' > select option[value="'+myaccess[tek_alb].pages[$(this).attr('pid')].ud+'"]').attr('selected', 'selected');
					}
				});
			});
		}
		else return;
	}
});
</script>
</body>
</html>
