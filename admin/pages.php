<?php require_once('../blocks/ewcore/bd.php'); 

require_once('blocks/ewadmincore/ew.admin.init.php');

$id_allblocks=safeGetNumParam ($_GET,'id_allblocks',1);//фильтрация по типу контента

if ($kat=safeGetNumParam($_GET, 'kat'))
{ 
	$resultkat = ew_mysqli_query("SELECT pages.*, pc.title, (SELECT doppc.title FROM pcontent as doppc WHERE doppc.id_pages=pages.id AND doppc.id_allblocks=1 limit 1) as doppc_title
	FROM pages left JOIN pcontent pc ON pc.id_pages=pages.id AND pc.id_allblocks='".$id_allblocks."'  WHERE pages.id='$kat'  LIMIT 1");
	$m3 = mysqli_fetch_assoc($resultkat);
}
$USER->access_action('pages', $kat, $id_allblocks, true);//определяем глобальный УД, т.е. УД к самой странице

$allverse = getSQLArr("SELECT allblocks.name as title, allblocks.id FROM allblocks order by npp asc"); //массив с типами версий контентов allblocks
$verse = ($USER->isAdmin()) ? $allverse : $USER->arr_filtr ($allverse);//если мы админ, то не надо фильтровать перечень доступных версий контента

if (isset($m3['doppc_title']) && $m3['doppc_title'])
	$m3['doppc_title'].='<br><em>Контент страницы для этой версии не создан!</em>';

$nkat = nearest (@$m3['title'], @$m3['doppc_title'], 'Основные разделы сайта');

if ($id=safeGetNumParam ($_GET,'id'))//полное удаление страницы по ключу
{
	if ($USER->isAdmin())
	{
		delpages($id);
		$USER->save_log("Полное удаление страницы: ".$id);
	}
	else 
	{
		phpalert(EWAdminUser::DELETE_ACCESS_ERROR);
		$USER->save_log("Ошибка полного удаления страницы: ".$id, EWAdminUser::DELETE_ACCESS_ERROR);
	}
}
	
if ($id=safeGetNumParam ($_GET,'id_pc'))//удаление контента версии страницы
{
	try{
		beginTransaction();
		$t = del_pcontent ($id, $id_allblocks, $USER);
		if(!$t[0]) throw new Exception($t[1]);
		$USER->save_log("Удаление контента страниц: ".$t[2]);
		commitTransaction();
	}
	catch (Exception $ex)  //d случае ошибки
	{
		rollbackTransaction();
		$USER->save_log("Ошибка удаления контента страниц: ".$ex->getMessage());
		phpalert ($ex->getMessage());
	}
}

if (($addcontentToId=safeGetNumParam ($_GET, 'addcontent')) && $USER->isAdmin())//добавление версий контента с соответствующим типом для всех страниц
{
	$flag_addDopContent = safeGetNumParam ($_GET, 'addcontent_dop');
	$kol_newContentByPages = 0;
	$pagesPerech = getPodstr ($addcontentToId);
	//echo json_encode($pagesPerech);
	
	$queryIdAllblocksPerech = ew_mysqli_query("SELECT id FROM allblocks WHERE id!='$id_allblocks'");//перебираем все версии контента кроме текущей
	while ($idAllblocksPerech = mysqli_fetch_assoc($queryIdAllblocksPerech))
	{
		foreach ($pagesPerech as $key => $t)
		$kol_newContentByPages += insNewPageConent($key, $idAllblocksPerech['id'], $id_allblocks, $flag_addDopContent, $USER);
	}
	phpalert ("Копирован контент " . $kol_newContentByPages);
	
}

switch($kat)//определяем сортировку
{
	case NEWS_PAGES:
	case ACTIVITY_PAGES:
	case EVENTS_PAGES:
	case CONF_PAGES:
		$orderby='pages.data desc, pages.npp asc, pages.id desc';
	break;

	default: $orderby='pages.npp, pages.id asc'; break;
}
echo $USER->glb_access;
?>
<html>
<head>
<?php 
include_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>
<div align="center">
<h2 align="center"><?= $nkat ?></h2>
<?php include_once('blocks/pages_links.php'); ?>
</div>
<br>
<td align="center" valign="middle"  >

<?= ew_filtr ('Версия контента: ', $verse, $id_allblocks, 'id_allblocks') ?>

<br>
<br>
<ul id="sortable">
<?php 
$dopquery = ", (select count(*) FROM pcontent WHERE pcontent.id_pages=pages.id) as pc_count";		
	
$myquery = ($kat) ? "SELECT pages.*, pc.title ".$dopquery.", pc.ontop, pc.id as id_pc FROM pages 
INNER JOIN pcontent as pc ON pc.id_pages=pages.id  
WHERE pages.id_pages = '#kat#' AND pc.id_allblocks='".$id_allblocks."' ORDER BY #orderby#" : "SELECT pages.*, pc.title ".$dopquery." FROM pages INNER JOIN pcontent as pc ON pc.id_pages=pages.id  
WHERE pages.id_pages is NULL AND pc.id_allblocks='".$id_allblocks."' ORDER BY #orderby#";//запрос корректируется в зависимости от значения $kat. Если в самом корневом уззле приходиться менять запрос на is NULL - такой вот костыль..

echo ew_pagesList ($kat, $myquery, $orderby, [
	[//кнопка редактировать
		//'func'=> function (){return 1;},//анонимная функция проверяющая условия для отображения кнопки
		'notfunc'=>true, //без проверки функцией
		'newbutton'=>'  <a href="red_pages.php?id=#id#&id_allblocks='.$id_allblocks.'" title="перейти к редактированию"><img src="picture/ref.gif" width="20" height="20"/></a>'
	],
	
	[//кнопка иерархии
		'param'=>'cansplit, maxnode, teknode',
		'func'=> function ($cansplit, $maxnode, $teknode){return ($cansplit==1 && ($maxnode>$teknode || $maxnode==0))?: 0;},//анонимная функция проверяющая условия для отображения кнопки
		'newbutton'=>' <a href="?kat=#id#&id_allblocks='.$id_allblocks.'" title="смотреть подстраницы"><img src="picture/limit.png" width="20" height="20"/></a> '
	],
	
	[//можно копировать
		'param'=>'ud, cancopy',
		'func'=> function ($ud, $cancopy) {
			 return ($ud==WRITE_ACCESS && $cancopy); //если есть доступ к странице
		 },//анонимная функция проверяющая условия для отображения кнопки
		'newbutton'=>' <a href="red_pages.php?id_copypages=#id#&id_allblocks='.$id_allblocks.'" target="_blank" title="создать копию страницы"><img src="picture/copy.png" width="20" height="20"/></a> '
	],

	[//кнопка удаления контента
		'param'=>'ud, candel',
		'func'=> function ($ud, $candel){
			return ($ud==WRITE_ACCESS && $candel); },//анонимная функция проверяющая условия для отображения кнопки
		'newbutton'=>' <a href="?id_pc=#id#&kat=#id_pages#&id_allblocks='.$id_allblocks.'" class="pc_delete" title="удалить контент версии"><img src="picture/del.gif" width="20" height="20"/></a>'
	],
	
	[//кнопка удаления
		'param'=>'candel',
		'func'=> function ($candel) use ($USER) {
			return ($USER->isAdmin() && $candel); },//анонимная функция проверяющая условия для отображения кнопки
		'newbutton'=>' <a href="?id=#id#&kat=#id_pages#&id_allblocks='.$id_allblocks.'" class="delete" title="полное удаление страницы (со всеми версиями контента, документами, фото и подстраницами)"><img src="picture/del2.png" width="20" height="20"/></a>'
	],
	
	[//кподсветка для окна
		'param'=>'inupmenu, indownmenu',
		'func'=> function ($inupmenu, $indownmenu)//если страница отображается в каком либо меню
			{
				return ($inupmenu || $indownmenu);
			},
		'newclass'=>'violettext',
		'bg_title'=>'Данная страница отображается в верхнем или нижнем меню'
	],
	
	[//кподсветка для окна
		'param'=>'cansearch, hiddencontent',
		'func'=> function ($cansearch, $hiddencontent)//если страница отображается в каком либо меню
			{
				return (!$cansearch || $hiddencontent);
			},
		'newclass'=>'orangetext',
		'bg_title'=>'Данная страница скрыта для поиска на сайте'
	],
	
	[//кподсветка для окна
		'param'=>'visible',
		'func'=> function ($visible)//если страница отображается в каком либо меню
			{
				return (!$visible);
			},
		'newclass'=>'redbg',
		'bg_title'=>'Данная страница отключена и не отображается на сайте'
	],
	
	[//если нет ни одной созданной страницы для данной версии контента
		'param'=>'pc_count',
		'func'=> function ($pc_count) use ($USER, $allverse) {
			 if ($USER->isAdmin()) return ($pc_count<count($allverse)); //если пользователь является админом
			 else return 0; //иначе убираем кнопку
		 },//анонимная функция проверяющая условия для отображения кнопки
		//'newclass'=>'yellowbg',
		'bg_title'=>'Страница доступна не во всех версиях контента',
		'newbutton'=>' <a href="?kat='.$kat.'&id_allblocks='.$id_allblocks.'&addcontent=#id#" class="addcontent" title="копировать только структуру из текущей версии во все остальные с установкой	наследования содержимого из текущей версии"><img src="picture/add_vers.png" width="20" height="20"/></a>
		<a href="?kat='.$kat.'&id_allblocks='.$id_allblocks.'&addcontent=#id#&addcontent_dop=1" class="addcontent_dop" title="копировать структуру и данные из текущей версии все остальные"><img src="picture/copy.png" width="20" height="20"/></a>'
	],
	
	[//вообще отображение записи
		'param'=>'ud',
		'func'=> function ($ud) use ($USER) {
			return !($ud>=READ_ACCESS || $USER->isAdmin()); //если УД на чтение или пользователь является админом
		},//анонимная функция проверяющая условия для отображения кнопки
		'newclass'=>'access_close'
	]
], '', function ($p) use ($USER, $id_allblocks){//анонимная функция для создания в массиве информации о каждой конкретной страницы информации о УД к ней текущего пользователя. Этот параметр $p['ud'] можно использовать в фильтрах выше
	return $USER->access_action_itemslist('pages', $p['id'], $id_allblocks);
});
?>
      </ul>   
</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript" src="js/ew.plugin.sort.js"></script>
<script type="text/javascript">
$(document).ready(function() {		
	delete_init ('Полностью удалить страницу (включая все версии контента, документы, фотографии и т.п.)?');
	
	delete_init ('Удалить контент страницы (включая все контенты подстраниц и файлов)?', '.pc_delete');
	
	<? if ($USER->glb_access==WRITE_ACCESS): ?>
		$('#sortable').ewSorter();
	<? endif ?>
	
	//работа фильтров
	js_filtr ({
		ind: '.filtr',
		clear_val: 'id_pc, id, addcontent, addcontent_dop'
	}); 
});
</script>
</body>
</html>