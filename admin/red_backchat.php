<?php require_once('../blocks/ewcore/bd.php'); 
require_once('blocks/ewadmincore/ew.admin.init.php');
$USER->dtrm_access_action('message');//проверка доступа к странице
if (!$id=safeGetNumParam($_GET, 'id', 0))
{
	alertAndRedirect ("Ошибка! Сообщение не найдено!");
}

$result = ew_mysqli_query("SELECT backchat.*, (SELECT title FROM glossary WHERE backchat.id_type=glossary.npp AND glossary.id_glossary='".CALLBACK_TYPE_GLOSSARY."' limit 1) as type FROM backchat 
WHERE backchat.id='$id' limit 1");
if (!$m = mysqli_fetch_assoc($result))
	alertAndRedirect ("Комментария не существует..");

$USER->access_action('backchat', $id, $id_allblocks, true);//определяем глобальный УД, т.е. УД к самой странице
echo $USER->glb_access;
	
if ($m['id_pages'])
{
	$resultm = ew_mysqli_query("SELECT * FROM pages WHERE id='".$m['id_pages']."' LIMIT 1");
	$m3 = mysqli_fetch_assoc($resultm);
}

?>
<html>
<head>
<?php include_once('blocks/upblock.php'); ?>
</head>
<body>
<table align="center">
<td>
<?php include_once('blocks/menu.php'); ?>
</td>
<div align="center"><h2 align="center">Дать ответ</h2><br>

<?php 

if ($m['id_pages']) include_once('blocks/pages_links.php'); 
else '<a href="backchat.php" title="Смотреть все сообщения"><img src="picture/list.png" width="32" height="32"></a>';

?>

</div>

<td align="center" valign="middle" >




<div class="block">
<?php 
$table = 'backchat';		

ew_select ("SELECT title, npp as id FROM glossary where id_glossary='".CALLBACK_TYPE_GLOSSARY."' order by npp asc", $m['id_type'], $m['id'], "Тип сообщения: ", 'id', $table, 'id_type');////условие	

changeFlag('Публиковать?', $m['public'], 'backchat', 'public', $m['id']);
?>
<ul>
<strong>Дата:</strong> <?php echo datatimeforhuman($m['data']); ?><br><br>

<?php 
if ($m['title'])
ew_textRedaktor ('Имя:', $m['id'], 'title', $m['title'], $table);

if ($m['tel'])
ew_textRedaktor ('Телефон:', $m['id'], 'tel', $m['tel'], $table);

if ($m['email'])
ew_textRedaktor ('Email:', $m['id'], 'email', $m['email'], $table);

if ($m['info'])
ew_textRedaktor ('Сообщение:', $m['id'], 'info', $m['info'], $table);
?>
<strong>IP-адрес:</strong> <?= $m['ip'] ?><br>

<?php
//вывод дополнительной информации, если она есть
$m['json_info'] = html_entity_decode($m['json_info']);
if($m['json_info'] and ($json_info=safeGetParam($m, 'json_info', 'JSON')))
	foreach ($json_info as $key => $val)
	{
		switch ($key)
		{
			case 'file': 
				echo '<strong>'.$key.':</strong> <a href="'.PEOPLE_DIR.$val.'" target="_blank" title="скачать файл">'.$val.'</a><br>';	
			break;
			
			default: 
				echo '<strong>'.$key.':</strong> '.$val.'<br>';	
			break;
		}
	}
		
?>
</ul>
</div>

<div>
<?php 
ew_wordRedaktor ('Основной текст', $m['id'], $m['otvet'], 'otvet', $table);
?>
</div>
</td>
</table>

<!-- BODY END HERE -->

<?php include_once('blocks/unterblock.php'); ?>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {		
<? if ($USER->glb_access==WRITE_ACCESS): ?>
	js_textRedaktor ('.editable');
	selectChange ('.myselect');
	js_changeFlag();
<? endif ?>	
	tinymce.init(universal_settings);
});
</script>
</body>
</html>