<?php
    require_once('blocks/ewinit.php');
    $p['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $p['id']);
?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <?php
        require_once('blocks/ewhead.php');
        require_once('blocks/jslibs.php');
    ?>
</head>

<body>
    <div class="page-preloader">
        <div class="preloader"></div>
    </div>
    <div class="root">

        <?php
            require_once('blocks/menu.php');
        ?>

        <main>

            <?php
                require_once('blocks/breadcrumbs.php');
            ?>

            <section class="section classic-text">
                <div class="container">
                    <h1><?= $p['title'] ?></h1>
                    <div class="contacts">
                        <div class="contacts__item">
                            <h4><?= strip_tags($p['info']) ?></h4>
                            <p><?= strip_tags($p['adr']) ?></p>
                            <p><a class="phone-link" href="tel:<?= $all['clear_tel'] ?>"><?= $all['tel'] ?></a></p>
                            <p><a class="go button scroll _long" x="<?= $p['x'] ?>" y="<?= $p['y'] ?>" adr="<?= $p['adr'] ?>" href="#map">Показать на карте</a></p>
                        </div>
                        <div class="contacts__item">
                            <h4>E-mail:</h4>
                            <p><a class="email-link" href="mailto:<?= $all['email'] ?>"><?= $all['email'] ?></a></p>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php
                foreach ($p['children'] as $sub) {
                    if ($sub['linkblock'])
                        include $sub['linkblock'];
                }
            ?>

            <section class="map">
                <div id="map"></div>
            </section>
        </main>

        <?php
            require_once('blocks/ewfooter.php');
        ?>

        <script src="https://api-maps.yandex.ru/2.1/?load=package.full&lang=ru_RU&apikey=0a4dd03b-147b-4dba-ba22-bea957f90654" type="text/javascript" defer></script>
        <script src="/js/maps.js" defer></script>

    </div>

    <?php
        require_once('blocks/unterblock.php');
    ?>

</body>

</html>