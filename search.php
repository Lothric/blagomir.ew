<?php
    require_once('blocks/ewinit.php');
    
    $kol = 0;//количество результатов поиска
    $flag_kol_symbol_err = false; //флаг что надо показать сообщение об ошибке
    if ($searchQ = safeGetParam($_GET, 'search', 'SEARCH_TEXT', false))
    {
        
      if (mb_strlen ($searchQ)>=3)//ищем только если больше 3х симвлов
      {
        $searchResult = getSQLdata(SEARCH_QUERY, ID_ALLBLOCKS, $searchQ);//запрашиваем основные рузельтаты поиска
        $kol = count($searchResult);
        
        $index_pages = ($structPages[INDEX_PAGES]) ?: getLightArray (getSQLdata (SINGLE_PAGES_ITEM_QUERY, ID_ALLBLOCKS, INDEX_PAGES));//выборка информации о главной странице
      }
      else $flag_kol_symbol_err = true;
    }

    if ($searchResult && $kol) {

        // ---------------------------------------------------------------------------
        // Постраничный вывод
        // ---------------------------------------------------------------------------

        $pagination = 5;
        $pages_amount = ($kol / $pagination) - (int)($kol / $pagination) > 0 ? (int)($kol / $pagination) + 1 : (int)($kol / $pagination);

        $page_number = $_GET['page'] ?: 1;

        $offset = $pagination * ($page_number - 1);

        $searchResult = array_slice($searchResult, $offset, $pagination);
    }
?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <?php
        require_once('blocks/ewhead.php');
        require_once('blocks/jslibs.php');
    ?>
</head>

<body>
    <div class="page-preloader">
        <div class="preloader"></div>
    </div>
    <div class="root">

        <?php
            require_once('blocks/menu.php');
        ?>

        <main>

            <?php
                require_once('blocks/breadcrumbs.php');
            ?>

            <section class="section classic-text _circle _ser">
                <div class="container">
                    <h1><?= $title ?></h1>
                    <form class="search__form" action="/search/" method="get">
                        <input name="search" class="search__input" type="text" placeholder="<?= $all['search'] ?>">
                        <button class="search__button" type="submit">Найти</button>
                    </form>
                </div>
            </section>
            <section class="section classic-text _search">
                <div class="container">
                    <div class="search__result">

                        <?php
                            if ($searchQ && $kol==0 && !$flag_kol_symbol_err) 
                                echo '<h2>'.$p['dopinfo'].'</h2>'; //если ничего не найдено
                            else {
                                if ($kol)//есть ответ
                                {
                                    printf('<h2>' . strip_tags($p['info']) . '(' . $kol . '):' . '</h2>');
                                    //теперь для найденных результатов подрубаем родительские данные из меню и выводим
                                    foreach ($searchResult as $t)
                                    {
                                        if ($t['id_pages'])//если в глобальном массиве страниц есть потомок
                                        {
                                            $qq = ($structPages[$t['id_pages']]) ? $structPages : "SELECT p.id, p.id_pages, pc.title, p.link, p.hiddencontent FROM pages p inner JOIN pcontent pc ON pc.id_pages=p.id WHERE p.id=%d AND p.visible=1 AND p.cansearch=1 AND pc.id_allblocks=%d LIMIT 1"; //есть в структурном массиве страниц из меню
                                            
                                            $t['parent'] = getPraInfo($qq, $t['id_pages'], ID_ALLBLOCKS);
                                        }
                                        else
                                        {
                                            $t['parent'] = [];
                                            array_unshift($t['parent'], ['title' => $index_pages['title'], 'link' => '/', 'hiddencontent' => '0']);
                                        }
                                        
                                        $t['parent_template'] = writeTemplate($index_pages, '<a class="breadcrumbs__link" href="#link#">#title#</a>'); // первой идёт главная
                                        foreach ($t['parent'] as $parent) {
                                            if (!$parent['hiddencontent'])
                                                $t['parent_template'] .= writeTemplate($parent, '<a class="breadcrumbs__link" href="#link#">#title#</a>');
                                        }
                                        
                                        $t['parent_template'] .= writeTemplate($t, '<a class="breadcrumbs__link" href="#link#">#title#</a>'); // а последней - сама страница
                                        
                                        $t['sokr'] = sokrinfo_search($searchQ, [$t['info'], $t['dopinfo']], 300);//заполняем выделяемую область

                                        $temp = $t;

                                        while ($temp['hiddencontent']) {
                                            $temp = getSQLdata(SINGLE_PAGES_ITEM_QUERY, ID_ALLBLOCKS, $temp['id_pages'])[0];
                                        }

                                        $t['link'] = $temp['link'];
                                        $t['title'] = $temp['title'];
                                            
                                        
                                        echo writeTemplate($t, '
                                            <div class="search__item">
                                                <h5><a href="#link#">#title#</a></h5>
                                                <p>#sokr#</p>
                                                <div class="breadcrumbs__content">#parent_template#</div>
                                            </div>
                                        ');//выводим строку с результатом
                                    }
                                }
                                else echo '<h3>'.$error[LOWER_SYMBOL_ERR]['title'].'</h3>'; //если ошибка в количестве символов
                            }
                        ?>

                    </div>
                    <div class="pagination">

                        <?php
                            for ($i = 1; $i <= $pages_amount; $i++) {
                                $class = $i == $page_number ? ' _active' : '';

                                echo '<a class="pagination__item' . $class . '" href="?page=' . $i . '&search=' . $searchQ . '">' . $i . '</a>';
                            }
                        ?>

                    </div>
                </div>
            </section>
        </main>

        <?php
            require_once('blocks/ewfooter.php');
        ?>

    </div>

    <?php
        require_once('blocks/unterblock.php');
    ?>

</body>

</html>