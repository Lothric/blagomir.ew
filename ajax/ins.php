<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') :
require_once('../blocks/ewcore/bd.php');
$error = getglossary(ERRORS_GLOSSARY);//получаем шаблоны ошибок
$data=$_POST;

if(!$mode = safeGetParam($data, 'mode', 'INT'))//тип обновления
	setotvet (0, $error[MESSAGE_SET_ERR]['title'], $error[MESSAGE_SET_ERR]['info']);
	
$data['id_pages']=safeGetParam($data, 'pid', 'INT');//привязка к странице
$data['canfoto'] = 1;

$data['data']=date('Y-m-d H:i:s');
$data['host']=HOST_PROTOCOL.$_SERVER['HTTP_HOST'];
$data['ip']=$_SERVER['REMOTE_ADDR'];
$data['id_allblocks']=safeGetParam($data, 'id_allblocks', 'INT', 1);
	
$adminEmail = getAdminEmail ('*', $data['id_allblocks']);//получаем email админа
$backchat_type = getglossary(CALLBACK_TYPE_GLOSSARY);//получаем типы стандартных обращений

switch ($mode)
{

	case FEEDBACK_MESSAGE:
		$data['title']=safeGetParam($data, 'name', 'CHAR');
		$data['tel']=safeGetParam($data, 'tel', 'TEL');
		$data['email']=safeGetParam($data, 'email', 'EMAIL');
		$data['info']=safeGetParam($data, 'message', 'CHAR');

		if ($data['title'] && $data['info'] && ($data['tel'] || $data['email']))//есть данные
		{
			$data['id_type']=$mode;
			$data['otd_type']=$backchat_type[$mode]['title'];

			$podquery = 'id, data, title, email, info, ip, otd_type, id_type, id_pages, tel, id_allblocks';//если удалось найти соответствие типу обращения, тогда указываем его

			$queryStr = createSetQuery($data, $podquery);

			if (($ins = ew_mysqli_query('insert INTO backchat SET '.$queryStr)) && !ew_mysqli_error())
			{
				$newid = ew_mysqli_insert_id();
				$data['adminlink'] = $data['host'].'/admin/red_backchat.php?id='.$newid;
				$data['alllink'] = $data['host'].'/admin/backchat.php?id_type='.$mode;
				$data['data']=datatimeforhuman($data['data']);

				$emailTemplate = getglossary(EMAIL_GLOSSARY)[CALLBACK_MAIL];//шаблоны писем

				@sendmymail ($adminEmail['title'], $adminEmail['email'], $adminEmail['email'], $backchat_type[$mode]['title'], writeTemplate($data, $emailTemplate['info']));
				
				if($backchat_type[$mode]['info'])//если есть доп email, отправляем туда
					@sendmymail ($adminEmail['title'], $adminEmail['email'], $backchat_type[$mode]['info'], $backchat_type[$mode]['title'], writeTemplate($data, $emailTemplate['info']));

				setotvet (1, $error[MESSAGE_SET_OK]['title'], $error[MESSAGE_SET_OK]['info']);//если все удачно
			}
			else setotvet (0, $error[MESSAGE_SET_ERR]['title'], $error[MESSAGE_SET_ERR]['info']);

		}
		else setotvet (0, $error[MESSAGE_ERR]['title'], $error[MESSAGE_ERR]['info']);
	break;

	case CLUB_FEEDBACK_MESSAGE:
		$data['title']=safeGetParam($data, 'name', 'CHAR');
		$data['tel']=safeGetParam($data, 'tel', 'TEL');
		$data['info']=safeGetParam($data, 'message', 'CHAR');

		if ($data['title'] && $data['info'] && $data['tel'])//есть данные
		{
			$data['id_type']=$mode;
			$data['otd_type']=$backchat_type[$mode]['title'];

			$podquery = 'id, data, title, info, ip, otd_type, id_type, id_pages, tel, id_allblocks';//если удалось найти соответствие типу обращения, тогда указываем его

			$queryStr = createSetQuery($data, $podquery);

			if (($ins = ew_mysqli_query('insert INTO backchat SET '.$queryStr)) && !ew_mysqli_error())
			{
				$newid = ew_mysqli_insert_id();
				$data['adminlink'] = $data['host'].'/admin/red_backchat.php?id='.$newid;
				$data['alllink'] = $data['host'].'/admin/backchat.php?id_type='.$mode;
				$data['data']=datatimeforhuman($data['data']);

				$emailTemplate = getglossary(EMAIL_GLOSSARY)[CALLBACK_MAIL];//шаблоны писем

				@sendmymail ($adminEmail['title'], $adminEmail['email'], $adminEmail['email'], $backchat_type[$mode]['title'], writeTemplate($data, $emailTemplate['info']));
				
				if($backchat_type[$mode]['info'])//если есть доп email, отправляем туда
					@sendmymail ($adminEmail['title'], $adminEmail['email'], $backchat_type[$mode]['info'], $backchat_type[$mode]['title'], writeTemplate($data, $emailTemplate['info']));

				setotvet (1, $error[MESSAGE_SET_OK]['title'], $error[MESSAGE_SET_OK]['info']);//если все удачно
			}
			else setotvet (0, $error[MESSAGE_SET_ERR]['title'], $error[MESSAGE_SET_ERR]['info']);

		}
		else setotvet (0, $error[MESSAGE_ERR]['title'], $error[MESSAGE_ERR]['info']);
	break;

	case JOINCLUB_MESSAGE:
		$data['title']=safeGetParam($data, 'name', 'CHAR');
		$data['tel']=safeGetParam($data, 'tel', 'TEL');
		$data['email']=safeGetParam($data, 'email', 'EMAIL');
		$data['info']=safeGetParam($data, 'message', 'CHAR');

		if ($data['title'] && $data['info'] && ($data['tel'] || $data['email']))//есть данные
		{
			$myfiles=$_FILES["file"];
			$valid_types = array("jpg", "png", "jpeg", 'gif', 'pdf', 'doc', 'txt', 'docx');

			$data['id_type']=$mode;
			$data['otd_type']=$backchat_type[$mode]['title'];

			if (is_uploaded_file($myfiles['tmp_name']))//если есть файл
			{
				if($myfiles['size']>MAX_FILE_SIZE)//если файл больше чем ограничение
					setotvet (0, $error[FILE_BIGSIZE_ERR]['title'], $error[FILE_BIGSIZE_ERR]['info']);
				
				$ext = strtolower(substr($myfiles['name'], 
					1 + strrpos($myfiles['name'], "."))); //определяем расширение файла
				
				if (!in_array($ext, $valid_types))//проверяем на допустимое расширение
					setotvet (0, $error[FORMAT_FILE_ERR]['title'], $error[FORMAT_FILE_ERR]['info']);
					
				$newname = date('Y-m-d-H-i-s-').rand(100, 999).'.'.$ext;//новое имя файла
				if (rename($myfiles['tmp_name'], PEOPLE_DIR.$newname))
				{
					$data['file'] = $newname; 
				}
				else 
				{
					@unlink(PEOPLE_DIR.$newname);//удаление файла
					setotvet (0, $error[FILE_SEND_ERR]['title'], $error[FILE_SEND_ERR]['info']);
				}
			}

			$data['json_info'] = json_encode ([
				'file'=>$data['file']
				], JSON_UNESCAPED_UNICODE); 

			$podquery = 'id, data, title, email, info, ip, otd_type, id_type, id_pages, tel, id_allblocks, json_info';//если удалось найти соответствие типу обращения, тогда указываем его

			$queryStr = createSetQuery($data, $podquery);

			if (($ins = ew_mysqli_query('insert INTO backchat SET '.$queryStr)) && !ew_mysqli_error())
			{
				$newid = ew_mysqli_insert_id();
				$data['adminlink'] = $data['host'].'/admin/red_backchat.php?id='.$newid;
				$data['alllink'] = $data['host'].'/admin/backchat.php?id_type='.$mode;
				$data['data']=datatimeforhuman($data['data']);

				$emailTemplate = getglossary(EMAIL_GLOSSARY)[CALLBACK_MAIL];//шаблоны писем

				@sendmymail ($adminEmail['title'], $adminEmail['email'], $adminEmail['email'], $backchat_type[$mode]['title'], writeTemplate($data, $emailTemplate['info']), PEOPLE_DIR.$newname, $newname);
				
				if($backchat_type[$mode]['info'])//если есть доп email, отправляем туда
					@sendmymail ($adminEmail['title'], $adminEmail['email'], $backchat_type[$mode]['info'], $backchat_type[$mode]['title'], writeTemplate($data, $emailTemplate['info']),PEOPLE_DIR.$newname, $newname);

				setotvet (1, $error[MESSAGE_SET_OK]['title'], $error[MESSAGE_SET_OK]['info']);//если все удачно
			}
			else setotvet (0, $error[MESSAGE_SET_ERR]['title'], $error[MESSAGE_SET_ERR]['info']);

		}
		else setotvet (0, $error[MESSAGE_ERR]['title'], $error[MESSAGE_ERR]['info']);
	break;

	case GET_MODAL:
		$data['id'] = safeGetParam($data, 'id', 'INT'); //ID страницы, которую надо получить
        $data['id_allblocks'] = safeGetParam($data, 'id_allblocks', 'INT', 1);

        if ($data['id'])//есть данные
        {
            $page = getSQLdata(PAGES_ITEM_QUERY, $data['id_allblocks'], $data['id'])[0];

            setotvet (1, $page);//если все удачно
        }
        
        else setotvet (0, $error[MESSAGE_ERR]['title'], $error[MESSAGE_ERR]['info']);
	break;

	case GET_CONF:
		$data['pid'] = safeGetParam($data, 'pid', 'INT'); //ID страницы, которую надо получить
        $data['id_allblocks'] = safeGetParam($data, 'id_allblocks', 'INT', 1);
        $data['year'] = safeGetParam($data, 'year', 'INT');
        $data['page'] = safeGetParam($data, 'page', 'INT');
        
        $offset = strval(($data['page'] - 1) * 6);

        if ($data['pid'])//есть данные
        {
            $confs = getSQLdata(NEWS_PER_YEAR_QUERY, $data['id_allblocks'], $data['pid'], $data['year'], 'LIMIT 6 OFFSET ' . $offset);

            setotvet (1, $confs);//если все удачно
        }
        
        else setotvet (0, $error[MESSAGE_ERR]['title'], $error[MESSAGE_ERR]['info']);
	break;

	default: setotvet (0, $error[MESSAGE_SET_ERR]['title'], $error[MESSAGE_SET_ERR]['info']); break;
}
	
endif;
?>