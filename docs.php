<?php
    require_once('blocks/ewinit.php');
    $p['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $p['id']);
?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <?php
        require_once('blocks/ewhead.php');
        require_once('blocks/jslibs.php');
    ?>
</head>

<body>
    <div class="page-preloader">
        <div class="preloader"></div>
    </div>
    <div class="root">

        <?php
            require_once('blocks/menu.php');
        ?>

        <main>

            <?php
                require_once('blocks/breadcrumbs.php');
            ?>

            <section class="section classic-text _fon">
                <div class="container">

                    <a class="go-back" href="<?= $parentPages[count($parentPages) - 1]['link'] ?>">
                        <div class="button-prev i-arrow _sm"></div><?= $parentPages[count($parentPages) - 1]['title'] ?>
                    </a>
                    <h1><?= $p['title'] ?></h1>
                    <?php 
                        if ($p['info']) {
                            echo $p['info'];
                            echo '<br><br>';
                        } 
                    ?>

                    <div class="tabs">
                        <div class="tabs__labels _lg">

                            <?php
                                foreach ($p['children'] as $index=>$child) {
                                    if ($index == 0) {
                                        $child['class'] = ' _active';
                                    }

                                    echo writeTemplate($child, '<div class="tabs__label#class#">#title#</div>');
                                }
                            ?>

                        </div>
                        <div class="tabs__contents">

                            <?php
                                foreach ($p['children'] as $index=>$child) {

                                    $docs = getSQLdata(PAGES_DOCS_QUERY, ID_ALLBLOCKS, $child['id']);

                                    $docs = arrRekompose($docs, 'id');

                                    if ($index == 0) {
                                        $child['class'] = ' _active';
                                    }

                                    echo writeTemplate($child, '<div class="tabs__content#class#"><div class="documents">');

                                    foreach ($child['foto'][DOCS_PHOTOS] as $photo) {
                                        if ($photo['id_sovmdocs']) {

                                            $doc = $docs[$photo['id_sovmdocs']][0];

                                            $photo['type'] = $doc['filetype'];
                                            $photo['doclink'] = $doc['filename'];

                                            $photo['doc'] = writeTemplate($photo, '<a target="_blank" class="document__download" href="#doclink#">Скачать #type#</a>');

                                            if ($doc['title'])
                                                $photo['title'] = $doc['title'];
                                        }

                                        if ($photo['info'])
                                            $photo['info'] = writeTemplate($photo, '<div class="document__type">#info#</div>');

                                        echo writeTemplate($photo, '
                                            <div class="document">
                                                #info#
                                                <div class="document__title">#title#</div><a class="document__preview gallery" href="#picname#"><img class="document__img" src="#picname2#" alt="#title#"></a>
                                                #doc#
                                            </div>
                                        ');
                                    }

                                    echo '</div></div>';
                                }
                            ?>

                        </div>
                    </div>

                    <?= $p['dopinfo'] ?>

                </div>
            </section>
        </main>

        <?php
            require_once('blocks/ewfooter.php');
        ?>

    </div>

    <?php
        require_once('blocks/unterblock.php');
    ?>

</body>

</html>