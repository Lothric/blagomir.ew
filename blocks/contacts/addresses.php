<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="fund-structure classic-text _contact">
    <div class="container">
        <h2><?= $sub['title'] ?></h2>
        <div class="regional-government__items _contact">

            <?php
                foreach ($sub['children'] as $child) {
                    $child['picname'] = $child['foto'][SPECIAL_PHOTOS][0]['picname'];
                    $child['tel'] = makeClearTel($child['info']);

                    echo writeTemplate($child, '
                        <div class="regional-government__item">
                            <div class="regional-government__preview"><img class="regional-government__img" src="#picname#" alt="#title#" style="#css_icon#"></div>
                            <div class="regional-government__desc">
                                <h4>#title#</h4>
                                <p>#adr#</p>
                                <p><a class="phone-link" href="tel:+#tel#">#info#</a></p>
                                <p><a class="go scroll button _long" x="#x#" y="#y#" adr="#adr#" href="#">#showmap#</a></p>
                            </div>
                        </div>
                    ');
                }
            ?>

        </div>
    </div>
</section>