<?php
    $add = 'LIMIT 4';

    $eventspage = getSQLdata(SINGLE_PAGES_ITEM_QUERY, ID_ALLBLOCKS, EVENTS_PAGES)[0];
    $events = getSQLdata(NEWS_QUERY, ID_ALLBLOCKS, EVENTS_PAGES, $add);

    $add = 'LIMIT 1';

    $activitiespage = getSQLdata(SINGLE_PAGES_ITEM_QUERY, ID_ALLBLOCKS, ACTIVITY_PAGES)[0];
    $activities = getSQLdata (NEWS_QUERY, ID_ALLBLOCKS, ACTIVITY_PAGES, $add);

    $add = 'LIMIT 10';

    $newspage = getSQLdata(SINGLE_PAGES_ITEM_QUERY, ID_ALLBLOCKS, NEWS_PAGES)[0];
    $news = getSQLdata (NEWS_QUERY, ID_ALLBLOCKS, NEWS_PAGES, $add);
?>

<section class="press-center">
    <div class="container">
        <div class="press-center__content">
            <div class="press-center__news">
                <h2 class="special"><?= $newspage['title'] ?><a class="button" href="<?= $newspage['link'] ?>"><?= $sub['dopinfo'] ?></a></h2>

                    <?php
                        $i = 0;

                        foreach ($news as $n) {
                            if ($n['foto'][NEWS_LARGE_PHOTOS]) {
                                $t = 2;
                                $n['class'] = ' _lg';
                                $n['img'] = writeTemplate($n['foto'][NEWS_LARGE_PHOTOS][0], '<img src="#picname2#" alt="#title#">');
                            }
                            elseif ($n['foto'][NEWS_SMALL_PHOTOS]) {
                                $t = 1;
                                $n['img'] = writeTemplate($n['foto'][NEWS_SMALL_PHOTOS][0], '<img src="#picname2#" alt="#title#">');
                            }
                            else {
                                $t = 1;
                            }

                            $n['preinfo'] = $n['preinfo'] ?: $p['title'];

                            if (($i + $t) <= 4) {
                                $i += $t;
                                echo writeTemplate($n, '
                                    <a class="article-preview#class#" href="#link#">
                                        <div class="article-preview__img">#img#</div>
                                        <div class="article-preview__desc">
                                            <div class="date">#preinfo# <span>/#data#</span></div>
                                            <div class="article-preview__title">#title#</div>
                                        </div>
                                        <div class="button-next _sm i-arrow"></div>
                                    </a>
                                ');
                            }
                        }
                    ?>

            </div>
            <div class="press-center__events">
                <div class="press-center__event-item">
                    <h2 class="special"><?= $eventspage['title'] ?><a class="button" href="<?= $eventspage['link'] ?>"><?= $sub['dopinfo'] ?></a></h2>
                    
                        <?php
                            foreach ($events as $n) {
                                $n['day'] = datetotext($n['data'], '#d#', $all['months']);
                                if (strlen($n['day']) < 2)
                                    $n['day'] = '0' . $n['day'];

                                echo writeTemplate($n, '
                                    <a class="event" href="#link#">
                                        <div class="event__number">#day#</div>
                                        <div class="date">#data#</div>
                                        <div class="event__title">#title#</div>
                                        <div class="event__bottom">
                                            <div class="button-next _sm i-arrow"></div>
                                            <div class="event__more">#more#</div>
                                        </div>
                                    </a>
                                ');
                            }
                        ?>

                </div>
                <div class="press-center__event-item">
                    <h2 class="special"><?= $activitiespage['title'] ?><a class="button" href="<?= $activitiespage['link'] ?>"><?= $sub['dopinfo'] ?></a></h2>
                    <h4>Ближайшее мероприятие:</h4>

                        <?php
                            foreach ($activities as $n) {
                                $n['data'] = datetotext($n['data'], '<span>#d#</span> #m#', $all['months']);
                                $n['logo'] = $p['info'];

                                echo writeTemplate($n, '
                                    <a class="merop" href="#link#">
                                        <div class="merop__date">#logo##data#</div>
                                        <div class="merop__desc">
                                            <div class="merop__title">#title#</div>
                                            <div class="merop__location">#preinfo#</div>
                                        </div>
                                    </a>
                                ');
                            }
                        ?>
                </div>
            </div>
        </div>
    </div>
</section>