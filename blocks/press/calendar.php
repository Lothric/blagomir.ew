<?php
    $month = date('m');
    $year = date('Y');
    $temp = [];
    $datearray2 = [];

    $dates = getSQLdata(CALENDAR_QUERY, ID_ALLBLOCKS, $sub['id']);
    $activities = getSQLdata(CALENDAR_QUERY, ID_ALLBLOCKS, ACTIVITY_PAGES);

    foreach ($activities as $act) {
        $datearray[] = $act['data'];
    }

    foreach ($dates as $d) {
        // меняем год на нынешний
        $date = explode('.', $d['data']);
        $d['data'] = $date[0] . '.' . $date[1] . '.' . $year;

        if (array_search($d['data'], $datearray) || array_search($d['data'], $datearray2)) {
            continue;
        }

        $datearray2[] = $d['data'];
        $temp[] = $d;
    }

    $dates = array_merge($temp, $activities);

    function sortByOrder($a, $b) {
        return strtotime($a['data']) - strtotime($b['data']);
    };
    
    usort($dates, 'sortByOrder');

    echo '<script>';
    echo 'let eventDates = [];';

    foreach ($dates as $n) {
        $m = explode('.', $n['data'])[1];
        $d = explode('.', $n['data'])[0];
        echo 'if (!eventDates[' . $m . ']) {eventDates[' . $m . '] = []};';
        echo 'eventDates[' . $m . '].push(' . $d . ');';
    }

    echo '</script>';
?>

<section class="section classic-text _fon">
    <div class="container">
        <h1><?= $p['title'] ?></h1>
        <p class="text"><?= $sub['title'] ?></p>
        <?= $sub['info'] ?>
        <div class="calendar">
            <div class="calendar__datapicker"></div>
            <div class="calendar__slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">

                        <?php
                            foreach ($dates as $n) {

                                // картинка
                                if ($n['foto'][CELEBRATION_PHOTOS]) {
                                    $n['foto'][CELEBRATION_PHOTOS][0]['title'] = $n['foto'][CELEBRATION_PHOTOS][0]['title'] ?: $n['title'];
                                    $n['img'] = writeTemplate($n['foto'][CELEBRATION_PHOTOS][0], '<img class="calendar__slider-img" src="#picname2#" alt="#title#">');
                                    if ($n['id_pages'] == ACTIVITY_PAGES)
                                        $n['class'] = ' _event';
                                }
                                else {
                                    if ($n['id_pages'] == CALENDAR_PAGES) {
                                        $n['img'] = writeTemplate($n, '<img class="calendar__slider-img" src="/img/celebration.png" alt="#title#">');
                                    }
                                    else {
                                        $n['img'] = writeTemplate($n, '<img class="calendar__slider-img" src="/img/event.png" alt="#title#">');
                                        $n['class'] = ' _event';
                                    }
                                }

                                if (!$n['hiddencontent']) {
                                    $n['linkmore'] = writeTemplate($n, '<p><a href="#link#">#more#</a></p>');
                                }

                                echo writeTemplate($n, '
                                    <div class="swiper-slide">
                                        <div class="calendar__slider-item#class#">
                                            #img#
                                            <div class="calendar__slider-date"><span>#remdate#</span><span class="caldate">#data#</span></div>
                                            <div class="calendar__slider-desc">
                                                <h4>#title#</h4>
                                                <p>#preinfo#</p>
                                                #linkmore#
                                            </div>
                                        </div>
                                    </div>
                                ');
                            }
                            
                        ?>

                    </div>
                </div>
                <div class="pagination-number"></div>
                <div class="naviblock">
                    <div class="button-prev _sm i-arrow"></div>
                    <div class="button-next _sm i-arrow"></div>
                </div>
            </div>
        </div>
    </div>
</section>