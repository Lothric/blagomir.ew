<?php
    $main = getSQLdata(SINGLE_PAGES_ITEM_QUERY, ID_ALLBLOCKS, INDEX_PAGES)[0];
?>

<section class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs__content">
            <?= '<a class="breadcrumbs__link" href="/">' . $main['title'] . '</a>'; ?>
            <?= writeItemsTemplate($parentPages, '<a class="breadcrumbs__link" href="#link#">#title#</a>'); ?>
            <span class="breadcrumbs__link"><?= $p['title'] ?></span>
            <!-- <a class="breadcrumbs__link" href="index.html">Главная</a>
            <span class="breadcrumbs__link">Контакты</span> -->
        </div>
    </div>
</section>