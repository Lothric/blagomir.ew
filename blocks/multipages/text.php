<?php
    require_once('blocks/ewinit.php');

    $title = '';

    if (strpos($sub['info'], '<h1') === 0) {

        $start = 0; // стартовая позиция, тайтл сработает только если он на первом месте
        $length = strpos($sub['info'], '</h1>') + 5 - $start; // длина заголовка с html-тегами, т.к. его вырежем и вставим туда картинку, если она есть
        $subtitleTags = substr($sub['info'], $start, $length); // берём заголовок вместе с тэгами, чтобы сохранить форматирование заголовка
        $title = $subtitleTags; // текст заголовка
        
        $sub['info'] = substr($sub['info'], $start + $length);
    }

    $title = $title ?: '<h1>' . $sub['title'] . '</h1>';


    $p['data'] = dataforhuman($p['data']);

    $top = '';
    
    // if ($parentPages[count($parentPages) - 1] == NEWS_PAGES && $sub['preinfo']) {
    //     $top .= writeTemplate($sub, '<div class="rubric">Рубрика: <span>#preinfo#</span></div>');
    // }
    if ($p['data'] && $index == 0)
        $top .= writeTemplate($p, '<div class="date">#data#</div>');
?>

<section class="section classic-text _fon">
    <div class="container">

        <?php
            // *******************************************************************************************
                    // фотки слева
            // *******************************************************************************************

            if ($sub['foto'][LEFT_SLIDER_PHOTOS]):
        ?>

        <div class="textblock">
            <div class="textblock__item _slider">

                <?php
                    if ($parentPages && $parentPages[0] && $index == 0) {
                        echo writeTemplate($parentPages[count($parentPages) - 1], '
                            <a class="go-back" href="#link#">
                                <div class="button-prev i-arrow _sm"></div>#title#
                            </a>
                        ');
                    }
                ?>

                <?php
                    // собираем все фотки на слайдер
                    foreach ($sub['foto'][LEFT_SLIDER_PHOTOS] as $photo) {

                        if ($photo['title']) {
                            $photo['alt'] = $photo['title'];
                            $photo['title'] = writeTemplate($photo, '<div class="slider__desc">#title#</div>');
                        }

                        $sub['leftslider'] .= writeTemplate($photo, '
                            <div class="swiper-slide">
                                <div class="slider__item">
                                    <div class="slider__preview"><a class="gallery" href="#picname#"><img class="slider__img" src="#picname2#" alt="#alt#"></a></div>
                                    #title#
                                </div>
                            </div>
                        ');
                    }

                    if (count($sub['foto'][LEFT_SLIDER_PHOTOS]) > 1) {
                        $sub['leftnavigation'] = '
                            <div class="button-prev i-arrow"></div>
                            <div class="button-next i-arrow"></div>
                        ';
                    }
                    
                    echo writeTemplate($sub, '
                        <div class="slider">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    #leftslider#
                                </div>
                            </div>
                            #leftnavigation#
                        </div>
                    ');
                ?>

            </div>

            <div class="textblock__item _lg">
                
                <?php
                    if ($parentPages && $parentPages[0] && $index == 0) {
                        echo writeTemplate($parentPages[count($parentPages) - 1], '
                            <a class="go-back" href="#link#">
                                <div class="button-prev i-arrow _sm"></div>#title#
                            </a>
                        ');
                    }
                ?>
                
                <?= $top ?>
                
                <?= $title ?>
                <?= $sub['info'] ?>
            </div>
        </div>

        <?= $sub['dopinfo'] ?>

        <?php
            // *******************************************************************************************
                    // фотки справа
            // *******************************************************************************************

            elseif ($sub['foto'][RIGHT_SLIDER_PHOTOS]):
        ?>

        <div class="textblock">

            <div class="textblock__item _md">
                
                <?php
                    if ($parentPages && $parentPages[0] && $index == 0) {
                        echo writeTemplate($parentPages[count($parentPages) - 1], '
                            <a class="go-back" href="#link#">
                                <div class="button-prev i-arrow _sm"></div>#title#
                            </a>
                        ');
                    }
                ?>
                
                <?= $top ?>
                
                <?= $title ?>
                <?= $sub['info'] ?>
            </div>

            <div class="textblock__item _slider _lg">

                <?php
                    if ($parentPages && $parentPages[0] && $index == 0) {
                        echo writeTemplate($parentPages[count($parentPages) - 1], '
                            <a class="go-back" href="#link#">
                                <div class="button-prev i-arrow _sm"></div>#title#
                            </a>
                        ');
                    }
                ?>

                <?php
                    // собираем все фотки на слайдер
                    foreach ($sub['foto'][RIGHT_SLIDER_PHOTOS] as $photo) {

                        if ($photo['title']) {
                            $photo['alt'] = $photo['title'];
                            $photo['title'] = writeTemplate($photo, '<div class="slider__desc">#title#</div>');
                        }

                        $sub['leftslider'] .= writeTemplate($photo, '
                            <div class="swiper-slide">
                                <div class="slider__item">
                                    <div class="slider__preview"><a class="gallery" href="#picname#"><img class="slider__img" src="#picname2#" alt="#alt#"></a></div>
                                    #title#
                                </div>
                            </div>
                        ');
                    }

                    if (count($sub['foto'][RIGHT_SLIDER_PHOTOS]) > 1) {
                        $sub['leftnavigation'] = '
                            <div class="button-prev i-arrow"></div>
                            <div class="button-next i-arrow"></div>
                        ';
                    }
                    
                    echo writeTemplate($sub, '
                        <div class="slider">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    #leftslider#
                                </div>
                            </div>
                            #leftnavigation#
                        </div>
                    ');
                ?>

            </div>

        </div>

        <?= $sub['dopinfo'] ?>

        <?php
            else:
            
                if ($parentPages && $parentPages[0] && $index == 0) {
                    echo writeTemplate($parentPages[count($parentPages) - 1], '
                        <a class="go-back" href="#link#">
                            <div class="button-prev i-arrow _sm"></div>#title#
                        </a>
                    ');
                }

                echo $top;
            
                echo $title;
                echo $sub['info'] . $sub['dopinfo'];

            endif;
            
        ?>

    </div>
</section>

<?php
    // если есть фотки на слайдер
    if ($sub['foto'][CENTRAL_SLIDER_PHOTOS]):
?>

<section class="section _slider">
    <div class="container">
        <div class="classic-slider">
            <div class="swiper-wrapper">

                <?php
                    foreach ($sub['foto'][CENTRAL_SLIDER_PHOTOS] as $photo) {
                        $photo['alt'] = $photo['title'] ?: $p['title'];
                        echo writeTemplate($photo, '
                            <div class="swiper-slide">
                                <a class="gallery" href="#picname#">
                                    <div class="classic-slider__item"><img class="classic-slider__img" src="#picname2#" alt="#alt#"></div>
                                </a>
                            </div>
                    ');
                    }
                ?>

            </div>

            <?php
                if (count($sub['foto'][CENTRAL_SLIDER_PHOTOS]) > 1) {
                    echo '
                        <div class="classic-pagination"></div>
                        <div class="button-prev i-arrow"></div>
                        <div class="button-next i-arrow"></div>
                    ';
                }
            ?>
            
        </div>
    </div>
</section>

<?php
    endif;
    if ($sub['banner']): 
?>

<section class="section classic-text _bg">
    <div class="container">
        <?= $sub['banner'] ?>
    </div>
</section>

<?php
    endif;
    if ($sub['docs'][BOTTOM_DOCSTYPE]):
?>

<section class="section _docs">
    <div class="container">
        <div class="docs">

            <?php

                foreach ($sub['docs'][BOTTOM_DOCSTYPE] as $doc) {
                    $doc['filesize'] = FileSizeConvert($doc['filesize'], $all['filesizes']);

                    echo writeTemplate($doc, '
                        <a target="_blank" class="bottom-doc" href="#filename#">
                            <div class="card _doc">
                                <div class="card__content">
                                    <div class="card__title">#title#</div>
                                    <div class="card__desc">#filetype#, #filesize#</div>
                                    <div class="card__icon i-download"></div>
                                </div>
                            </div>
                        </a>
                    ');
                }
            ?>
            
        </div>
    </div>
</section>

<?php endif; ?>