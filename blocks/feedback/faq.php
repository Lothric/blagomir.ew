<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="section classic-text _fon">
    <div class="container">
        <div class="feedback">
            <div class="feedback__desc">
                <p class="text"><?= $p['title'] ?></p>
                <h1><?= $sub['info'] ?></h1>
                <p class="line"><?= $sub['dopinfo'] ?></p>

                <?php
                    if ($sub['foto'][SPECIAL_PHOTOS]) {
                        echo writeTemplate($sub['foto'][SPECIAL_PHOTOS][0], '<p class="img"><img src="#picname#" alt="#picname#"></p>');
                    }
                ?>

            </div>
            <div class="questions">

                <?=
                    writeItemsTemplate($sub['children'], '
                        <div class="question">
                            <div class="question__message">#title#</div>
                            <div class="question__answer">
                                #info#
                            </div>
                        </div>
                    ');
                ?>

            </div>
        </div>
    </div>
</section>