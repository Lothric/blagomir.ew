<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="target <?= $sub['css_icon'] ?>">
    <div class="container">
        <div class="target-slider swiper-container">
            <div class="swiper-wrapper">

                <?=
                    writeItemsTemplate($sub['children'], '
                        <div class="swiper-slide">
                            <div class="target-slider__item">
                                <div class="target-slider__title">#info#</div>
                                <div class="target-slider__desc">#dopinfo#</div>
                            </div>
                        </div>
                    ');
                ?>

            </div>
            <div class="pagination-slider"></div>
        </div>
    </div>
</section>