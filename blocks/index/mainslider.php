<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="test">
    <div class="test-anim">

        <?php
            foreach ($sub['children'] as $index=>$child) {
                $child['picname'] = $child['foto'][SPECIAL_PHOTOS][0]['picname'];
                $child['title'] = $child['foto'][SPECIAL_PHOTOS][0]['title'] ?: $child['title'];

                if ($index == 0) {
                    $child['class'] = ' _active';
                }

                echo writeTemplate($child, '<img class="test-anim__img#class#" src="#picname#" alt="#title#">');
            }
        ?>
        
        <div class="test-anim__fon">
            <img class="test-anim__fon-img" src="/img/main-slider/anim/1.png" alt="">
            <img class="test-anim__fon-img" src="/img/main-slider/anim/2.png" alt="">
            <img class="test-anim__fon-img" src="/img/main-slider/anim/3.png" alt="">
            <img class="test-anim__fon-img" src="/img/main-slider/anim/4.png" alt="">
            <img class="test-anim__fon-img" src="/img/main-slider/anim/5.png" alt="">
            <img class="test-anim__fon-img" src="/img/main-slider/anim/6.png" alt="">
            <img class="test-anim__fon-img" src="/img/main-slider/anim/7.png" alt="">
            <img class="test-anim__fon-img" src="/img/main-slider/anim/8.png" alt="">
            <img class="test-anim__fon-img" src="/img/main-slider/anim/9.png" alt="">
        </div>
    </div>
    <div class="test-slider">
        <div class="swiper-wrapper">

            <?php
                $classes = [' _first', ' _second', ' _third'];

                foreach ($sub['children'] as $index=>$child) {
                    $child['picname'] = $child['foto'][SPECIAL_PHOTOS][0]['picname'];

                    $child['class'] = $classes[$index];

                    echo writeTemplate($child, '
                        <div class="swiper-slide" style="background-image: url(\'#picname#\')">
                            <div class="test-slider__item#class#">
                                <div class="test-slider__title"><span>#info#</span>#dopinfo#</div>
                                <div class="test-slider__desc">
                                    <p>#banner#</p>
                                </div>
                            </div>
                        </div>
                    ');
                }
            ?>

        </div>
        <div class="test-pagination"></div>
    </div>
    <div class="container">
        <div class="test-imgs__left">

            <?php
                foreach ($sub['children'] as $index=>$child) {
                    $child['picname'] = $child['foto'][SPECIAL_PHOTOS][1]['picname'];
                    $child['title'] = $child['foto'][SPECIAL_PHOTOS][1]['title'] ?: $child['title'];

                    if ($index == 0) {
                        $child['class'] = ' _active';
                    }

                    echo writeTemplate($child, '<img class="test-imgs__img#class#" src="#picname#" alt="#title#">');
                }
            ?>
            
        </div>
        <div class="test-imgs__right">

            <?php
                foreach ($sub['children'] as $index=>$child) {
                    $child['picname'] = $child['foto'][SPECIAL_PHOTOS][2]['picname'];
                    $child['title'] = $child['foto'][SPECIAL_PHOTOS][2]['title'] ?: $child['title'];

                    if ($index == 0) {
                        $child['class'] = ' _active';
                    }

                    echo writeTemplate($child, '<img class="test-imgs__img#class#" src="#picname#" alt="#title#">');
                }
            ?>

        </div>
    </div>
</section>