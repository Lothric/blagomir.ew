<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="fund-activities">
    <div class="container">
        <div class="fund-activities__title">
            <h2><?= $sub['info'] ?></h2>
            <p class="text"><?= $sub['dopinfo'] ?></p>
        </div>
        <div class="fund-activities__content">

            <?=
                writeItemsTemplate($sub['children'], '
                    <div class="fund-activities__item">
                        <div class="fund-activities__number">#info#</div>
                        <div class="fund-activities__name">#dopinfo#</div>
                        <div class="fund-activities__desc">#banner#</div>
                    </div>
                ');
            ?>

        </div>
    </div>
</section>