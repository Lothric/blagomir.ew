<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
    $sub['children'][0]['class'] = ' _active';
?>

<section class="entrepreneurs-club">
    <div class="container">
        <div class="entrepreneurs-club__content">
            <div class="entrepreneurs-club__test">
                <h2><span><?= $sub['title'] ?></span></h2>
                <p class="text"><?= $sub['info'] ?></p>

                <?php
                    if ($sub['foto'][SPECIAL_PHOTOS]) {
                        echo writeTemplate($sub['foto'][SPECIAL_PHOTOS][0], '
                            <div class="entrepreneurs-club__person">
                                <img class="entrepreneurs-club__foto" src="#picname#" alt="#title#">
                                <div class="entrepreneurs-club__name">#title#<span>#info#</span></div>
                                <div class="entrepreneurs-club__rank">#dopinfo#</div>
                            </div>
                        ');
                    }
                ?>

            </div>
            <div class="entrepreneurs-club__desc classic-text">
                <div class="tabs _index">
                    <div class="tabs__labels">
                        
                        <?= writeItemsTemplate($sub['children'], '<div class="tabs__label#class#">#title#</div>') ?>

                    </div>
                    <div class="tabs__contents">

                        <?=
                            writeItemsTemplate($sub['children'], '
                                <div class="tabs__content#class#">
                                    #info#
                                </div>
                            ');
                        ?>

                    </div>
                </div>
            </div>
        </div>

        <?php
            if ($sub['foto'][CLUB_PHOTOS]) {
                echo '<div class="slider-left _index"><div class="swiper-wrapper">';
                echo writeItemsTemplate($sub['foto'][CLUB_PHOTOS], '
                    <div class="swiper-slide">
                        <a class="entrepreneurs-club__preview gallery" href="#picname#">
                            <img class="entrepreneurs-club__img" src="#picname2#" alt="#title#">
                        </a>
                    </div>
                ');
                echo '
                    </div>
                    <div class="pagination-number"></div>
                    <div class="naviblock">
                        <div class="button-prev _sm i-arrow"></div>
                        <div class="button-next _sm i-arrow"></div>
                    </div>
                </div>
                ';
            }
        ?>
        
    </div>
</section>