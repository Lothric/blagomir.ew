<?php
    $newspage = getSQLdata(SINGLE_PAGES_ITEM_QUERY, ID_ALLBLOCKS, NEWS_PAGES)[0];
    $news = getSQLdata(NEWS_QUERY, ID_ALLBLOCKS, NEWS_PAGES);
?>

<section class="index-news">
    <div class="container">
        <h2 class="special"><?= $sub['title'] ?><a class="button" href="<?= $newspage['link'] ?>">Смотреть все</a></h2>
        <div class="index-news__slider swiper-container">
            <div class="swiper-wrapper">

                <?php

                    // ну, тут без поллитра не разберёшься
                    // в общем, из-за особого дизайна новости выводятся в две колонки на слайд
                    // в каждой колонке может быть либо две новости с маленькой картинкой, либо одна с большой
                    // если две маленькие новости, то надо нижней ещё класс mt-auto навесить для правильного margin
                    // очевидно, что если прошла одна новость с маленькой картинкой, а следом за ней идёт новость с большой картинкой, то это не впишется в данную картину мира
                    // пришлось делать так:
                    // если произошло то, что описано выше, то новость с большой картинкой идёт в массив temp 
                    // если за ней опять с большой картинкой - то и она в temp
                    // и так, пока не дождёмся новости с маленькой картинкой
                    // как только она прошла - у нас карт-бланш, можем высыпать все новости с большой картинкой подряд, что заботливо хранятся в temp 
                    // функция deal выводит эти самые залежалые новости с большой картинкой, а цикл ниже функции колдует над тем, чтобы всё это работало и выводит всё в штатном режиме, если ситуаций описанных выше не происходит

                    $i = 0;

                    // выводим три слайда новостей, по количеству новостей считать не можем, так как всё зависит от прикреплённых картинок, пользуем index
                    $index = 0;

                    $temp = [];

                    function deal($n) {
                        global $i, $newspage, $index;

                        if ($i == 0) {
                            echo '<div class="swiper-slide"><div class="index-news__content"><div class="index-news__left">';
                        }
                        if ($i == 2) {
                            echo '<div class="index-news__right">';
                        }

                        if ($n['foto'][NEWS_LARGE_PHOTOS] && ($i == 0 || $i == 2)) {
                            $n['class'] = ' _lg';
                            $n['img'] = writeTemplate($n['foto'][NEWS_LARGE_PHOTOS][0], '<img src="#picname2#" alt="#title#">');
                            $t = 2;
                        }
                        elseif ($n['foto'][NEWS_SMALL_PHOTOS]) {
                            $n['img'] = writeTemplate($n['foto'][NEWS_SMALL_PHOTOS][0], '<img src="#picname2#" alt="#title#">');
                            $t = 1;
                        }
                        else {
                            $t = 1;
                        }

                        $n['preinfo'] = $n['preinfo'] ?: $newspage['title'];

                        $i += $t;
                        $index += $t;

                        echo writeTemplate($n, '
                            <a class="article-preview#class##subclass#" href="#link#">
                                <div class="article-preview__img">#img#</div>
                                <div class="article-preview__desc">
                                    <div class="date">#preinfo# <span>/#data#</span></div>
                                    <div class="article-preview__title">#title#</div>
                                </div>
                                <div class="button-next _sm i-arrow"></div>
                            </a>
                        ');

                        if ($i == 2) {
                            echo '</div>';
                        }

                        if ($i == 4) {
                            echo '</div></div></div>';
                            $i = 0;
                        }
                    };

                    foreach ($news as $n) {

                        if ($index >= 12)
                            break;

                        if ($i == 0) {
                            echo '<div class="swiper-slide"><div class="index-news__content"><div class="index-news__left">';
                        }
                        if ($i == 2) {
                            echo '<div class="index-news__right">';
                        }

                        if ($n['foto'][NEWS_LARGE_PHOTOS]) {
                            $n['class'] = ' _lg';
                            $n['img'] = writeTemplate($n['foto'][NEWS_LARGE_PHOTOS][0], '<img src="#picname2#" alt="#title#">');
                            $t = 2;
                        }
                        elseif ($n['foto'][NEWS_SMALL_PHOTOS]) {
                            $n['img'] = writeTemplate($n['foto'][NEWS_SMALL_PHOTOS][0], '<img src="#picname2#" alt="#title#">');
                            $t = 1;
                        }
                        else {
                            $t = 1;
                        }

                        $n['preinfo'] = $n['preinfo'] ?: $newspage['title'];

                        if ($t == 2 && ($i + $t)%2) {
                            $temp[] = $n;
                            continue;
                        }

                        $i += $t;
                        $index += $t;

                        if (($i == 2 || $i == 4) && $t == 1) {
                            $n['subclass'] = ' mt-auto';
                        }

                        echo writeTemplate($n, '
                            <a class="article-preview#class##subclass#" href="#link#">
                                <div class="article-preview__img">#img#</div>
                                <div class="article-preview__desc">
                                    <div class="date">#preinfo# <span>/#data#</span></div>
                                    <div class="article-preview__title">#title#</div>
                                </div>
                                <div class="button-next _sm i-arrow"></div>
                            </a>
                        ');

                        

                        if ($i == 2) {
                            echo '</div>';
                        }

                        if ($temp) {
                            $temp = array_reverse($temp);
                            foreach ($temp as $t) {
                                if ($index >= 12)
                                    break;
                                deal(array_pop($temp));
                            }
                        }

                        if ($i == 4) {
                            echo '</div></div></div>';
                            $i = 0;
                        }
                    }

                    if ($i) {
                        echo '</div></div></div>';
                    }
                ?>

            </div>
            <div class="pagination-slider _dark"></div>
        </div>
    </div>
</section>