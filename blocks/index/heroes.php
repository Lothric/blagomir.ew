<?php
    $heroes = getSQLdata(PAGES_ITEM_QUERY, ID_ALLBLOCKS, HEROES_PAGES)[0];
    $people = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, PEOPLE_PAGES);
?>

<section class="heroes _index">
    <div class="container">
        <h2 class="heroes__title"><?= $sub['title'] ?></h2>
        <div class="heroes-slider">
            <div class="swiper-container">
                <div class="swiper-wrapper">

                    <?php
                        foreach ($people as $man) {
                            $hashtags = explode(', ', $man['name_hashtags']);

                            if (in_array($heroes['hashtags'], $hashtags)) {

                                if ($man['foto'][SPECIAL_PHOTOS]) {
                                    $man['preview'] = writeTemplate($man['foto'][SPECIAL_PHOTOS][0], '<img class="heroes-slider__foto" src="#picname#" alt="#title#">');
                                }

                                if ($man['foto'][MEDAL_PHOTOS]) {
                                    $man['medals'] = '<div class="medals">';
                                    $man['medals'] .= writeItemsTemplate($man['foto'][MEDAL_PHOTOS], '<img class="medal" src="#picname#" alt="#title#">');
                                    $man['medals'] .= '</div>';
                                }

                                $man['modal'] = MODAL_HEROES_PAGES;

                                echo writeTemplate($man, '
                                    <div class="swiper-slide">
                                        <a class="heroes-slider__item" calling="#id#" href="#modal-#modal#">
                                            <div class="heroes-slider__preview">#preview#</div>
                                            <div class="heroes-slider__desc">
                                                <div class="heroes-slider__title">#title#<span>#preinfo#</span></div>
                                                <p>#position#</p>
                                            </div>
                                            <div class="heroes-slider__bottom">
                                                #medals#
                                                <div class="heroes-slider__rank">#rank#</div>
                                            </div>
                                        </a>
                                    </div>
                                ');
                            }
                        }
                    ?>

                </div>
            </div>
            <div class="button-prev i-arrow"></div>
            <div class="button-next i-arrow"></div>
        </div>
    </div>
</section>