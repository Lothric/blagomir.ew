<script src="/libs/jquery/jquery-3.4.1.min.js" defer></script>
<script src="/libs/swiper/js/swiper.min.js" defer></script>
<script src="/libs/scroll/jquery.mousewheel.js" defer></script>
<script src="/libs/scroll/jquery.classyscroll.js" defer></script>
<script src="/libs/fancybox/jquery.fancybox.min.js" defer></script>
<script src="/libs/cleave/cleave.min.js" defer></script>
<script src="/libs/validate/jquery.validate.min.js" defer></script>
<script src="/js/ewjscore.js" defer></script>
<script src="/js/functions.js" defer></script>

<?php
    if ($p['id'] == PRESS_CENTER_PAGES) {
        echo '
            <script src="/libs/datapicker/js/datepicker.min.js" defer></script>
            <script src="/js/press-center.js" defer></script>
        ';
    }
?>

<script>
    var id_allblocks = <?= ID_ALLBLOCKS ?>;
    var special_photos = <?= SPECIAL_PHOTOS ?>;
    var central_slider_photos = <?= CENTRAL_SLIDER_PHOTOS ?>;
    var medal_photos = <?= MEDAL_PHOTOS ?>;
    var face_id = <?= MODAL_FACE_PAGES ?>;
    var regional_id = <?= MODAL_REGION_PAGES ?>;
    var message_id = <?= MODAL_MESSAGE_PAGES ?>;
    var heroes_id = <?= MODAL_HEROES_PAGES ?>;
    var biography = "<?= $p['biography'] ?>";
    var confpage = <?= CONF_PAGES ?>;
    var news_small_photos = <?= NEWS_SMALL_PHOTOS ?>;
</script>