<section class="section _feedback">
    <div class="container">
        <h2 class="ta-c"><?= $sub['title'] ?></h2>
        <div class="form-block">
            <?= $sub['info'] ?>
            <form class="form" name="joinform">
                <div class="form-group _half">
                    <input type="text" placeholder="Фамилия Имя" name="name">
                    <label>Введите Ваше имя</label>
                </div>
                <div class="form-group _half">
                    <input type="text" placeholder="+7 (XXX) XXX-XX-XX" name="tel">
                    <label>Введите Ваш телефон</label>
                </div>
                <div class="form-group">
                    <input type="text" placeholder="E-mail" name="email">
                    <label>Введите E-mail</label>
                </div>
                <!-- <div class="form-group _half">
                    <input type="text" placeholder="__ . __ . ____">
                    <label>Ваша дата рождения</label>
                </div> -->
                <div class="form-group">
                    <textarea type="text" placeholder="Введите текст" name="message"></textarea>
                    <label>Оставьте комментарий или вопрос</label>
                </div>
                <input type="hidden" value="3" name="mode">
                <input type="hidden" value="<?= $p['id'] ?>" name="pid">
                <div class="form-group">
                    <label class="file">
                        <input class="file__input" type="file">
                        <div class="file__doc"></div>
                        <div class="file__button">Прикрепить анкету</div>
                    </label>
                </div>
                <button class="btn-form" type="submit">ОТПРАВИТЬ СООБЩЕНИЕ</button>
                <img id="loader" src="/img/loader.gif" alt="loader" style="display:none;">
                <div class="form-group">
                    <label class="checkbox">
                        <input class="checkbox__input" type="checkbox" name="checkbox"><span class="checkbox__name"><?= $all['agreement'] ?></span>
                    </label>
                </div>
            </form>
        </div>
    </div>
</section>