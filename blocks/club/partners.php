<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="section">
    <div class="container">
        <h2><?= $sub['title'] ?></h2>
        <?= $sub['info'] ?>
        <div class="slider-left">
            <div class="swiper-wrapper">

                <?php
                    foreach ($sub['children'] as $child) {
                        if ($child['foto'][SPECIAL_PHOTOS]) {
                            $child['img'] = writeTemplate($child['foto'][SPECIAL_PHOTOS][0], '<img class="partner__img" src="#picname#" alt="#title#">');
                        }
                        echo writeTemplate($child, '
                            <div class="swiper-slide">
                                <a class="partner" href="#link#">
                                    <div class="partner__preview">#img#</div>
                                    <div class="partner__desc">#title#</div>
                                    <div class="partner__button">#more#</div>
                                </a>
                            </div>
                        ');
                    }
                ?>

            </div>
            <div class="pagination-number"></div>
            <div class="naviblock">
                <div class="button-prev _sm i-arrow"></div>
                <div class="button-next _sm i-arrow"></div>
            </div>
        </div>
    </div>
</section>