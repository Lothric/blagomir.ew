<section class="section _feedbackk">
    <div class="container">
        <h2 class="ta-c"><?= $sub['title'] ?></h2>
        <div class="form-block _dark">
            <h4>Заполните форму</h4>
            <p>Мы обязательно ответим и презвоним</p>
            <form class="form"  name="clubform">
                <div class="form-group _half">
                    <input type="text" placeholder="Фамилия Имя" name="name">
                    <label>Введите Ваше имя</label>
                </div>
                <div class="form-group _half">
                    <input type="text" placeholder="+7 (XXX) XXX-XX-XX" name="tel">
                    <label>Введите Ваш телефон</label>
                </div>
                <input type="hidden" value="2" name="mode">
                <input type="hidden" value="<?= $p['id'] ?>" name="pid">
                <div class="form-group">
                    <textarea type="text" placeholder="Введите текст" name="message"></textarea>
                    <label>Оставьте комментарий или вопрос</label>
                </div>
                <button class="btn-form" type="submit">ОТПРАВИТЬ СООБЩЕНИЕ</button>
                <img id="loader" src="/img/loader.gif" alt="loader" style="display:none;">
                <div class="form-group">
                    <label class="checkbox">
                        <input class="checkbox__input" type="checkbox" name="checkbox"><span class="checkbox__name"><?= $all['agreement'] ?></span>
                    </label>
                </div>
            </form>
        </div>
    </div>
</section>