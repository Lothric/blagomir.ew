<?php 
require_once('ewcore/bd.php');

//получаем информацию о возможных версия контента
$allverse = arrPersonality(getSQLdata (ALLBLOCK_QUERY),'id');
//$all = $allverse[getContentTypeId($allverse)]; //получаем основные данные от типа версии контента
$all = $allverse[1];//получаем данные от типа версии контента
define('ID_ALLBLOCKS', $all['id']);
//----------------------------------------------//

if(!$link=safeGetNumParam($_GET, 'p'))//проверка входных данных
alertAndRedirect();

if (!$p = getLightArray (getSQLdata (PAGES_ITEM_QUERY, ID_ALLBLOCKS, $link)))//получаем данные конктретной страницы включая фото
	alertAndRedirect();

$menu = getSQLdata (MENU_QUERY, ID_ALLBLOCKS, $link, $p['id_pages']);

foreach ($menu as $point) {
	if ($point['indownmenu']) {
		$downMenu[] = $point;
	}
}

// $downMenu = [];
$structPages = arrDeffindo($menu);//все страницы в один ряд без вложений по-строчно

$parentPages = getPraInfo ($structPages, $p['id_pages'], 'id_pages');//получаем иерархию предков страницы	

$error = getglossary(ERRORS_GLOSSARY);//получаем шаблоны ошибок

$wdgts = getglossary(WDGTS_GLOSSARY);

$all['clear_tel']="+".makeClearTel($all['tel']); //обрабатываем номер телефона чтобы вставить ссылкой
?>