<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="history">
    <div class="container">
        <div class="history__content">

            <?php
                if ($sub['foto'][SPECIAL_PHOTOS]) {
                    echo writeTemplate($sub['foto'][SPECIAL_PHOTOS][0], '<img src="#picname#" alt="#title#">');
                }
            ?>
            
            <div class="history__block">

                <?php
                    for ($i = 0; $i < count($sub['children']); $i += 2) {
                        echo writeTemplate($sub['children'][$i], '
                            <div class="history__item" style="#css_icon#">
                                <div class="history__year" style="#csspoint#">#title#<span>#preinfo#</span></div>
                                <a class="card" href="#">
                                    <div class="card__content">
                                        <div class="card__title">#info#</div>
                                        <div class="card__icon i-arrow"></div>
                                    </div>
                                </a>
                            </div>
                        ');
                    }
                ?>

            </div>

            <div class="history__block">

            <?php
                    for ($i = 1; $i < count($sub['children']); $i += 2) {
                        echo writeTemplate($sub['children'][$i], '
                            <div class="history__item" style="#css_icon#">
                                <div class="history__year" style="#csspoint#">#title#<span>#preinfo#</span></div>
                                <a class="card" href="#">
                                    <div class="card__content">
                                        <div class="card__title">#info#</div>
                                        <div class="card__icon i-arrow"></div>
                                    </div>
                                </a>
                            </div>
                        ');
                    }
                ?>

            </div>
        </div>
    </div>
</section>