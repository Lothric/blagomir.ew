<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="section classic-text _about _fon">
    <div class="container">
        <div class="about">
            <div class="about__desc">
                
                <?php
                    if ($sub['foto'][SPECIAL_PHOTOS])
                        echo writeTemplate($sub['foto'][SPECIAL_PHOTOS][0], '<img class="about-img" src="#picname#" alt="#title#">');
                ?>
                
                <p class="text"><?= $p['title'] ?></p>
                <h1><?= $sub['title'] ?></h1>
                <?= $sub['info'] ?>
            </div>
            <div class="about-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">

                        <?=
                            writeItemsTemplate($sub['children'], '
                                <div class="swiper-slide">
                                    <div class="about__item">
                                        <div class="about__number"><span>#title#</span>#info#</div>
                                        <div class="about__text">#dopinfo#</div>
                                    </div>
                                </div>
                            ');
                        ?>
                        
                    </div>
                </div>
                <div class="about-prev i-arrow"></div>
                <div class="about-next i-arrow"></div>
            </div>
        </div>
    </div>
</section>