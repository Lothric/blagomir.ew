<section class="section">
    <div class="container">
        <div class="cards <?= $sub['css_icon'] ?>">

            <?php
                foreach ($p['children'] as $child) {
                    if (!$child['hiddencontent']) {
                        echo writeTemplate($child, '
                            <a class="card" href="#link#">
                                <div class="card__content">
                                    <div class="card__title">#title#</div>
                                    <div class="card__desc">#preinfo#</div>
                                    <div class="card__icon i-arrow"></div>
                                </div>
                            </a>
                        ');
                    }
                }
            ?>

        </div>
    </div>
</section>