<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="section classic-text _activity">
    <div class="container">
        <div class="activity">
            <div class="activity__desc">
                <h2><?= $sub['title'] ?></h2>
                <?= $p['info'] ?>
            </div>
            <div class="activity__content">

                <?php
                    foreach ($sub['children'] as $index=>$child) {
                        $child['info'] = strip_tags($child['info']);

                        if ($index > 1)
                            $child['class'] = ' _lg';

                        echo writeTemplate($child, '
                            <div class="activity__item#class#">
                                <div class="activity__title">#title#</div>
                                <div class="activity__desc">#info#</div>
                            </div>
                    ');
                    }
                ?>

            </div>
        </div>
    </div>
</section>