<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="structural-units">
    <div class="container">
        <div class="structural-units__content">
            <div class="structural-units__desc">
                <h2><?= $sub['title'] ?></h2>
                <p class="text"><?= $sub['preinfo'] ?></p>
                <div class="structural-units__kvs">
                    
                    <?php
                        if ($sub['foto'][SPECIAL_PHOTOS]) {
                            echo writeTemplate($sub['foto'][SPECIAL_PHOTOS][0], '<img class="structural-units__kvs-img" src="#picname#" alt="#title#">');
                        }
                    ?>
                    
                    <div class="structural-units__kvs-text"><?= $sub['info'] ?><span><?= $sub['dopinfo'] ?></span></div>
                </div>
            </div>
            <div class="structural-units__items">

                <?php
                    foreach ($sub['children'] as $index=>$child) {
                        if ($index == 0) {
                            $child['subtitle'] = writeTemplate($child, '<div class="structural-units__item-title">#title#</div>');
                        }

                        echo writeTemplate($child, '
                            <div class="structural-units__item">
                                <div class="structural-units__block">
                                    #subtitle#
                                    <div class="structural-units__item-desc #css_icon#">#preinfo#</div>
                                    <div class="structural-units__item-number"><span>#info#</span>#dopinfo#</div>
                                </div>
                            </div>
                        ');
                    }
                ?>

            </div>
        </div>
    </div>
</section>