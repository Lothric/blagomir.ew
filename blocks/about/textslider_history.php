<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
    $history = getSQLdata(SINGLE_PAGES_ITEM_QUERY, ID_ALLBLOCKS, HISTORY_PAGES)[0];
?>

<section class="target _red">
    <div class="container">
        <div class="target-slider swiper-container">
            <div class="swiper-wrapper">

                <?=
                    writeItemsTemplate($sub['children'], '
                        <div class="swiper-slide">
                            <div class="target-slider__item">
                                <div class="target-slider__title">#title#</div>
                                <div class="target-slider__desc">#info#</div>
                            </div>
                        </div>
                    ');
                ?>

            </div>
            <div class="pagination-slider"></div>
        </div>
        <div class="target__right">
            <div class="target__year"><span class="target__year-number"><?= $sub['preinfo'] ?></span><span class="target__text"><?= $sub['info'] ?></span><?= $sub['dopinfo'] ?></div><a class="button" href="<?= $history['link'] ?>"><?= $sub['banner'] ?></a>
        </div>
    </div>
</section>