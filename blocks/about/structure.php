<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);

    $people = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, PEOPLE_PAGES);
?>

<section class="fund-structure">
    <div class="container">
        <h2><?= $sub['title'] ?></h2>
        <div class="tabs">
            <div class="tabs__labels">

                <?php

                    $i = 0;

                    foreach ($sub['children'] as $child) {

                        if ($child['id'] != PEOPLE_PAGES) {
                            if ($i == 0) {
                                $child['class'] = ' _active';
                                $i++;
                            }

                            echo writeTemplate($child, '<div class="tabs__label#class#">#title#</div>'); 
                        }
                    }
                ?>

            </div>
            <div class="tabs__contents">

                <?php

                    $i = 0;

                    foreach ($sub['children'] as $child) {

                        $child['slides'] = '';

                        if ($child['id'] != PEOPLE_PAGES) {
                            if ($i == 0) {
                                $child['class'] = ' _active';
                                $i++;
                            }

                            if ($child['id'] != HEROES_PAGES) {

                                foreach ($people as $man) {
                                    $hashtags = explode(', ', $man['name_hashtags']);

                                    if (in_array($child['hashtags'], $hashtags)) {

                                        if ($man['foto'][SPECIAL_PHOTOS]) {
                                            $man['preview'] = writeTemplate($man['foto'][SPECIAL_PHOTOS][0], '<img class="team__foto" src="#picname#" alt="#title#">');
                                        }

                                        if ($man['foto'][MEDAL_PHOTOS]) {
                                            $man['medals'] = '<div class="medals">';
                                            $man['medals'] .= writeItemsTemplate($man['foto'][MEDAL_PHOTOS], '<img class="medal" src="#picname#" alt="#title#">');
                                            $man['medals'] .= '</div>';
                                        }

                                        $man['modal'] = MODAL_FACE_PAGES;

                                        $child['slides'] .= writeTemplate($man, '
                                            <div class="swiper-slide">
                                                <div class="team__item">
                                                    <div class="team__top">
                                                        #medals#
                                                        <div class="team__preview">#preview#</div>
                                                    </div>
                                                    <div class="team__info">
                                                        <div class="team__name">#title#<span>#preinfo#</span></div>
                                                        <div class="team__desc">#position#</div><a class="team__biography" calling="#id#" href="#modal-#modal#">#biography#</a>
                                                    </div>
                                                </div>
                                            </div>
                                        ');
                                    }
                                }
                                
                                echo writeTemplate($child, '
                                    <div class="tabs__content#class#">
                                        <div class="team-slider">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    #slides#
                                                </div>
                                            </div>
                                            <div class="button-prev i-arrow"></div>
                                            <div class="button-next i-arrow"></div>
                                        </div>
                                    </div>
                                ');
                            }

                            else {

                                foreach ($people as $man) {
                                    $hashtags = explode(', ', $man['name_hashtags']);

                                    if (in_array($child['hashtags'], $hashtags)) {

                                        if ($man['foto'][SPECIAL_PHOTOS]) {
                                            $man['preview'] = writeTemplate($man['foto'][SPECIAL_PHOTOS][0], '<img class="heroes-slider__foto" src="#picname#" alt="#title#">');
                                        }

                                        if ($man['foto'][MEDAL_PHOTOS]) {
                                            $man['medals'] = '<div class="medals">';
                                            $man['medals'] .= writeItemsTemplate($man['foto'][MEDAL_PHOTOS], '<img class="medal" src="#picname#" alt="#title#">');
                                            $man['medals'] .= '</div>';
                                        }

                                        $man['modal'] = MODAL_HEROES_PAGES;

                                        $child['slides'] .= writeTemplate($man, '
                                            <div class="swiper-slide">
                                                <a class="heroes-slider__item" calling="#id#" href="#modal-#modal#">
                                                    <div class="heroes-slider__preview">#preview#</div>
                                                    <div class="heroes-slider__desc">
                                                        <div class="heroes-slider__title">#title#<span>#preinfo#</span></div>
                                                        <p>#position#</p>
                                                    </div>
                                                    <div class="heroes-slider__bottom">
                                                        #medals#
                                                        <div class="heroes-slider__rank">#rank#</div>
                                                    </div>
                                                </a>
                                            </div>
                                        ');
                                    }
                                }
                                
                                echo writeTemplate($child, '
                                    <div class="tabs__content#class#">
                                        <div class="heroes-slider">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    #slides#
                                                </div>
                                            </div>
                                            <div class="button-prev i-arrow"></div>
                                            <div class="button-next i-arrow"></div>
                                        </div>
                                    </div>
                                ');

                            }

                        }
                    }
                ?>

            </div>

        </div>
    </div>
</section>