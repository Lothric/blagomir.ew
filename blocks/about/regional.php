<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="regional-government">
    <div class="container">
        <h2><?= $sub['title'] ?></h2>

        <div class="regional-government__content">
            
            <?php
                foreach ($sub['children'] as $child) {
                    if ($child['foto'][SPECIAL_PHOTOS]) {
                        $child['foto'][SPECIAL_PHOTOS][0]['css'] = $child['csspoint'];
                        $child['foto'][SPECIAL_PHOTOS][0]['title'] = $child['foto'][SPECIAL_PHOTOS][0]['title'] ?: $child['title'];
                        $child['img'] = writeTemplate($child['foto'][SPECIAL_PHOTOS][0], '<img class="regional-government__icon-img" src="#picname#" alt="#title#" style="#css#">');
                    }

                    $child['modal'] = MODAL_REGION_PAGES;

                    echo writeTemplate($child, '
                        <a class="regional-government__icon" calling="#id#" href="#modal-#modal#" style="#css_icon#">
                            #img#
                            <div class="regional-government__icon-text">#title#</div>
                        </a>
                    ');
                }
            ?>
            
            <img class="regional-government__map" src="/img/regional-government/map.png" alt="">
        </div>

        <div class="regional-government__items">

            <?php
                foreach ($sub['children'] as $child) {
                    
                    if ($child['foto'][SPECIAL_PHOTOS]) {
                        $child['foto'][SPECIAL_PHOTOS][0]['title'] = $child['foto'][SPECIAL_PHOTOS][0]['title'] ?: $child['title'];
                        $child['img'] = writeTemplate($child['foto'][SPECIAL_PHOTOS][0], '<img class="regional-government__img" src="#picname#" alt="#title#" style="#info#">');
                    }

                    $child['modal'] = MODAL_REGION_PAGES;

                    echo writeTemplate($child, '
                        <a class="regional-government__item" calling="#id#" href="#modal-#modal#">
                            <div class="regional-government__preview">
                                #img#
                            </div>
                            <div class="regional-government__desc">
                                <div class="regional-government__name">#title#</div>
                                <div class="regional-government__city">#preinfo#</div>
                                <div class="regional-government__location">#banner#</div>
                                <div class="regional-government__more">#more#</div>
                            </div>
                        </a>
                    ');
                }
            ?>

        </div>
    </div>
</section>