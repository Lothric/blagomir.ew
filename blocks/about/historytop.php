<section class="section classic-text">
    <div class="container">
        
        <?php
            if ($sub['foto'][SPECIAL_PHOTOS]) {
                echo writeTemplate($sub['foto'][SPECIAL_PHOTOS][0], '<img class="img-top" src="#picname#" alt="#title#">');
            }

            if ($parentPages && $parentPages[0] && $index == 0) {
                echo writeTemplate($parentPages[count($parentPages) - 1], '
                    <a class="go-back" href="#link#">
                        <div class="button-prev i-arrow _sm"></div>
                        #title#
                    </a>
                ');
            }
        ?>

        <h1><?= $sub['title'] ?></h1>
        <?= $sub['info'] ?>
    </div>
</section>