<section class="section">
    <div class="container">
        <h2><?= $sub['title'] ?></h2>
        <p class="text"><?= $sub['info'] ?></p>
        <div class="slider-left _partner">
            <div class="swiper-wrapper">

                <?php
                    if ($sub['foto'][SPECIAL_PHOTOS])
                        echo writeItemsTemplate($sub['foto'][SPECIAL_PHOTOS], '
                            <div class="swiper-slide">
                                <a class="partner" target="_blank" rel="noreferrer" href="#href#">
                                    <div class="partner__preview _left"><img class="partner__img" src="#picname#" alt="#title#"></div>
                                    <div class="partner__desc">#title#</div>
                                </a>
                            </div>
                        ');
                ?>

            </div>
            <div class="pagination-number"></div>
            <div class="naviblock">
                <div class="button-prev _sm i-arrow"></div>
                <div class="button-next _sm i-arrow"></div>
            </div>
        </div>
    </div>
</section>