<?php
    $sub['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $sub['id']);
    $years = getSQLdata(YEARS_QUERY, ID_ALLBLOCKS, $sub['id']);
?>

<section class="section _conferences">
    <div class="container tabs">
        <h2 class="special"><?= $sub['title'] ?>

            <div class="tabs__labels">
                <?php
                    foreach ($years as $index=>$year) {
                        if ($index == 0) {
                            $year['class'] = ' _active';
                        }

                        echo writeTemplate($year, '<div class="tabs__label#class#">#year# год</div>');
                    }
                ?>
                
            </div>
        </h2>
        <div class="tabs__contents">

            <?php

                foreach ($years as $index=>$year) {
                    $amount = count(getSQLdata(NEWS_PER_YEAR_QUERY, ID_ALLBLOCKS, $sub['id'], $year['year'], ''));
                    $confs = getSQLdata(NEWS_PER_YEAR_QUERY, ID_ALLBLOCKS, $sub['id'], $year['year'], 'LIMIT 6');

                    $class = $index == 0 ? ' _active' : '';

                    echo '<div class="tabs__content' . $class . '"><div class="conferences">';
                    foreach ($confs as $conf) {

                        if ($conf['foto'][NEWS_SMALL_PHOTOS])
                            $conf['img'] = writeTemplate($conf['foto'][NEWS_SMALL_PHOTOS][0], '<img src="#picname2#" alt="#title#">');

                        $conf['data'] = dataforhuman($conf['data']);

                        echo writeTemplate($conf, '
                            <a class="article-preview" href="#link#">
                                <div class="article-preview__img">#img#</div>
                                <div class="article-preview__desc">
                                    <div class="date">#data#</div>
                                    <div class="article-preview__title">#title#</div>
                                </div>
                                <div class="button-next _sm i-arrow"></div>
                            </a>
                        ');
                    }
                    echo '</div>';

                    if ($amount > 6) {
                        $pages = (int)($amount/6) + 1;

                        echo '<div class="pagination">';

                            for ($i=1; $i<=$pages; $i++) {
                                $c = $i == 1 ? ' _active' : '';

                                echo '<a class="pagination__item confpage' . $c . '" href="#">' . $i . '</a>';
                                
                            }

                        echo '</div>';
                    }

                    echo '</div>';
                }
            ?>
            
            </div>
        </div>
    </div>
</section>