<?php
/**
 * Класс для инкапсуляции функций работы с url - кодирование (генерация) и декодирование (парсинг)
 * Включает в себя кеширование результата выборки
 * @author Def
 */
class UrlManager {
  /**
   * PAGESHASH должен быть массив полученный из файла хэша с перечнем pages (атрибуты id, link, url, childlink). 
   * причем $PAGESHASH[12]["id"]==12 . Т.о. это ассоциативный массив, 
   * где порядковый номер элемента массива изначально соответствует его pages.id
   */
  private static $pageHash = [];
  private static $cacheDir = '/blocks';
  
  const DEFAULT_LINK='pages.php'; //страница по умолчанию, если нет link или childlink
  const MAIN = '/index.php';
  const NOT_FOUND = '404.php';
  const CACHE_FILENAME = 'url.cache';

  /**
   * Функция найдет нужную запись pages в хэше и построит до него полный путь красивых url
   * вида /news/342
   * @param int id - pages.id искомой страницы
   * @param bool norec - флаг наличия рекурсии
   
   */
  public static function createUrl($id, $norec=false) {
    $pull = self::getPageHash();
	
	if ($id==1)//главная страница
		return '/';
	
	/*
	if ($pages AND $pages['hiddencontent'])//у страницы скрытый контент
	{	//return nearest($pages['children'][0]['link'], '/').'#1';
		$pull[$id]['url'] = nearest($pages['children'][0]['link'], '/');//передаем ссылку на дочернюю страницу или на главную, контент закрыт
	}
  */
	
	if(@$pull[$id]['hiddencontent'] && !$norec)//генерацию url с учетом hiddencontent на уровне urlmanager
	{
		//echo self::createUrl($pull[$id]['id_firstchild']).'#<br>';
	  return (isset($pull[$id]['id_firstchild']) && $pull[$id]['id_firstchild']) ? self::createUrl($pull[$id]['id_firstchild']) : ((isset($pull[$id]['id_pages']) && $pull[$id]['id_pages']) ? self::createUrl($pull[$id]['id_pages']) : "/");
	}
	
    //----------------------
	
	if($pull[$id]['cansearch']==0)
		$pull[$id]['url'] = "/";	
	
    //формирование url узла
    if (@$pull[$id]['url']) 
	{
     $url = $pull[$id]['url'];
	 
	 if ((strpos (@$pull[$id]['url'], 'https') === 0) || (strpos (@$pull[$id]['url'], 'http') === 0) || (strpos (@$pull[$id]['url'], '/') === 0))//задан внешний url или относительный url
		return $pull[$id]['url'];
    } 
	else $url = $id;
		
	if (@$pull[$id]['id_pages'] != 0) {
     //конечный узел не достигнут - рекурсия (исключение для меню)
      return self::removeSlash (self::createUrl($pull[$id]['id_pages'], true) . $url . '/');
    } 
	else return  self::removeSlash ('/' .$url. '/') ; //конечный узел достингут
  }

  /**
   * Вернет кеш url в памяти, прочитает если он пуст
   * @return type
   */
  public static function getPageHash() {
    if (empty(self::$pageHash)) {
      self::readCache();
    }
    return self::$pageHash;
  }
  
  /**
   * Функция будет разбирать красивый url вида /news/342 и выдавать в виде имени файла, который нужно подключить
   * @param type $url
   * @return type
   */
  public static function parseUrl($url) {
    // обрежем последний /
    if (substr($url, -1) == '/')
      $url = substr($url, 0, strlen($url) - 1);

    // возмем последний параметр до разделителя /
    $parts = array_reverse(explode('/', $url));
	$parts_kol = count($parts);
	$first = $parts[$parts_kol-1];
    $last = $parts[0];
    if ($last == '' || $last == '1' || $last == '0') {
      include_once self::MAIN;
	  return;
    }
	
	/*
	if ($parts_kol==2 && ($parts[$parts_kol-1]=='tours' || $parts[$parts_kol-1]=='hotels'))//значит попалась ссылка на тур или отель
	{
		$_GET['ts_slug']=$parts[0];
		unset($parts[0]);
		//print_r($parts); exit();
	}
	*/
	if ($parts_kol==2 && $parts[$parts_kol-1]=='projects')//значит попался проект
	{
		unset($parts[1]);//отвязываем его чтобы информация нашлась. приходится использовать костыль
		//print_r($parts); exit();
	}
	
	// заготовки для запроса
    $select = 'SELECT p1.id, p1.url, p1.link';
    $from = ' FROM pages p1';
    $where = ' WHERE ';
    
    $level = 1;
    // переберем все переданные параметры
    foreach ($parts as $url_part) {
      if ($level > 1 ) {
		  if ($level < 3 )
         $select .= ', p'.$level.'.childlink';
		  
         $from .= sprintf(' INNER JOIN pages p%d ON p%d.id = p%d.id_pages', $level, $level, $level-1);
      }
      $where .= self::getWhereForParam($url_part, $level);
      $level++;
    }

    $query = $select.$from.$where.' AND p1.hiddencontent=0 AND p1.visible=1';
	//exit( $query);
    $result = ew_mysqli_query($query);
    if ($result !== FALSE) {
      if ($page = mysqli_fetch_assoc($result))
	  {
		  $file = nearest($page['link'], $page['childlink'] , self::DEFAULT_LINK );
		  if ($file) {
			$_GET['p'] = $page['id'];
	//        echo '/' . $file;
			include_once $file;
			return;
		  } 
	  }
	  else 
	  {
		// include_once self::NOT_FOUND;//ошибка 404
		return;
	  }
    } else {
      // TODO на период разработки
      echo mysqli_error().'<br/>'; return;
    }
    echo $query.'<br/>';
    echo 'nothing found';
	//	include_once self::NOT_FOUND;
  }
  
  /**
   * создание файла кеша
   */
  public static function rewriteCache() {
    // запрос к базе
    $query = 'SELECT id, id_pages, link, childlink, url, hiddencontent, cansearch, (SELECT p.id FROM pages as p WHERE pages.hiddencontent=1 AND p.id_pages IS NOT NULL AND p.hiddencontent=0 AND pages.id=p.id_pages  order by p.npp asc limit 1) as id_firstchild FROM pages';
    $result = ew_mysqli_query($query);
    if ($result === false) 
	{
      // echo mysqli_error();
      return;
    }
    $hash = array();
    while ($row = mysqli_fetch_assoc($result)) 
	{
      $hash[$row['id']] = $row;
    }
    mysqli_free_result($result);
    // сохраним у себя 
    self::$pageHash = $hash;

    // запись в файл
    $filename = self::getCacheFilename();
    $handle = fopen($filename, "w+");

    //Lock File, error if unable to lock
    if (flock($handle, LOCK_EX)) {
      $serial = serialize($hash);

      fwrite($handle, $serial);

      flock($handle, LOCK_UN);
      fclose($handle);
    } else {
      echo "Could not Lock File!";
    }
  }

  /**
   * Чтение из кеша файла
   */
  public static function readCache() {
    $filename = self::getCacheFilename();
    // чтение из кеша
    $handle = fopen($filename, "r", 1);
	
    if ($handle) 
	{
      $serial = fread($handle, filesize($filename));
      fclose($handle);
      // десериализуем из файла 
      self::$pageHash = unserialize($serial);
    } 
	else 
	{
      // если файла нет, пересоздать (да, могут плодиться файлы, пока ничего лучше не придумал)
      //self::rewriteCache();
    }
//    print_r(self::$PAGEHASH);
  }

  /**
   * Вернет часть условия where для функции parseUrl
   * @param type $param
   * @param type $level
   * @return string
   */
  private static function getWhereForParam($param, $level = 1) {
    if ($param) {
      $and = $level > 1 ? ' AND ' : '';
      if (is_numeric($param)) {
        return $and . 'p' . $level . '.id=' . $param;
      } else {
        $param = ew_mysqli_real_escape_string($param);
        return $and . 'p' . $level . '.url="' . $param . '"';
      }
    }
    return "";
  }
  
  private static function getCacheFilename() {
    return $_SERVER['DOCUMENT_ROOT'].self::$cacheDir.'/'.self::CACHE_FILENAME;
  }
  
  /**
   * Вернет часть url без лишних / вконце
   * @param string $url
   * @return string
   */
  private static function removeSlash ($url) 
  {
    return str_replace ('#/', '', $url);
  }
}
