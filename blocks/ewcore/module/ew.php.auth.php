<?php 
/* Класс для ведения авторизации  */
class Auth 
{
	private $sessionName, $cookieName, $myhost, $ID;
	const ID_PARAM = 'id';//ассоциативный ключ параметра идентификатора
	/* const QUERY = "SELECT p.id, p.email, p.tel, p.fam, p.name, p.ot, p.gold, p.city, p.adr,
	CONCAT_WS(' ', fam, name, ot) as fio,
	CONCAT_WS(', ', pindex, city, adr) as dostadr
	FROM people as p 
	 where %s AND p.aktiv = 1 limit 1"; //запрос на восстановление данных */
	
	const QUERY = "SELECT p.id, p.email, p.tel, p.fam, p.name, p.ot, p.gold, p.city, p.adr,
	CONCAT_WS(' ', fam, name, ot) as fio,
	CONCAT_WS(', ', pindex, city, adr) as dostadr
	FROM people as p 
	 where %s limit 1"; //без проверки на активацию

	public $err = array('authFalse'=>"Вы не вошли в систему", 'passFalse'=>'Неверный логин или пароль');
	public $authStatus=false;
	
	function  __construct ($sname="auth") //конструктор
	{
		$HOST_NAME = HOST_NAME;//глобальное имя домена сайта
		
		$this->myhost = $HOST_NAME;
		$this->sessionName = $sname;
		session_set_cookie_params(7200, "/", $this->myhost, false, false);
		session_name($this->sessionName); 
		session_start();   
		
		//$this->err = getglossary (self::ERR_CODE, 'id_glossary', 0, 0 , 'npp'); //сбор кодов ошибо         
		
		$this->ID=0;//по-умолчанию  
	}
	
	
	/* 
	Метод возвращающий массив с данными из БД
	 */
	protected function getDataFromSQL ($subquery=false)
	{
		if ($subquery)
		{ 
			$query = sprintf(self::QUERY, $subquery);
			
			$result2 = ew_mysqli_query($query);//подключаем БД
			if ($p = mysqli_fetch_assoc($result2))
			{
				$this->ID=$p[self::ID_PARAM];
				$this->clearData();
				$this->setData($p);//обновляем данные в сессии
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	/* 
	Метод возвращающий значение из сессии по ключу $key	
	*/
	public function getData ($key)
	{
		if ($_SESSION[$key]!='' && $_SESSION[$key]!=NULL)
			return $_SESSION[$key];
		elseif ($key=='all')
			return $_SESSION;
		else return '';
	}
	
	/* Метод принимающий на сохранение в сессию данные
	array mass - ассоциативный массив. Его ключ=значение сохраняем в сессии
	*/
	public function setData ($mass=array())
	{
		foreach ($mass as $i => $t)
			$_SESSION[$i]=$t;
		return true;
	}
	
	/* оичстка данных сессии*/
	public function clearData()
	{
		foreach ($_SESSION as $t)
			unset($t);
		return;
	}
	
	/* Функция проверки на существование сессии
	bool $param - ключ подзагрузки информации
	
	 */
	public function checkLegal ($param=false, $kaput=true)
	{
		if (count ($_SESSION)>1)
		{
			if ($param)//точечная проверка
			{
				$this->ID=$this->getData(self::ID_PARAM);
				if ($this->getDataFromSQL ('p.id='.$this->ID))
				{
					$this->authStatus=true;
				}
				else $this->authStatus=false;
				
			}
			else $this->authStatus=true;
		}
		else //если не существует
		{
			if ($kaput)
				$this->sessionKaput();
			
			$this->authStatus = false;
		}
		return $this->authStatus;
	}
	
	/* Метод разрыва сессии */
	public function sessionKaput ($full=true)
	{
		@$this->clearData ();//очистка массива сессии
		unset ($_SESSION);
		//unset($this);
		
		if ($full)//если полное уничтожение закрываем вообще сессию
		@session_destroy(); 
		
		return;
	}
	
	/*Авторизация через логин и пароль*/
	public function comeIn ($login=false, $password=false)
	{
		if ($login && $password)
		{
			$login=clearQuatsch($login);
		 	$password=clearQuatsch($password);
			
			return $this->getDataFromSQL(" p.email like '".$login."' AND p.password like '".sha1($password)."' ");
		}
		else //неудачная авторизация
		{
			$this->sessionKaput ();
		}
		
		return false;
	}
	/*------------------------------------------------*/
	
	
	
}


class People extends Auth
{
	/*Авторизация через id*/
	public function comeInPerId ($pid)
	{
		if ($pid)
		{
			$pid=clearQuatsch($pid);
			
			$this->authStatus=true;
			
			return $this->getDataFromSQL(" p.id = '".$pid."' ");
		}
		else //неудачная авторизация
		{
			$this->sessionKaput ();
		}
		
		return false;
	}
	/*------------------------------------------------*/
	
	/*Авторизация через id*/
	public function comeInPerULogin ($uid, $network, $email)
	{
		$dat=[
			'uid'=>$uid,
			'network'=>$network,
			'email'=>$email
		];
		
		if (safeGetParam($dat, 'email', 'EMAIL') && safeGetParam($dat, 'uid', 'ALL') && safeGetParam($dat, 'network', 'CHAR'))//если есть минимальный набор данных
		{
			return $this->getDataFromSQL(" p.uid='".$dat['uid']."' AND p.network='".$dat['network']."' and p.email='".$dat['email']."' ");
		}
		else //неудачная авторизация
		{
			$this->sessionKaput ();
		}
		
		return false;
	}
	/*------------------------------------------------*/
	
	/*получение информации о пользователе*/
	public static function onlyGetInfo ($pid=0)
	{
		if (!$pid) return ($_SESSION);
		else return ($_SESSION['id']==$pid) ? $_SESSION : mysqli_fetch_assoc(ew_mysqli_query(sprintf(self::QUERY, "p.id = '".$pid."'")));
	}
}
		
?>