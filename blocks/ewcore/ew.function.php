<?php
/* 
 * Библиотека функций empire web
 */
function ew_mysqli_affected_rows(&$database=NULL)
{
	global $db;
	
	$database = $database ?: $db;//если ссылка на подключение не передана, то по-умолчанию $db
	
	return mysqli_affected_rows ($database);
}

function ew_mysqli_real_escape_string ($param, &$database=NULL)
{
	global $db;
	
	$database = $database ?: $db;//если ссылка на подключение не передана, то по-умолчанию $db
	
	return mysqli_real_escape_string ($database, $param);
}

function ew_mysqli_error (&$database=NULL)
{
	global $db;
	
	$database = $database ?: $db;//если ссылка на подключение не передана, то по-умолчанию $db
	
	return /* mysqli_connect_error($database).' '. */mysqli_error($database);
}

/*функция запроса к бд для получения последнего ключа insert, чтобы обходиться без передачи $db */
function ew_mysqli_insert_id (&$database=NULL)
{
	global $db;
	
	$database = $database ?: $db;//если ссылка на подключение не передана, то по-умолчанию $db
	
	return mysqli_insert_id($database);
}

/*функция запроса к бд, чтобы обходиться без передачи $db */
function ew_mysqli_query ($query, &$database=NULL)
{
	global $db;
	
	$database = $database ?: $db;//если ссылка на подключение не передана, то по-умолчанию $db
	
	return mysqli_query($database, $query);
}



/*функция мультизапроса запроса к бд, чтобы обходиться без передачи $db */
function ew_mysqli_multi_query ($query, &$database=NULL)
{
	global $db;
	
	$database = $database ?: $db;//если ссылка на подключение не передана, то по-умолчанию $db
	
	return mysqli_multi_query($database, $query);
}

//выводит сообщение в js через php
function phpalert ($brif)
{
	echo "<script>alert ('".htmlspecialchars(trim( str_replace(array("\r\n", "\r", "\n"), '', $brif)), ENT_QUOTES)."');</script>";
}

//выводит сообщение в js через php в красиовм окне
function ewalert ($brif)
{
	echo "<script>window.onload = function () {
		$(function (){
			  ewalert ('".trim( str_replace(array("\r\n", "\r", "\n"), '', $brif))."');
			
			});
  
   };
   </script>";
}

/**
 * вывод текста с обрезанием по количеству символов с потерей HTML тэгов
 * @param type $news текст 
 * @param type $kolsymbol - количество символов
 * @param type $symbol символ остановки обрезания. Например если пробел, то текст обрежется до последнего пробела входящего в указанное количество символов 
 * @param type $end - подставновка в конце
 */
function badnews($news, $kolsymbol, $symbol=' ', $end = "...") {
 
 $news = strip_tags($news);
	
	if (strlen ($news)>$kolsymbol)
	{
		$news = substr($news, 0, $kolsymbol);
		
		$news = rtrim($news, "!,.-");
		
		$news = substr($news, 0, strrpos($news, $symbol));
		
		$news.=$end;
	}
	
	return $news;
}
//-------------------------------------------------


/**
 * Выводим сообщение в js и редиректим на нужный url
 * @param type $message текст сообщения
 * @param type $url
 */
function alertAndRedirect($message="Страница не найдена!!!", $url=false) {
	
	if ($message)
  		phpalert($message);
  
  	if (!$url)
	{
		$t=explode ('/',$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		$n=count($t);
		$preurl=$t[0]; 
		unset($t[0]);
		unset($t[$n-1]);
		if ($t[$n-1]=='')//если последний элемент пуст, возможно забыли последний слэш
			unset($t[$n-2]);
		
		$url=$preurl;
		
		if (count($t)>0)
		$url.= '/'.implode('/',$t).'/';
			
		
	}
	
  
  echo'<p>'.$message.'</p><META HTTP-EQUIV="Refresh" CONTENT="0; URL=http://'.$url.'">';
  exit();
}



/**
 * Вернет безопасное представление параметра из массива в соответствии с типом проверки, либо значение по умолчанию
 * @param type $array массив, например, $_GET или $_POST
 * @param type $name имя параметра
 * @param type $default значение по умолчанию
 * @array type $query SQL запрос на вывод значения для сравнения на уникальность: table - название таблицы, pole - поле, subquery - дополнительное условие
 * @return param
 */
function safeGetParam($array, $name, $type=0, $default='', $query=NULL) {
	
    $res = $default;
	$maxbukv = 800;
	
	if(isset($array[$name]))
		switch($type)
		{
			case 'ALL':
			case 'CHAR':
			case 'TEXT': //тескт
			if ($array[$name]!='')
			{
				$res = addslashes(trim(strip_tags($array[$name])));
				
				if (strlen($res)<=$maxbukv)  
				$res = badnews($array[$name], $maxbukv+100);
			}
			break;
			
			case 'HTML': //html
			if ($array[$name]!='')  
			$res = addslashes(trim($array[$name]));
			break;
			
			case 'DATA': //дата в чел формате
			if ( preg_match("/^(\d{1,2}\.\d{1,2}\.\d{4})$/", $array[$name]))  
			$res = trim(strip_tags($array[$name]));
			break;
			
			case 'INT': //число
			if ( preg_match("/^[\d]+$/", $array[$name]))  
			$res = trim(strip_tags($array[$name]));
			break;
			
			case 'GOLD': //число
			if ( preg_match("/^\d*\.\d{2}$/", $array[$name]))  
			$res = trim(strip_tags($array[$name]));
			break;
			
			case 'EMAIL': //email
			if (preg_match("/(@){1}/", $array[$name]))  
			$res = trim(strip_tags($array[$name]));
			break;
			
			case 'RUSCHAR': //пароль
			if (preg_match("/^([А-Яа-я1-9-. ]+)$/u", $array[$name]))  
			$res = trim($array[$name]);
			break;
			
			case 'PASS': //пароль
			if (preg_match("/^([A-Za-z0-9]){6,10}$/", $array[$name]))  
			$res = trim($array[$name]);
			break;
			
			case 'LOGIN': //логин
			if (preg_match("/^([0-9]){6}$/", $array[$name]))  
			$res = trim($array[$name]);
			break;
			
			case 'AKT': //логин
			if (preg_match("/^([A-Za-z0-9]){8}$/", $array[$name]))  
			$res = strip_tags(trim($array[$name]));
			break;
			
			case 'TEL': //логин
			if (preg_match("/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/", $array[$name]))  
			$res = trim($array[$name]);
			break;
			
			case 'JSON': //json строка
				if (isJSON($array[$name])) 
					$res = 	json_decode(str_replace('\"', '&quot;', $array[$name]), true);
				else $res = [];
			break;
			
			case 'UNIQUE': //уникальное значение
				if ($query && ($t=safeGetParam($array,$name,'CHAR', false)))
				{
					$result = ew_mysqli_query(sprintf($query, $t));
					
					if ($pages = mysqli_fetch_assoc($result))
					return true;  
				}
				return false;
			break;
			
			case 'SEARCH_TEXT': //тескт
				if ($array[$name]!='')
				{
					$res = htmlspecialchars (trim(strip_tags($array[$name])));
					
					if (mb_strlen($res)<=100)  
					$res = badnews($array[$name], $maxbukv+100);
				}
			break;
			
			default: break;
		}
    
    return $res;    
}

/*является ли строка json*/
function isJSON($string){
   return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

/**
 * Вернет безопасное числовое представление параметра из массива, либо значение по умолчанию
 * @param type $array массив, например, $_GET или $_POST
 * @param type $name имя параметра
 * @param type $default значение по умолчанию
 * @return type
 */
function safeGetNumParam($array, $name, $default=0) {
    $res = $default;
    if(isset($array[$name]) && preg_match("/^[\d]+$/", $array[$name])) {
        $res = intval($array[$name]);
    }
    return $res;    
}


/**
 * Вернет безопасное текстовое представление параметра из массива, либо значение по умолчанию
 * @param type $array массив, например, $_GET или $_POST
 * @param type $name имя параметра
 * @param type $default значение по умолчанию
 * @return type
 */
function safeGetTextParam($array, $name, $default='') {
    $res = $default;
    if(isset($array[$name]) && $array[$name]!='') {
        $res = trim(htmlspecialchars($array[$name]));
    }
    return $res;    
}

/**
 * Вернет первую непустую строку из переданных аргументов, если все пустые, вернет пустую строку ''
 * @return string
 */
function nearest($arg_list) {
  $arg_list = func_get_args();
  foreach ($arg_list as $arg) {
    if (isset($arg) && ($arg!='' || $arg!=NULL || (is_array($arg) && count($arg)))) 
	return $arg;
  }
  return '';
}

/**
 * Вернет первое положительное число из переданных аргументов, если все 0, вернет 0
 * @return number
 */
function nearestInt($arg_list) {
  $arg_list = func_get_args();
  foreach ($arg_list as $arg) {
    if ($arg>0) 
	return $arg;
  }
  return 0;
}

/**
 * Вернет первый непустой массив из переданных аргументов, если все пустые, вернет пустой массив
 * @return array
 */
function nearestArr(...$arg_list) {
  $res = [];
  foreach ($arg_list as $arg) {
    if (is_array($arg) && count($arg)) 
		return $arg;
  }
  return $res;
}

/* 
Функция получения id прородителя по по дереву таблицы (всплытие)

* @param type $currentId - текущий id элемента (число)
	
* @param type $nameTable - наименование таблицы где происходит поиск
 
* @param type $nameAttr - наименование аттрибута по которому поднимаемся:
	id
	pages.npp
* @param type $nameRetAttr - наименование возвращаемого аттрибута:
	id_pages* @param type $limit - предел подъема:
 	0 - поднимаемся, пока $attr не станет = 0
	..
	2 - поднимаемся на 2 прородителя (отец, дед)
	..
 
 * @return type int: значение возвращаемого атрибута $nameRetAttr
 */
function getpraded ($currentId, $nameTable, $nameAttr="id", $nameRetAttr="id_pages")
{
	while ($currentId!=0)
	{	
		$result = ew_mysqli_query("SELECT `$nameRetAttr` from `$nameTable` 
		where `$nameAttr`='$currentId' limit 1");
		if ($g = mysqli_fetch_assoc($result))
		{
			if ($g[$nameRetAttr]==0)
			return $currentId;
			else return getpraded ($g[$nameRetAttr], $nameTable, $nameAttr, $nameRetAttr);
		}
		else return;
	}
	return;

}


/*
 
Функция отправки письма с заботой о кодировке и т.п. 
В общем по фенщую

* @param type $sendfrom - отправитель письма (имя):
	Сайт spln.ru
	Администрация
	
* @param type $sendfromEmail - email отправителя письма
 
* @param type $sendto - получатель, или получатели письма:
	user@example.com
	user@example.com, anotheruser@example.com
	User <user@example.com>
	User <user@example.com>, Another User <anotheruser@example.com>

 * @param type $title - заголовок письма (тема)
 
 * @param type $text - текст письма (text/html)
 
 * @return type bool: отправлено или нет (true/false)
 */
function sendmymail ($sendfrom, $sendfromEmail, $sendto, $title, $text, $filepath='', $filename='')
{
	include_once ('module/class.phpmailer.php');
	//include_once ('module/class.smtp.php'); // для отправки SMTP 1/2
		$mail = new PHPMailer; 
		
		/* 2/2
		$mail->isSMTP();                                      // Set mailer to use SMTP 
		$mail->Host = 'smtp.yandex.ru';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'noreply@uraland.ru';                 // SMTP username
		$mail->Password = 'CQBnAT';                           // SMTP password
		$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 465;                                    // TCP port to connect to
		
		$mail->From = 'noreply@uraland.ru';      // от кого SMTP
		// закоменть строчку ниже, если используешь SMTP
		*/
		$mail->From = $sendfromEmail;      // от кого 
		$mail->FromName = $sendfrom;   // от кого 
		$mail->isHTML(true);        // выставляем формат письма HTML 
		$mail->Subject = $title;  // тема письма 
		$mail->Body = $text;
		
		if($filepath!='' && $filename!='')
			$mail->addAttachment($filepath, $filename);
		
		$mail->addAddress($sendto);//кому
		
		//отправка
		if (!$mail->send()) die ('Mailer Error: '.$mail->ErrorInfo);  
			$mail->clearAddresses();	
			
			
	/* $headers = "MIME-Version: 1.0\r\n"; 
 	$headers .= "Content-type: text/html; charset=utf-8\r\n"; 
 	$headers .= "Content-Transfer-Encoding: 8bit \r\n";
 	$headers .= "From: ".$sendfrom." <".$sendfromEmail.">";
	
	$title = "=?utf-8?B?" . base64_encode($title) . "?=";
	
	mail($sendto,  $title,  $text,  $headers );  */
}

/**
 * Возвращает по-умолчанию админский email, лежащий в allblocks c id=1
 Также в переменную query можно передать ыйд перечисление необходимых атрибутов allblocks и они вернуться в виде ассоциативного массива 
 */
function getAdminEmail($query='email', $id_allblocks=1) {
   
    
    $result = ew_mysqli_query("SELECT $query FROM allblocks WHERE id='".$id_allblocks."' LIMIT 1");
    $pages = mysqli_fetch_assoc($result);
	
	
	if ($query=='email')
    return $pages['email'];
	else return $pages;
}


/*Функция возвращает массив из экземпляров предков с набьором значений, который потом можно использовать для иерархического наследования параметров. От текущей страницы мы идем вверх по иерархии до самого раннего предка (где id_pages=0)
sqlString/array $query - типовой запрос для поиска
...$inf - параметры:
	если $query - строка
		int $id - с какого id начинать поиск
		int $id_allblocks - текущая версия контента
		другие параметры, которые будут вставляться в строку по sprintf
	ecли $query - думерный массив со строками страниц
		int $search_id - с какого id начинать поиск
		string $pole - ключ поля из массива с которым будем сравнивать $id (по-умолчанию д.б. = 'id_pages')
		function $dopdata - анонимная функция для переработки строки 
*/
function getPraInfo ($query, ...$inf)
{
	$query = $query ?: "SELECT p.id, pc.title, p.id_pages, pc.banner, p.visible, p.hiddencontent, p.link FROM pages p INNER JOIN pcontent pc ON pc.id_pages=p.id WHERE p.id=%d AND p.visible=1 AND pc.id_allblocks=%d LIMIT 1";//запрос по-умолчанию

	$mass = []; 
	
	if (is_string($query))//если пришла sql-строка
	{
		$id = $inf[0];
		$id_allblocks = $inf[1];
		do
		{
			$result = ew_mysqli_query(sprintf($query, $id, $id_allblocks));
			if ($m3 = mysqli_fetch_assoc($result))
			{
				if (isset($m3['link']))
					$m3['link'] = UrlManager::createUrl($m3['id']);
				
				if ($m3['hiddencontent'])
					$m3['link'] = '#';
				
				$mass[]=$m3;
				$id = $m3['id_pages'];
			}
			if (ew_mysqli_error()) echo ew_mysqli_error();
		}while($m3['id_pages']!=NULL);
	}
	else if (is_array($query))//если передали массив
	{
		$search_id = $inf[0] ?: 0;
		$pole = $inf[1] ?: 'id_pages';
		
		while($search_id && $query)
		{
			if ($query[$search_id])//если есть запись
			{
				if(is_callable($inf[2]))//если передан 3й параметр, и он является функцией - запускаем
				{
					$query[$search_id] = $inf[2]($query[$search_id]);//передаем в нее найденную строку и получаем ее изменение. Поможет, если нужно дополнить массив строки
				}
				
				$mass[]=$query[$search_id];
				$search_id=$query[$search_id][$pole];//какой следующий предок
			}

			else {
				$temp = getSQLdata(SINGLE_PAGES_ITEM_QUERY, ID_ALLBLOCKS, $inf[0]);
				if (!$temp[0]['hiddencontent'])
					$mass[] = $temp[0];
				$search_id = $temp[0]['id_pages'];
			}
		}
	}
	
	return array_reverse($mass);
}

/*вспомогательная функция чтобы из массива (предположительно с страницами меню) вытащить подстраницы в один ряд со страницами. Причем ключами к строкам становиться значение $alterkey (предпологается что id)
@array arr - массив
string $key - поле разделитель
string $alterkey - по какому полю добавлять в новый массив
Т.е. массив имел структуру:
[
	'0'=>[
		'id'=>1
		'title'=>..
		$key=>[
				0'=>[
				'id'=>121
				'title'=>..
		
			  ]
		...
	   ],
	 ...
]
а станет
'1'=>[
		'id'=>1
		'title'=>..
		...
	   ],
'121'=>[
		'id'=>121
		'title'=>..
		...
	]	 
*/
function arrDeffindo ($arr, $key='children', $alterkey = 'id')
{
	$new=[];
	
	foreach ($arr as $t)
	{
		if(is_array($t[$key]))
			foreach ($t[$key] as $tt)
				$new[$tt[$alterkey]]=$tt;//записываем подстраницы
		
		unset($t[$key]);//подчищаем начальную ячейку
		
		$new[$t[$alterkey]]=$t;
	}
	
	return $new;
}


/* получение списка узлов ветки prolist
array $mass - массив страниц пердков и их данных, полученый от функции getPraInfo
 */
function getprolist ($mass, $template = '<li><a href="#link#" title="#title#">#title#</a></li>')
{
	$resulthtml = ''; 
	if ($mass)
	{
		//$mass = array_reverse ($mass);
		foreach($mass as $prolist)
		{
			if(!$prolist['link'])
				$prolist['link']='#';
			$prolist['title']=strip_tags($prolist['title']);
			
			$resulthtml.=writeTemplate ($prolist, $template);
		}
		
	}
	
	return $resulthtml;

}
//------------------------------------

/*функция иерархического фомирования баннера по принцупу иерархической наследуемости 
param array $parentmass - массив страниц предков
param array $p - массив данных текущей страницы
*/
function getbanner ($parentmass=array(), $p=array(), $key = 'banner')
{
	$resulthtml = '';
	if ($p)
	{
		$parentmass[]=$p;
		//$parentmass = array_reverse($parentmass);//переворачиваем массив
		
		foreach ($parentmass as $t)
		{
			if ($t[$key])
				$resulthtml = $t[$key];
		}
	}
	
	return $resulthtml;
}



/* Функция запроса словаря glossary 
char* mode - текстовое обозначение поля по которому будет осуществляться выборка. Возможны варианты: "id", "id_pages", "id_glossary". По-умолчанию = id_glossary

int* pid - цифровой идентификатор для mode, является ключем к выборке

int* limit - предел количества элементов вывода. Варианты: 
 ВариантыПо-умолчанию = 0 ограничений нет.
 
int* usl - дополнительное условие соответствует glossary.id_usl = glossary.id. По-умолчанию = 0 без условий

bool full

char* order - что будет ключем для возвращаемого ассоциативного массива. Варинаты "id" и "npp". По-умолчанию "id"

bool $include_parent - флаг, если true то включить в качестве первого элемента (id=0) сведения о родителе
*/

function getglossary($pid=0, $mode='id_glossary', $usl='', $limit=0, $orderby='npp', $full=true, $include_parent=false)
{
	if (is_array($pid))//если первым аргументом передан массив, значит используется альтернативная иннициализация 
	{
		$arr = $pid;
		$pid=$arr['id'];
		$mode=nearest($arr['mode'], 'id_glossary');
		$usl=$arr['usl'];
		$limit=nearest($arr['limit'], 0);
		$orderby=nearest($arr['orderby'], 'npp');
		$full=($arr['full'])?:true;
		
		if (isset($arr['id_allblocks']))
			$id_allblocks=$arr['id_allblocks'];
		
		/*if (isset($arr['id_child_allblocks']))
			$id_child_allblocks=$arr['id_child_allblocks'];*/
		
		$include_parent=($arr['include_parent'])?:false;
	}
	
	
	if ($pid!=0)//идентификатор д.б. !=0
	{
		$query = 'SELECT * FROM glossary WHERE ';
		
		switch ($mode)
		{
			case 'id_glossary':
				$query.=$mode.'='.$pid;
			break;
			
			case 'id_pages': 
				$orderby='id'; // не может быть по npp
				$query.=$mode.'='.$pid;
			break;
			
			case 'npp':
			case 'id': 
				$limit = 1; // не может быть = 0
				$query.=$mode.'='.$pid;
			break;
			
			default: return 0; break;
		}
		
		if ($id_allblocks)
			$query.=' AND id_allblocks="'.$id_allblocks.'" '; 
		
		/*if (isset($id_child_allblocks))
			$query.=' AND id_allblocks="'.$id_child_allblocks.'" '; */
		
		//обработка usl
		if ($usl)
			$query.=' '.$usl." "; 
		//---------------
		
		//обработка orderby
		switch ($orderby)
		{
			case 'id': 
			case 'id_pages': 
				//$query.=' order by "'.$orderby.'"';
			case 'npp': 
				$query.=' order by '.$orderby.' asc';
			break;
			
			default: return 0; break;
		}
		//---------------
		
		
		//обработка limit
		switch ($limit)
		{
			case 1: 
			$query.=' limit '.$limit;
			break;
			
			case 0: break;
			
			default: break;
		}
		//---------------
		
		$mass = array ();
	
		//echo $query;
		$result2 = ew_mysqli_query($query);
		while ($p = mysqli_fetch_assoc($result2))
		{
			if ($limit==1)//если один элемент в массиве упраздняем массив
			$mass = $p;
			else 
			{
				if ($full) //выборка со всеми атрибутами
				$mass[$p[$orderby]]=$p;
				else //выборка в формате ключ=значение по id=title
				$mass[$p[$orderby]]=$p['title'];
			}
			
		}
		
		if ($include_parent)//если надо включить сведения о родителе
			$mass[0] = getglossary($pid, 'id');
		
		return $mass;
		
	}else return 0;
}

//--------------------------------------------------------

/* Функция удаления куки регистрации. Впринципе может быть использована для удаления любых куки */
function deleteCookie ($cook)
{
	setcookie ($cook, "", time() - 3600);
}
//--------------------------------------------------------

/* Функция очистки куки от щелухи после ajax и json */
function repareCookie ($cook)
{
	return (stripslashes(urldecode($cook)));
}
//----------------------------------------------------

/* генерируем случайный пароль 
*int count - количество символов в пароле. (по-умолчанию 8)
*/
function getpass($count=8) {
$m_rand = mt_rand(); //генерируем случайное целое
$u_id = uniqid("AMNXYZKVCRTPQWDB");//создаем уникальный идентификатор с префиксом, постфиксом и повышенной энтропией
$combine = $m_rand.$u_id;// Комбинируем переменные для формирования строки
$new = str_shuffle($combine);//смешиваем строку
return str_replace('0','1',substr($new, 2, $count));//возвращаем пароль. если что заменяем ноль на 1
}
//-------------------

/*отчистка пароля и логина от шелухи*/
function clearQuatsch ($t)
{
	return str_replace(array("'","\"",'"'), "", strip_tags(trim($t)));
}

/* Функция формирования массива для ajax ответа и отправка его через прерываение 
array* otvet - массив значений еоторый со стороны js обрабатываются функией alertAndRedirect
*/
function setotvet ()
{ 
	echo json_encode(func_get_args());
	exit();
}

/*Функция которая из названия файла получает его расширение
char filename - название файла
*/
function fileTypeInfo ($file=false)
{
	if($file)
	{
		$t = explode('.', $file);
		return $t[count($t)-1];
	}
	else return;
}

/*функция для поиска в шаблоне места для вставки виджетов и их заполнения*/
function writeTemplateWidgets (array $wdgt=[], string $template='')
{
	foreach ($wdgt as $w)
		$template=str_ireplace ("{{wdgt".$w['id']."}}", $w['info'], $template);
	
	$template=preg_replace ('/{{wdgt\w+}}/', "",$template);//очищаем неиспользованные ключи
	
	return $template;
}

/*функция заполнения шаблона данными ассоциативного массива
	 text* template - html-код шаблона со вставками вида #ТЭГ#
	 array* massiv - кортеж с атрибутами вида ТЭГ, которые и будут искаться в шаблоне
     int $mode - тип использования (с ## или по %s и т.п.) 
	 return вовзращает готовый html-код для вставки
	*/
function writeTemplate ($massiv=array(), $template='<li class="#class#"><a href="#link#" title="#title#">#title#</a></li>', $mode=0, array $wdgt=[])
	{	
		if ($massiv)
		{
			if ($mode==1)
				$template = vsprintf ($template, $massiv);
			else
			{
				foreach ($massiv as $key => $val)
					$template=str_ireplace ("#".$key."#", $val, $template);
				$template=preg_replace ('/#\w+#/', "",$template);//очищаем неиспользованные ключи
				
				if($wdgt)//есть виджеты
					$template = writeTemplateWidgets($wdgt, $template);
			}
			
			return $template;
		}
	}
	
/*функция заполнения шаблона данными массива берущимися последовательно по порядку
	 text* template - html-код шаблона со вставками вида #ТЭГ#
	 array* massiv - кортеж с атрибутами вида ТЭГ, которые и будут искаться в шаблоне 
	 int mode - переключатель типа обраотки: 1 - через массив по порядку, 2 - через массив по ключам
	 func obrab - функция обработки итерационного массива. принимает итерационный массив, производит над ним действия и возвращает
	 return вовзращает готовый html-код для вставки
	*/
function writeItemsTemplate ($massiv=array(), $template='<li class="#class#"><a href="#link#" title="#title#">#title#</a></li>', $mode=2, $obrab_func=0)
	{
		$res = '';
		if($obrab_func===0) 
			$obrab_func = function ($n){
				return $n;
			};
		foreach ($massiv as $t)
		{
			$t = $obrab_func ($t);
			if($t)
			{
				if ($mode==1)
					$res .= vsprintf ($template, $t);
				else $res .= writeTemplate ($t, $template);
			}
		}
		return $res;
	}

/*Функция вспомогательная при writeTemplate принимает заполненный ассоциативный массив и возвращает его с очищенными ячейками при сохранении ключей
array mass - исходный массив
*/
function arrayClear ($mass=array())
{
	foreach ($mass as $i => $t)
		$mass[$i]='';
	
	return $mass;
}

/*Функция превращает значения ячеек массива в их ключи*/
function resToKey ($mass=array())
{
	$newmass=array();
	
	foreach($mass as $i=>$t)
	$newmass[trim($t)]=trim($i);
	
	return $newmass;
}


/*Функция формирует из принимаемого массива $mass строку для sql SET запроса
array $mass - принимаемый массив
array $perech - перечнь ключей массива $mass которые должны использоваться
*/
function createSetQuery ($mass=array(), $perech='')
{
	$str = array();$contrl = 0;

	if (is_string($perech) && $perech!='')
	{	
		$perech=explode(',',$perech);
	
		$contrl = count($perech);
		
		$perech=resToKey($perech);
	}
	
	foreach($mass as $i => $t)
	{ 
		if ($contrl)//если ограничение на название полей
		{
			if ($perech[$i])
			$str[]=$i.'="'.htmlspecialchars($t).'"';
		}
		else
		$str[]=$i.'="'.htmlspecialchars($t).'"';
	}

	return implode(', ',$str);
}

/*Функция формирует из принимаемого массива $mass строку для sql INSERT INTO values запроса
array $mass - принимаемый массив
array $perech - перечнь ключей массива $mass которые должны использоваться
*/
function createIntoQuery ($mass=array(), $perech='')
{
	$str = array();

	if ($perech!='')
	{	
		$perech=explode(',',$perech);
	
		$contrl = count($perech);
		
		$perech=resToKey($perech);
	}
	
	foreach ($perech as $i => $p)
	{
		$str[]='"'.$mass[$i].'"';
	}

	return implode(', ',$str);
}

/*функция вывода постраничной навигации
int $postr_kolpages - количество возможных страниц всего
int $postr_tekpages - текущая страница
int $postr_maxkol - максимальное количество элементов на одной странице
bool/array $prevnext - флаг, нужно или нет отображать пункты вперед/назад. Если нет, то д.б. = false, иначе передаем массив, где [0] элемент - это шаблон кнопкии предыдущая страница, 
а [1] элемент - шаблон кнопки следующая страница
char $template - шаблон html для вывода
*/
function getpagination ($postr_kolpages, $postr_tekpages, $postr_maxkol, $prevnext=false, $template='<a class="%s" href="%s">%s</a>')
{
	if ($postr_kolpages>1)//постраничная навигация существует
	{
		if (is_array($prevnext))//до и после
		{
			$delta = $postr_tekpages-$postr_maxkol;//разница
			$tprev =''; $ty ='';
			if (!$postr_tekpages)
				$ty ='active';
			else $tprev='?a='.$delta;

			printf ($template, $ty, $tprev, nearest($prevnext[0],'Следующая страница'));

		}
		
		for ($i=0;$i<$postr_kolpages;$i++)
		{
			$ty='';
			if ($postr_tekpages==$postr_maxkol*($i))
				$ty='active';
			printf ($template, $ty, '?a='.$i*$postr_maxkol, $i+1);
		}
		
		if (is_array($prevnext))//до и после
		{
			$delta = $postr_tekpages+$postr_maxkol;//разница
			$tprev =''; $ty ='';
			if ($delta>=$postr_maxkol*$postr_kolpages)
				$ty ='active';
			else $tprev='?a='.$delta;

			printf ($template, $ty, $tprev, nearest($prevnext[1],'Предыдущая страница'));
		}
	}
}

/*функция получает двумерный массив и, если у него только 1 строка, привращает его в одномерный массив*/
function getLightArray ($arr=array())
{
	return (count ($arr)==1)? $arr[0]:$arr;
}

/*функция декодирования строки в массив в соответствии с шаблоном
char $string - строка, элементы которой надо растасовать в массив
array $separator - массив разделителей по порядку через которые будут формироваться ячейкии происходить разбиение
*/
function decodeTemplate ($string, $separator=array(','))
{
	$res=array();
	if ($string && $separator)
	{
		$res=explode ($separator[0], $string);//первый уровень
		array_splice($separator,0,1);//убираем исопльзованный разделитель 
		
		if (count ($separator))//если еще остались разделители
		{
			if (is_array($res))
				foreach ($res as $i => $t)
					$res[$i]=decodeTemplate ($t, $separator); 
			else $res=decodeTemplate ($t, $separator); 
		}
				
	}
	
	return $res;
	
}

/*функция получает двумерный ассоциативный массив $arr и выводит первый попавшийся подмассив, имеющий переменную с именем $key и значением $val*/
function getFirstElm ($arr=[], $key=0, $val=0)
{
	$res = [];
	
	foreach ($arr as $t)
	{
		if (isset($t[$key]) && $t[$key]==$val)
		{
			$res = $t;
			return $res;
		}
	}
	
	return $res;
}

/*функция получает 2мерный массив $mass и создает новый ассоциативный массив, где ключем к строке становится значение поля $key */
function arrPersonality ($mass=[], $key='id')
{
	$newmass = [];
	
	foreach($mass as $t)
		if($t[$key])
			$newmass[$t[$key]]=$t;
	
	return $newmass;
}



/*функция возвращает количество дней от даты 1 до даты 2
string date $data1  (вида "Y-m-d")
string date $data2
*/
function dayBefore ($data2, $data1=false)
{
	if (!$data1)
	$data1=date('Y-m-d');
	
	$data1 = date_create($data1);
	$data2 = date_create($data2);
	
	$interval = date_diff($data1, $data2);
	return $interval->days;
}
/*------------------------------------------------*/

//формирует инициалы из строки фио на основе пробелов
function inizialy ($face)
{
	$t=explode (' ', $face);$a=array ();
	for ($i=count ($t)-1;$i>=(count ($t)-3);$i--)
	{
		if ($i==count ($t)-3)
		$a[]=$t[$i];
		else $a[]=$t[$i][0].".";
		
	}//echo  $face;
	
return $a[2]." ".$a[1].$a[0];
}
//------------------------------------------------//

/*функция убивает перенос каретки/
Использовать перед выводом контента php в js*/
function nobr ($string)
{
	$string = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $string);
	$string = str_replace(array("\r\n", "\r", "\n", "\t", "'", 'x0D', 'x0A'), '', $string);
	
	return $string;
   
	//return str_replace(array("\r\n", "\r", "\n", "\t"), '', strip_tags($string)); 
}
//-----------------------------//

//функции обработки транзакций
function beginTransaction() {
    ew_mysqli_query('START TRANSACTION');
}

function commitTransaction() {
    ew_mysqli_query('COMMIT');
}

function rollbackTransaction() {
    ew_mysqli_query('ROLLBACK');
}
//------------------------------//

//библиотека функций даты и времени
function dataforhuman ($t='')//перевод записи в формате date в человеческую форму 
{
	$result = false;
	
	if ($t)
	{
		$mass=explode ('-', $t);
		$result = $mass[2].'.'.$mass[1].'.'.$mass[0];
	}
	
	return $result;
}
function dataformachine ($t='')//наобарот
{
	$result = false;
	
	if ($t)
	{
		$mass=explode ('.', $t);
		$result = $mass[2].'-'.$mass[1].'-'.$mass[0];
	}
	
	return $result;
}
function datatimeforhuman ($t='')//перевод записи в формате datetime в человеческую форму 
{
	$result = false;
	
	if ($t)
	{
		$mass=explode (' ', $t);
		$data=explode ('-',$mass[0]);
		$result = $mass[1].", ".$data[2].'.'.$data[1].'.'.$data[0];
	}
	
	return $result;
}
function datatimeformachine ($t='')//наобарот
{
	$result = false;
	
	if ($t)
	{
		$mass=explode (', ', $t);
		$data=explode ('.', $mass[1]);
		$result = $data[2].'-'.$data[1].'-'.$data[0]." ".$mass[0];
	}
	
	return $result;
}
function dataint ($t='')//дата в виде целого числа со дня эпохи, только для дд.мм.гггг.
{
	$result = false;
	
	if ($t)
	{
		$data=explode ('.', $t);
		$result = mktime (0,0,0, $data[1], $data[0], $data[2]);
	}
	
	return $result;
}
function datatimeforhumanbody ($t)//перевод записи в формате datetime в человеческую форму 
{
	$mass=explode (' ', $t);
	$data=explode ('-',$mass[0]);
	
	return @$data[2].'.'.@$data[1].'.'.@$data[0];
}


//вывод названия месяца по числу
function getmonatpertext($monat, $massiv=["Неизвестно","января",'февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'] )
{
  return @$massiv[$monat];
}

//получение название месяца в виде текста из целой даты в формате sql
function printfmonatpertext ($data, array $massiv=[], $shema="%s %s %s")
{
	$data = strtotime($data);
	return sprintf ($shema, date('j', $data), getmonatpertext(date('n', $data), $massiv), date('Y', $data));
}
//-------------------------------------------------------------

/*функция вычлинения поддомена*/
function getpoddomen ($host='')
{
	if(empty($host))
		$host = $_SERVER['SERVER_NAME'];
	
	$tmp = explode('.', $host);
	$tmp = array_slice($tmp, 0, -2);
	return implode(".", $tmp);
}

/*функция возвращающая id типа версии контента*/
function getContentTypeId ($allverse=[], $host='')
{
	return getFirstElm($allverse,'domen', getpoddomen($host))['id'];
}

/*функция приримает массив $arr с множеством строк и возвращает массив, где строки рассортированы по ключу $param
т.е. например есть массив
[
	0=>[
	id=>1,
	name=>Первый
	type=>A
	],
	
	1=>[
	id=>2,
	name=>Второй
	type=>A
	],
	
	2=>[
	id=>3,
	name=>Третий
	type=>B
	]
]
Передав в $param = type получим 
[
	А=>[
		0=>[
		id=>1,
		name=>Первый
		type=>A
		],
	
		1=>[
		id=>2,
		name=>Второй
		type=>A
		]
	],
	B=>[
		0=>[
		id=>3,
		name=>Третий
		type=>B
		]
	],
]
Схоже с arrPersonality только для множества схожих строк
*/
function arrRekompose ($arr=[], $param='')
{
	$res = [];
	
	if($param && count($arr))
		foreach($arr as $t)
			$res[$t[$param]][]=$t;
	
	return nearestArr($res, $arr);
}

/*функция принимает искомую фразу string $research и строку или массив string/array $intext где нужно найти вхождения искомой фразы. В зависимости от параметра int $strmax - возвращается определенное количество символов*/
function sokrinfo_search ($search='', $intext, $strmax=300)
{
	if (is_array($intext))
		$intext = implode(' ', $intext);//соединяем в текстик
	
	//преобразование найденного текста. Выделение искомой фразы, скорщаение вывода
	$info=strip_tags ($intext);//избавляемся от тэгов
	$a=stripos ($info, $search);//находим первое вхождение искомой фразы
	$b=strripos ($info, $search);//находим последнее вхождение искомой фразы
	$n=strlen ($info);//длинна строки
	$z=strlen ($search);//длинна искомой строки

	if ($a==$b)//одно совпадение
	{
		$myinfo=substr ($info, $a, strrpos($info, " "));
	}
	else //несколько
	{
			$myinfo=substr ($info, $a,  strrpos($info, " "))."... ";

			$y=($n-$b-$z)/2;
			$y=(int)$y;
			if ($y>$strmax)
			$y=$strmax;
			$myinfo.=substr ($info, $b-5, strrpos($info, " ") );

	}
	//$info= badnews($myinfo, $strmax);
	$info= str_ireplace ($search, "<strong>".$search."</strong>", badnews($myinfo, $strmax));
	
	return $info;

}

/*функция аналог json_encode но работает с cp1251*/
function php2js($a=false)
{
    if (is_null($a) || is_resource($a)) {
        return 'null';
    }
    if ($a === false) {
        return 'false';
    }
    if ($a === true) {
        return 'true';
    }
    
    if (is_scalar($a)) {
        if (is_float($a)) {
            //Always use "." for floats.
            $a = str_replace(',', '.', strval($a));
        }

        // All scalars are converted to strings to avoid indeterminism.
        // PHP's "1" and 1 are equal for all PHP operators, but
        // JS's "1" and 1 are not. So if we pass "1" or 1 from the PHP backend,
        // we should get the same result in the JS frontend (string).
        // Character replacements for JSON.
        static $jsonReplaces = array(
            array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'),
            array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"')
        );

        return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
    }

    $isList = true;

    for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
        if (key($a) !== $i) {
            $isList = false;
            break;
        }
    }

    $result = array();
    
    if ($isList) {
        foreach ($a as $v) {
            $result[] = php2js($v);
        }
    
        return '[ ' . join(', ', $result) . ' ]';
    } else {
        foreach ($a as $k => $v) {
            $result[] = php2js($k) . ': ' . php2js($v);
        }

        return '{ ' . join(', ', $result) . ' }';
    }
}

/*html редирект*/
function html_redirect($link, $delay=2)
{
	echo '<meta http-equiv="refresh" content="'.$delay.';'.$link.'">';
}

/*функция избавления от мнемоник в строке JSON*/
function mnemonic_to_cyr_decode($str) {
/*$arr_replace_utf = array('\u0410', '\u0430','\u0411','\u0431','\u0412','\u0432',
'\u0413','\u0433','\u0414','\u0434','\u0415','\u0435','\u0401','\u0451','\u0416',
'\u0436','\u0417','\u0437','\u0418','\u0438','\u0419','\u0439','\u041a','\u043a',
'\u041b','\u043b','\u041c','\u043c','\u041d','\u043d','\u041e','\u043e','\u041f',
'\u043f','\u0420','\u0440','\u0421','\u0441','\u0422','\u0442','\u0423','\u0443',
'\u0424','\u0444','\u0425','\u0445','\u0426','\u0446','\u0427','\u0447','\u0428',
'\u0448','\u0429','\u0449','\u042a','\u044a','\u042b','\u044b','\u042c','\u044c',
'\u042d','\u044d','\u042e','\u044e','\u042f','\u044f');*/

$arr_replace_utf = array('%u0410', '%u0430','%u0411','%u0431','%u0412','%u0432',
'%u0413','%u0433','%u0414','%u0434','%u0415','%u0435','%u0401','%u0451','%u0416',
'%u0436','%u0417','%u0437','%u0418','%u0438','%u0419','%u0439','%u041a','%u043a',
'%u041b','%u043b','%u041c','%u043c','%u041d','%u043d','%u041e','%u043e','%u041f',
'%u043f','%u0420','%u0440','%u0421','%u0441','%u0422','%u0442','%u0423','%u0443',
'%u0424','%u0444','%u0425','%u0445','%u0426','%u0446','%u0427','%u0447','%u0428',
'%u0448','%u0429','%u0449','%u042a','%u044a','%u042b','%u044b','%u042c','%u044c',
'%u042d','%u044d','%u042e','%u044e','%u042f','%u044f');

$arr_replace_cyr = array('А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
'Ё', 'ё', 'Ж','ж','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н','О','о',
'П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш',
'Щ','щ','Ъ','ъ','Ы','ы','Ь','ь','Э','э','Ю','ю','Я','я');
$str1 = $str;
$str2 = str_ireplace($arr_replace_utf, $arr_replace_cyr, $str1);
return $str2;
}

/** 
* Converts bytes into human readable file size. 
* 
* @param string $bytes 
* @return string human readable file size (2,87 Мб)
* @author Mogilev Arseny 
*/ 
function FileSizeConvert($bytes, $units)
{
    $bytes = floatval($bytes);
	$unit = explode(', ', $units);
	$arBytes = array(
		0 => array(
			"UNIT" => $unit[0],
			"VALUE" => 1099511627776
		),
		1 => array(
			"UNIT" => $unit[1],
			"VALUE" => 1073741824
		),
		2 => array(
			"UNIT" => $unit[2],
			"VALUE" => 1048576
		),
		3 => array(
			"UNIT" => $unit[3],
			"VALUE" => 1024
		),
		4 => array(
			"UNIT" => $unit[4],
			"VALUE" => 1
		)
	);

    foreach($arBytes as $arItem)
    {
        if($bytes >= $arItem["VALUE"])
        {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
            break;
        }
    }
    return $result;
}

//получение название месяца в виде текста из целой даты в формате sql
function datetotext ($data, $schema="#d# #m# #y#", $months)
{
	$data = strtotime($data);
	if(is_string($months))
		$months = explode(', ', $months);
	$rdate = str_replace('#d#', date('j', $data), $schema);
	$rdate = str_replace('#dnum#', date('d', $data), $rdate);
	$rdate = str_replace('#m#', getmonatpertext(date('n', $data), $months), $rdate);
	$rdate = str_replace('#mnum#', date('m', $data), $rdate);
	$rdate = str_replace('#y#', date('Y', $data), $rdate);
	return $rdate;
}

/*функция возвращает праивльныый путь к директории учитывая директорию скрипта из которого этот путь запрашивается
$dir - путь к директории скрипта, обычно из __DIR__
$path - директория, куда надо попасть. не должен начинаться с /
*/
function dir_to_path ($dir, $path)
{
	$res = '';
	if ($path)
	{
		$path_first_elm = explode('/', $path);
		$path_first_elm = (count ($path_first_elm)) ? $path_first_elm[0] : $path_first_elm;
		
		$dir_last = explode($path_first_elm, $dir)[1];
		
		for ($i=0; count(explode('\\', $dir_last)); $i++)
			$res .= '../';
		
		$res .= $path;
	}
	
	return $res; 
}

/** 
* Склонение существительных с числительными. 
* Функция принимает число $n и массив с тремя строками -  
* разные формы произношения измерения величины. 
* Необходимая величина будет возвращена. 
* Например: pluralForm(100, "рубль", "рубля", "рублей") 
* вернёт "рублей". 
*  
* @param int величина 
* @param array [форма1, форма2, форма3] 
* @return string 
*/ 
function sklonenInt($n, $forms) 
{ 
	if(count($forms) != 3) // английский, может и др. языки
	{
		if(count($forms) == 2)
			return ($n == 1) ? $forms[0] : $forms[1];
		elseif(count($forms) == 1)
			return $forms[0];
		else
			return '';
	}
			
    $n = abs($n) % 100; 
    $n1 = $n % 10; 
     
    if ($n > 10 && $n < 20) { 
        return $forms[2]; 
    } 
     
    if ($n1 > 1 && $n1 < 5) { 
        return $forms[1]; 
    } 
     
    if ($n1 == 1) { 
    return $forms[0]; 
    } 
     
    return $forms[2]; 
}

//убираем из номера телефона лишние символы
function makeClearTel($teltext)
{
	return preg_replace('/[^0-9]/', '', $teltext);
}

//убираем из номеров телефона лишние символы и возвращаем как ссылки (надо бы переделать более универсально)
function makeTelsBlock($teltext)
{
	$t = explode(', ', $teltext);
	if(count($t) == 1)
		$t = explode(',', $teltext);
	foreach($t as &$tt)
		$tt = '<a href="tel:'. makeClearTel($tt) .'">'. $tt .'</a>';
	unset($tt);
	return implode(', ', $t);
}

/*функция получает массив страниц меню и возвращает разделенный массив в соответствии с количеством столбцов в которое надо вывести это меню*/
function menuChunk (array $menu=[], int $column=1)
{
	return ($column>1 && $menu) ? array_chunk ($menu, ceil(count($menu)/$column)) : [];
}

// функция приведения даты из формата php в *день месяца*
function formatDate($date) {
	global $all;
	$date = explode('.', $date);
	return (int)$date[0] . '.' . $date[1] . '.' . $date[2];
}
