<?php
/* вспомогательныне Функции разработанные специально для проекта */
require_once('ew.project.constants.php');

/*функция производит запрос к бд и возвращает */
function getSQLdata ($querytype)
{
	$res = array();
	$params = func_get_args();

	switch ($querytype)//определение строки запроса по типу запроса
	{
		case ALLBLOCK_QUERY: 
			$query = sprintf("SELECT * FROM allblocks order by id asc");
		break;
		
		case SINGLE_PAGES_ITEM_QUERY: 
		case FULL_PAGES_ITEM_QUERY: 
		case PAGES_ITEM_QUERY: 
			$query = sprintf("SELECT pc.*, p.data, p.id_pages, p.teknode, p.id, p.childlistview, p.css_icon, p.hiddencontent, p.name_hashtags, p.hashtags, p.link, p.link as linkblock, p.id_inherit, pc.preinfo, allblocks.biography FROM pages p inner JOIN pcontent pc ON pc.id_pages=p.id inner JOIN allblocks on allblocks.id=%d WHERE pc.id_allblocks='%d' AND p.id='%d' AND p.visible=1 LIMIT 1", $params[1], $params[1], $params[2]); //встраивание параметров
		break;
			
		case PAGES_FOTO_QUERY:
			$query = sprintf("SELECT foto.id, fc.title, foto.picname, foto.picname2, foto.filetype, fc.info, fc.href, foto.type, fc.id_allblocks, foto.npp, foto.id_pages, foto.id_sovmdocs, fc.dopinfo, fc.preinfo FROM files foto
			INNER JOIN fcontent fc ON fc.id_files=foto.id
			WHERE fc.id_allblocks='%d' AND foto.id_pages='%d' AND foto.docsorfoto=1 AND foto.picname!='' order by foto.type, foto.npp asc", $params[1], $params[2]); 
		break;
			
		case PAGES_DOCS_QUERY:
			$query = sprintf("SELECT foto.id, fc.title, foto.picname as filename, fc.info, foto.type, foto.filetype, foto.filesize, fc.id_allblocks, foto.npp, foto.id_pages, fc.dopinfo FROM files foto
			INNER JOIN fcontent fc ON fc.id_files=foto.id
			WHERE fc.id_allblocks='%d' AND foto.picname!='' AND foto.id_pages='%d' AND foto.docsorfoto=0 order by foto.type, foto.npp asc", $params[1], $params[2]); 
		break;
		
		case SIMPLY_PODPAGES_QUERY:
		case FULL_PODPAGES_QUERY:
		case PODPAGES_QUERY:
			$query = sprintf("SELECT  p.id, p.css_icon, p.csspoint, pc.title, p.data, pc.info, pc.description, pc.dopinfo as clear_dopinfo, pc.dopinfo, pc.position, pc.rank, pc.banner, p.id_pages, pc.id_allblocks, p.npp, p.link as linkblock, p.link, pc.x, pc.y, pc.adr, pc.id_pcinherit, p.id_inherit, p.canhashtags, p.ishashtags, p.name_hashtags, p.hashtags, p.hiddencontent, allblocks.learnmore, allblocks.more, allblocks.showmap, allblocks.biography, pc.preinfo FROM pages p inner JOIN pcontent pc ON pc.id_pages=p.id inner JOIN allblocks on allblocks.id=%d WHERE pc.id_allblocks=%d AND p.id_pages=%d AND p.visible=1 order by npp asc", $params[1], $params[1], $params[2]); //встраивание параметров
		break;

		case NEWS_PER_YEAR_QUERY:
			$query = sprintf("SELECT  p.id, pc.title, p.data, pc.info, pc.description, pc.dopinfo as clear_dopinfo, pc.dopinfo, pc.banner, p.id_pages, pc.id_allblocks, p.npp, p.link as linkblock, p.link, pc.x, pc.y, pc.adr, pc.id_pcinherit, p.id_inherit, p.canhashtags, p.ishashtags, p.name_hashtags, p.hashtags, p.hiddencontent, p.css_icon, p.csspoint, allblocks.learnmore, allblocks.showmap, pc.preinfo FROM pages p inner JOIN pcontent pc ON pc.id_pages=p.id inner JOIN allblocks on allblocks.id=1 WHERE pc.id_allblocks=%d AND p.id_pages=%d AND p.visible=1 AND YEAR(p.data) in (%s) order by data desc %s", $params[1], $params[2], $params[3], $params[4]); //limit
		break;

		case CALENDAR_QUERY:
			$query = sprintf("SELECT  p.id, pc.title, p.data, pc.info, pc.description, pc.dopinfo as clear_dopinfo, pc.dopinfo, pc.banner, p.id_pages, pc.id_allblocks, p.npp, p.link as linkblock, p.link, pc.x, pc.y, pc.adr, pc.id_pcinherit, p.id_inherit, p.canhashtags, p.ishashtags, p.name_hashtags, p.hashtags, p.hiddencontent, p.css_icon, p.csspoint, allblocks.learnmore, allblocks.showmap, allblocks.more, allblocks.remdate, pc.preinfo FROM pages p inner JOIN pcontent pc ON pc.id_pages=p.id inner JOIN allblocks on allblocks.id=1 WHERE pc.id_allblocks=%d AND p.id_pages=%d AND p.visible=1 order by data asc", $params[1], $params[2]);
		break;

		case YEARS_QUERY:
			$query = sprintf("SELECT YEAR(data) as year
				FROM pages p
				inner JOIN pcontent pc ON pc.id_pages=p.id
				WHERE pc.id_allblocks='%d' AND p.id_pages='%d' AND p.visible=1 group by YEAR(data) order by p.data desc", $params[1], $params[2]); //встраивание параметров
		break;

		case LAST_NEWS_QUERY:
			$query = sprintf("SELECT  p.id, p.css_icon, pc.title, p.data, pc.info, pc.description, pc.dopinfo as clear_dopinfo, pc.dopinfo, pc.banner, p.id_pages, pc.id_allblocks, p.npp, p.link as linkblock, p.link, pc.x, pc.y, pc.adr, pc.id_pcinherit, p.id_inherit, p.canhashtags, p.ishashtags, p.name_hashtags, p.hashtags, p.hiddencontent, allblocks.learnmore, allblocks.more, allblocks.showmap, pc.preinfo FROM pages p inner JOIN pcontent pc ON pc.id_pages=p.id inner JOIN allblocks on allblocks.id=%d WHERE pc.id_allblocks=%d AND p.id_pages=%d AND p.visible=1 order by data desc LIMIT 2", $params[1], $params[1], $params[2]);
		break;

		case PROJECTS_YEARS_QUERY:
			$query = sprintf("SELECT YEAR(data) as year
				FROM pages p
				inner JOIN pcontent pc ON pc.id_pages=p.id
				WHERE pc.id_allblocks='%d' AND p.id_pages='%d' AND p.visible=1 group by YEAR(data) order by p.data desc", $params[1], $params[2]); //встраивание параметров
		break;

		case COUNT_NEWS:
			$query = sprintf("SELECT COUNT(*) AS total FROM pages WHERE id_pages=%d AND visible=1", $params[1]);
		break;

		case GET_ROW_POSITION:
			$query = sprintf("SELECT COUNT(*) AS oneless FROM pages WHERE visible=1 AND id_pages=15 AND id > (SELECT id from pages WHERE id='%d')", $params[1]);
		break;

		case GET_CONTACTS_TITLE:
			$query = "SELECT title FROM pcontent WHERE id=4";
		break;

		case MENU_QUERY: 
			$query = sprintf("SELECT p.id, p.link, p.visible, p.id_pages, p.hiddencontent, p.css_icon, pc.title, pc.banner, p.inupmenu, p.indownmenu, p.childlistview, pc.id_allblocks, pc.info, pc.dopinfo, p.hashtags FROM pages p 
			INNER JOIN pcontent pc ON pc.id_pages=p.id 
			WHERE pc.id_allblocks='%d' AND p.visible=1 AND (p.inupmenu=1 OR p.indownmenu=1) order by p.npp", $params[1]);
		break;
		
		case PODMENU_QUERY: 
			$query = sprintf("SELECT p.id, p.link, p.visible, p.hiddencontent, p.id_pages, pc.title, p.inupmenu, p.childlistview, pc.id_allblocks, pc.info, pc.dopinfo, p.hashtags 
			FROM pages p 
			INNER JOIN pcontent pc ON pc.id_pages=p.id 
			WHERE p.visible=1 AND pc.id_allblocks='%d' AND p.hiddencontent=0 AND p.id_pages = '%d' AND p.visible=1 order by p.npp asc", $params[1], $params[2]); 
		break;
		
		case SINGLE_FOTO_QUERY:
			$query = sprintf("SELECT foto.id, fc.title, foto.picname, foto.picname2, fc.info, fc.href, foto.type, fc.id_allblocks, foto.npp, foto.id_pages, foto.id_sovmdocs, fc.dopinfo FROM files foto
			INNER JOIN fcontent fc ON fc.id_files=foto.id
			WHERE fc.id_allblocks='%d' AND foto.id_pages='%d' AND foto.docsorfoto=1 AND foto.type='%d' AND foto.picname!='' order by foto.npp, foto.id limit 1", $params[1], $params[2], $params[3]); 
		break;
		
		case INDEX_NEWS_QUERY:
			$query = sprintf("SELECT  p.id, pc.title, p.data, pc.info, pc.description, pc.dopinfo as clear_dopinfo, pc.dopinfo, pc.banner, p.id_pages, pc.id_allblocks, p.npp, p.link, pc.x, pc.y, pc.adr, pc.id_pcinherit, p.id_inherit FROM pages p inner JOIN pcontent pc ON pc.id_pages=p.id WHERE pc.id_allblocks='%d' AND p.id_pages='%d' AND p.visible=1 order by p.data desc, npp asc limit 16", $params[1], $params[2]); //встраивание параметров
		break;	

		case COUNT_PAGES_QUERY: 
			if($params[3])//если есть фильтр по хештегу
				$params[3] = " AND p.hashtags LIKE '%#".$params[3]."%' ";
				
			$query = sprintf("SELECT count(*) as kol
			FROM pages p 
			inner JOIN pcontent pc ON pc.id_pages=p.id 
			WHERE pc.id_allblocks='%d' AND p.id_pages='%d' AND p.visible=1 %s", $params[1], $params[2], $params[3]); //встраивание параметров
		break;
		
		case SIMPLY_NEWS_QUERY:
			if($params[5])//если есть фильтр по хештегу
				$params[5] = " AND p.hashtags LIKE '%#".$params[5]."%' ";
				
			$query = sprintf("SELECT pc.title, p.data, pc.info, pc.description, pc.dopinfo, p.id_pages, p.id, p.link, pc.id_allblocks, p.id_inherit
			FROM pages p
			inner JOIN pcontent pc ON pc.id_pages=p.id
			WHERE pc.id_allblocks='%d' AND p.id_pages=%d AND p.visible=1 %s order by p.data desc, p.npp asc, p.id desc LIMIT %d, %d", $params[1], $params[2], $params[5], $params[3], $params[4]); //встраивание параметров
		break;

		case PROJECTS_QUERY:
			if($params[3])//если есть фильтр по хештегу
				$params[3] = " AND p.hashtags LIKE '%".$params[3]."%' ";
				
			$query = sprintf("SELECT pc.title, p.data, pc.info, pc.description, pc.dopinfo, p.id_pages, p.id, p.link, pc.id_allblocks, p.id_inherit
			FROM pages p
			inner JOIN pcontent pc ON pc.id_pages=p.id
			WHERE pc.id_allblocks='%d' AND p.id_pages=%d AND p.visible=1 %s order by p.npp asc, p.id desc", $params[1], $params[2], $params[3]);
		break;
		
		case COUNT_BACKCHAT_QUERY:  
			$query = sprintf("SELECT count(*) as kol
			FROM backchat b 
			WHERE b.id_pages='%d' AND b.public=1", $params[1]); //встраивание параметров
		break;
		
		case BACKCHAT_QUERY:  
			$query = sprintf("(SELECT b.*
			FROM backchat b 
			WHERE b.id_pages='%d' AND b.public=1 order by b.data desc limit %d, %d)", $params[1], $params[2], $params[3]); //встраивание параметров
		break;
		
		case SEARCH_QUERY: 
			$query = "SELECT pages.id, pc.title, pc.direct_title, pc.keywords, pc.description, pc.info, pc.dopinfo, pc.banner, pc.adr, pages.link, pages.hiddencontent, pages.id_pages 
			FROM pages 
			INNER join pcontent pc ON pc.id_pages=pages.id
			WHERE pages.visible = 1 and pages.cansearch=1 AND pc.id_allblocks = '".$params[1]."' AND CONCAT(pc.title, pc.direct_title, pc.keywords, pc.description, pc.info, pc.dopinfo, pc.adr, pc.banner) LIKE '%".$params[2]."%'"; //встраивание параметров просиходит не через sprintf, потому что %%s% срабатывал не корректно
		break;

		case NEWS_QUERY:
			$query = sprintf("SELECT p.id, pc.title, p.data, pc.info, pc.description, pc.dopinfo as clear_dopinfo, pc.dopinfo, pc.banner, p.id_pages, pc.id_allblocks, p.npp, p.link as linkblock, p.link, pc.x, pc.y, pc.adr, pc.id_pcinherit, p.id_inherit, p.canhashtags, p.ishashtags, p.name_hashtags, p.hashtags, p.hiddencontent, allblocks.learnmore, allblocks.more, pc.preinfo FROM pages p inner JOIN pcontent pc ON pc.id_pages=p.id inner JOIN allblocks on allblocks.id=1 WHERE pc.id_allblocks=%d AND p.id_pages=%d AND p.visible=1 order by data desc %s", $params[1], $params[2], $params[3]);
		break;
		

		default:
			$query = $params[0];
			unset ($params[0]);
			$query = sprintf($query, ...$params);
		break;
	}

	if ($query)//если нашелся запрос
	{ $i=0;
		$result = ew_mysqli_query($query);//обработка запроса и получение строк результата

		while ($pages = mysqli_fetch_assoc($result))
		{
			if (ew_mysqli_error())
				exit (ew_mysqli_error());

			$i++;
			$pages['i']=$i;
			
			if (isset($pages['link']))
			{
				if ($pages['id']==INDEX_PAGES)
					$pages['link']='/'; //исключение для главной
				else $pages['link']=UrlManager::createUrl($pages['id']);//получаем ссылку на страницу если она нужна
			}

			if (isset($pages['datatime']))
				$pages['datatime'] = datatimeforhuman($pages['datatime']);

			if (isset($pages['picname']) || isset($pages['picname2']))
			{
				$pages['pic']=FOTO_DIR.nearest ($pages['picname2'], $pages['picname'], NOFOTO);//страхуем фото
				$pages['picname']=FOTO_DIR.$pages['picname'];
				$pages['picname2']=FOTO_DIR.$pages['picname2'];
			}
			else $pages['pic']=FOTO_DIR.NOFOTO;

			if (isset($pages['filename']))
			{
				$pages['filename']=DOCS_DIR.$pages['filename'];
				$pages['fil']=DOCS_DIR.nearest($pages['filename'], NOFILE);//файл по умолчанию
			}

			if(isset($pages['id_inherit']) && $pages['id_inherit'])//если установлено нследование для страницы
			{
				$res[] = getLightArray(getSQLdata (PAGES_ITEM_QUERY, $pages['id_allblocks'], $pages['id_inherit']));//подменяем id искомой страницы на id страницы, с которой надо произвести наследование информации. По-умолчанию id должен быть в 3й ячейке массива	
				continue;
			}
			
			/*
			if (isset($pages['picname_chalenge']))
				$pages['picname2'] = nearest(...explode('-', $pages['picname_chalenge']));
			
			if (isset($pages['clear_dopinfo']))
				$pages['clear_dopinfo']=nearest (strip_tags($pages['clear_dopinfo']), 'general-ico'); //иконка для категорий фоток на старнице продукта
			*/


			//действия над полученными данными в зависимости от типа
			switch ($querytype)
			{
				case MENU_QUERY:

					if ($pages['childlistview'])//если нужны потомки и не товары
					{
						$pages['children'] = getSQLdata (PODMENU_QUERY, ID_ALLBLOCKS, $pages['id'], $pages['link'], $params[2]);//передаем id страницы родителя и ее ссылку на всякий случай, получим информацию о товарах отдельно
					}

					if ($pages['hiddencontent'])//у страницы скрытый контент
						$pages['link']=nearest($pages['children'][0]['link'], '/');//передаем ссылку на дочернюю страницу или на главную, контент закрыт

					if ($params[2]==$pages['id'] || $params[3]==$pages['id'])//если совпадает с id текущейс страницы или с id страницы родителя
						$pages['class']='_active';
				break;

				case PODMENU_QUERY:
					if ($pages['hiddencontent'])//если нужны потомкиvb0
							$pages['link'] = $params[3];//передаем ссылку на родительскую страницу, контент закрыт
					if ($params[4] == $pages['id'])//если совпадает с id текущей страницы
						$pages['class'] = '_active';
				break;
				
				case FULL_PODPAGES_QUERY:
				case FULL_PAGES_ITEM_QUERY:
					$pages['children'] = getSQLdata (PODPAGES_QUERY, $pages['id_allblocks'], $pages['id']);
				case PAGES_ITEM_QUERY:		
				case PODPAGES_QUERY:
				case PROJECTS_QUERY:
				case LAST_NEWS_QUERY:
				case NEWS_PER_YEAR_QUERY:
					$pages['foto']=arrRekompose(getSQLdata (PAGES_FOTO_QUERY, $pages['id_allblocks'], $pages['id']), 'type');//собираем все фотки страницы
					$pages['docs']=arrRekompose(getSQLdata (PAGES_DOCS_QUERY, $pages['id_allblocks'], $pages['id']), 'type');//собираем все документы страницы
				break;

				case NEWS_QUERY:
				case LAST_NEWS_QUERY:
				case CALENDAR_QUERY:
					$pages['data']=dataforhuman($pages['data']);
					$pages['foto']=arrRekompose(getSQLdata (PAGES_FOTO_QUERY, $pages['id_allblocks'], $pages['id']), 'type');//собираем все фотки страницы
					$pages['docs']=arrRekompose(getSQLdata (PAGES_DOCS_QUERY, $pages['id_allblocks'], $pages['id']), 'type');//собираем все документы страницы
				break;
				
				// case NEWS_QUERY:
				// 	$k=200;
				// 	$pages['sokr']=nearest($pages['description'], badnews ($pages['info'].' '.$pages['dopinfo'], $k, ' ', '...'));
					
				// 	$pages['foto']=arrRekompose(getSQLdata (PAGES_FOTO_QUERY, $pages['id_allblocks'], $pages['id']), 'type');//собираем все фотки страницы
				// break;
				
				case INDEX_NEWS_QUERY:
					$k=300;
					$pages['sokr']=nearest($pages['description'], badnews ($pages['info'].' '.$pages['dopinfo'], $k, ' ', '...'));
					$pages['data']=dataforhuman($pages['data']);
					
					$pages['foto']=arrRekompose(getSQLdata (PAGES_FOTO_QUERY, $pages['id_allblocks'], $pages['id']), 'type');//собираем все фотки страницы
				break;
				
				case BACKCHAT_QUERY:
					$pages['data']=datatimeforhuman($pages['data']);
				break;

				default:
					$pages['data']=dataforhuman($pages['data']);
				break;
			}

			$res[]=$pages;

		}
	}
	return $res;
}

/*функция для определения в сколько столбцов выводить меню
mixed int/arr $count_arr  - количество подстраниц в подменю либо сам массив подстраниц
return возвращает название css класса, который надо поставить
*/
function get_menu_stolbec ($count_arr)
{
	if(is_array ($count_arr))
		$count_arr = count($count_arr);
	
	if($count_arr < 3)
		return "";
	
	if($count_arr < 5)
		return "_two";
	
	if($count_arr < 7)
		return "_three";
	
	if($count_arr < 7)
		return "_three";
	
	return "_four";
}
?>