<?php
/* Подключение к серверу MySQL */
$db = mysqli_connect('localhost', 'root', 'root', 'blagomir');
// $db = mysqli_connect('localhost', 'empireweb_test', 'YDVKhhPE', 'empireweb_test8');

if (!$db) {
   printf("Невозможно подключиться к базе данных. Код ошибки: %s\n", mysqli_connect_error());
   exit;
}

mysqli_query($db, "SET NAMES utf8");
date_default_timezone_set('Asia/Yekaterinburg');

define('HOST_NAME', join('.', array_slice(explode('.', trim($_SERVER['HTTP_HOST'], '.')), -2)));

/* if (isset($_SERVER['HTTPS']))
	define ('HOST_PROTOCOL', 'https://');
else define ('HOST_PROTOCOL', 'http://'); */

define ('HOST_PROTOCOL', 'http://');

const FOTO_DIR = '/foto/';//стандартная папка для фоток
const DOCS_DIR = '/docs/';//стандартная папка для документов
const NOFILE = 'closebox.png';
const NOFOTO = 'nofoto.png'; //картинка по умолчанию

//константы уровней доступа для админки
const NO_ACCESS=1, READ_ACCESS=2, WRITE_ACCESS=3;//консанты УД

require_once('ew.function.php');//библиотека функций
require_once('ew.project.function.php');//библиотека доп функций
//require_once('ew.php.auth.php');//библиотека доп функций
require_once('urlmanager.php');
?>
