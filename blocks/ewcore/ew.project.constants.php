<?php 
//вспомогательные константы
const PEOPLE_DIR = '../people_docs/'; 
const FILESIZE_TO_CONVERT = 'ТБ, ГБ, МБ, КБ, Б'; 
const KOL_GOODS_DEFAULT = 12;
const OSN_GOODS_PROPERTY_GLOSSARY = 13;// в id_usl параметр отмечается так, если он важный для товаров
const MAX_FILE_SIZE = 10485760;
const DOC_TYPES = [
	'docx' => 'doc',
	'doc' => 'doc',
	'xlsx' => 'xls',
	'xls' => 'xls',
	'pdf' => 'pdf',
	'rar' => 'rar',
	'zip' => 'rar'
];
const YANDEX_API_KEY = '0a4dd03b-147b-4dba-ba22-bea957f90654';

//константы id страниц для прямых ссылок
const INDEX_PAGES = 1;
const PRESS_CENTER_PAGES = 4;
const SEARCH_PAGES = 7;
const MAINSLIDER_PAGES = 8;
const FUND_ACTIVITY_PAGES = 10;
const OUR_TARGETS_PAGES = 11;
const CLUB_PAGES = 13;
const ADDRESSES_PAGES = 15;
const FAQ_PAGES = 28;
const NEWS_PAGES = 38;
const ACTIVITY_PAGES = 39;
const EVENTS_PAGES = 40;
const DOCS_PAGES = 42;
const HISTORY_PAGES = 43;
const CALENDAR_PAGES = 44;
const SOCIAL_HELP_PAGES = 51;
const OUR_ACTIVITIES_PAGES = 52;
const TEXTSLIDER_HISTORY_PAGES = 53;
const ABOUT_LINKS_PAGES = 54;
const STRUCTURE_FACES_PAGES = 95;
const PEOPLE_PAGES = 96;
const HEROES_PAGES = 100;
const UNITS_PAGES = 102;
const REGIONAL_PAGES = 106;
const HISTORY_POINTS_PAGES = 115;
const CONF_PAGES = 121;
const CLUB_TARGETS_PAGES = 131;
const CLUB_PARTNERS_PAGES = 134;
const MODAL_PAGES = 144;
const MODAL_FACE_PAGES = 145;
const MODAL_REGION_PAGES = 146;
const MODAL_MESSAGE_PAGES = 147;
const MODAL_HEROES_PAGES = 148;


//--------------------

//константы словарей
const HELP_GLOSSARY = 1;//словарь помощи
const SLIDER_GLOSSARY = 2; //словарь типов слайдеров
const DOCS_TYPE_GLOSSARY = 3;//словарь типов документов
const PHP_TEMPLATE_GLOSSARY = 4; //словарь типов исполняемых php страниц
const ERRORS_GLOSSARY = 5;//словарь ошибок и уведомлений
const USERTYPE_GLOSSARY = 6;//словарь ошибок и уведомлений
const CALLBACK_TYPE_GLOSSARY = 7;//словарь для типов обратной связи
const WDGTS_GLOSSARY = 189;//словарь виджетов
const EMAIL_GLOSSARY = 9;//словарь с email шаблонами
//------------------//

//константы типов обращений
const FEEDBACK_MESSAGE = 1;
const CLUB_FEEDBACK_MESSAGE = 2;
const JOINCLUB_MESSAGE = 3;
const GET_MODAL = 4;
const GET_CONF = 5;
//-----------------------//

//константы ошибок и уведомлений
const MESSAGE_SET_ERR = 1;
const MESSAGE_SET_OK = 2;
const MESSAGE_ERR = 3;
const NO_MODE_ERR = 4;
const FILE_BIGSIZE_ERR = 5;
const FORMAT_FILE_ERR = 6;
const FILE_SEND_ERR = 7;
const NOTHING_FIND_ERR = 8;
const LOWER_SYMBOL_ERR = 9;
//-----------------------------//

//константы email уведомлений
const CALLBACK_MAIL = 1;
//-----------------------------//

//константы прикрепляемых фоток
const SPECIAL_PHOTOS = 1;
const CLUB_PHOTOS = 2;
const NEWS_SMALL_PHOTOS = 3;
const NEWS_LARGE_PHOTOS = 4;
const MANAGEMENT_PHOTOS = 5;
const DOCS_PHOTOS = 6;
const LEFT_SLIDER_PHOTOS = 7;
const RIGHT_SLIDER_PHOTOS = 8;
const CENTRAL_SLIDER_PHOTOS = 9;
const MEDAL_PHOTOS = 10;
const CELEBRATION_PHOTOS = 11;

//------------------------------

//константы типов прикрепления документов
const INVISIBLE_DOCSTYPE = 1; // без отображения
const BOTTOM_DOCSTYPE = 2; // документ внизу страницы
//--------------------------

//константы запросов
const ALLBLOCK_QUERY = 1; //запрос данных из allblocks
const PAGES_ITEM_QUERY = 2; //запрос стандатных данных для pages
const MENU_QUERY = 3;
const PODMENU_QUERY = 4;
const PODPAGES_QUERY = 5;
const SIMPLY_PODPAGES_QUERY = 6;//для запроса подстраниц pages без фото и документов
const PAGES_FOTO_QUERY = 8;
const PAGES_DOCS_QUERY = 9;
const SINGLE_PAGES_ITEM_QUERY=10;
const FULL_PODPAGES_QUERY = 12;
const SINGLE_FOTO_QUERY = 113;
const REVIEWS_PHOTO_QUERY = 69;
const BEST_REVIEWS_PHOTO_QUERY = 70;
const GET_CONTACTS_TITLE = 71;
const ANSWERS_QUERY = 72;
const COUNT_NEWS = 73;
const GET_ROW_POSITION = 74;
const PROJECTS_QUERY = 75;
const LAST_NEWS_QUERY = 76;
const NEWS_PER_YEAR_QUERY = 77;
const NEWS_MAIN_SLIDER_PHOTOS_QUERY = 78;
const NEWS_FOR_MAIN_SLIDER_QUERY = 79;
const PROJECTS_YEARS_QUERY = 81;
const YEARS_QUERY = 82;
const CALENDAR_QUERY = 83;




const INDEX_NEWS_QUERY = 13;
const COUNT_PAGES_QUERY = 14;
const NEWS_QUERY = 15;
const SIMPLY_NEWS_QUERY = 16;
const COUNT_BACKCHAT_QUERY = 17;
const BACKCHAT_QUERY = 18;
const SEARCH_QUERY = 19;
const ALL_FILTER_QUERY = 20;
const CATEGORY_QUERY = 21;
const FULL_PAGES_ITEM_QUERY = 22;
const FILTER_GOODS_QUERY = 23;
const PROPERTY_GOODS_QUERY = 24;
const GOODS_QUERY = 25;
const FILTER_QUERY = 27;
const METKA_FILTER_QUERY = 28;
const GOODS_ITEM_QUERY = 29;
const MINMAX_GOODS_ITEM_QUERY = 30;
const MOBILMENU_QUERY = 31;

//--------------//


//константы html-шаблонов

?>