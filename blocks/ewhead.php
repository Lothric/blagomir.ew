<title><?= strip_tags(nearest($p['direct_title'], $p['title']." | ".$all['name']))?></title>

<meta name="description" content="<?= strip_tags($p['description']." ".$all['description']);?>">
<meta name="keywords"  content="<?= strip_tags($p['keywords'].' , '.$all['keywords']); ?>">

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
<link rel="manifest" href="/favicon/site.webmanifest">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">


<link rel="stylesheet" href="/css/normalize.css">
<link rel="stylesheet" href="/css/reset.css">
<link rel="stylesheet" href="/libs/swiper/css/swiper.min.css">
<link rel="stylesheet" href="/libs/scroll/jquery.classyscroll.css">
<link rel="stylesheet" href="/css/iconfont.css">
<link rel="stylesheet" href="/libs/datapicker/css/datepicker.min.css">
<link rel="stylesheet" href="/css/style.css">
<link rel="stylesheet" href="/css/add.css">
<link rel="stylesheet" href="/libs/fancybox/jquery.fancybox.min.css">