<footer class="footer">
    <div class="container">
        <div class="footer__contacts">
            <h2 class="cl-white"><?= $all['footertitle'] ?></h2>
            <p><a class="phone-link" href="tel:<?= $all['clear_tel'] ?>"><?= $all['tel'] ?></a></p><br>
            <br>
            <p><?= $all['adr'] ?></p><br>
            <br>
            <p><a class="email-link" href="mailto:<?= $all['email'] ?>"><?= $all['email'] ?></a></p>
        </div>
        <div class="footer-menu">

            <?php
                $i = 0;

                foreach ($downMenu as $index=>$m) {

                    if ($i == 0)
                        echo '<div class="footer-menu__block">';

                    if ($m['children']) {

                        if ($i != 0) {
                            echo '</div><div class="footer-menu__block">';
                            $i = 0;
                        }

                        echo writeTemplate($m, '<a class="footer-menu__title" href="#link#">#title#</a>');
                        echo '<div class="footer-menu__list">';
                        foreach ($m['children'] as $child) {
                            echo writeTemplate ($child, '<div class="footer-menu__item"><a class="footer-menu__link" href="#link#">#title#</a></div>');
                        }
                        echo '</div></div>';
                    }
                    else {
                        echo writeTemplate($m, '<a class="footer-menu__title" href="#link#">#title#</a>');
                        $i++;
                        if ($i == 3) {
                            $i = 0;
                            echo '</div>';
                        }
                    }

                    if ($index == count($downMenu) - 1 && $i != 0) {
                        echo '</div>';
                    }
                }
            ?>

        </div>
        <div class="socials">
            <a class="social i-youtube" target="_blank" rel="noreferrer" href="<?= $all['youtube'] ?>"></a>
            <a class="social i-facebook" target="_blank" rel="noreferrer" href="<?= $all['facebook'] ?>"></a>
            <a class="social i-vk" target="_blank" rel="noreferrer" href="<?= $all['vk'] ?>"></a>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <p class="admin"><?= $all['untertextleft'] ?></p>
        </div>
    </div>
</footer>