<section class="section classic-text _fon">
    <div class="container">
        <div class="textblock">
            <div class="textblock__item _md">

                <?php
                    if ($parentPages && $parentPages[0] && $index == 0) {
                        echo writeTemplate($parentPages[count($parentPages) - 1], '
                            <a class="go-back" href="#link#">
                                <div class="button-prev i-arrow _sm"></div>#title#
                            </a>
                        ');
                    }
                ?>

                <h1><?= $sub['title'] ?></h1>
                <?= $sub['info'] ?>
            </div>

            <?php
                if ($sub['foto'][RIGHT_SLIDER_PHOTOS]) {
                    echo '<div class="slider _lg"><div class="swiper-container"><div class="swiper-wrapper">';

                    foreach ($sub['foto'][RIGHT_SLIDER_PHOTOS] as $photo) {
                        if ($photo['title']) {
                            $photo['desc'] = writeTemplate($photo, '<div class="slider__desc">#title#</div>');
                        }
                        else {
                            $photo['title'] = $sub['title'];
                        }

                        echo writeTemplate($photo, '
                            <div class="swiper-slide">
                                <div class="slider__item">
                                    <div class="slider__preview"><img class="slider__img" src="#picname2#" alt="#title#"></div>
                                    #desc#
                                </div>
                            </div>
                        ');
                    }

                    echo '</div></div>';

                    if (count($sub['foto'][RIGHT_SLIDER_PHOTOS]) > 1) {
                        echo '<div class="button-prev i-arrow"></div><div class="button-next i-arrow"></div>';
                    }

                    echo '</div>';
                }
            ?>

            <!-- <div class="slider _lg">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="slider__item">
                                <div class="slider__preview"><img class="slider__img" src="/img/img-right1.jpg" alt=""></div>
                                <div class="slider__desc">Фото с мероприятия годовщины фонда</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="slider__item">
                                <div class="slider__preview"><img class="slider__img" src="/img/img-right.jpg" alt=""></div>
                                <div class="slider__desc">Второй слайд</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button-prev i-arrow"></div>
                <div class="button-next i-arrow"></div>
            </div> -->
        </div>
        <div class="textblock">
            <div class="textblock__item">
                <?= $sub['dopinfo'] ?>
            </div>
            <div class="textblock__item">
                <?= $sub['banner'] ?>
            </div>
        </div>
    </div>
</section>