<?php
    $search = getSQLdata(SINGLE_PAGES_ITEM_QUERY, ID_ALLBLOCKS, SEARCH_PAGES)[0];
    if ($p['id'] == 1) {
        $class = ' _index';
    }
?>

<header class="header<?= $class ?>">
    <div class="container"><a class="header__logo" href="/"><img class="header__logo-img" src="/img/logo.png" alt="">
            <div class="header__logo-desc"><?= $all['pretitle'] ?></div>
        </a>
        <nav class="menu">
            <ul class="menu__list">

                <?php
                    foreach ($menu as $m) {

                        if ($p['id'] == $m['id'] || ($parentPages && $parentPages[0] == $m['id']))
                            $m['class'] = ' _active';

                        if ($m['children']) {
                            echo writeTemplate($m, '<li class="menu__item _drop #class#"><a class="menu__link" href="#link#">#title#</a><ul class="submenu">');
                            echo writeItemsTemplate($m['children'], '<li class="submenu__item"><a class="submenu__link" href="#link#">#title#</a></li>');
                            echo '</ul></li>';
                        }
                        else {
                            echo writeTemplate($m, '<li class="menu__item#class#"><a class="menu__link" href="#link#">#title#</a></li>');
                        }
                    }
                ?>
            </ul>
        </nav><a class="search" href="<?= $search['link'] ?>"></a>
    </div>
</header>
<div class="navbar-toggle"></div>
<div class="mobile__overlay"></div>
<div class="mobile">
    <div class="mobile-container">
        <nav class="mobile-menu">

            <?php
                foreach ($menu as $m) {

                    if ($m['children']) {
                        echo writeTemplate($m, '<div class="mobile-menu__block"><a class="mobile-menu__title" href="#link#">#title#</a><ul class="mobile-menu__list">');
                        echo writeItemsTemplate($m['children'], '<li class="mobile-menu__item"><a class="mobile-menu__link" href="#link#">#title#</a></li>');
                        echo '</ul></div>';
                    }
                    else {
                        echo writeTemplate($m, '<div class="mobile-menu__block"><a class="mobile-menu__title" href="#link#">#title#</a></div>');
                    }
                }
            ?>

        </nav>
        <div class="mobile__bottom">
            <p><a class="phone-link" href="tel:<?= $all['clear_tel'] ?>"><?= $all['tel'] ?></a></p><br>
            <br>
            <p><?= $all['adr'] ?></p><br>
            <br>
            <p><a class="email-link" href="mailto:<?= $all['email'] ?>"><?= $all['email'] ?></a></p>
            <div class="socials">
                <a class="social i-youtube" target="_blank" rel="noreferrer" href="<?= $all['youtube'] ?>"></a>
                <a class="social i-facebook" target="_blank" rel="noreferrer" href="<?= $all['facebook'] ?>"></a>
                <a class="social i-vk" target="_blank" rel="noreferrer" href="<?= $all['vk'] ?>"></a>
            </div>
        </div>
    </div>
</div>