<?php
    $modals = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, MODAL_PAGES);
?>

<div class="modal">
    <div class="modal__overlay">

        <?php
            foreach ($modals as $modal) {
                $modal['dopinfo'] = writeTemplate($modal, $modal['dopinfo']);
                echo $modal['dopinfo'];
            }
        ?>
        
    </div>
</div>