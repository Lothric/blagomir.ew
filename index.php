<?php
    $_GET['p'] = 1;
    require_once('blocks/ewinit.php');
    $p['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $p['id']);
?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <?php
        require_once('blocks/ewhead.php');
        require_once('blocks/jslibs.php');
    ?>
</head>

<body>
    <div class="page-preloader">
        <div class="preloader"></div>
    </div>
    <div class="root">

        <?php
            require_once('blocks/menu.php');
        ?>

        <main>
            
            <?php 
                foreach ($p['children'] as $sub) {
                    include $sub['linkblock'];
                }
            ?>

        </main>
        
        <?php
            require_once('blocks/ewfooter.php');
            require_once('blocks/modal.php');
        ?>
        
        <script src="js/index.js" defer></script>
    </div>

    <?php
        require_once('blocks/unterblock.php');
    ?>

</body>

</html>