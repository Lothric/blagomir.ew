<?php
    require_once('blocks/ewinit.php');
    $p['children'] = getSQLdata(PODPAGES_QUERY, ID_ALLBLOCKS, $p['id']);
?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <?php
        require_once('blocks/ewhead.php');
        require_once('blocks/jslibs.php');
    ?>
</head>

<body>
    <div class="page-preloader">
        <div class="preloader"></div>
    </div>
    <div class="root">

        <?php
            require_once('blocks/menu.php');
        ?>

        <main>

            <?php
                // if ($parentPages && $parentPages[0])
                    require_once('blocks/breadcrumbs.php');
            ?>

            <?php 
                foreach ($p['children'] as $index=>$sub) {
                    if ($sub['linkblock']) {
                        if ($sub['hiddencontent'])
                            include $sub['linkblock'];
                    }

                    else {
                        if ($sub['hiddencontent'])
                            include('blocks/multipages/text.php');
                    }
                }
            ?>

        </main>

        <?php
            require_once('blocks/ewfooter.php');
            require_once('blocks/modal.php');
        ?>

    </div>

    <?php
        require_once('blocks/unterblock.php');
    ?>

</body>

</html>