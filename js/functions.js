// Общий для всего проекта JS-код

$(window).on('load', function () {

	// Прелоадер
	var $preloader = $('.page-preloader'),
		$spinner = $preloader.find('.preloader');
	$spinner.fadeOut();
	$preloader.delay(500).fadeOut('slow');



	// Бургер меню
	function mobileMenu() {
		$('.navbar-toggle').on('click', function() {
			$(this).toggleClass('_open');
			$('.mobile__overlay').fadeToggle(300);
			$('.mobile').toggleClass('_open');
			$('body').toggleClass('no-scroll');
		});

		$('.mobile__overlay').on('click', function () {
			$(this).fadeOut(300);
			$('.navbar-toggle').removeClass('_open');
			$('.mobile').removeClass('_open');
			$('body').removeClass('no-scroll');
		})
	}
	mobileMenu();


	// Ответы на вопросы
	$('.question__message').click(function () {
			$(this).toggleClass('_open').next().slideToggle();
			$('.question__message').not(this).removeClass('_open').next().slideUp();
	});


	// Слайдер героев
	$('.heroes-slider .swiper-container').each(function(){
		var heroesSlider = new Swiper (this, {
			autoplay: true,
			autoplay: {
				delay: 4000,
				disableOnInteraction: false,
			},
			speed: 600,
			loop: true,
			slidesPerView: 3,
			observer: true,
    			observeParents: true,

			navigation: {
				nextEl: $(this).parent().find('.button-next'),
				prevEl: $(this).parent().find('.button-prev'),
			},

			breakpoints: {
				1181: {
					slidesPerView: 3,
					spaceBetween: 25,
				},

				810: {
					slidesPerView: 2,
					spaceBetween: 0,
				},

				320: {
					slidesPerView: 1,
					spaceBetween: 0,
				},
			}
		})
	});


	// Слайдер героев
	$('.team-slider .swiper-container').each(function(){
		var teamSlider = new Swiper (this, {
			// autoplay: true,
			// autoplay: {
			// 	delay: 4000,
			// 	disableOnInteraction: false,
			// },
			speed: 600,
			loop: true,
			observer: true,
    			observeParents: true,

			navigation: {
				nextEl: $(this).parent().find('.button-next'),
				prevEl: $(this).parent().find('.button-prev'),
			},

			breakpoints: {
				1181: {
					slidesPerView: 4,
					spaceBetween: 35,
				},

				900: {
					slidesPerView: 3,
					spaceBetween: 0,
				},

				620: {
					slidesPerView: 2,
					spaceBetween: 0,
				},

				320: {
					slidesPerView: 1,
					spaceBetween: 0,
				},
			}
		})
	});


	// Слайдер целей
	if( $('.target-slider').length ) {
		// Слайдер слева
		var targetSlider = new Swiper('.target-slider', {
			autoplay: true,
			autoplay: {
				delay: 4000,
				disableOnInteraction: false,
			},
			speed: 600,
			loop: true,
			spaceBetween: 25,

			pagination: {
				el: '.pagination-slider',
				type: 'bullets',
				clickable: true,
			}
		})
	};



	// Слайдер целей
	if( $('.about-slider').length ) {
		// Слайдер слева
		var aboutSlider = new Swiper('.about-slider .swiper-container', {
			autoplay: true,
			autoplay: {
				delay: 4000,
				disableOnInteraction: false,
			},
			speed: 600,
			loop: true,

			navigation: {
				nextEl: '.about-next',
				prevEl: '.about-prev',
			},
		})
	};



	// Слайдер клуба
	$('.slider .swiper-container').each(function(){
		var sliderSlider = new Swiper (this, {
			autoplay: true,
			autoplay: {
				delay: 4000,
				disableOnInteraction: false,
			},
			speed: 600,
			loop: true,
			spaceBetween: 25,
			observer: true,
    			observeParents: true,

			navigation: {
				nextEl: $(this).parent().find('.button-next'),
				prevEl: $(this).parent().find('.button-prev'),
			},
		})
	});



	// Слайдер клуба
	if( $('.slider-left').length ) {
		// Слайдер слева
		var sliderLeftSlider = new Swiper('.slider-left', {
			autoplay: true,
			autoplay: {
				delay: 4000,
				disableOnInteraction: false,
			},
			speed: 600,
			loop: true,
			loopedSlides: 5,

			pagination: {
				el: '.pagination-number',
				type: 'fraction',
				renderFraction: function (currentClass, totalClass) {
					return '<span class="' + currentClass + '"></span>' +
					'/' +
					'<span class="' + totalClass + '"></span>';
				},
			},

			navigation: {
				nextEl: $('.slider-left').find('.button-next'),
				prevEl: $('.slider-left').find('.button-prev'),
			},

			breakpoints: {
				1241: {
					slidesPerView: 4,
					spaceBetween: 15,
				},

				930: {
					slidesPerView: 3,
					spaceBetween: 0,
				},

				601: {
					slidesPerView: 2,
					spaceBetween: 0,
				},

				320: {
					slidesPerView: 1,
					spaceBetween: 15,
				},
			}
		})
	};




	// Слайдер клуба
	if( $('.classic-slider').length ) {
		// Слайдер слева
		var classicSlider = new Swiper('.classic-slider', {
			autoplay: true,
			autoplay: {
				delay: 4000,
				disableOnInteraction: false,
			},
			speed: 600,
			loop: true,
			loopedSlides: 5,

			pagination: {
				el: '.classic-pagination',
				type: 'fraction',
				renderFraction: function (currentClass, totalClass) {
					return '<span class="' + currentClass + '"></span>' +
					' из ' +
					'<span class="' + totalClass + '"></span>';
				},
			},

			navigation: {
				nextEl: $('.classic-slider').find('.button-next'),
				prevEl: $('.classic-slider').find('.button-prev'),
			},

			breakpoints: {
				1201: {
					spaceBetween: 120,
				},

				601: {
					spaceBetween: 60,
				},

				320: {
					spaceBetween: 20,
				},
			}
		})
	};

	// скролл на карту
	$(".scroll").on('click', function(e) {
		e.preventDefault();

		var top = $('#map').offset().top - 60;
		$('body,html').animate({scrollTop: top}, 1000);
	});
	
	// fancybox
	$().fancybox({
		selector: 'a.gallery',
		loop: true,
		preventCaptionOverlap: true,
		infobar: true
	});

	// маска телефонного номера
	$('input[name=tel]').each(function() {
		new Cleave(this, {
			prefix: '+7',
			//noImmediatePrefix: true,
			blocks: [2, 0, 3, 0, 3, 2, 2],
			delimiters: [' ', '(', ')', ' ', '-'],
			numericOnly: true
		});
	});

	// валидация форм
	$('form[name="feedbackform"], form[name="clubform"], form[name="joinform"]').each(function (){
		$(this).validate({
			invalidHandler: function(event, validator) {
				$(this).find('.error').removeClass('error');
			},
			errorPlacement: function(error, element) {
				$(element).parents('label').addClass('error');
			},
			rules: {
				name: "required",
				message: "required",
				tel: {
					required: {
					depends: function(element) {
							return ($(element).parents('form').find('input[name="email"]').val()=='');//телефон обязателен если не введен email
						}
					},
					minlength: 11
				},
				email: {
					required: {
						depends: function(element) {
							return ($(element).parents('form').find('input[name="tel"]').val()=='');//email обязателен если не введен телефон
						}
					},
					email: true
				},
				checkbox: "required"
			},
			submitHandler: function(form, event) {
				event.preventDefault();
				onSubmit($(form));
			}
		});
	});

	// ссылка на админку
	$('.admin').click(function () {
		window.location = '/admin';
	});

	// достаём имя прикреплённого файла
	$('input[type=file]').change(function (e) {
		if (this.files) {
			let reader = new FileReader();
			reader.onload = e => {
				file = {
					name: this.files[0].name,
					file: e.target.result
				};
			}
			reader.readAsDataURL(this.files[0]);
			$('.file__text').text(this.files[0].name);
		}
	});

	// Обертка таблицы
	$('table').wrap("<div class='table'></div>");

	// Обертка картинки
	$('.img-right').wrap("<div class='img-right__block'></div>");



	// Табы
	$('.tabs__label').on('click', function() {
		$(this)
			.addClass('_active').siblings().removeClass('_active')
			.closest('.tabs').find('.tabs__content').removeClass('_active').eq($(this).index()).addClass('_active');
	});

	// -----------------------------------------------------
	// постраничный вывод в табах
	// -----------------------------------------------------
	function showConf(confs) {
		let holder = $('.tabs__content._active').find('.conferences');

		let massive = '';

		confs.forEach(el => {
			let date = el['data'].split('-');
			el['data'] = date[2] + '.' + date[1] + '.' + date[0];

			if (el['foto'] && el['foto'][news_small_photos]) {
				el['img'] = writeTemplate(el['foto'][news_small_photos][0], '<img src="#picname2#" alt="#title#">');
			}

			massive += writeTemplate(el, '<a class="article-preview" href="#link#"><div class="article-preview__img">#img#</div><div class="article-preview__desc"><div class="date">#data#</div><div class="article-preview__title">#title#</div></div><div class="button-next _sm i-arrow"></div></a>');
		});

		holder.html(massive);
	};

	$('.confpage').click(function (e) {
		e.preventDefault();
		let page = $(this).text();
		let year = $('.tabs__label._active').text().split(' ')[0];

		let req = "pid=" + confpage + "&id_allblocks=1&year=" + year + "&page=" + page + "&mode=5";

		$('.confpage').removeClass('_active');
		$(this).addClass('_active');

		// отправляем запрос
		var xhr = new XMLHttpRequest();

		xhr.open('POST', location.protocol + '//' + location.host + '/ajax/ins.php', true);
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.onreadystatechange = function() { //обработка результата
			if (xhr.readyState == 4)// по завершению загрузки
			{
				if (this.status == 200 && this.response!=null && this.response!='') 
				{
					var resp = this.response;
					
					resp = JSON.parse(resp);

					if(resp[0] == 1) //если всё ок
					{
						showConf(resp[1]);
					}
				}
			}
		};
		
		xhr.send(req); // отправляем запрос с данными формы
	});

	// -----------------------------------------------------
	// -----------------------------------------------------

	$(".modal__content").ClassyScroll();

	// Модальные окна
	function modal() {
		$("[href^='#modal']").on("click", function(event) {
			event.preventDefault();
			$('.modal').fadeIn();

			let id = $(this).attr('calling');
			let modal = $($(this).attr("href"));
			let title = '';

			const showRes = (resp) => {

				// обнуляем все ключи, если их нет
				if (!resp['title'])
					resp['title'] = '';

				if (!resp['preinfo'])
					resp['preinfo'] = '';

				if (!resp['position'])
					resp['position'] = '';

				if (!resp['rank'])
					resp['rank'] = '';

				if (!resp['info'])
					resp['info'] = '';

				if (!resp['dopinfo'])
					resp['dopinfo'] = '';

				if (!resp['banner'])
					resp['banner'] = '';

				resp['medals'] = '';

				// -----------------------------------------------------
				if ($(this).attr("href").split('-')[1] == regional_id) {
					// ставим превью фото
					if (resp['foto'] && resp['foto'][special_photos]) {
						modal.find('.modal__logo').attr('src', resp['foto'][special_photos][0]['picname']);	
					}

					title = resp['title'];
					resp['cleartel'] = resp['position'].replace(/[^\d\+]/g,"");
					resp['desc'] = writeTemplate(resp, ' <h4>#adr#</h4><div class="modal__info"><div class="modal__address">#banner#</div><div class="modal__contacts"><a class="phone-link" href="tel:#cleartel#">#position#</a><br><a class="email-link" href="mailto:#rank#">#rank#</a></div></div>');
				}

				else {
					// ставим превью фото
					if (resp['foto'] && resp['foto'][special_photos]) {
						resp['img'] = writeTemplate(resp['foto'][special_photos][0], '<img class="modal__person-foto" src="#picname#" alt="#title#">');
					}
					else {
						resp['img'] = '';
					}

					title = biography;

					// герой ли это и есть ли ордена
					if ($(this).attr("href").split('-')[1] == heroes_id) {
						if (resp['foto'] && resp['foto'][medal_photos]) {
							let medals = '';

							resp['foto'][medal_photos].forEach(el => {
								medals += writeTemplate(el, '<img class="medal" src="#picname#" alt="#title#">');
							});

							resp['medals'] = '<div class="medals">' + medals + '</div>';
						}
					}

					resp['desc'] = writeTemplate(resp, '<div class="modal__info"><div class="modal__desc"><h4>#title# <span>#preinfo#</span></h4><p>#position#</p></div><div class="modal__rank">#medals##rank#</div></div>');
				}

				// есть ли центральный слайдер
				if (resp['foto'] && resp['foto'][central_slider_photos]) {
					let photos = '';
					
					resp['foto'][central_slider_photos].forEach(element => {
						photos += writeTemplate(element, '<div class="swiper-slide"><div class="slider__item"><div class="slider__preview"><a class="gallery" href="#picname#"><img class="slider__img" src="#picname2#" alt="#title#"></a></div>');
						if (element['title']) {
							photos += writeTemplate(element, '<div class="slider__desc">#title#</div>');
						}
						photos += '</div></div>';
					});

					pg = resp['foto'][central_slider_photos].length > 1 ? '<div class="button-prev i-arrow"></div><div class="button-next i-arrow"></div>' : '';
					resp['slider'] = '<div class="slider _lg"><div class="swiper-container"><div class="swiper-wrapper">' + photos + '</div></div>' + pg + '</div>';
				}
				else
					resp['slider'] = '';

				// ---------------------------------------------------
				// собираем модальное окно
				// ---------------------------------------------------

				modal.find('.modal__title').text(title);

				if (resp['img']) {
					modal.find('.modal__person-preview').html(resp['img']);
				}

				modal.find('.loader-holder').fadeOut();
				modal.find('.modal__content').html(writeTemplate(resp, '#desc##info##slider##dopinfo#'));

				$(".modal__content").ClassyScroll();

				$('.slider .swiper-container').each(function(){
					var sliderSlider = new Swiper (this, {
						autoplay: true,
						autoplay: {
							delay: 4000,
							disableOnInteraction: false,
						},
						speed: 600,
						loop: true,
						spaceBetween: 25,
						observer: true,
							observeParents: true,
			
						navigation: {
							nextEl: $(this).parent().find('.button-next'),
							prevEl: $(this).parent().find('.button-prev'),
						},
					})
				});

				// ---------------------------------------------------
			};

			fd = "id=" + id + "&id_allblocks=1&mode=4";

			// отправляем запрос
			var xhr = new XMLHttpRequest();

			xhr.open('POST', location.protocol + '//' + location.host + '/ajax/ins.php', true);
			xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xhr.onreadystatechange = function() { //обработка результата
				if (xhr.readyState == 4)// по завершению загрузки
				{
					if (this.status == 200 && this.response!=null && this.response!='') 
					{
						var resp = this.response;
						
						resp = JSON.parse(resp);

						if(resp[0] == 1) //если всё ок
						{
							showRes(resp[1]);
						}
						// else //иначе выводим полученное сообщение
						// {
						// 	if($window.length)
						// 		windows_close($window, false); //закрываем окно, но не трогаем фон
							
						// 	but.show();
						// 	uialert(resp[1], resp[2]);
						// }
					}
				}
			};
			
			xhr.send(fd); // отправляем запрос с данными формы

			modal.fadeIn();
			$('body').addClass('no-scroll');
		});

	}

	// меняем надпись кнопки на название прикреплённого файла
	$('input[type=file]').change(function (e) {
		if (this.files) {
			let reader = new FileReader();
			reader.onload = e => {
				file = {
					name: this.files[0].name,
					file: e.target.result
				};
			}
			reader.readAsDataURL(this.files[0]);
			$('.file__button').text(this.files[0].name);
		}
	});

	$(".modal__overlay, .modal-close, .button._clear, .modal__button").on("click", function(e) {
		if(e.target != this) 
			return;
		$('.modal').fadeOut();
		$(".modal__inner").fadeOut();
		$('body').removeClass('no-scroll');
	});
	
	modal();
});

function onSubmit(requestform, onSuccess) {
	var t = requestform,
		but=t.find('.button'),
		loader = $('#loader'),
		$window = t.parents('.form-block__content');
	
	if(onSuccess===undefined)
		onSuccess = function (resp, t) {
			$('#modal-' + message_id).find('.modal-message').html(resp[2]);
			$('.modal').fadeIn();
			$('#modal-' + message_id).fadeIn();
		};
	
	but.hide();
	loader.fadeIn(500);
	
	var fd = new FormData(t[0]); //собираем данные формы для отправки
	var xhr = new XMLHttpRequest(); // создаем новый запрос

	if (t.find('input[type=file]').prop("files")) {
		var file_data = t.find('input[type=file]').prop("files")[0];
    	fd.append("file", file_data);
	}
	
	xhr.open('POST', location.protocol + '//' + location.host + '/ajax/ins.php', true);
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhr.onreadystatechange = function() { //обработка результата
		if (xhr.readyState == 4)// по завершению загрузки
		{
			if (this.status == 200 && this.response!=null && this.response!='') 
			{
				var resp = this.response;
				
				resp = JSON.parse(resp);

				if(resp[0] == 1) //если всё ок
				{
					onSuccess(resp, t);
				}
				else //иначе выводим полученное сообщение
				{
					if($window.length)
						windows_close($window, false); //закрываем окно, но не трогаем фон
					
					but.show();
					uialert(resp[1], resp[2]);
				}
			}
			else {
				if($window.length)
						windows_close($window, false); //закрываем окно, но не трогаем фон
					
				but.show();
				uialert(this.response['title'], this.response['info']);
			}
			loader.hide();
		}
	};
	xhr.onerror = function (){
		if($window.length)
			windows_close($window, false); //закрываем окно, но не трогаем фон
		loader.hide();but.show();
		
		uialert(this.response['title'], this.response['info']);
	};
	xhr.send(fd); // отправляем запрос с данными формы
};