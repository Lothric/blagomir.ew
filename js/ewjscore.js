// JavaScript Document
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* Функция информаировани и редиректа*/
function alertAndRedirect (result)
{
	switch (parseInt(result[0]))
	{
		 
		case 1: 
			if (result[1]!='')
			uialert ({
				info: result[1],
				ok: function (){
					location.href=result[2];
					}
				});
			return 1;
		 break;	
		 
		case 2: 
			return result[1];
		 break;	
		 
		 case 3: 
			if (result[1]!='')
			uialert ({
				title: result[1],
				info:result[2]});
			return 0;
		 break;	
		 
		 case 4: 
			if (result[1]!='')
			uialert ({
				title: result[1],
				info: result[2]});
			return 1;
		 break;	
		 
		 case 5: 
			if (result[3]!='')
			uialert ({
				title: result[1],
				info: result[2],
				ok: function (){
					if (result[3])
						location.href=result[3];
					}
				});
			return 5;
		 break;	
		 
		  case 10: 
			if (result[1]!='')
			uiinfo ({
				title: result[1],
				info: result[2]});
			return 1;
		 break;
		 
		  case 0: 
		  	if (result[2]==null)
				result[2]='/';
				
			if (result[1]!='')
				alert (result[1]);
			else location.href=result[2];
			return false;
		 break;
		 
		default: 
			if (result[1]!='')
			alert (result[1]);
			return true;
		break;				
	}
} 




function isset (param)
{
	if (param===undefined)
	return 0;
	else return 1;
}


//функции создания и приемки куки
function get_cookie ( cookie_name )
{
	var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
	if ( results )
		return ( unescape ( results[2] ) );
	else
		return null;
}
function set_cookie(name, value, expires, path, domain, secure)
{
	if (path==null)
	path="/";
document.cookie =    name + "=" + escape(value) +
((expires) ? "; expires=" + (new Date(expires)) : "") +
((path) ? "; path=" + path : "") +
((domain) ? "; domain=" + domain : "") +
((secure) ? "; secure" : "");
}
function delete_cookie ( cookie_name ){var cookie_date = new Date ( );cookie_date.setTime ( cookie_date.getTime() - 1 );document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString()+"; path=/";}
//-------------------------------	

/*функция проверки поля на формат значения. Предназначена для проверки при вводе значения (событие change или blur). Делает текст в поле красным при ошибке. Может выводить поясняющую информацию об ошибке. При успешной проверке делает вводимый текст зеленым. 
Цвета можно корректировать с помощью параметров ish_color и goodcolor 

char element - идентификатор input в виде текста (например '#input')
char type - ключ определяющий тип поля в соответствии с которым выбирается процесс его обработки
char errorId - идентификатор html блока куда можно выводить сообщение об ошибке errorInfo
*/
function fieldPruff (element, type, errorId, errorInfo)
{
	ish_color = '#5f5c4d';
	goodcolor = '#39c800';
	badcolor = 'red';
	
	$(element).on ('focus',
		function ()
			{
				$(this).css ({'color' : ish_color});
			}
	);
	
	$(element).on ('blur',
		function ()
		{
			var error=false;
			var reg='';
			switch (type)
			{
				case 'int': reg=/([0-9]+)/; break;
				case 'email': reg=/(@{1})/; break;
				case 'data': reg=/(\d{1,2}\.\d{1,2}\.\d{4})/; break;
				case 'gold': reg=/^\d*\.\d{2}$/; break;
				case 'char': reg=/([A-Za-zА-Яа-я-]+)/; break;
				case 'ruschar': reg=/^([А-Яа-я1-9-. ]+)$/; break;
				case 'login': reg=/^([A-Za-z0-9_]){1,20}$/; break;
				case 'pass': reg=/^([A-Za-z0-9]){6,10}$/; break;
				case 'tel': reg=/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{10}$/; break;
				case 'all': reg=/([ A-Za-zА-Яа-я0-9-+]{1,})/; break;
				
				default: $(this).css ({'color' : 'black'}); return; break;
			}
			
			if (!$(this).val().match (reg))
			{
				$(this).css ({'color' : badcolor}); 
				error=true;
			}
			else 
			{
				$(this).css ({'color' : goodcolor});
			}
			
			if (error && errorId!=null && errorInfo!=null)
				$(errorId).html(errorInfo);//вывод ошибки
		}
	);
	return;
}

/*bool функция проверки поля на формат значения. Предназначена для проверки в условии. Возвращает true или false

char element - идентификатор input в виде текста (например '#input')
char type - ключ определяющий тип поля в соответствии с которым выбирается процесс его обработки
array options - массив дополнительных настроек*/
function pruff (element, type, options){

if (type=='' || type==null) return false;

//настройки по-умолчанию
   var settings = $.extend( {
	   passBegin : null, 
	   passEnd : null, 
	   notClass: null,//vj;yj задать класс при наличии которого будет выводиться ошибка
	   errorId : '', 
	   errorInfo : 'Неверно заполнено поле', 
	   errClass : 'input_err',
	   okClass: 'input_ok',
	   value: '',
	   clearErrorInfo: true, // подчищать ли область вывода ошибок
	   onError: function (elm, s){ 
		   elm.removeClass (s.okClass).addClass (s.errClass);
		   if (s.errorId && s.errorInfo)
				$(s.errorId).html(s.errorInfo).fadeIn();//вывод ошибки
	   },
	   onGood: function (elm, s){
		   elm.removeClass (s.errClass).addClass (s.okClass);
	   },
	   onFinaly: function (elm, s){}
    }, options);
	//----------------------

	var error=false;	
	var  reg;
	
	element.removeClass(settings.errClass).remove(settings.okClass);//обнуление класса ошибки
	
	if (settings.errorId && settings.clearErrorInfo)
		$(settings.errorId).hide().html('');//очистка информации об ошибке
	
	switch (type)
	{
		case 'int': reg=/^(\d{1,})$/; break;
			
		case 'data': reg=/^(\d{1,2}\.\d{1,2}\.\d{4})$/;	 break;
		
		case 'email': reg=/(@{1})/;  break;
			
		case 'char': reg=/^(\D{1,})$/; break;
		
		case 'ruschar': reg=/^([А-Яа-я1-9-. ]+)$/; break;
		
		case 'nalich':
		case 'all': reg=/([ A-Za-zА-Яа-я0-9-+]{1,})/; break;
		
		case 'login': reg=/^([A-Za-z0-9_]){1,20}$/; break;
		
		case 'tel': reg=/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/; break;
		
		case 'pass': 
			if (settings.passEnd!=null && settings.passBegin!=null)
			{
				reg='^([A-Za-z0-9]){'+settings.passBegin+','+settings.passEnd+'}$';
				//error = true;
			}
		break;
			
		case 'ravno': 
			reg='^'+settings.value+'$'; 
		break;
		
		case 'consent': //согласие, обычно для галочек перс данных
			reg = (element.is(':checked')) ? /^on$/ : /^disabled$/ ; //д.б. = on	
		break;
		
		
		default: 
			return false;
		break;
	}
	 klass = element.attr('class');


	if ((settings.notClass!=null && (element.attr('class')!== undefined && ~element.attr('class').indexOf(settings.notClass))) || (reg!='' && element.val().search(reg)==-1))//если ошибка
	{
		error = true;
		
		settings.onError(element, settings);
	}
	else //если все хорошо
	{
		settings.onGood(element, settings);
	}
	
	settings.onFinaly(element, settings);
	
return !error;

}

/*функция заполнения шаблона данными ассоциативного массива
	 text* template - html-код шаблона со вставками вида #ТЭГ#
	 array* massiv - кортеж с атрибутами вида ТЭГ, которые и будут искаться в шаблоне 
	 return вовзращает готовый html-код для вставки
	*/
function writeTemplate (massiv, template)
{	
	for (var key in massiv) { 
   		 var val = massiv [key];
    	template=template.replace(new RegExp('#'+key+'#','g'),val);
	} 
	return template;
}


// Strip whitespace (or other characters) from the beginning and end of a string
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: mdsjack (http://www.mdsjack.bo.it)
	// +   improved by: Alexander Ermolaev (http://snippets.dzone.com/user/AlexanderErmolaev)
	// +	  input by: Erkekjetter
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
function trim( str, charlist ) {	
	if (str)
	{
		charlist = !charlist ? ' \s\xA0' : charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
	var re = new RegExp('^[' + charlist + ']+|[' + charlist + ']+$', 'g');
	return str.replace(re, '');
	}
	else return null;
	
}

// Strip HTML and PHP tags from a string
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
function strip_tags( str ){	
	return str.replace(/<\/?[^>]+>/gi, '');
}

/*функция получающая HOST NAME из строки var url*/
function getHostName (url, poddomen)
{
	var pattern = "^(([^:/\\?#]+):)?(//(([^:/\\?#]*)(?::([^/\\?#]*))?))?([^\\?#]*)(\\?([^#]*))?(#(.*))?$"; //регулярное вырожение
	
	if (poddomen!=null)
	poddomen+=".";
	
	/*Описание значений регулярного вырожения
    // Match #0. URL целиком (#0 - это HREF, в терминах window.location). 
    // Например, #0 == "https://example.com:8080/some/path/index.html?p=1&q=2&r=3#some-hash"
    "^" + 
    // Match #1 & #2. SCHEME (#1 - это PROTOCOL, в терминах window.location). 
    // Например, #1 == "https:", #2 == "https"
    "(([^:/\\?#]+):)?" + 
    // Match #3-#6. AUTHORITY (#4 = HOST, #5 = HOSTNAME и #6 = PORT, в терминах window.location)
    // Например, #3 == "//example.com:8080", #4 == "example.com:8080", #5 == "example.com", #6 == "8080"
    "(" + 
        "//(([^:/\\?#]*)(?::([^/\\?#]*))?)" +
    ")?" + 
    // Match #7. PATH (#7 = PATHNAME, в терминах window.location). 
    // Например, #7 == "/some/path/index.html"    
    "([^\\?#]*)" + 
    // Match #8 & #9. QUERY (#8 = SEARCH, в терминах window.location). 
    // Например, #8 == "?p=1&q=2&r=3", #9 == "p=1&q=2&r=3"    
    "(\\?([^#]*))?" + 
    // Match #10 & #11. FRAGMENT (#10 = HASH, в терминах window.location). 
    // Например, #10 == "#some-hash", #11 == "some-hash"
    "(#(.*))?" + "$";
*/
	var resurl = '';//сюда запишем результирующий url
	 var rx = new RegExp(pattern); 
     var parts = rx.exec(url);
		
		resurl = parts[5];
	 var t = resurl.split('.');// #5 == "example.com"
	
	if (t.length==3)//уже есть поддомен
	{
		resurl = t[1]+"."+t[2];
	}
	
	return "http://"+poddomen+resurl+parts[7];
	//теперь надо убрать поддомен, если он есть
	
	
	
}

/**
 * Вернет первую непустую строку из переданных аргументов, если все пустые, вернет пустую строку ''
 * @return string
 */
function nearest() {
	
  for (var arg in arguments) 
  {
	var val = arguments[arg];
    if (val && val!='' && val!=null) 
	return val;
  }
  return null;
}

//вывод названия месяца по числу
function getmonatpertext($monat )
{
	var $massiv = ["Январь",'Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
	
  return $massiv[$monat];
}

/*функция вывода числа с разделением по разрядам (удобно для ценников)*/
function int_per_price (pr)
{
	pr+='';//приводим к строке
	var re = /(?=\B(?:\d{3})+(?!\d))/g
	return pr.replace( re, ' ' );
}

/*
функция для раскодировки специальных html chars
*/
function htmlspecialchars_decode (string, quoteStyle) { // eslint-disable-line camelcase
  //       discuss at: http://locutus.io/php/htmlspecialchars_decode/
  //      original by: Mirek Slugen
  //      improved by: Kevin van Zonneveld (http://kvz.io)
  //      bugfixed by: Mateusz "loonquawl" Zalega
  //      bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //         input by: ReverseSyntax
  //         input by: Slawomir Kaniecki
  //         input by: Scott Cariss
  //         input by: Francois
  //         input by: Ratheous
  //         input by: Mailfaker (http://www.weedem.fr/)
  //       revised by: Kevin van Zonneveld (http://kvz.io)
  // reimplemented by: Brett Zamir (http://brett-zamir.me)
  //        example 1: htmlspecialchars_decode("<p>this -&gt; &quot;</p>", 'ENT_NOQUOTES')
  //        returns 1: '<p>this -> &quot;</p>'
  //        example 2: htmlspecialchars_decode("&amp;quot;")
  //        returns 2: '&quot;'
  var optTemp = 0
  var i = 0
  var noquotes = false
  if (typeof quoteStyle === 'undefined') {
    quoteStyle = 2
  }
  string = string.toString()
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>')
  var OPTS = {
    'ENT_NOQUOTES': 0,
    'ENT_HTML_QUOTE_SINGLE': 1,
    'ENT_HTML_QUOTE_DOUBLE': 2,
    'ENT_COMPAT': 2,
    'ENT_QUOTES': 3,
    'ENT_IGNORE': 4
  }
  if (quoteStyle === 0) {
    noquotes = true
  }
  if (typeof quoteStyle !== 'number') {
    // Allow for a single string or an array of string flags
    quoteStyle = [].concat(quoteStyle)
    for (i = 0; i < quoteStyle.length; i++) {
      // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
      if (OPTS[quoteStyle[i]] === 0) {
        noquotes = true
      } else if (OPTS[quoteStyle[i]]) {
        optTemp = optTemp | OPTS[quoteStyle[i]]
      }
    }
    quoteStyle = optTemp
  }
  if (quoteStyle & OPTS.ENT_HTML_QUOTE_SINGLE) {
    // PHP doesn't currently escape if more than one 0, but it should:
    string = string.replace(/&#0*39;/g, "'")
    // This would also be useful here, but not a part of PHP:
    // string = string.replace(/&apos;|&#x0*27;/g, "'");
  }
  if (!noquotes) {
    string = string.replace(/&quot;/g, '"')
  }
  // Put this in last place to avoid escape being double-decoded
  string = string.replace(/&amp;/g, '&')
  return string
}

/*функция плавного скролинга до элемента
scroll_el - ссылка на блок, к которому скролим вида #id
delay - скорость анимации
*/
function animateScroll (scroll_el, duration )
{
	if (duration ==null || duration  == undefined)
		duration  = 500;
	
	 if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
	    $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, duration); // анимируем скроолинг к элементу scroll_el
        }
}


/*функция вывода красивого модального окна*/
function uialert (options, info)
{
	if (options!= undefined && typeof( options ) == 'string')//если передана строка, то это простой вызов
	{
		if (info!= undefined && info!= '')
			options={
				title: options,
				info: info
			};
		else options={
				title: "Внимание!",
				info: options
			};
	}
	
	//настройки по-умолчанию
    var settings = $.extend( {
	   title : 'Сообщение', 
	   info : "", 
	   before: function (){
		   $('body').addClass('disable-overflow'); 
		   $(settings.ind).remove();
		   $(settings.innerBlock).fadeIn();
		},
	   ok: function (tek){},
	   no: function (tek){},
	   close: function (tek){
		   tek.fadeOut(function (){//скрыание окна и удаление html
			   tek.remove();
			   $('body').removeClass('disable-overflow');
			   $(settings.innerBlock).fadeOut();
		   });
	   },
	   template : '<div class="modal" id="uialert"><div class="modal-close"><svg class="icon icon-close uialert_close"><use xlink:href="/img/svg/icons.svg#icon-close"></use></svg></div><h2>#title#</h2><p>#info#</p><div class="button_blocks"><button class="btn-form uialert_button">#buttonText#</button></div></div>',
	   buttonText: 'Ок',
	   ind: '#uialert',
	   indButton: '.uialert_button',
	   indClose: '.uialert_close',
	   innerBlock: 'div.modal-bg' //вконце какого блока вставлять?
    }, options);
	//----------------------//
	
	settings.before();
	$(settings.innerBlock).append(writeTemplate(settings, settings.template)).find(settings.ind).fadeIn();
	
	//$('body').find(settings.ind).fadeIn();
	
	$(settings.innerBlock).on('click', settings.indButton, function(){
								settings.ok();
								settings.close($(settings.ind));
							})
						 .on('click', settings.indClose, function(){
								settings.no();
								settings.close($(settings.ind));
							});
}

/*функция принимает jquery ссылку на форму или же непосредственно массив,
и формирует строку get запроса из всех внутренних input и textarea*/
function getInputDataToGetUrl ($form)
{
	var data= new Array(),
		t = new Array(),
		res = '';
	if ($form)
	{
		if ($.isArray($form))//пришел массив
		{
			for (var key in $form)
			{
				t[t.length]=key+"="+$form[key];
			}
			
			res = t.join('&');
			
			return res;
		}
		
		if ($.isPlainObject($form))
		{
			$form.find('input, textarea').each(function(){//собираем данные
				data[$(this).attr('name')]=$(this).val();
				t[t.length]=$(this).attr('name')+"="+$(this).val();
			});

			res = t.join('&');
			
			return res;
		}
		
		//if ($.type($form) === "string")//если строка
		
		
	}
	
	return res;
}

/*функция js считывания get параметров*/
function getUrlVars(url) {

  // извлекаем строку из URL или объекта window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
  queryString= UrlDecoding(queryString);
	//console.log(queryString);
  // объект для хранения параметров
  var obj = {};

  // если есть строка запроса
  if (queryString) {

    // данные после знака # будут опущены
    queryString = queryString.split('#')[0];

    // разделяем параметры
    var arr = queryString.split('&');
	
	if (arr.length!=undefined)
		for (var i=0; i<arr.length; i++) {
		  // разделяем параметр на ключ => значение
		  var a = arr[i].split('=');

		  // обработка данных вида: list[]=thing1&list[]=thing2
		  var paramNum = undefined;
		  var paramName = a[0].replace(/\[\d*\]/, function(v) {
			paramNum = v.slice(1,-1);
			return '';
		  });

		  // передача значения параметра ('true' если значение не задано)
		  var paramValue = typeof(a[1])==='undefined' ? true : a[1];

		  // преобразование регистра
		  //paramName = paramName.toLowerCase();
		  //paramValue = paramValue.toLowerCase();

		  // если ключ параметра уже задан
		  if (obj[paramName]) {
			// преобразуем текущее значение в массив
			if (typeof obj[paramName] === 'string') {
			  obj[paramName] = [obj[paramName]];
			}
			// если не задан индекс...
			if (typeof paramNum === 'undefined') {
			  // помещаем значение в конец массива
			  obj[paramName].push(paramValue);
			}
			// если индекс задан...
			else {
			  // размещаем элемент по заданному индексу
			  obj[paramName].push(paramValue);
			}
		  }
		  // если параметр не задан, делаем это вручную
		  else {
			obj[paramName] = paramValue;
		  }
		}
  }

  return obj;
}

/*функция создания url строки из массива js*/
function getUrlString (arr)
{
	var str = [];
	
	if (arr!=undefined)
	{
		
		for(var p in arr) {
			
			if (typeof arr[p] == 'object' && arr[p] != null)//если у нас подмассив
			{
				for(var i in arr[p]) 
					str.push(p + "[]=" + arr[p][i]);
			}
			else str.push(p + "=" + arr[p]);

		}
	}
	
	return '?'+str.join("&");
}

/*функции кодирования и декодирования url */
function UrlCoding(url) {
    return encodeURIComponent(url);
}
function UrlDecoding(url) {
    return decodeURIComponent(url.replace(/\+/g,  " "));
}
//---------------------------------------//

/*функция подсчета количества элементов в объекте*/
function countElmToObjct (obj)
{
	var k = 0;
	for(var i in obj)
		k++;
	return k;
}

//проверка что браузер это IE 
function IEBrowser()
{
	var ua = navigator.userAgent;    
	return (ua.search(/MSIE/) > -1 || ua.search(/Trident/) > -1) ? true : false;
}

/*проверяет является ли строка json*/
function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}