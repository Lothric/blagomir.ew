let coords = [];
let index = 0;
$('.scroll').each(function () {
    coords[index] = {};
    coords[index]['center'] = [$(this).attr('x'), $(this).attr('y')];
    coords[index]['adr'] = $(this).attr('adr');
    index++;
});

ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [$('.scroll').attr('x'), $('.scroll').attr('y')],
            zoom: 18,
            controls: []
        }, {}),

        adrCollection = new ymaps.GeoObjectCollection(null, {
            preset: 'default#image'
        });

        for (var i = 0, l = coords.length; i < l; i++) {
            let pm = new ymaps.Placemark(coords[i]['center'], {
                balloonContent: coords[i]['adr']
            }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: '../img/icon.png',
                    // Размеры метки.
                    iconImageSize: [47, 63],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-23, -63],
                    hideIconOnBalloonOpen: false
            });

            pm.events.add('open', function(e) {
                e.get('target').options.set('iconImageHref', '../img/icon-f.png');
            });

            pm.events.add('close', function(e) {
                e.get('target').options.set('iconImageHref', '../img/icon.png');
            });

            adrCollection.add(pm);
        }
    
        myMap.geoObjects.add(adrCollection);

    $('.scroll').click(function (e) {
        e.preventDefault();
        let x = $(this).attr('x');
        let y = $(this).attr('y');
        myMap.setCenter([x,y]);
    })
});