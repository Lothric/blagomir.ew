// JS-код для главной страницы

$(window).on('load', function () {
	// Слайдер клуба
	if( $('.test-slider').length ) {
		// Слайдер слева
		var testSlider = new Swiper('.test-slider', {
			autoplay: true,
			autoplay: {
				delay: 6000,
				disableOnInteraction: false,
			},
			speed: 2800,
			loop: true,
			effect: 'fade',
			simulateTouch: false,
			pagination: {
				el: '.test-pagination',
				type: 'bullets',
				clickable: true,
			},

			breakpoints: {
				1025: {
					speed: 2800,
				},

				320: {
					speed: 2000,
				},
			},
		});

		var activeSlide;

		testSlider.on('slideChangeTransitionStart', function(){
			activeSlide = testSlider.realIndex;
			$('.test-imgs__left .test-imgs__img').removeClass('_active')
								.eq(activeSlide).addClass('_active');
			$('.test-imgs__right .test-imgs__img').removeClass('_active')
								.eq(activeSlide).addClass('_active');

			$('.test-anim__fon').addClass('_up');
			setTimeout(function() {
				$('.test-anim__fon').removeClass('_up');
				$('.test-anim__img').removeClass('_active');
				$('.test-anim__img').eq(activeSlide).addClass('_active');
				$('.test-anim__fon').addClass('_down');
			}, 1400);
			setTimeout(function() {
				$('.test-anim__fon').removeClass('_down');
			}, 2800);
		});
	};



	if( $('.index-news__slider').length ) {
		// Слайдер слева
		var indexNewsSlider = new Swiper('.index-news__slider', {
			autoplay: true,
			autoplay: {
				delay: 5000,
				disableOnInteraction: false,
			},
			speed: 1000,
			loop: false,
			spaceBetween: 40,
			pagination: {
				el: '.pagination-slider',
				type: 'bullets',
				clickable: true,
			}
		});
	};


	// Ответы на вопросы
	$('.heroes__title').click(function () {
		if ($('.test-anim__fon').hasClass('_up')) {
			$('.test-anim__fon').removeClass('_up');
			$('.test-anim__fon').addClass('_down');
			setTimeout(function() {
				$('.test-anim__fon').removeClass('_down');
			}, 1400);
		} else {
			$('.test-anim__fon').addClass('_up');
		}
	});
});