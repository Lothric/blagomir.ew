// JS-код для главной страницы

$(window).on('load', function () {

	// Слайдер календаря
	if( $('.calendar__slider').length ) {
		// Слайдер слева
		var preccCenterSlider = new Swiper('.calendar__slider .swiper-container', {
			autoplay: true,
			autoplay: {
				delay: 4000,
				disableOnInteraction: false,
			},
			speed: 600,
			loop: false,
			loopedSlides: 5,
			spaceBetween: 25,

			pagination: {
				el: '.pagination-number',
				type: 'fraction',
				renderFraction: function (currentClass, totalClass) {
					return '<span class="' + currentClass + '"></span>' +
					'/' +
					'<span class="' + totalClass + '"></span>';
				},
			},

			navigation: {
				nextEl: $('.calendar__slider').find('.button-next'),
				prevEl: $('.calendar__slider').find('.button-prev'),
			},
		});

		// при смене слайда меняем и дату на календаре
		preccCenterSlider.on('slideChange', function () {
			let slide = $('.calendar__slider').find('.swiper-slide')[preccCenterSlider.activeIndex];
			let date = $(slide).find('.caldate').text();
			let predate = date.split('.');
			date = new Date(predate[2], predate[1] - 1, predate[0]);
			$('.calendar__datapicker').datepicker().data('datepicker').selectDate(date);
		});
	};


	$('.calendar__datapicker').datepicker({
		inline: true,
		onRenderCell: function (date, cellType) {
			var currentDate = date.getDate();
			var month = date.getMonth() + 1;
	
			// Добавляем вспомогательный элемент, если число содержится в `eventDates`
			if (cellType == 'day' && eventDates[month] && eventDates[month].indexOf(currentDate) != -1) {
				return {
					classes: '-holiday-'
				};
			}
			else {
				return {
					disabled: true
				};
			}
		},
		// при клике на дату меняем и слайд на нужный
		onSelect(formattedDate, date, inst) {

			$('.calendar__slider').find('.swiper-slide').toArray().forEach((el, index) => {
				if ($(el).find('.caldate').text() == formattedDate) {
					preccCenterSlider.slideTo(index);
				}
			});
		}
	});

});